using UnityEngine;

namespace Global
{
    /// <summary>
    /// Helper Class for all kinds of Constants
    /// </summary>
    public static class Constants
    {
        public const int HalfRoomGridSize = 4;
        public const int RoomGridSize = 2 * HalfRoomGridSize;
        public const int RoomYGridSize = 4;
        public const int SmallRoomSize = RoomGridSize;
        public static readonly Vector3Int SmallFlatRoomSize = new Vector3Int(SmallRoomSize, RoomYGridSize, SmallRoomSize);

        public const int StandardRoomSize = SmallRoomSize * 2;
        public const int BigRoomSize = StandardRoomSize * 2;
        public const int HugeRoomSize = BigRoomSize * 2;
    }
}
