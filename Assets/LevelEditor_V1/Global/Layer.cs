﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Unity
{
    public enum Layer
    {
        BuiltinLayerBegin = 0,
        Default= BuiltinLayerBegin,
        Transparent,
        IgnoreRaycast,
        BuiltinLayer3,
        Water,
        UI,
        BuiltinLayer6,
        BuiltinLayer7,
        BuiltinLayerEnd = BuiltinLayer7,
        CustomLayerBegin,
        EditorCollider = CustomLayerBegin,
    }

    public enum LayerMask
    {
        Default = 1 << 0,
        Transparent = 1 << 1,
        IgnoreRaycast = 1 << 2,
        BuiltinLayer3 = 1 << 3,
        Water = 1 << 4,
        UI = 1 << 5,
        BuiltinLayer6 = 1 << 6,
        BuiltinLayer7 = 1 << 7,
        EditorCollider = 1 << 8,
    }
}

