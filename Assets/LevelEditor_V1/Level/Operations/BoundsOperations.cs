﻿using UnityEngine;
using Core;
using Core.Types;
using Core.Unity.Extensions;
using static Global.Constants;

public static class BoundsOperations
{
    public static void GetBoundsNextTo(Bounds originalBounds, Cardinals cardinal, out Bounds bounds, int level, Vector3Int size, float offsetFromMin = -1)
    {
        Vector3 minPosition = Vector3.zero;
        switch (cardinal)
        {
            case Cardinals.North:
                {
                    float x = offsetFromMin == -1
                        ? originalBounds.center.x - size.x / 2.0f
                        : originalBounds.min.x + offsetFromMin - size.x / 2.0f;

                    minPosition = new Vector3(x, originalBounds.min.y + level, originalBounds.max.z);
                }
                break;
            case Cardinals.East:
                {
                    float z = offsetFromMin == -1
                        ? originalBounds.center.z - size.z / 2.0f
                        : originalBounds.min.z + offsetFromMin - size.z / 2.0f;

                    minPosition = new Vector3(originalBounds.max.x, originalBounds.min.y + level, z);
                }
                break;
            case Cardinals.South:
                {
                    float x = offsetFromMin == -1
                        ? originalBounds.center.x - size.x / 2.0f
                        : originalBounds.min.x + offsetFromMin - size.x / 2.0f;

                    minPosition = new Vector3(x, originalBounds.min.y + level, originalBounds.min.z - size.z);
                }
                break;
            case Cardinals.West:
                {
                    float z = offsetFromMin == -1
                        ? originalBounds.center.z - size.z / 2.0f
                        : originalBounds.min.z + offsetFromMin - size.z / 2.0f;

                    minPosition = new Vector3(originalBounds.min.x - size.x, originalBounds.min.y + level, z);
                }
                break;
        }
        bounds = new Bounds(minPosition + (size.Vector3() / 2.0f), size);
    }

    public static Bounds GetExitBounds(Bounds roomBounds, Room.ExitInfo exit)
    {
        int tileSize = 1 + (exit.toIdx - exit.fromIdx);
        float centerAdd = exit.fromIdx;
        if (exit.toIdx > exit.fromIdx)
        {
            centerAdd = exit.fromIdx + ((float)(exit.toIdx - exit.fromIdx) / 2);
        }

        int outwardSize = 1;

        Vector3Int size;
        if (exit.ExitDirection == Cardinals.North || exit.ExitDirection == Cardinals.South)
        {
            size = new Vector3Int(tileSize, 1, outwardSize);
        }
        else
        {
            size = new Vector3Int(outwardSize, 1, tileSize);
        }
        Bounds exitBounds;
        GetBoundsNextTo(roomBounds, exit.ExitDirection, out exitBounds, exit.level, size, centerAdd);
        return exitBounds;
    }

    public static Bounds GrowBounds(Bounds b, Vector3 dir, Vector3 size)
    {
        Vector3 pos = b.center
            - new Vector3(dir.x * b.extents.x, dir.y * b.extents.y, dir.z * b.extents.z)
            + new Vector3(dir.x * size.x / 2, dir.y * size.y / 2, dir.z * size.z / 2);
        return new Bounds(pos, size);
    }

    public static Bounds GrowBoundsGridAligned(Bounds b)
    {
        // todo: do not align y-axis?
        return GrowBoundsGridAligned(b, new Vector3Int(RoomGridSize, RoomYGridSize, RoomGridSize));
    }

    public static Bounds GrowBoundsGridAligned(Bounds b, Vector3Int gridSize)
    {
        var minX = Mathf.FloorToInt(b.min.x / gridSize.x) * gridSize.x;
        var minY = Mathf.FloorToInt(b.min.y / gridSize.y) * gridSize.y;
        var minZ = Mathf.FloorToInt(b.min.z / gridSize.z) * gridSize.z;

        var maxX = Mathf.CeilToInt(b.max.x / gridSize.x) * gridSize.x;
        var maxY = Mathf.CeilToInt(b.max.y / gridSize.y) * gridSize.y;
        var maxZ = Mathf.CeilToInt(b.max.z / gridSize.z) * gridSize.z;

        Vector3 minVec = new Vector3(minX, minY, minZ);
        Vector3 maxVec = new Vector3(maxX, maxY, maxZ);
        Vector3 sizeVec = maxVec - minVec;
        Vector3 center = minVec + sizeVec / 2;

        return new Bounds(center, sizeVec);
    }

    public static IntersectionType GetIntersectionType(this Bounds a, Bounds b)
    {
        if (a.Intersects(b))
        {
            if (a.Contains(b.min) && a.Contains(b.max))
                return IntersectionType.Contains;
            if (b.Contains(a.min) && b.Contains(a.max))
                return IntersectionType.Contained;

            if (a.max.x == b.min.x || a.max.y == b.min.y || a.max.z == b.min.z
                || a.min.x == b.max.x || a.min.y == b.max.y || a.min.z == b.max.z)
                return IntersectionType.Touch; // touch does not count
            return IntersectionType.Intersects;
        }
        return IntersectionType.None;
    }
}

