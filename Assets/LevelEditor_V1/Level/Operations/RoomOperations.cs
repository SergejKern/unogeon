using System.Collections.Generic;
using UnityEngine;

using Core;
using Core.Types;
using Core.Unity.Extensions;
using Global;
using static Global.Constants;
using static BoundsOperations;
using static Tiles;

public static class RoomOperations
{
    class LevelArea : System.IComparable<LevelArea>
    {
        public List<Vector2Int> tiles;
        public List<Vector2Int> edgeTiles;
        public List<LevelArea> connectedAreas;
        public int levelAreaSize;
        public bool isAccessibleFromMainLevelArea;
        public bool isMainArea;

        public LevelArea() { }

        public LevelArea(List<Vector2Int> areaTiles, Room r, int levelIdx)
        {
            tiles = areaTiles;
            levelAreaSize = tiles.Count;
            connectedAreas = new List<LevelArea>();
            edgeTiles = new List<Vector2Int>();

            var lvl = r.Levels[levelIdx];

            foreach (Vector2Int tile in tiles)
            {
                for (int x = tile.x - 1; x <= tile.x + 1; ++x)
                    for (int z = tile.y - 1; z <= tile.y + 1; ++z)
                    {
                        var adjacent = x == tile.x || z == tile.y;
                        if (!adjacent)
                            continue;

                        if (!lvl.IsInside(x - lvl.Position.x, z - lvl.Position.y))
                            continue;

                        if (OriginType(r.GetTile(levelIdx, x, z)) != Type.Floor)
                            continue;

                        edgeTiles.Add(tile);
                    }
            }
        }

        public void SetAccessibleFromMainLevelArea()
        {
            if (isAccessibleFromMainLevelArea)
                return;
            isAccessibleFromMainLevelArea = true;
            foreach (var area in connectedAreas)
                area.SetAccessibleFromMainLevelArea();
        }

        public static void ConnectLevelAreas(LevelArea areaA, LevelArea areaB)
        {
            if (areaA.isAccessibleFromMainLevelArea)
                areaB.SetAccessibleFromMainLevelArea();
            else if (areaB.isAccessibleFromMainLevelArea)
                areaA.SetAccessibleFromMainLevelArea();

            areaA.connectedAreas.Add(areaB);
            areaB.connectedAreas.Add(areaA);
        }

        public bool IsConnected(LevelArea otherArea) { return connectedAreas.Contains(otherArea); }

        public int CompareTo(LevelArea other)
        {
            return other.levelAreaSize.CompareTo(levelAreaSize);
        }
    }

    public enum OutOfLevelBoundsAction
    {
        IgnoreTile,
        Error,
        ExpandBounds
    }

    public class ErrorReporter {
        public List<Bounds> ErrorBounds = new List<Bounds>();
        //room or level where error happened
        public List<Bounds> OpBounds = new List<Bounds>();

        public OutOfLevelBoundsAction action = OutOfLevelBoundsAction.Error;
        public bool allOk;
        public OperationResult Result { get { return allOk ? OperationResult.OK : OperationResult.Error; } }
    }

    public static ErrorReporter _DefaultReporter = new ErrorReporter();
    public static bool ReportErrorHelper(string errorString, Bounds err, Bounds op, ErrorReporter rep)
    {
        Debug.LogError(errorString);
        rep.ErrorBounds.Add(err);
        rep.OpBounds.Add(op);
        return false;
    }

    public static RoomComponent CreateObjectForRoom(Room room, string name ="")
    {
        GameObject obj = RoomGameObjectPool.I.Get(name);
        obj.transform.position = room.Position.Vector3();
        var rc = obj.GetComponent<RoomComponent>();
        if (!rc)
            rc = obj.AddComponent<RoomComponent>();
        rc.Room = room;

        //StaticMeshMethods.GenerateMesh(room);
        return rc;
    }

    public static Room CreateFlatRoom(Vector3Int size) { return CreateFlatRoom(size.x, size.y, size.z); }
    public static Room CreateFlatRoom(int xSize, int ySize, int zSize)
    {
        var room = ScriptableObject.CreateInstance<Room>();
        room.Setting = Global.Resources.Instance.StandardLevelSetting;

        room.Size = new Vector3Int(xSize, ySize, zSize);
        room.Levels.Add(new Room.Level());
        var level = 0;
        var lvl = room.Levels[level];
        lvl.Position = Vector2Int.zero;
        lvl.Size = new Vector2Int(xSize, zSize);
        lvl.Tiles = new ushort[lvl.TileDim.x * lvl.TileDim.y];
        return room;
    }

    public static OperationResult FillLevelWithFloorTiles(Room room, int lvlIdx)
    {
        var lvl = room.Levels[lvlIdx];
        bool allOk = true;
        for (int z = 0; z < lvl.TileDim.y; ++z)
        {
            for (int x = 0; x < lvl.TileDim.x; ++x)
            {
                var rx = x + lvl.Position.x;
                var rz = z + lvl.Position.y;

                if (lvl.IsInsideWithoutEdge(x, z))
                    allOk &= room.SetTile(lvlIdx, rx, rz, (ushort)Type.Floor) == OperationResult.OK;
            }
        }
        if (allOk)
            return OperationResult.OK;
        return OperationResult.Error;
    }
    public static Room CreateStandardRoom()
    {
        Room r = CreateFlatRoom(StandardRoomSize, RoomYGridSize, StandardRoomSize);
        FillLevelWithFloorTiles(r, 0);
        return r;
    }

    public static void AddOpenExitsFromTiles(Room room)
    {
        room.OpenExits.Clear();
        for (var levelI = 0; levelI < room.Levels.Count; ++levelI)
        {
            var level = room.Levels[levelI];
            // Add open exits Where level is on the edge of the room
            if (level.Position.x == 0)
            {
                _AddOpenExitsFromTiles(room, levelI, level, Cardinals.West);
            }
            if (level.Position.y == 0)
            {
                _AddOpenExitsFromTiles(room, levelI, level, Cardinals.South);
            }
            if (level.Extends.x == room.Size.x)
            {
                _AddOpenExitsFromTiles(room, levelI, level, Cardinals.East);
            }
            if (level.Extends.y == room.Size.z)
            {
                _AddOpenExitsFromTiles(room, levelI, level, Cardinals.North);
            }
        }
    }

    /// <summary>
    /// Closes exits that apply to given arguments
    /// </summary>
    /// <param name="connector">the room that will connect to this exit</param>
    /// <param name="room">the room where exits should be closed</param>
    /// <param name="exitDirextion">the direction of exits to close</param>
    /// <param name="bounds">the bounds the exit should be in to close</param>
    /// <param name="exits">list of bounds of exits that will be closed</param>
    /// <returns>how many exits have been closed</returns>
    public static int ConnectOpenEndsInsideBounds(Room connector, Room room, Cardinals exitDirextion, Bounds bounds, ref List<Bounds> exits)
    {
        int exitsClosed = 0;
        for (int i = room.OpenExits.Count - 1; i >= 0; i--)
        {
            var exit = room.OpenExits[i];
            if (exit.ExitDirection != exitDirextion)
                continue;

            var exitBounds = GetExitBounds(room.Bounds, exit);
            if (bounds.GetIntersectionType(exitBounds) == IntersectionType.Contains)
            {
                exits.Add(exitBounds);
                room.OpenExits.Remove(exit);
                exit.ConnectedTo = connector;
                room.ClosedExits.Add(exit);
                exitsClosed++;
            }
        }
        return exitsClosed;
    }

    /// <summary>
    /// Gathers all connecting rooms that are max given steps (in rooms) away.
    /// </summary>
    /// <param name="room">The room which should be connected</param>
    /// <param name="steps">steps until it is considered too far or too indirectly connected</param>
    /// <param name="connecting">all the connecting rooms</param>
    public static void GatherConnectingRecursive(Room room, int yPos, int steps, ref List<Room> connecting)
    {
        connecting.Add(room);
        if (steps <= 0)
            return;
        foreach (var e in room.ClosedExits)
        {
            if ( e.ConnectedTo.Bounds.max.y <= (yPos+1) || e.ConnectedTo.Bounds.min.y > yPos)
                continue;
            if (connecting.Contains(e.ConnectedTo))
                continue;
            GatherConnectingRecursive(e.ConnectedTo, yPos, steps - 1, ref connecting);
        }
    }

    public static OperationResult AddTilesAtBounds(Room room, List<Bounds> bounds, 
        Type type = Type.Floor, ErrorReporter errorRep = null)
    {
        if (errorRep == null) errorRep = _DefaultReporter;

        bool allOk = true;
        foreach (var ex in bounds)
        {
            var min = ex.min - room.Bounds.min;
            var max = ex.max - room.Bounds.min;
            var bSize = max - min;
            var relativeBounds = new Bounds(room.Position.Vector3() + min + bSize / 2, bSize);
            int levelIdx = (int)(min.y);

            if (ExpandLevelIfRequired(room, levelIdx, errorRep) == OperationResult.Error)
            {
                if (errorRep.action == OutOfLevelBoundsAction.IgnoreTile)
                    continue;
                allOk = ReportErrorHelper($"AddTilesAtBounds failed, because bounds are above or below Room! Bounds:{relativeBounds} Room-Bounds{room.Bounds}", 
                    relativeBounds, room.Bounds, errorRep);
                continue;
            }

            //Debug.Log("levelIdx: " + levelIdx + " LevelCount: "+ room.Levels.Count);
            var lvlBounds = room.LevelBounds(levelIdx);

            //TODO!!
            //if (errorRep.action == OutOfLevelBoundsAction.IgnoreTile)
            //{
            //    min = new Vector3(Mathf.Max(min.x, lvlBounds.min.x), 0.0f, Mathf.Max(min.z, lvlBounds.min.z));
            //    max = new Vector3(Mathf.Min(max.x, lvlBounds.max.x), 0.0f, Mathf.Min(max.z, lvlBounds.max.z));
            //}
            //else 
            if (lvlBounds.GetIntersectionType(relativeBounds) != IntersectionType.Contains)
            {
                if (errorRep.action == OutOfLevelBoundsAction.Error)
                {
                    allOk = ReportErrorHelper($"AddTilesAtBounds failed because bounds are not in level Bounds:{relativeBounds} Level-pos{lvlBounds} extends", 
                        relativeBounds, lvlBounds, errorRep);
                    continue;
                }
                else if(errorRep.action == OutOfLevelBoundsAction.ExpandBounds)
                {
                    allOk = ReportErrorHelper($"ExpandBounds not implemented! level Bounds:{relativeBounds} Level-pos{lvlBounds} extends", 
                        relativeBounds, lvlBounds, errorRep);
                    continue;

                    //todo: this is untested, probably does not work, check method in RoomEditor to expand level
                    //level.Expand(new Vector2Int((int)Mathf.Min(min.x, lvlBounds.min.x), (int)Mathf.Min(min.z, lvlBounds.min.z)),
                    //        new Vector2Int((int)Mathf.Max(max.x, lvlBounds.max.x), (int)Mathf.Max(max.z, lvlBounds.max.z)));
                }
            }
            for (int x = (int)Mathf.Ceil(min.x); x <= Mathf.Floor(max.x); ++x)
            {
                for (int z = (int)Mathf.Ceil(min.z); z <= Mathf.Floor(max.z); ++z)
                {
                    room.SetTile(levelIdx, x, z, (ushort)type);
                }
            }
        }

        if(errorRep.action != OutOfLevelBoundsAction.IgnoreTile)
            errorRep.allOk &= allOk;

        if (allOk)
            return OperationResult.OK;
        return OperationResult.Error;
    }

    public static void ChangeLevelCount(Room room, int newLevelCount)
    {
        int prevCount = room.Levels.Count;

        // how many levels does the room have
        newLevelCount = Mathf.Clamp(newLevelCount, 1, room.MaxLevel);
        if (newLevelCount == room.Levels.Count)
            return;

        List<Room.Level> newLevels = new List<Room.Level>();
        var lastLevel = room.Levels[room.Levels.Count - 1];
        for (int i = 0; i < newLevelCount; ++i)
        {
            if (i < room.Levels.Count)
                newLevels.Add(room.Levels[i]);
            else
            {
                var lvl = new Room.Level();
                lvl.Position = new Vector2Int(0, 0);
                lvl.Size = new Vector2Int(room.Size.x, room.Size.z);
                lvl.Tiles = new ushort[lvl.TileDim.x * lvl.TileDim.y];

                // set lvl-tiles according to level below
                for (int x = 0; x < room.TileDim.x; ++x)
                {
                    for (int z = 0; z < room.TileDim.y; ++z)
                    {
                        lvl.SetTile(x, z, (ushort)Type.AutoNonFloor);
                        var belowTile = lastLevel.GetTile(x, z, action: OutOfLevelBoundsAction.IgnoreTile);
                        if (belowTile == ushort.MaxValue)
                            belowTile = (ushort)Type.Wall << 2;
                        var belowResult = ResultingType(belowTile);
                        if (belowResult == Type.Floor)
                            belowResult = Type.Empty;
                        lvl.SetTile(x, z, (ushort)belowResult, ResultMask, 2);
                    }
                }
                newLevels.Add(lvl);
            }
        }
        room.Levels = newLevels;

        if (prevCount > newLevelCount)
        {
            room.UpdateResulting(newLevelCount - 1);
        }
        else for (int i = prevCount - 1; i < room.Levels.Count; ++i)
        {
            room.UpdateResulting(i);
        }
    }

    public static OperationResult ExpandLevelIfRequired(Room room, int levelIdx, ErrorReporter errorRep = null)
    {
        if (errorRep == null) errorRep = _DefaultReporter;

        bool allok = true;
        if (levelIdx + 1 > room.Levels.Count)
        {
            if (errorRep.action == OutOfLevelBoundsAction.ExpandBounds && (levelIdx + 1) <= room.MaxLevel)
            {
                ChangeLevelCount(room, levelIdx + 1);
                return OperationResult.OK;
            }
            else allok = false;
        }
        else if (levelIdx < 0)
            allok = false;

        if (errorRep.action != OutOfLevelBoundsAction.IgnoreTile)
            errorRep.allOk &= allok;
        return allok ? OperationResult.OK : OperationResult.Error;
    }

    public static void RingConnection(Room room, int levelIdx, ErrorReporter errorRep = null)
    {
        if (errorRep == null) errorRep = _DefaultReporter;

        if (ExpandLevelIfRequired(room, levelIdx, errorRep) == OperationResult.Error)
        {
            if (errorRep.action == OutOfLevelBoundsAction.IgnoreTile)
                return;
            Debug.LogError($"RingConnection failed, because Room {room.Name} has no Level {levelIdx}");
        }

        var level = room.Levels[levelIdx];
        for (int x = level.Position.x + 1; x < level.Extends.x; ++x)
        {
            room.SetTile(levelIdx, x, 1, (ushort)Type.Floor, action: errorRep.action);
            room.SetTile(levelIdx, x, room.TileDim.y - 2, (ushort)Type.Floor, action: errorRep.action);
        }
        for (int z = level.Position.y + 1; z < level.Extends.y; ++z)
        {
            room.SetTile(levelIdx, 1, z, (ushort)Type.Floor, action: errorRep.action);
            room.SetTile(levelIdx, room.TileDim.x - 2, z, (ushort)Type.Floor, action: errorRep.action);
        }
    }

    public static void RandomFillRoomLevel(Room room, int levelIdx, int randomFillPercent, string seed = null)
    {
        bool useRandomSeed = seed == null;
        if (useRandomSeed)
            seed = System.DateTime.Now.ToString();

        System.Random rand = new System.Random(seed.GetHashCode());

        var level = room.Levels[levelIdx];
        for (int x = 0; x < level.TileDim.x; ++x)
        {
            for (int z = 0; z < level.TileDim.y; ++z)
            {
                var rx = level.Position.x + x;
                var rz = level.Position.y + z;

                if (level.IsOnEdge(x, z))
                    room.SetTile(levelIdx, rx, rz, (ushort)Type.AutoNonFloor); //level.Tiles[z * room.Size.x + x] = (int)TileType.StandardWall;
                else
                    room.SetTile(levelIdx, rx, rz, rand.Next(0, 100) < randomFillPercent ? (ushort)Type.AutoNonFloor : (ushort)Type.Floor);
            }
        }
    }

    public static void DestroyRegionsThatAreTooSmall(Room room, int levelIdx, Type tileType, Type replaceType, int threshold)
    {
        var lvl = room.Levels[levelIdx];
        List<List<Vector2Int>> regions = GetRegions(lvl, tileType);

        foreach (List<Vector2Int> region in regions)
        {
            if (region.Count >= threshold)
                continue;

            // destroy:
            foreach (Vector2Int tile in region)
            {
                var rx = lvl.Position.x + tile.x;
                var rz = lvl.Position.y + tile.y;

                room.SetTile(levelIdx, rx, rz, (ushort)replaceType);
            }
        }
    }

    public static List<List<Vector2Int>> GetRegions(Room.Level lvl, Type tileType)
    {
        List<List<Vector2Int>> regions = new List<List<Vector2Int>>();
        // mark tiles that have already been processed
        int[,] mapFlags = new int[lvl.TileDim.x, lvl.TileDim.y];

        for (int x = 0; x < lvl.TileDim.x; ++x)
            for (int z = 0; z < lvl.TileDim.y; ++z)
            {
                if (mapFlags[x, z] != 0)
                    continue;
                
                if (OriginType(lvl.GetTile(x, z)) != tileType)
                    continue;

                List<Vector2Int> newRegion = GetRegionTiles(lvl, x, z);
                regions.Add(newRegion);

                foreach (Vector2Int tile in newRegion)
                {
                    mapFlags[tile.x, tile.y] = 1;
                }
            }

        return regions;
    }

    public static List<Vector2Int> GetRegionTiles(Room.Level lvl, int startX, int startZ, int compareMask = OriginMask)
    {
        List<Vector2Int> tiles = new List<Vector2Int>();
        // mark tiles that have already been processed
        int[,] mapFlags = new int[lvl.TileDim.x, lvl.TileDim.y];

        var tileType = (lvl.GetTile(startX, startZ) & compareMask);
        //lvl.Tiles[startZ * room.Size.x + startX];

        Queue<Vector2Int> queue = new Queue<Vector2Int>();
        queue.Enqueue(new Vector2Int(startX, startZ));
        mapFlags[startX, startZ] = 1;

        // spread-algorithm, select all adjacent tiles with same tileType
        while (queue.Count > 0)
        {
            Vector2Int tile = queue.Dequeue();
            tiles.Add(tile);

            for (int x = tile.x - 1; x <= tile.x + 1; ++x)
                for (int z = tile.y - 1; z <= tile.y + 1; ++z)
                {
                    var adjacent = (z == tile.y || x == tile.x);
                    if (!adjacent)
                        continue;
                    if (z == tile.y && x == tile.x)
                        continue;
                    if (!IsInside(lvl, x, z))
                        continue;
                    if (mapFlags[x, z] == 0 && (lvl.GetTile(x, z) & compareMask) == tileType)
                    {
                        mapFlags[x, z] = 1;
                        queue.Enqueue(new Vector2Int(x, z));
                    }
                }
        }

        return tiles;
    }

    public static void ConnectClosestAreas(Room room, int lvlIdx)
    {
        var lvl = room.Levels[lvlIdx];
        var regions = GetRegions(lvl, Type.Floor);
        List<LevelArea> areas = new List<LevelArea>();
        foreach (var r in regions)
        {
            areas.Add(new LevelArea(r, room, lvlIdx));
        }
        areas.Sort();
        if (areas.Count > 0)
        {
            areas[0].isMainArea = true;
            areas[0].isAccessibleFromMainLevelArea = true;
        }
        _ConnectClosestAreas(room, lvlIdx, areas);
    }

    public static List<Vector2Int> GetLine(Vector2Int from, Vector2Int to)
    {
        List<Vector2Int> line = new List<Vector2Int>();

        int x = from.x;
        int z = from.y;

        int dx = to.x - from.x;
        int dy = to.y - from.y;

        bool inverted = false;
        int step = System.Math.Sign(dx);
        int gradientStep = System.Math.Sign(dy);

        int longest = Mathf.Abs(dx);
        int shortest = Mathf.Abs(dy);

        inverted = (longest < shortest);
        if (inverted)
        {
            step = System.Math.Sign(dy);
            gradientStep = System.Math.Sign(dx);

            longest = Mathf.Abs(dy);
            shortest = Mathf.Abs(dx);
        }

        int gradientAccumulation = longest / 2;
        for (int i = 0; i < longest; i++)
        {
            line.Add(new Vector2Int(x, z));
            if (inverted) z += step;
            else x += step;

            gradientAccumulation += shortest;
            if (gradientAccumulation >= longest)
            {
                if (inverted) x += gradientStep;
                else z += gradientStep;
                gradientAccumulation -= longest;
            }
        }
        return line;
    }

    public static void SmoothMap(Room room, int levelIdx, int thresholdWall = 4, int thresholdGround = 4)
    {
        var lvl = room.Levels[levelIdx];
        Room.Level newLevel = new Room.Level();
        newLevel.Position = lvl.Position;
        newLevel.Size = lvl.Size;
        newLevel.Tiles = new ushort[newLevel.TileDim.x * newLevel.TileDim.y];
        lvl.Tiles.CopyTo(newLevel.Tiles, 0);
        for (int x = 0; x < lvl.TileDim.x; ++x)
        {
            for (int z = 0; z < lvl.TileDim.y; ++z)
            {
                var tile = newLevel.GetTile(x, z);
                int neighborWallTiles = GetSurroundingWallCount(room, lvl, x, z);
                if (neighborWallTiles > thresholdWall && OriginType(tile) == Type.Floor)
                    newLevel.SetTile(x, z, (ushort)Type.AutoNonFloor);
                if (neighborWallTiles < thresholdGround && OriginType(tile) != Type.Floor)
                    newLevel.SetTile(x, z, (ushort)Type.Floor);
            }
        }
        newLevel.Tiles.CopyTo(lvl.Tiles, 0);

        room.UpdateResulting(levelIdx);
    }

    public static int GetSurroundingWallCount(Room room, Room.Level lvl, int gridX, int gridZ)
    {
        //var lvl = room.Levels[levelIdx];
        int wallCount = 0;
        for (int neighborX = gridX - 1; neighborX <= gridX + 1; ++neighborX)
            for (int neighborZ = gridZ - 1; neighborZ <= gridZ + 1; ++neighborZ)
            {
                if (gridX == neighborX && gridZ == neighborZ)
                    continue;

                if (lvl.IsInside(neighborX, neighborZ))
                {
                    var tile = lvl.GetTile(neighborX, neighborZ);
                    if (OriginType(tile) != Type.Floor)
                        wallCount ++;
                }
                else
                    wallCount++;
            }
        return wallCount;
    }


    /// <summary>
    /// loops through the edge of the room-level only to add floortiles as exits
    /// </summary>
    static void _AddOpenExitsFromTiles(Room room, int levelIdx, Room.Level level, Cardinals cardinals)
    {
        Room.ExitInfo info = null;

        int loopSize = -1;
        int roomSize = 0;
        int addToIdx = 0;
        int mulXIdx = 0;
        int mulZIdx = 0;
        int addXIdx = 0;
        int addZIdx = 0;

        if (cardinals == Cardinals.North || cardinals == Cardinals.South)
        {
            loopSize = level.TileDim.x;
            roomSize = room.TileDim.x;
            addToIdx = level.Position.x;

            mulXIdx = 1;
            if (cardinals == Cardinals.North)
                addZIdx = level.TileDim.y - 1;
        }
        else
        {
            loopSize = level.TileDim.y;
            roomSize = room.TileDim.y;
            addToIdx = level.Position.y;

            mulZIdx = 1;
            if (cardinals == Cardinals.East)
                addXIdx = level.TileDim.x - 1;
        }

        var lvl = room.Levels[levelIdx];
        for (int idx = 0; idx < loopSize; ++idx)
        {
            var roomIdx = addToIdx + idx;
            if (roomIdx == 0 || roomIdx == (roomSize - 1)) // don't add corners
            {
                continue;
            }
            // the x and z positions in the array, this loop will only iterate through one of them (mul_Idx is 0 or 1) and set to the corresponding side via add_Idx
            var xComp = (mulXIdx * idx) + addXIdx;
            var zComp = (mulZIdx * idx) + addZIdx;

            var tile = lvl.GetTile(xComp, zComp);
            if (ResultingType(tile) == Type.Floor)
            {
                if (info == null)
                {
                    info = new Room.ExitInfo();
                    info.fromIdx = roomIdx;
                    info.ExitDirection = cardinals;
                    info.level = levelIdx;
                }
                info.toIdx = roomIdx;
            }
            // if the wall is interrupted the exit is pushed so a new exit can be found on that side
            else if (info != null)
            {
                room.OpenExits.Add(info);
                info = null;
            }
            // if this is at the end of the room grid push the exit (because we always check, connect & close exits with RoomGridSize)
            if ((roomIdx + 1) % Constants.RoomGridSize == 0 && info != null)
            {
                room.OpenExits.Add(info);
                info = null;
            }
        }
        // not to leave any exits forgotten
        if (info != null)
        {
            room.OpenExits.Add(info);
            info = null;
        }
    }

    static void _ConnectClosestAreas(Room room, int lvlIdx, List<LevelArea> allareas, bool forceAccessibilityFromMainarea = false)
    {
        List<LevelArea> areaListA = forceAccessibilityFromMainarea ? new List<LevelArea>() : allareas;
        List<LevelArea> areaListB = forceAccessibilityFromMainarea ? new List<LevelArea>() : allareas;
        if (forceAccessibilityFromMainarea)
        {
            foreach (LevelArea area in allareas)
                if (area.isAccessibleFromMainLevelArea)
                    areaListB.Add(area);
                else areaListA.Add(area);
        }

        int bestDistance = int.MaxValue;
        Vector2Int bestTileA = new Vector2Int();
        Vector2Int bestTileB = new Vector2Int();
        LevelArea bestareaA = new LevelArea();
        LevelArea bestareaB = new LevelArea();
        bool possibleConnectionFound = false;

        foreach (LevelArea areaA in areaListA)
        {
            if (!forceAccessibilityFromMainarea)
            {
                bestDistance = int.MaxValue;
                possibleConnectionFound = false;
            }
            foreach (LevelArea areaB in areaListB)
            {
                if (areaA == areaB)
                    continue;
                for (int tileIndexA = 0; tileIndexA < areaA.edgeTiles.Count; ++tileIndexA)
                    for (int tileIndexB = 0; tileIndexB < areaB.edgeTiles.Count; ++tileIndexB)
                    {
                        Vector2Int tileA = areaA.edgeTiles[tileIndexA];
                        Vector2Int tileB = areaB.edgeTiles[tileIndexB];
                        int distanceBetweenareas = (int)(Mathf.Pow(tileA.x - tileB.x, 2) + Mathf.Pow(tileA.y - tileB.y, 2));

                        if (distanceBetweenareas >= bestDistance)
                            continue;
                        bestDistance = distanceBetweenareas;
                        possibleConnectionFound = true;
                        bestTileA = tileA;
                        bestTileB = tileB;
                        bestareaA = areaA;
                        bestareaB = areaB;
                    }
            }
            if (possibleConnectionFound && !forceAccessibilityFromMainarea)
            {
                if (bestareaA.IsConnected(bestareaB))
                    continue;
                _CreatePassage(room, lvlIdx, bestareaA, bestareaB, bestTileA, bestTileB);
            }
        }
        if (possibleConnectionFound && forceAccessibilityFromMainarea)
        {
            _CreatePassage(room, lvlIdx, bestareaA, bestareaB, bestTileA, bestTileB);
            _ConnectClosestAreas(room, lvlIdx, allareas, true);
        }
        if (!forceAccessibilityFromMainarea)
            _ConnectClosestAreas(room, lvlIdx, allareas, true);
    }

    static void _CreatePassage(Room room, int lvlIdx, LevelArea areaA, LevelArea areaB, Vector2Int tileA, Vector2Int tileB)
    {
        LevelArea.ConnectLevelAreas(areaA, areaB);

        List<Vector2Int> line = GetLine(tileA, tileB);
        foreach (var c in line)
            _DrawCircle(room, lvlIdx, c, 1);
    }

    public static void CreatePassage(Room room, int lvlIdx, Vector2Int tileA, Vector2Int tileB)
    {
        List<Vector2Int> line = GetLine(tileA, tileB);
        foreach (var c in line)
            _DrawCircle(room, lvlIdx, c, 1);
    }
    public static void DrawCircle(Room room, int lvlIdx, Vector2Int c, int r) {
        _DrawCircle(room, lvlIdx, c, r);
    }
    static void _DrawCircle(Room room, int lvlIdx, Vector2Int c, int r, ushort drawTile = (ushort)Type.Floor)
    {
        var lvl = room.Levels[lvlIdx];
        for (int x = -r; x <= r; ++x)
        {
            for (int z = -r; z <= r; ++z)
            {
                if (x * x + z * z > r * r)
                    continue;

                int tileX = c.x + x;
                int tileZ = c.y + z;
                if (IsInside(lvl, tileX, tileZ))
                {
                    var rx = lvl.Position.x + tileX;
                    var rz = lvl.Position.y + tileZ;

                    room.SetTile(lvlIdx, rx, rz, drawTile);
                    //lvl.SetTile(tileX, tileZ, drawTile);
                }
            }
        }
    }

    #region Extensions
    public static bool IsOnEdge(this Room room, int x, int z)
    {
        return x == 0 || x == room.TileDim.x - 1 || z == 0 || z == room.TileDim.y - 1;
    }
    public static bool IsInside(this Room room, int x, int z)
    {
        return x >= 0 && x < room.TileDim.x && z >= 0 && z < room.TileDim.y;
    }

    public static bool IsOnEdge(this Room.Level lvl, int x, int z)
    {
        return x == 0 || x == lvl.TileDim.x - 1 || z == 0 || z == lvl.TileDim.y - 1;
    }

    public static bool IsInside(this Room.Level lvl, int x, int z)
    {
        return x >= 0 && x < lvl.TileDim.x && z >= 0 && z < lvl.TileDim.y;
    }
    public static bool IsInsideWithoutEdge(this Room.Level lvl, int x, int z)
    {
        return x > 0 && x < lvl.TileDim.x - 1 && z > 0 && z < lvl.TileDim.y - 1;
    }

    public static void UpdateResulting(this Room r, int lvlIdx)
    {
        var lvl = r.Levels[lvlIdx];

        for (int x = 0; x < lvl.TileDim.x; ++x)
        {
            for (int z = 0; z < lvl.TileDim.y; ++z)
            {
                var rx = lvl.Position.x + x;
                var rz = lvl.Position.y + z;
                r.UpdateResulting(lvlIdx, rx, rz);
            }
        }
    }
    public static void UpdateResulting(this Room r, int lvlIdx, int x, int z)
    {
        var lvl = r.Levels[lvlIdx];
        var lvlX = x - lvl.Position.x;
        var lvlZ = z - lvl.Position.y;
        if (!lvl.IsInside(lvlX, lvlZ))
            return;
        var tile = lvl.Tiles[lvlZ * lvl.TileDim.x + lvlX];

        Type previous = ResultingType(tile);
        Type origin = OriginType(tile);
        Type resulting = origin;

        if (resulting != Type.Wall && lvlIdx + 1 < r.Levels.Count) // if floor above this needs to be wall
        {
            var aboveTile = GetTile(r, lvlIdx + 1, x, z, action: OutOfLevelBoundsAction.IgnoreTile);
            if (aboveTile != ushort.MaxValue && ResultingType(aboveTile) == Type.Floor)
                resulting = Type.Wall;
        }
        if (resulting != Type.Wall && lvlIdx > 0)
        {
            var belowTile = GetTile(r, lvlIdx - 1, x, z, action: OutOfLevelBoundsAction.IgnoreTile);
            if (belowTile != ushort.MaxValue)
            {
                var belowResult = OriginType(belowTile);
                if (belowResult == Type.AutoNonFloor)
                    belowResult = ResultingType(belowTile);

                if (belowResult == Type.Wall && origin == Type.Empty)
                    resulting = Type.Empty;
                if (origin == Type.AutoNonFloor)
                {
                    if (belowResult == Type.Floor)
                        resulting = Type.Empty;
                    else resulting = belowResult;
                }
            }
        }
        else if(origin == Type.AutoNonFloor)
        {
            resulting = Type.Wall;
        }

        lvl.SetTile(lvlX, lvlZ, (ushort)resulting, ResultMask, 2);

        if (previous != resulting)
        {
            if ((resulting == Type.Floor || previous == Type.Floor) && lvlIdx > 0)
                r.UpdateResulting(lvlIdx - 1, x, z);
            if (lvlIdx+1 < r.Levels.Count)
                r.UpdateResulting(lvlIdx + 1, x, z);
        }
    }

    public static ushort GetTile(this Room r, int lvlIdx, int x, int z, OutOfLevelBoundsAction action = OutOfLevelBoundsAction.Error)
    {
        var lvl = r.Levels[lvlIdx];
        var lvlX = x - lvl.Position.x;
        var lvlZ = z - lvl.Position.y;
        if (!lvl.IsInside(lvlX, lvlZ))
        {
            if (action == OutOfLevelBoundsAction.Error)
                Debug.LogError("tile is out of level bounds");
            else if (action == OutOfLevelBoundsAction.ExpandBounds)
                Debug.LogError("ExpandBounds not supported in this method");
            return ushort.MaxValue;
        }
        return lvl.Tiles[lvlZ * lvl.TileDim.x + lvlX];
    }

    public static OperationResult SetTile(this Room r, int lvlIdx, int x, int z, ushort tile, 
        ushort mask = OriginMask, ushort shift = 0, OutOfLevelBoundsAction action = OutOfLevelBoundsAction.Error)
    {
        var lvl = r.Levels[lvlIdx];
        var lvlX = x - lvl.Position.x;
        var lvlZ = z - lvl.Position.y;
        if (!lvl.IsInside(lvlX, lvlZ))
        {
            if (action == OutOfLevelBoundsAction.Error)
                Debug.LogError("tile is out of level bounds");
            else if (action == OutOfLevelBoundsAction.ExpandBounds)
                Debug.LogError("ExpandBounds not supported in this method");
            return OperationResult.Error;
        }
        var invertedMask = (ushort)~mask;
        var origTile = lvl.Tiles[lvlZ * lvl.TileDim.x + lvlX];
        lvl.Tiles[lvlZ * lvl.TileDim.x + lvlX] = (ushort)((origTile & invertedMask) | (mask & (tile << shift)));
        if (mask == OriginMask && shift == 0)
        {
            r.UpdateResulting(lvlIdx, x, z);
        }
        return OperationResult.OK;
    }

    public static ushort GetTile(this Room.Level lvl, int x, int z, OutOfLevelBoundsAction action = OutOfLevelBoundsAction.Error)
    {
        if (!lvl.IsInside(x, z))
        {
            if (action == OutOfLevelBoundsAction.Error)
                Debug.LogError("tile is out of level bounds");
            else if (action == OutOfLevelBoundsAction.ExpandBounds)
                Debug.LogError("ExpandBounds not supported in this method");
            return ushort.MaxValue;
        }
        return lvl.Tiles[z * lvl.TileDim.x + x];
    }

    public static void SetTile(this Room.Level lvl, int x, int z, ushort tile, 
        ushort mask = OriginMask, ushort shift = 0, OutOfLevelBoundsAction action = OutOfLevelBoundsAction.Error)
    {
        if (!lvl.IsInside(x, z))
        {
            if (action == OutOfLevelBoundsAction.Error)
                Debug.LogError("tile is out of level bounds");
            else if (action == OutOfLevelBoundsAction.ExpandBounds)
                Debug.LogError("ExpandBounds not supported in this method");
            return;
        }
        var invertedMask = (ushort)~mask;
        var origTile = lvl.Tiles[z * lvl.TileDim.x + x];
        lvl.Tiles[z * lvl.TileDim.x + x] = (ushort)((origTile & invertedMask) | (mask & (tile << shift)));
    }

    public static void SetTile(this Room.Level lvl, int idx, ushort tile,
        ushort mask = OriginMask, ushort shift = 0)
    {
        var invertedMask = (ushort)~mask;
        var origTile = lvl.Tiles[idx];
        lvl.Tiles[idx] = (ushort)((origTile & invertedMask) | (mask & (tile << shift)));
    }

    // untested, check method in RoomEditor
    //public static void Expand(this Room.Level lvl, Vector2Int from, Vector2Int to)
    //{
    //    Vector2Int newSize = to-from;
    //    int[] newTiles = new int[newSize.x * newSize.z];
    //    for (int x = 0; x< newSize.x; ++x)
    //    {
    //        for (int z = 0; z < newSize.z; ++z)
    //        {
    //            var oldX = x + from.x - lvl.Position.x;
    //            var oldZ = z + from.z - lvl.Position.z;
    //            if (!lvl.IsInside(oldX, oldZ))
    //            {
    //                newTiles[z * newSize.z + x] = (int)TileType.StandardWall;
    //                continue;
    //            }
    //            newTiles[z * newSize.z + x] = lvl.GetTile(oldX, oldZ);
    //        }
    //    }
    //    lvl.Position = from;
    //    lvl.Size = newSize;
    //}



    //public static void CreatePassageHor(Room room, int lvlIdx, Vector2Int tileA, Vector2Int tileB, int w, ushort drawTile = (ushort)Type.Floor)
    //{
    //    var lvl = room.Levels[lvlIdx];

    //    if (tileB.x < tileA.x)
    //    {
    //        var temp = tileB;
    //        tileB = tileA;
    //        tileA = temp;
    //    }

    //    int xSteps = tileB.x - tileA.x;

    //    for (int xStep = 0; xStep <= xSteps; ++xStep)
    //    {
    //        int x = tileA.x + xStep;
    //        int yStart = Mathf.FloorToInt(Mathf.Lerp(tileA.y, tileB.y, (float)xStep / xSteps));

    //        for (int yStep = 0; yStep < w; yStep++)
    //        {
    //            int y = yStart + yStep;

    //            if (IsInside(lvl, x, y))
    //            {
    //                var rx = lvl.Position.x + x;
    //                var rz = lvl.Position.z + y;

    //                room.SetTile(lvlIdx, rx, rz, drawTile);
    //                //lvl.SetTile(tileX, tileZ, drawTile);
    //            }
    //        }
    //    }
    //}

    //public static void CreatePassageVer(Room room, int lvlIdx, Vector2Int tileA, Vector2Int tileB, int w, ushort drawTile = (ushort)Type.Floor)
    //{
    //    var lvl = room.Levels[lvlIdx];

    //    if (tileB.y < tileA.y)
    //    {
    //        var temp = tileB;
    //        tileB = tileA;
    //        tileA = temp;
    //    }

    //    int ySteps = tileB.y - tileA.y;

    //    for (int yStep = 0; yStep <= ySteps; ++yStep)
    //    {
    //        int y = tileA.y + yStep;
    //        int xStart = Mathf.FloorToInt(Mathf.Lerp(tileA.x, tileB.x, (float)yStep / ySteps));

    //        for (int xStep = 0; xStep < w; xStep++)
    //        {
    //            int x = xStart + xStep;

    //            if (IsInside(lvl, x, y))
    //            {
    //                var rx = lvl.Position.x + x;
    //                var rz = lvl.Position.z + y;

    //                room.SetTile(lvlIdx, rx, rz, drawTile);
    //                //lvl.SetTile(tileX, tileZ, drawTile);
    //            }
    //        }
    //    }
    //}

    #endregion
}

