using UnityEngine;
using System.Collections.Generic;
using Core.Types;
using static RoomOperations;
using static Tiles;

public static class RoomMeshOperations
{
    const float kSquareSize = 1.0f;

    public enum ActiveVariant
    {
        Floor,
        Wall,
        Ceiling,
        TexTypeOnly
    }

    enum NodePos
    {
        BottomLeft,
        BottomRight,
        TopRight,
        TopLeft,

        Centre,
        CentreBottom,
        CentreLeft,
        CentreTop,
        CentreRight,
    }

    /// <summary>
    /// all cases considering specific texture/geometry-type on one tile for mesh generation
    /// </summary>
    enum ECases
    {
        None = 0,
        BottomLeft = 1,
        BottomRight = 2,
        TopRight = 4,
        TopLeft = 8,

        Bottom = BottomLeft | BottomRight, //3
        Top = TopLeft | TopRight, // 12
        Left = BottomLeft | TopLeft, // 9
        Right = TopRight | BottomRight, // 6

        Diag1 = BottomLeft | TopRight, // 5
        Diag2 = BottomRight | TopLeft, // 10

        NotBottomLeft = BottomRight | TopRight | TopLeft, // 14
        NotBottomRight = BottomLeft | TopRight | TopLeft, // 13
        NotTopRight = BottomLeft | BottomRight | TopLeft, // 11
        NotTopLeft = BottomLeft | BottomRight | TopRight, // 7

        All = BottomLeft | BottomRight | TopRight | TopLeft // 15
    }

    public struct UVData
    {
        public UVData(OperationData opData, ushort texType,
            int variation = 0, bool halfSize = false, int config = 15, bool hard = false, int uvConfig = 15, TransitionTextureData transition = null)
        {
            var square = opData.Square;
            var setting = opData.Room.Setting;

            var squarePos = square.centre.position;

            //float tileSize = 0.0f;
            //float uvInset = 0.0f;

            uvMin = Vector2.zero;
            uvMax = Vector2.zero;

            if (transition != null)
                setting.GetTransitionUVCoords(transition, config, uvConfig, ref uvMin, ref uvMax);
            else if (texType < setting?.LevelTextureCollections?.Count && texType >= 0)
            {
                //tileSize = setting.UVTileSize;
                //uvInset = setting.UVInset;
                if (opData.WallOperation)
                {
                    setting.GetWallUVCoords(texType, variation, ref uvMin, ref uvMax);// + new Vector2(variation * tileSize, 0.0f);
                }
                else if (uvConfig == 15)
                    setting.GetFloorUVCoords(texType, variation, ref uvMin, ref uvMax);// + new Vector2(variation * tileSize, 0.0f);
                else
                    setting.GetEdgeUVCoords(texType, variation, config, uvConfig, hard, ref uvMin, ref uvMax);// + new Vector2(variation * tileSize, 0.0f);
            }

            //float minX = uvOffset.x + uvInset;
            //float minZ = uvOffset.y + uvInset;
            //float maxX = tileSize + uvOffset.x - uvInset;
            //float maxZ = tileSize + uvOffset.y - uvInset;
            //uvMin = new Vector2(minX, minZ);
            //uvMax = new Vector2(maxX, maxZ);

            SquarePos = squarePos;
            uvXModifier = halfSize ? 0.5f : 1.0f;
            uvXOffset = 0.0f;
        }

        public Vector2 uvMin;
        public Vector2 uvMax;
        public Vector3 SquarePos;

        internal Vector2 GetUVs(Vector3 position)
        {
            var uvX = Mathf.Lerp(uvMin.x, uvMax.x, position.x - SquarePos.x + 0.5f);
            var uvZ = Mathf.Lerp(uvMin.y, uvMax.y, position.z - SquarePos.z + 0.5f);

            //if (!ForWall)
            //{
            //var uvX = Mathf.Lerp(uvMin.x, uvMax.x, position.x - SquarePos.x + 0.5f);
            //var uvZ = Mathf.Lerp(uvMin.y, uvMax.y, position.z - SquarePos.z + 0.5f);
            return new Vector2(uvX, uvZ);
            //}
            //var u = Mathf.Max(uvX, uvZ);
            //var v = Mathf.Lerp(uvMin.y, uvMax.y, position.y - SquarePos.y);
            //return new Vector2(u, v);
        }
        internal Vector2 GetUVs(Vector2 uvPos)
        {
            uvPos = new Vector2(uvXOffset + (uvPos.x * uvXModifier), uvPos.y);

            var uvX = Mathf.Lerp(uvMin.x, uvMax.x, uvPos.x);
            var uvZ = Mathf.Lerp(uvMin.y, uvMax.y, uvPos.y);

            return new Vector2(uvX, uvZ);
        }

        float uvXModifier;
        float uvXOffset;

        internal void SetHalfSize() { uvXModifier = 0.5f; }

        internal void SwitchOffset()
        {
            if (uvXOffset > float.Epsilon)
                uvXOffset = 0.0f;
            else
                uvXOffset = 0.5f;
        }
    }

    /// <summary>
    /// Pool for all kinds of Helper-objects to avoid memory allocation
    /// </summary>
    public class PooledObjects
    {
        List<SquareGrid> SquareGridsInUse = new List<SquareGrid>();
        List<Square> SquaresInUse = new List<Square>();
        List<MeshData> MeshDataInUse = new List<MeshData>();
        List<Node> NodesInUse = new List<Node>();
        List<ControlNode> ControlNodesInUse = new List<ControlNode>();

        internal T GetPooledObject<T>(List<T> inUse) where T : new()
        {
            var obj = SimplePool<T>.I.Get();
            inUse.Add(obj);
            return obj;
        }
        internal void Return<T>(List<T> inUse) where T : new()
        {
            foreach (var obj in inUse)
                SimplePool<T>.I.Return(obj);
            inUse.Clear();
        }
        internal SquareGrid GetSquareGrid() { return GetPooledObject(SquareGridsInUse); }
        internal Square GetSquare() { return GetPooledObject(SquaresInUse); }
        internal MeshData GetMeshData() { return GetPooledObject(MeshDataInUse); }
        internal Node GetNode() { return GetPooledObject(NodesInUse); }
        internal ControlNode GetControlNode() { return GetPooledObject(ControlNodesInUse); }

        internal void ReturnAll()
        {
            foreach (var g in SquareGridsInUse)
                g.Return();
            Return(SquareGridsInUse);
            Return(SquaresInUse);
            Return(MeshDataInUse);
            Return(NodesInUse);
            Return(ControlNodesInUse);
        }
    }

    /// <summary>
    /// Contains objects for the current MeshOperations to simplify method parameters.
    /// </summary>
    public class OperationData
    {
        public PooledObjects PooledObjs = new PooledObjects();

        public Room Room = null;
        public Mesh Mesh = null;
        public MeshData MeshData = null;
        public MeshCollider Collider = null;

        public SquareGrid SquareGrid = null;
        public Square Square = null;

        public List<Vector3Int> HardEdges = new List<Vector3Int>(1000);
        public bool WallOperation;

        public int[] NodeActive = new int[4];
        public int ActiveNodes;

        public void Finish()
        {
            PooledObjs.ReturnAll();
            HardEdges.Clear();

            Room = null;
            Mesh = null;
            MeshData = null;
            SquareGrid = null;
            Square = null;
        }
    }

    /// <summary>
    /// Data that is set to the MeshFilter
    /// </summary>
    public class MeshData
    {
        public MeshData()
        {
            vertices = new List<Vector3>(1000);
            uvs = new List<Vector2>(1000);
            triangles = new List<int>(1000);
        }

        public List<Vector3> vertices;
        public List<Vector2> uvs;
        public List<int> triangles;

        internal static MeshData CreateMeshData(OperationData opDat)
        {
            var meshDat = opDat.PooledObjs.GetMeshData();
            meshDat.vertices.Clear();
            meshDat.uvs.Clear();
            meshDat.triangles.Clear();

            opDat.MeshData = meshDat;
            return meshDat;
        }
    }

    /// <summary>
    /// Generates the complete mesh for a room,
    /// please set the room in OperationData
    /// </summary>
    public static void GenerateMesh(OperationData opData)
    {
        var room = opData.Room;
        var rc = room.RoomComponent;
        // makes sure we have all GameObjects with mesh/render-components to support the meshes of all levels
        rc.UpdateVisuals();

        SquareGrid[] grids = _CreateSquareGrids(opData);

        _PropagateHardEdges(opData, grids);

        // creates for every level a floor, wall and ceiling
        for (int i = 0; i < grids.Length; ++i)
        {
            //if (i < room.Levels.Count)
            //    level = room.Levels[i];

            opData.Mesh = rc.GetFloorVisuals(i).Mesh;
            opData.SquareGrid = grids[i];
            opData.Collider = rc.GetFloorVisuals(i).Collider;
            Debug.Assert(opData.Collider.sharedMesh);

            MeshData.CreateMeshData(opData);
            // when i == room.Levels.Count we are above level-count, don't make a floor but only one additional wall
            if (i < room.Levels.Count)
                _GenerateMeshDataForFloor(opData);
            _GenerateMeshDataForWall(opData);
            _ApplyMeshDataToMesh(opData);

            // for first level ceiling is ignored
            if (i != 0)
            {
                opData.Mesh = rc.GetCeilingVisuals(i-1).Mesh;
                opData.Collider = rc.GetCeilingVisuals(i - 1).Collider;

                MeshData.CreateMeshData(opData);
                _GenerateMeshDataForCeiling(opData);
                _ApplyMeshDataToMesh(opData);
            }
        }

        opData.Finish();
    }

    static SquareGrid[] _CreateSquareGrids(OperationData opData)
    {
        var room = opData.Room;

        int i = 0;
        Room.Level level = null;
        SquareGrid[] grids = new SquareGrid[room.MaxLevel + 1];

        opData.HardEdges.Clear();
        for (; i < room.Levels.Count; ++i)
        {
            level = room.Levels[i];
            grids[i] = SquareGrid.CreateSquareGrid(opData, i, opData.HardEdges);
        }
        for (; i <= room.MaxLevel; ++i)
        {
            grids[i] = SquareGrid.CreateSquareGrid(opData, room.Levels.Count - 1, null);
        }
        return grids;
    }

    /// <summary>
    /// for all HardEdges propagate through the levels
    /// </summary>
    static void _PropagateHardEdges(OperationData opData, SquareGrid[] grids)
    {
        int i = 0;
        foreach (var edge in opData.HardEdges)
        {
            for (i = edge.y - 1; i >= 0; --i)
            {
                Square sq = grids[i].GetSquare(edge.x, edge.z);
                if (sq.breakHardEdge)
                    break;

                sq.hardEdge = true;
            }
            for (i = edge.y + 1; i < grids.Length; ++i)
            {
                Square sq = grids[i].GetSquare(edge.x, edge.z);
                if (sq.breakHardEdge)
                    break;

                sq.hardEdge = true;
            }
        }
    }

    static void _GenerateMeshDataForFloor(OperationData opData)
    {
        var grid = opData.SquareGrid;
        var setting = opData.Room.Setting;
        var transitionPossible = setting != null && setting.Transitions != null && setting.Transitions.Count > 0;

        for (int x = 0; x < grid.GetSquareDimensionX(); ++x)
        {
            for (int y = 0; y < grid.GetSquareDimensionZ(); ++y)
            {
                var square = grid.GetSquare(x, y);
                opData.Square = square;

                int texTypesDone = 0;
                int startNode = 0;
                ushort texTypesDoneA = ushort.MaxValue, texTypesDoneB = ushort.MaxValue, texTypesDoneC = ushort.MaxValue;

                // todo: this rule square.activeFloorNodes == 4 only looks good if there is an edge
                if (square.floorTextypes == 2 && transitionPossible)
                {
                    var texTypeA = square.NextTexType(ref startNode, ref texTypesDoneA, ref texTypesDoneB, ref texTypesDoneC);
                    var texTypeB = square.NextTexType(ref startNode, ref texTypesDoneA, ref texTypesDoneB, ref texTypesDoneC);

                    var transition = setting.Transitions.Find(a => (a.TexAIdx == texTypeA && a.TexBIdx == texTypeB) || (a.TexAIdx == texTypeB && a.TexBIdx == texTypeA));
                    if (transition == null)
                    {
                        _TriangulateFloorSquare(opData, texTypeA);
                        _TriangulateFloorSquare(opData, texTypeB);
                    }
                    else
                    {
                        _TriangulateSquareWFloorTransition(opData, (ushort)transition.TexAIdx, (ushort)transition.TexBIdx, transition);
                    }
                    continue;
                }
                for (int i = texTypesDone; i < square.floorTextypes; ++i)
                {
                    var texType = square.NextTexType(ref startNode, ref texTypesDoneA, ref texTypesDoneB, ref texTypesDoneC);
                    _TriangulateFloorSquare(opData, texType);
                }
            }
        }
    }

    static void _GenerateMeshDataForCeiling(OperationData opData)
    {
        var grid = opData.SquareGrid;
        for (int x = 0; x < grid.GetSquareDimensionX(); ++x)
        {
            for (int y = 0; y < grid.GetSquareDimensionZ(); ++y)
            {
                var square = grid.GetSquare(x, y);
                opData.Square = square;
                _TriangulateCeilingSquare(opData);
            }
        }
    }

    static void _GenerateMeshDataForWall(OperationData opData)
    {
        var grid = opData.SquareGrid;
        for (int x = 0; x < grid.GetSquareDimensionX(); ++x)
        {
            for (int y = 0; y < grid.GetSquareDimensionZ(); ++y)
            {
                var square = grid.GetSquare(x, y);
                opData.Square = square;

                _TriangulateWallSquare(opData);
            }
        }
    }

    static void _ApplyMeshDataToMesh(OperationData opData)
    {
        var meshData = opData.MeshData;
        var mesh = opData.Mesh;

        mesh.Clear();

        mesh.vertices = meshData.vertices.ToArray();
        mesh.uv = meshData.uvs.ToArray();
        mesh.triangles = meshData.triangles.ToArray();

        mesh.RecalculateNormals();
        mesh.RecalculateTangents();

        if (opData.Collider != null)
        {
            opData.Collider.sharedMesh = null;
            opData.Collider.sharedMesh = mesh;
        }

        opData.MeshData = null;
        opData.Mesh = null;
    }

    static void _TriangulateWallSquare(OperationData opData)
    {
        opData.WallOperation = true;

        var square = opData.Square;
        var setting = opData.Room.Setting;

        var conf = square.Configuration(ActiveVariant.Wall);
        var variation = square.VarType;

        bool halfSize = (conf != 3 && conf != 6 && conf != 9 && conf != 12) && square.hardEdge;

        var uvDatTopL = new UVData(opData, square.topLeft.TextureType, variation: square.VarType, halfSize: halfSize);
        var uvDatTopR = new UVData(opData, square.topRight.TextureType, variation: square.VarType, halfSize: halfSize);
        var uvDatBotL = new UVData(opData, square.bottomLeft.TextureType, variation: square.VarType, halfSize: halfSize);
        var uvDatBotR = new UVData(opData, square.bottomRight.TextureType, variation: square.VarType, halfSize: halfSize);

        // method CloneForAbove not so performant, used convention in _AssignVertices instead
        Square above = square;
        switch ((ECases) conf)
        {
            case ECases.None:
            case ECases.All:
                break;
            // 1 points:
            case ECases.BottomLeft:
                if (square.hardEdge)
                {
                    _MeshFromPoints(opData, ref uvDatTopL, above.centre, square.centre, square.centreLeft, above.centreLeft);
                    _MeshFromPoints(opData, ref uvDatBotR, above.centreBottom, square.centreBottom, square.centre, above.centre);
                }
                else
                    _MeshFromPoints(opData, ref uvDatTopR, above.centreBottom, square.centreBottom, square.centreLeft, above.centreLeft);
                break;
            case ECases.BottomRight:
                if (square.hardEdge)
                {
                    _MeshFromPoints(opData, ref uvDatBotL, above.centre, square.centre, square.centreBottom, above.centreBottom);
                    _MeshFromPoints(opData, ref uvDatTopR, above.centreRight, square.centreRight, square.centre, above.centre);
                }
                else _MeshFromPoints(opData, ref uvDatTopL, above.centreRight, square.centreRight, square.centreBottom, above.centreBottom);
                break;
            case ECases.TopRight:
                if (square.hardEdge)
                {
                    _MeshFromPoints(opData, ref uvDatBotR, above.centre, square.centre, square.centreRight, above.centreRight);
                    _MeshFromPoints(opData, ref uvDatTopL, above.centreTop, square.centreTop, square.centre, above.centre);
                }
                else
                    _MeshFromPoints(opData, ref uvDatBotL, above.centreTop, square.centreTop, square.centreRight, above.centreRight);
                break;
            case ECases.TopLeft:
                if (square.hardEdge)
                {
                    _MeshFromPoints(opData, ref uvDatTopR, above.centre, square.centre, square.centreTop, above.centreTop);
                    _MeshFromPoints(opData, ref uvDatBotL, above.centreLeft, square.centreLeft, square.centre, above.centre);
                }
                else
                    _MeshFromPoints(opData, ref uvDatBotR, above.centreLeft, square.centreLeft, square.centreTop, above.centreTop);
                break;
            // 2 points:
            case ECases.Bottom:
                if (square.topLeft.TextureType == square.topRight.TextureType)
                {
                    _MeshFromPoints(opData, ref uvDatTopL, above.centreRight, square.centreRight, square.centreLeft, above.centreLeft);
                }
                else
                {
                    uvDatTopR.SetHalfSize(); uvDatTopL.SetHalfSize(); uvDatTopR.SwitchOffset();
                    _MeshFromPoints(opData, ref uvDatTopR, above.centreRight, square.centreRight, square.centre, above.centre);
                    _MeshFromPoints(opData, ref uvDatTopL, above.centre, square.centre, square.centreLeft, above.centreLeft);
                }
                break;
            case ECases.Right:
                if (square.topLeft.TextureType == square.bottomLeft.TextureType)
                {
                    _MeshFromPoints(opData, ref uvDatTopL, above.centreTop, square.centreTop, square.centreBottom, above.centreBottom);
                }
                else
                {
                    uvDatTopL.SetHalfSize(); uvDatBotL.SetHalfSize(); uvDatTopL.SwitchOffset();
                    _MeshFromPoints(opData, ref uvDatTopL, above.centreTop, square.centreTop, square.centre, above.centre);
                    _MeshFromPoints(opData, ref uvDatBotL, above.centre, square.centre, square.centreBottom, above.centreBottom);
                }
                break;
            case ECases.Left:
                if (square.topRight.TextureType == square.bottomRight.TextureType)
                {
                    _MeshFromPoints(opData, ref uvDatTopR, above.centreBottom, square.centreBottom, square.centreTop, above.centreTop);
                }
                else
                {
                    uvDatTopR.SetHalfSize(); uvDatBotR.SetHalfSize(); uvDatBotR.SwitchOffset();
                    _MeshFromPoints(opData, ref uvDatTopR, above.centre, square.centre, square.centreTop, above.centreTop);
                    _MeshFromPoints(opData, ref uvDatBotR, above.centreBottom, square.centreBottom, square.centre, above.centre);
                }
                break;
            case ECases.Top:
                if (square.bottomLeft.TextureType == square.bottomRight.TextureType)
                {
                    _MeshFromPoints(opData, ref uvDatBotL, above.centreLeft, square.centreLeft, square.centreRight, above.centreRight);
                }
                else
                {
                    uvDatBotL.SetHalfSize(); uvDatBotR.SetHalfSize(); uvDatBotL.SwitchOffset();
                    _MeshFromPoints(opData, ref uvDatBotL, above.centreLeft, square.centreLeft, square.centre, above.centre);
                    _MeshFromPoints(opData, ref uvDatBotR, above.centre, square.centre, square.centreRight, above.centreRight);
                }
                break;
            case ECases.Diag1:
                _MeshFromPoints(opData, ref uvDatTopL, above.centre, square.centre, square.centreLeft, above.centreLeft);
                _MeshFromPoints(opData, ref uvDatBotR, above.centre, square.centre, square.centreRight, above.centreRight);
                _MeshFromPoints(opData, ref uvDatBotR, above.centreBottom, square.centreBottom, square.centre, above.centre);
                _MeshFromPoints(opData, ref uvDatTopL, above.centreTop, square.centreTop, square.centre, above.centre);
                break;
            case ECases.Diag2:
                _MeshFromPoints(opData, ref uvDatBotL, above.centre, square.centre, square.centreBottom, above.centreBottom);
                _MeshFromPoints(opData, ref uvDatTopR, above.centre, square.centre, square.centreTop, above.centreTop);
                _MeshFromPoints(opData, ref uvDatTopR, above.centreRight, square.centreRight, square.centre, above.centre);
                _MeshFromPoints(opData, ref uvDatBotL, above.centreLeft, square.centreLeft, square.centre, above.centre);
                break;
            // 3 point:
            case ECases.NotTopLeft:
                if (square.hardEdge)
                {
                    _MeshFromPoints(opData, ref uvDatTopL, above.centre, square.centre, square.centreLeft, above.centreLeft);
                    _MeshFromPoints(opData, ref uvDatTopL, above.centreTop, square.centreTop, square.centre, above.centre);
                }
                else
                    _MeshFromPoints(opData, ref uvDatTopL, above.centreTop, square.centreTop, square.centreLeft, above.centreLeft);
                break;
            case ECases.NotTopRight:
                if (square.hardEdge)
                {
                    _MeshFromPoints(opData, ref uvDatTopR, above.centre, square.centre, square.centreTop, above.centreTop);
                    _MeshFromPoints(opData, ref uvDatTopR, above.centreRight, square.centreRight, square.centre, above.centre);
                }
                else
                    _MeshFromPoints(opData, ref uvDatTopR, above.centreRight, square.centreRight, square.centreTop, above.centreTop);
                break;
            case ECases.NotBottomRight:
                if (square.hardEdge)
                {
                    _MeshFromPoints(opData, ref uvDatBotR, above.centre, square.centre, square.centreRight, above.centreRight);
                    _MeshFromPoints(opData, ref uvDatBotR, above.centreBottom, square.centreBottom, square.centre, above.centre);
                }
                else
                    _MeshFromPoints(opData, ref uvDatBotR, above.centreBottom, square.centreBottom, square.centreRight, above.centreRight);
                break;
            case ECases.NotBottomLeft:
                if (square.hardEdge)
                {
                    _MeshFromPoints(opData, ref uvDatBotL, above.centre, square.centre, square.centreBottom, above.centreBottom);
                    _MeshFromPoints(opData, ref uvDatBotL, above.centreLeft, square.centreLeft, square.centre, above.centre);
                }
                else
                    _MeshFromPoints(opData, ref uvDatBotL, above.centreLeft, square.centreLeft, square.centreBottom, above.centreBottom);

                break;
        }
    }

    static void _TriangulateBottomLeftSquareQuad(OperationData opData, int uvConf, ushort texType = ushort.MaxValue, TransitionTextureData data = null)
    {
        var square = opData.Square;
        var uvDat = new UVData(opData, texType, variation: square.VarType, hard: true, config: (int)ECases.BottomLeft, uvConfig: uvConf, transition: data);
        _MeshFromPoints(opData, ref uvDat, square.centreLeft, square.centre, square.centreBottom, square.bottomLeft);
    }
    static void _TriangulateBottomRightSquareQuad(OperationData opData, int uvConf, ushort texType = ushort.MaxValue, TransitionTextureData data = null)
    {
        var square = opData.Square;
        var uvDat = new UVData(opData, texType, variation: square.VarType, hard: true, config: (int)ECases.BottomRight, uvConfig: uvConf, transition: data);
        _MeshFromPoints(opData, ref uvDat, square.bottomRight, square.centreBottom, square.centre, square.centreRight);
    }
    static void _TriangulateTopRightSquareQuad(OperationData opData, int uvConf, ushort texType = ushort.MaxValue, TransitionTextureData data = null)
    {
        var square = opData.Square;
        var uvDat = new UVData(opData, texType, variation: square.VarType, hard: true, config: (int)ECases.TopRight, uvConfig: uvConf, transition: data);
        _MeshFromPoints(opData, ref uvDat, square.topRight, square.centreRight, square.centre, square.centreTop);
    }
    static void _TriangulateTopLeftSquareQuad(OperationData opData, int uvConf, ushort texType = ushort.MaxValue, TransitionTextureData data = null)
    {
        var square = opData.Square;
        var uvDat = new UVData(opData, texType, variation: square.VarType, hard: true, config: (int)ECases.TopLeft, uvConfig: uvConf, transition: data);
        _MeshFromPoints(opData, ref uvDat, square.topLeft, square.centreTop, square.centre, square.centreLeft);
    }

    static void _TriangulateSquareWFloorTransition(OperationData opData, ushort texTypeA, ushort texTypeB, TransitionTextureData transition)
    {
        var setting = opData.Room.Setting;
        var square = opData.Square;

        int texABL, texABR, texATR, texATL, texBBL, texBBR, texBTR, texBTL;
        //var texBConfig = 
        square.Configuration(out texBBL, out texBBR, out texBTR, out texBTL, ActiveVariant.Floor, texTypeB);
        var texAConfig = square.Configuration(out texABL, out texABR, out texATR, out texATL, ActiveVariant.Floor, texTypeA);

        var texBConfig = ~texAConfig & 15;

        if (texABL == 1)
        {
            _TriangulateBottomLeftSquareQuad(opData, (int)ECases.All, texTypeA);
        }
        else if(texBBL == 1)
        {
            var subConf = texBConfig & ~(int)ECases.TopRight;
            _TriangulateBottomLeftSquareQuad(opData, subConf, data: transition);
        }

        if (texABR == 1)
        {
            _TriangulateBottomRightSquareQuad(opData, (int)ECases.All, texTypeA);
        }
        else if (texBBR == 1)
        {
            var subConf = texBConfig & ~(int)ECases.TopLeft;
            _TriangulateBottomRightSquareQuad(opData, subConf, data: transition);
        }

        if (texATR == 1)
        {
            _TriangulateTopRightSquareQuad(opData, (int)ECases.All, texTypeA);
        }
        else if (texBTR == 1)
        {
            var subConf = texBConfig & ~(int)ECases.BottomLeft;
            _TriangulateTopRightSquareQuad(opData, subConf, data: transition);
        }

        if (texATL == 1)
        {
            _TriangulateTopLeftSquareQuad(opData, (int)ECases.All, texTypeA);
        }
        else if (texBTL == 1)
        {
            var subConf = texBConfig & ~(int)ECases.BottomRight;
            _TriangulateTopLeftSquareQuad(opData, subConf, data: transition);
        }
    }

    static void _TriangulateSquareQuads(OperationData opData, ushort texType, int uvConf)
    {
        //var setting = opData.Room.Setting;
        var square = opData.Square;

        if (opData.NodeActive[(int)NodePos.BottomLeft] == 1)
            _TriangulateBottomLeftSquareQuad(opData, uvConf, texType);
        if (opData.NodeActive[(int)NodePos.BottomRight] == 1)
            _TriangulateBottomRightSquareQuad(opData, uvConf, texType);
        if (opData.NodeActive[(int)NodePos.TopRight] == 1)
            _TriangulateTopRightSquareQuad(opData, uvConf, texType);
        if (opData.NodeActive[(int)NodePos.TopLeft] == 1)
            _TriangulateTopLeftSquareQuad(opData, uvConf, texType);
    }

    static void _TriangulateCeilingSquare(OperationData opData)
    {
        opData.WallOperation = false;

        var setting = opData.Room.Setting;
        var square = opData.Square;

        var config = square.Configuration(opData, ActiveVariant.Ceiling);

        // hard means no diagonals, we never generate diagonals with two active nodes
        var hard = square.hardEdge || opData.ActiveNodes == 2;
        var uvDat = new UVData(opData, ushort.MaxValue, variation: square.VarType, hard: hard, config: config, uvConfig: config);

        var split = config == (int)ECases.Diag1 || config == (int)ECases.Diag2
            || (hard && opData.ActiveNodes == 1);

        if (split)
        {
            _TriangulateSquareQuads(opData, ushort.MaxValue, config);
            return;
        }

        _DefaultTriangulationCases(opData, ref uvDat, (ECases)config, ushort.MaxValue, hard, false);
    }

    static void _TriangulateFloorSquare(OperationData opData, ushort texType, TransitionTextureData transition = null)
    {
        opData.WallOperation = false;

        var setting = opData.Room.Setting;
        var square = opData.Square;

        var configuration = square.Configuration(opData, ActiveVariant.Floor, texType);
        var activeNodes = opData.ActiveNodes; 

        // uv-configuration helps choosing the right uv-coords
        var uvConf = configuration;

        var edgetype = (setting == null || transition != null) ? TextureCollection.ETexType.Invalid : setting.EdgeType(texType);
        // if all node types are equal or we have no edge-texture in our texture-collection, we have no edge
        bool noEdge = edgetype == TextureCollection.ETexType.Invalid || configuration == (int)ECases.All;

        if (transition != null)
        {
            uvConf = configuration;
            configuration = (int)ECases.All;
        }
        // if there should be no edge towards walls of same texture type, we have a different uv-config
        else if (setting != null && setting.TexTypeEdgeOnly(texType) && !noEdge)
            uvConf = square.Configuration(ActiveVariant.TexTypeOnly, texType);

        // if the texType of all nodes is equal (uvConf == (int)ECases.All) we have no edge
        noEdge |= (uvConf == (int)ECases.All);

        bool simpleEdge = !noEdge && edgetype == TextureCollection.ETexType.SimpleEdgeTile;
        bool complexEdge = !(simpleEdge || noEdge);

        // hard means no diagonals, we never generate diagonals with two active nodes
        var hard = square.hardEdge || activeNodes == 2;
        var uvDat = new UVData(opData, texType, variation: square.VarType, hard: hard, config: configuration, uvConfig: uvConf, transition: transition);

        var split = (uvConf != configuration && uvConf != (int)ECases.All)
            || configuration == (int)ECases.Diag1 || configuration == (int)ECases.Diag2
            || transition != null
            || (hard && activeNodes == 1)
            // we cannot make the edge with our HardEdgeSq-texture in one mesh in this case
            || (complexEdge && hard && activeNodes == 3);

        if (split)
        {
            _TriangulateSquareQuads(opData, texType, uvConf); return;
        }

        _DefaultTriangulationCases(opData, ref uvDat, (ECases)configuration, texType, hard, simpleEdge);
    }

    static void _DefaultTriangulationCases(OperationData opData, ref UVData uvDat, ECases configuration, ushort texType, bool hard, bool simpleEdge)
    {
        var square = opData.Square;

        switch (configuration)
        {
            case ECases.None:
                break;
            // 1 points:
            case ECases.BottomLeft: _MeshFromPoints(opData, ref uvDat, square.centreLeft, square.centreBottom, square.bottomLeft); break;
            case ECases.BottomRight: _MeshFromPoints(opData, ref uvDat, square.bottomRight, square.centreBottom, square.centreRight); break;
            case ECases.TopRight: _MeshFromPoints(opData, ref uvDat, square.topRight, square.centreRight, square.centreTop); break;
            case ECases.TopLeft: _MeshFromPoints(opData, ref uvDat, square.topLeft, square.centreTop, square.centreLeft); break;
            // 2 points:
            case ECases.Bottom: _MeshFromPoints(opData, ref uvDat, square.centreRight, square.bottomRight, square.bottomLeft, square.centreLeft); break;
            case ECases.Right: _MeshFromPoints(opData, ref uvDat, square.centreTop, square.topRight, square.bottomRight, square.centreBottom); break;
            case ECases.Left: _MeshFromPoints(opData, ref uvDat, square.topLeft, square.centreTop, square.centreBottom, square.bottomLeft); break;
            case ECases.Top: _MeshFromPoints(opData, ref uvDat, square.topLeft, square.topRight, square.centreRight, square.centreLeft); break;
            // 3 point:
            case ECases.NotTopLeft:
                if (hard)
                {
                    _MeshFromPoints(opData, ref uvDat, square.centre, square.centreTop, square.topRight, square.bottomRight, square.bottomLeft, square.centreLeft);
                }
                else
                {
                    if (simpleEdge)
                    {   // dont draw bottom-right quarter with edge texture:
                        _MeshFromPoints(opData, ref uvDat, square.centre, square.centreBottom, square.bottomLeft, square.centreLeft, square.centreTop, square.topRight, square.centreRight);
                        _TriangulateBottomRightSquareQuad(opData, (int)ECases.All, texType);
                    }
                    else
                    {
                        _MeshFromPoints(opData, ref uvDat, square.centreTop, square.topRight, square.bottomRight, square.bottomLeft, square.centreLeft);
                    }
                }
                break;
            case ECases.NotTopRight:
                if (hard)
                {
                    _MeshFromPoints(opData, ref uvDat, square.centre, square.centreRight, square.bottomRight, square.bottomLeft, square.topLeft, square.centreTop);
                }
                else
                {
                    if (simpleEdge)
                    {   // dont draw bottom-left quarter with edge texture:
                        _MeshFromPoints(opData, ref uvDat, square.centre, square.centreLeft, square.topLeft, square.centreTop, square.centreRight, square.bottomRight, square.centreBottom);
                        _TriangulateBottomLeftSquareQuad(opData, (int)ECases.All, texType);
                    }
                    else
                        _MeshFromPoints(opData, ref uvDat, square.topLeft, square.centreTop, square.centreRight, square.bottomRight, square.bottomLeft);
                }
                break;
            case ECases.NotBottomRight:
                if (hard)
                {
                    _MeshFromPoints(opData, ref uvDat, square.centre, square.centreBottom, square.bottomLeft, square.topLeft, square.topRight, square.centreRight);
                }
                else
                {
                    if (simpleEdge)
                    {   // dont draw top-left quarter with edge texture:
                        _MeshFromPoints(opData, ref uvDat, square.centre, square.centreTop, square.topRight, square.centreRight, square.centreBottom, square.bottomLeft, square.centreLeft);
                        _TriangulateTopLeftSquareQuad(opData, (int)ECases.All, texType);
                    }
                    else
                        _MeshFromPoints(opData, ref uvDat, square.topLeft, square.topRight, square.centreRight, square.centreBottom, square.bottomLeft);
                }
                break;
            case ECases.NotBottomLeft:
                if (hard)
                {
                    _MeshFromPoints(opData, ref uvDat, square.centre, square.centreLeft, square.topLeft, square.topRight, square.bottomRight, square.centreBottom);
                }
                else
                {
                    if (simpleEdge)
                    {   // dont draw top-right quarter with edge texture:
                        _MeshFromPoints(opData, ref uvDat, square.centre, square.centreRight, square.bottomRight, square.centreBottom, square.centreLeft, square.topLeft, square.centreTop);
                        _TriangulateTopRightSquareQuad(opData, (int)ECases.All, texType);
                    }
                    else
                        _MeshFromPoints(opData, ref uvDat, square.topLeft, square.topRight, square.bottomRight, square.centreBottom, square.centreLeft);
                }
                break;

            // 4 point:
            case ECases.All:
                _MeshFromPoints(opData, ref uvDat, square.topLeft, square.topRight, square.bottomRight, square.bottomLeft);
                break;
        }
    }

    static void _MeshFromPoints(OperationData opDat, ref UVData uvDat, params Node[] points)
    {
        var meshDat = opDat.MeshData;
        // convention that first and last point will be for upper vertex in walls
        if (opDat.WallOperation)
        {
            points[0] = Node.CreateNode(opDat, points[0].position + Vector3.up);
            points[3] = Node.CreateNode(opDat, points[3].position + Vector3.up);
        }

        _AssignVertices(points, opDat, ref uvDat);

        for (int i=0; points.Length >= (i+3); ++i)
        {
            _CreateTriangle(points[0], points[i+1], points[i+2], meshDat);
        }

        if (opDat.WallOperation)
        {
            meshDat.uvs.Add(uvDat.GetUVs(Vector2.right + Vector2.up));
            meshDat.uvs.Add(uvDat.GetUVs(Vector2.right));
            meshDat.uvs.Add(uvDat.GetUVs(Vector2.zero));
            meshDat.uvs.Add(uvDat.GetUVs(Vector2.up));

            uvDat.SwitchOffset();
        }
    }

    static void _AssignVertices(Node[] points, OperationData opDat, ref UVData uvDat)
    {
        var meshDat = opDat.MeshData;
        for (int i = 0; i < points.Length; ++i)
        {
            points[i].vertexIndex = meshDat.vertices.Count;
            meshDat.vertices.Add(points[i].position);
            if (!opDat.WallOperation)
                meshDat.uvs.Add(uvDat.GetUVs(points[i].position));
        }
    }

    static void _CreateTriangle(Node a, Node b, Node c, MeshData opDat)
    {
        opDat.triangles.Add(a.vertexIndex);
        opDat.triangles.Add(b.vertexIndex);
        opDat.triangles.Add(c.vertexIndex);
    }

    public struct Triangle
    {
        public int vertexIndexA;
        public int vertexIndexB;
        public int vertexIndexC;
        int[] vertices;

        public Triangle(int a, int b, int c)
        {
            vertexIndexA = a;
            vertexIndexB = b;
            vertexIndexC = c;

            vertices = new int[3];
            vertices[0] = a;
            vertices[1] = b;
            vertices[2] = c;
        }

        public int this[int i]
        {
            get { return vertices[i]; }
        }

        public bool Contains(int vertexIndex)
        {
            return vertexIndexA == vertexIndex
                || vertexIndexB == vertexIndex
                || vertexIndexC == vertexIndex;
        }
    }

    public class SquareGrid
    {
        public List<Square> squares;
        public List<ControlNode> controlNodes;
        int sqDimX, sqDimZ;

        internal int GetSquareDimensionX() { return sqDimX; }
        internal int GetSquareDimensionZ() { return sqDimZ; }
        public Square GetSquare(int x, int z) { return squares[z * sqDimX + x]; }

        public static SquareGrid CreateSquareGrid(OperationData data, int lvlIdx, List<Vector3Int> hardEdges)
        {
            var objs = data.PooledObjs;
            SquareGrid grid = objs.GetSquareGrid();
            grid.squares = SimplePool<List<Square>>.I.Get();
            grid.controlNodes = SimplePool<List<ControlNode>>.I.Get();
            grid.InitWithData(data, lvlIdx, hardEdges);
            return grid;
        }

        void InitWithData(OperationData data, int lvlIdx, List<Vector3Int> hardEdges)
        {
            var room = data.Room;
            Room.Level lvl = room.Levels[lvlIdx];
            int nodeCountX = room.TileDim.x;
            int nodeCountZ = room.TileDim.y;

            for (int z = 0; z < nodeCountZ; ++z)
            {
                for (int x = 0; x < nodeCountX; ++x)
                {
                    Vector3 pos = new Vector3(x * kSquareSize, 0, z * kSquareSize);

                    var tile = room.GetTile(lvlIdx, x, z, OutOfLevelBoundsAction.IgnoreTile);
                    if (tile == ushort.MaxValue)
                        tile = ((ushort)Type.Wall << 2);
                    tile = (ushort)(tile & ~OriginMask);
                    controlNodes.Add(ControlNode.CreateControlNode(data, pos, tile));
                }
            }

            sqDimX = nodeCountX-1;
            sqDimZ = nodeCountZ-1;
            for (int z = 0; z < sqDimZ; ++z)
            {
                for (int x = 0; x < sqDimX; ++x)
                {
                    var square = Square.CreateSquare(data, 
                        controlNodes[(z+1) * nodeCountX + x], controlNodes[(z + 1) * nodeCountX + x+1], 
                        controlNodes[z * nodeCountX + x+1], controlNodes[z * nodeCountX + x]);

                    if (square.hardEdge)
                        hardEdges?.Add(new Vector3Int(x, lvlIdx, z));
                    squares.Add(square);
                }
            }

        }

        public void Return()
        {
            SimplePool<List<Square>>.I.Return(squares);
            SimplePool<List<ControlNode>>.I.Return(controlNodes);

            squares = null;
            controlNodes = null;
        }
    }

    public class Square
    {
        public ControlNode topLeft { get { return Nodes[(int)NodePos.TopLeft] as ControlNode; } }
        public ControlNode topRight { get { return Nodes[(int)NodePos.TopRight] as ControlNode; } }
        public ControlNode bottomRight { get { return Nodes[(int)NodePos.BottomRight] as ControlNode; } }
        public ControlNode bottomLeft { get { return Nodes[(int)NodePos.BottomLeft] as ControlNode; } }

        public Node centre { get { return Nodes[(int)NodePos.Centre]; } }
        public Node centreBottom { get { return Nodes[(int)NodePos.CentreBottom]; } }
        public Node centreLeft { get { return Nodes[(int)NodePos.CentreLeft]; } }
        public Node centreRight { get { return Nodes[(int)NodePos.CentreRight]; } }
        public Node centreTop { get { return Nodes[(int)NodePos.CentreTop]; } }

        public Node[] Nodes = new Node[9];
        public int activeFloorNodes;

        public bool hardEdge;
        public bool breakHardEdge;
        public int floorTextypes;

        public static Square CreateSquare(OperationData opDat, ControlNode _topLeft, ControlNode _topRight, ControlNode _bottomRight, ControlNode _bottomLeft)
        {
            var sq = opDat.PooledObjs.GetSquare();
            sq.InitWithData(opDat, _topLeft, _topRight, _bottomRight, _bottomLeft);
            return sq;
        }

        void InitWithData(OperationData opDat, ControlNode _topLeft, ControlNode _topRight, ControlNode _bottomRight, ControlNode _bottomLeft)
        {
            Nodes[(int)NodePos.TopLeft] = _topLeft;
            Nodes[(int)NodePos.TopRight] = _topRight;
            Nodes[(int)NodePos.BottomLeft] = _bottomLeft;
            Nodes[(int)NodePos.BottomRight] = _bottomRight;

            Nodes[(int)NodePos.CentreBottom] = _bottomLeft.right;
            Nodes[(int)NodePos.CentreLeft] = _bottomLeft.above;
            Nodes[(int)NodePos.CentreTop] = _topLeft.right;
            Nodes[(int)NodePos.CentreRight] = _bottomRight.above;

            Nodes[(int)NodePos.Centre] = Node.CreateNode(opDat, _bottomLeft.position + new Vector3(kSquareSize * 0.5f, 0.0f, kSquareSize * 0.5f));
            CheckHardEdge(opDat.Room);
        }

        public int Configuration(ActiveVariant v, ushort texType = ushort.MaxValue)
        {
            var configuration = 0;
            int posVal = 1;
            for (int i = 0; i <= (int)NodePos.TopLeft; ++i, posVal *= 2)
            {
                if ((Nodes[i] as ControlNode).IsActive(v, texType))
                {
                    configuration += posVal;
                }
            }
            return configuration;
        }

        public int Configuration(out int bL, out int bR, out int tR, out int tL, ActiveVariant v, ushort texType = ushort.MaxValue)
        {
            bL = 0; bR = 0; tR = 0; tL = 0;

            if (bottomLeft.IsActive(v, texType)) bL = 1;
            if (bottomRight.IsActive(v, texType)) bR = 1;
            if (topRight.IsActive(v, texType)) tR = 1;
            if (topLeft.IsActive(v, texType)) tL = 1;

            return bL * (int)ECases.BottomLeft + bR * (int)ECases.BottomRight + tL * (int)ECases.TopLeft + tR * (int)ECases.TopRight;
        }

        public int Configuration(OperationData opDat, ActiveVariant v, ushort texType = ushort.MaxValue)
        {
            int bL, bR, tR, tL;
            var config = Configuration(out bL, out bR, out tR, out tL, v, texType);

            opDat.ActiveNodes = bL + bR + tR + tL;
            opDat.NodeActive[(int)NodePos.BottomRight] = bR;
            opDat.NodeActive[(int)NodePos.BottomLeft] = bL;
            opDat.NodeActive[(int)NodePos.TopLeft] = tL;
            opDat.NodeActive[(int)NodePos.TopRight] = tR;

            return config;
        }

        public ushort VarType { get { return VariationType(bottomLeft.tile); } }

        public ushort NextTexType (ref int startNode, ref ushort texTypeDoneA, ref ushort texTypeDoneB, ref ushort texTypeDoneC)
        {
            for (; startNode <= (int)NodePos.TopLeft; ++startNode)
            {
                var cNode = (Nodes[startNode] as ControlNode);
                if (cNode.ResultingType != Type.Floor)
                    continue;
                var type = cNode.TextureType;
                if (texTypeDoneA == ushort.MaxValue)
                {
                    texTypeDoneA = type;
                    return type;
                }
                if (texTypeDoneA == type)
                    continue;
                if (texTypeDoneB == ushort.MaxValue)
                {
                    texTypeDoneB = type;
                    return type;
                }
                if (texTypeDoneB == type)
                    continue;
                if (texTypeDoneC == ushort.MaxValue)
                {
                    texTypeDoneC = type;
                    return type;
                }
                if (texTypeDoneC == type)
                    continue;
                return type;
            }
            return ushort.MaxValue;
        }

        void CheckHardEdge(Room room)
        {
            if (room == null)
                return;

            int equalNodesTileType = 0;
            activeFloorNodes = 0;

            floorTextypes = 4;
            bool forceHardEdge = false;

            for (int i=0; i <= (int)NodePos.TopLeft; ++i)
            {
                bool equalsInTexType = false;

                var nodeA = (Nodes[i] as ControlNode);
                var nodeATileType = nodeA.ResultingType;
                ushort nodeATexType = ushort.MaxValue;
                if (nodeATileType == Type.Floor)
                {
                    ++activeFloorNodes;
                    nodeATexType = nodeA.TextureType;
                    forceHardEdge |= (room.Setting?.IsForce(nodeATexType) == true);
                }
                else --floorTextypes;

                for (int j = i + 1; j <= (int)NodePos.TopLeft; ++j)
                {
                    var nodeB = (Nodes[j] as ControlNode);
                    var nodeBTileType = nodeB.ResultingType;
                    var nodeBTexType = nodeB.TextureType;

                    if (nodeBTileType == Type.Floor)
                        equalsInTexType |= (nodeATexType == nodeBTexType);
                    if (nodeATileType == nodeBTileType)
                        ++equalNodesTileType;
                }

                if (equalsInTexType)
                    --floorTextypes;
            }

            //bool differentTextures = floorTextypes > 1;

            bool hard = floorTextypes > 2 || (floorTextypes > 1 && equalNodesTileType != 6) || equalNodesTileType <= 1;
            // when we have 2 activeFloorNodes or all node types are equal we don't have diagonal edges anyway, so it breaks the force-hard-edge propagation
            bool breakHard = !hard && (activeFloorNodes == 2 || equalNodesTileType == 6);
            _SetHardSoftEdge(hard || forceHardEdge, breakHard);
        }

        void _SetHardSoftEdge(bool hard, bool breakHard)
        {
            hardEdge = hard;
            breakHardEdge = breakHard;
        }
    }

    public class Node
    {
        public Vector3 position;
        public int vertexIndex = -1;

        internal static Node CreateNode(OperationData opData, Vector3 _pos)
        {
            var node = opData.PooledObjs.GetNode();
            node.InitWithData(_pos);
            return node;
        }

        protected void InitWithData(Vector3 _pos)
        {
            position = _pos;
            vertexIndex = -1;
        }
    }

    public class ControlNode : Node
    {
        public ushort tile;
        public Node above, right;
        public Type ResultingType { get { return ResultingType(tile); } }
        public ushort TextureType { get { return TextureType(tile); } }

        internal static ControlNode CreateControlNode(OperationData opData, Vector3 pos, ushort tile)
        {
            var node = opData.PooledObjs.GetControlNode();
            node.InitWithData(opData, pos, tile);
            return node;
        }
        void InitWithData(OperationData opData, Vector3 _pos, ushort _tile)
        {
            InitWithData(_pos);
            tile = _tile;
            above = CreateNode(opData, position + Vector3.forward * kSquareSize * 0.5f);
            right = CreateNode(opData, position + Vector3.right * kSquareSize * 0.5f);
        }

        public bool IsActive(ActiveVariant v, ushort texType = ushort.MaxValue)
        {
            switch (v)
            {
                case ActiveVariant.Floor:
                    if (texType == ushort.MaxValue)
                        return ResultingType(tile) == Type.Floor;
                    return ResultingType(tile) == Type.Floor && TextureType == texType;
                case ActiveVariant.Wall:  return ResultingType(tile) != Type.Wall;
                case ActiveVariant.Ceiling: return ResultingType(tile) == Type.Wall;
                case ActiveVariant.TexTypeOnly: return TextureType == texType;
            }
            return false;
        }

    }
}
