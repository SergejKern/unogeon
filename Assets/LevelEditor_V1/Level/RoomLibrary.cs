﻿
using System.Collections.Generic;
using UnityEngine;

public class RoomLibrary: ScriptableObject
{
    public List<Room> EndRoomsSmall = new List<Room>();

    public List<Room> TRoomsSmall = new List<Room>();
    public List<Room> LRoomsSmall = new List<Room>();
    public List<Room> IRoomsSmall = new List<Room>();

    public List<Room> Tower = new List<Room>();
}
