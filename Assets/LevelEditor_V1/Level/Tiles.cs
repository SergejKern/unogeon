public static class Tiles
{
    public enum Type
    {
        None = -1,
        AutoNonFloor = 0, // picks wall or floor depending on below
        Wall = 1,
        Empty = 2,
        Floor = 3, // floor should have prio for tiletype over walls
    }

    public const ushort OriginMask = 3; //0011
    public const ushort ResultMask = 12; //1100

    public const ushort TexShift = 4;
    public const ushort TexMask = 1008; //11 1111 0000
    // todo 62 does not work with CheckHardEdge in MeshOperations because uint only has 32 bits, 
    // also if game wants to compete with minecraft or terraria 158 and 84 blocks exist in those games
    public const ushort TexMax = 62; //11 1111 0000

    // Variations
    public const ushort VarShift = 10;
    public const ushort VarMask = 15360; //11 1100 0000 0000
    public const ushort VarMax = 14; //11 1100 0000 0000

    // last numbers? : disable mesh generation for specific objects? f.e. like beach-shore-water-transition

    public static Type OriginType(ushort tile) { return (Type)(tile & OriginMask); }
    public static Type ResultingType(ushort tile) { return (Type)((tile & ResultMask) >> 2); }
    public static ushort TextureType(ushort tile) { return (ushort)((tile & TexMask) >> TexShift); }

    public static ushort VariationType(ushort tile) { return (ushort)((tile & VarMask) >> VarShift); }
}
