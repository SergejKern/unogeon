
using UnityEngine;
using System.Collections.Generic;
using Core.Interface;
using Core.Unity.Extensions;

public class RoomComponent : MonoBehaviour, IProvider<Bounds>
{
    public struct LevelVisuals
    {
        public LevelVisuals(GameObject go, MeshRenderer mr, MeshFilter mf, MeshCollider col = null)
        {
            Go = go;
            Renderer = mr;
            Filter = mf;
            Mesh = new Mesh();
            Filter.sharedMesh = Mesh;
            Collider = col;

            if (col != null)
            {
                col.sharedMesh = null;
                col.sharedMesh = Mesh;
            }
        }

        public void Set (GameObject go, MeshRenderer mr, MeshFilter mf, MeshCollider col = null)
        {
            Go = go;
            Renderer = mr;
            Filter = mf;
            if (Mesh == null)
                Mesh = new Mesh();
            Filter.sharedMesh = Mesh;
            Collider = col;

            if (col != null)
            {
                col.sharedMesh = null;
                col.sharedMesh = Mesh;
            }
        }

        public GameObject Go;
        public MeshRenderer Renderer;
        public MeshFilter Filter;
        public MeshCollider Collider;
        public Mesh Mesh;
    }

    Transform _FloorVisualsParent;
    Transform _CeilingVisualsParent;

    List<LevelVisuals> _FloorVisuals = new List<LevelVisuals>();
    List<LevelVisuals> _CeilingVisuals = new List<LevelVisuals>();

    public Bounds Bounds { get { return Room.Bounds; } }
    public Room Room;

    public int OpenEnds { get { return Room.OpenExits.Count; } }

    /// <summary>
    /// sets name
    /// </summary>
    public void SetName(string _name) { name = _name; }

    public LevelVisuals GetFloorVisuals(int levelIdx)
    {
        return _FloorVisuals[levelIdx];
    }
    public LevelVisuals GetCeilingVisuals(int levelIdx)
    {
        return _CeilingVisuals[levelIdx];
    }
	
    void OnDestroy()
    {
        foreach(var fv in _FloorVisuals)
        {
            fv.Mesh?.DestroyEx();
        }
        foreach (var cv in _CeilingVisuals)
        {
            cv.Mesh?.DestroyEx();
        }
    }

    /// <summary>
    /// Sets the Level the player is currently at, eg. what should be visible
    /// </summary>
    /// <param name="lvl">the level</param>
    public void SetLevel(int lvl)
    {
        //int ceilLvl = lvl;
        //if (ceilLvl >= _CeilingVisuals.Count)
        //    ceilLvl = _CeilingVisuals.Count -1;

        if (_FloorVisuals.Count != (_CeilingVisuals.Count + 1))
        {
            Debug.LogError("_FloorVisuals.Count != _CeilingVisuals.Count + 1");
            return;
        }
        if (!Application.isPlaying)
        {
            //Global.Resources.Instance.DungeonFloorMaterialEditor.SetInt("_Lvl", lvl);
            Room?.Setting?.Material?.SetInt("_Lvl", lvl);
        }

        int i = 0;
        for (; i< _CeilingVisuals.Count; ++i)
        {
            _CeilingVisuals[i].Go.SetActive(lvl == i);
            _FloorVisuals[i].Go.SetActive(i <= lvl+1);
        }
        // cannot remember why i >= lvl - 1 (which means for last level we cannot go higher then one above or it will not show)
        //_FloorVisuals[i].Go.SetActive(i >= lvl - 1 && i <= lvl + 1);
        _FloorVisuals[i].Go.SetActive(i <= lvl + 1);
    }

    internal void UpdateVisuals()
    {
        if (_FloorVisualsParent == null && (_FloorVisualsParent=transform.Find("FloorVisuals")) == null)
            transform.CreateChildObject("FloorVisuals", out _FloorVisualsParent);
        if (_CeilingVisualsParent == null && (_CeilingVisualsParent = transform.Find("CeilingVisuals")) == null)
            transform.CreateChildObject("CeilingVisuals", out _CeilingVisualsParent);
        _CeilingVisualsParent.localPosition = 2 * Vector3.up;

        //var floorMaterial = Application.isPlaying ? Global.Resources.Instance.DungeonFloorMaterial : Global.Resources.Instance.DungeonFloorMaterialEditor;
        Material floorMaterial = Room?.Setting?.Material;

        int i = 0;
        for (; i < Room.MaxLevel; ++i)
        {
            _EnsureVisuals(_CeilingVisualsParent, _CeilingVisuals, i, Global.Resources.Instance.DungeonCeilingMaterial);
            _EnsureVisuals(_FloorVisualsParent, _FloorVisuals, i, floorMaterial, true);
        }
        // we always need one extra floor visual, because we render 2 for each level
        _EnsureVisuals(_FloorVisualsParent, _FloorVisuals, i, floorMaterial, true);

        var killLoopCeilings = Mathf.Max(_CeilingVisualsParent.childCount, _CeilingVisuals.Count);
        var killLoopFloors = Mathf.Max(_FloorVisualsParent.childCount, _FloorVisuals.Count);
        var loop = Mathf.Max(killLoopCeilings, killLoopFloors);

        for (var j = loop-1; j >= i; --j)
        {
            _RemoveVisuals(_CeilingVisualsParent, _CeilingVisuals, j);
            if (j > i)
                _RemoveVisuals(_FloorVisualsParent, _FloorVisuals, j);
        }
    }

    void _RemoveVisuals(Transform parent, List<LevelVisuals> visuals, int idx)
    {
        if (parent.childCount > idx)
            parent.GetChild(idx).gameObject.DestroyEx();
        if (visuals.Count > idx)
        {
            visuals[idx].Mesh?.DestroyEx();
            visuals.RemoveAt(idx);
        }
    }

    void _EnsureVisuals(Transform parent, List<LevelVisuals> visuals, int idx, Material m, bool needCollider = false)
    {
        MeshRenderer rend = null;
        MeshFilter filt = null;
        GameObject visualGo = null;
        Transform visualTr = null;
        MeshCollider coll = null;

        if (parent.childCount <= idx)
        {
            parent.CreateChildObject(idx.ToString(), out visualGo, out visualTr);
            visualTr.localPosition = new Vector3(0, idx, 0);
        }
        else
        {
            var tr = parent.GetChild(idx);
            visualGo = tr.gameObject;
            rend = visualGo.GetComponent<MeshRenderer>();
            filt = visualGo.GetComponent<MeshFilter>();
            coll = visualGo.GetComponent<MeshCollider>();
        }

        if(rend == null)
        {
            rend = visualGo.AddComponent<MeshRenderer>();
        }
        rend.sharedMaterial = m;

        if (filt == null)
            filt = visualGo.AddComponent<MeshFilter>();
        if (coll == null && needCollider)
            coll = visualGo.AddComponent<MeshCollider>();

        if (visuals.Count <= idx)
            visuals.Add(new LevelVisuals(visualGo, rend, filt, coll));
        else
            visuals[idx].Set(visualGo, rend, filt, coll);
    }

    public void Get(out Bounds provided) => provided = Bounds;
}
