using UnityEngine;
using System;
using System.Collections.Generic;
using System.Collections;
using Core.Extensions;

[Serializable]
public class TextureCollection : IEnumerable<TextureCollection.Data>, IEnumerator<TextureCollection.Data>
{
    public static int SortBiggestDimensionFirst(Data dat1, Data dat2)
    {
        var dim1 = dat1.TileDimension();
        var dim2 = dat2.TileDimension();
        if (dim1.y > dim2.y) return -1;
        if (dim1.y < dim2.y) return 1;
        if (dim1.x > dim2.x) return -1;
        if (dim1.x < dim2.x) return 1;
        return 0;
    }

    public enum ETexType
    {
        SimpleFloorTile,
        SimpleWallTile,
        SimpleEdgeTile,
        HardEdgeSq,
        DiagEdge,
        WallTriplet,
        WallTripletEdged,
        Transition,
        Invalid
    }
    public enum EAtlasType
    {
        Default =0,
        Normal,
        MetallicSmoothness,
        Occlusion,
        Emission,
        Length
    }

    public static readonly string[] kTexNames = new string[] {
        "_MainTex",
        "_BumpMap",
        "_MetallicGlossMap",
        "_OcclusionMap",
        "_EmissionMap"
    };
    public static Vector2Int TileDimension(ETexType type)
    {
        switch(type)
        {
            case ETexType.SimpleFloorTile:
            case ETexType.SimpleWallTile:
            case ETexType.SimpleEdgeTile: return Vector2Int.one;
            case ETexType.HardEdgeSq:
            case ETexType.DiagEdge:
            case ETexType.Transition:
                return Vector2Int.one * 2;
            case ETexType.WallTriplet: return new Vector2Int(1, 3);
            case ETexType.WallTripletEdged: return new Vector2Int(2, 3);
            default: return Vector2Int.zero;
        }
    }

    [Serializable]
    public class Data
    {
        public Texture2D DiffuseTex;
        [SerializeField]
        Texture2D[] Textures = new Texture2D[(int)EAtlasType.Length];

        public Vector2 UVOffset;
        public ETexType Type;

        public bool IsSet () {
            if (DiffuseTex != null) return true;

            return Textures != null 
                && Textures[(int)EAtlasType.Default] != null;
        } 
        public Vector2Int TileDimension() { return TextureCollection.TileDimension(Type); }
        public Texture2D GetTexture(EAtlasType atlasType = EAtlasType.Default)
        {
            if (atlasType == EAtlasType.Default && DiffuseTex != null) return DiffuseTex;

            return Textures[(int)atlasType];
        }
        public void SetTexture(EAtlasType atlasType, Texture2D tex)
        {
            Textures[(int)atlasType] = tex;
            if (atlasType == EAtlasType.Default && tex == null)
            {
                Type = ETexType.Invalid;
                DiffuseTex = null;
            }
        }
        public void Has(Texture2D tex, HashSet<EAtlasType> types)
        {
            if (DiffuseTex == tex) types.Add(EAtlasType.Default);

            for (int i = 0; i < Textures.Length; i++)
            {
                if (Textures[i] == tex && !types.Contains((EAtlasType)i))
                    types.Add((EAtlasType)i);
            }
        }

        public void UpgradeToArray() {
            if (Textures?.Length == (int)EAtlasType.Length && !Textures.IsNullOrEmpty())
            {
                DiffuseTex = null;
                return;
            }
            Textures = new Texture2D[(int)EAtlasType.Length];
            Textures[(int)EAtlasType.Default] = DiffuseTex;
            DiffuseTex = null;
        }
    }

    public string Name;

    int _Position = -1;

    public Data Current
    {
        get
        {
            int floorVarCount = FloorVariations?.Count ?? 0;
            int floorVarEnd = floorVarCount;

            int wallVarStart = floorVarEnd;
            int wallVarCount = WallVariations?.Count ?? 0;
            int wallVarEnd = wallVarStart + wallVarCount;

            if (_Position < 0)
                return null;

            if (_Position < floorVarEnd)
                return FloorVariations[_Position];
            else if (_Position < wallVarEnd)
                return WallVariations[_Position - wallVarStart];
            else if (_Position == wallVarEnd && Edge.IsSet())
                return Edge;
            else if (_Position == wallVarEnd+1 && DiagEdge.IsSet())
                return DiagEdge;
            throw new InvalidOperationException();
        }
    }

    object IEnumerator.Current { get { return Current; } }

    public List<Data> FloorVariations = new List<Data>();
    public List<Data> WallVariations = new List<Data>();
    public Data Edge = new Data();
    public Data DiagEdge = new Data();

    public bool HasEdge { get { return Edge.IsSet(); } }
    public bool HasDiagEdge { get { return DiagEdge.IsSet(); } }

    public bool UseEdgeOnlyForTexTypeChanges = false;
    public bool ForceHardEdge = false;

    public int Count()
    {
        int textureCount = 0;
        if (FloorVariations != null)
            textureCount += FloorVariations.Count;
        if (WallVariations != null)
            textureCount += WallVariations.Count;
        if (Edge.IsSet())
            textureCount++;
        if (DiagEdge.IsSet())
            textureCount++;
        return textureCount;
    }

    public bool MoveNext()
    {
        _Position++;
        return (_Position < Count());
    }
    public void Dispose() { Reset(); }
    public void Reset() { _Position = -1; }
    IEnumerator IEnumerable.GetEnumerator() { _Position = -1; return this; }
    public IEnumerator<Data> GetEnumerator() { _Position = -1; return this; }

    internal Vector2 GetFloorUVOffset(int variation)
    {
        variation = Mathf.Clamp(variation, 0, FloorVariations.Count - 1);
        if (variation < 0)
            return Vector2.zero;
        return FloorVariations[variation].UVOffset;
    }

    internal Vector2 GetWallUVOffset(int variation)
    {
        variation = Mathf.Clamp(variation, 0, WallVariations.Count - 1);
        if (variation < 0)
            return Vector2.zero;
        return WallVariations[variation].UVOffset;
    }

    internal Vector2 GetEdgeUVOffset()
    {
        return Edge.UVOffset;
    }
    internal Vector2 GetDiagEdgeUVOffset()
    {
        return DiagEdge.UVOffset;
    }

    public void OnEnable()
    {
        if (Edge == null)
            Edge = new Data();
        if (DiagEdge == null)
            DiagEdge = new Data();
        if (FloorVariations == null)
            FloorVariations = new List<Data>();
        if (WallVariations == null)
            WallVariations = new List<Data>();

        foreach (var dat in this)
            dat.UpgradeToArray();
    }

    public bool Has(Texture2D tex, HashSet<EAtlasType> types)
    {
        foreach (var vari in FloorVariations)
            vari.Has(tex, types);
        foreach (var vari in WallVariations)
            vari.Has(tex, types);

        Edge.Has(tex, types);
        DiagEdge.Has(tex, types);

        return !types.IsNullOrEmpty();
    }
}

[Serializable]
public class TransitionTextureData : TextureCollection.Data
{
    public string TexAName;
    public string TexBName;

    public int TexAIdx;
    public int TexBIdx;
}

[CreateAssetMenu(fileName = nameof(LevelTextureSetting), menuName = "LevelEditor_V1/LevelTextureSetting")]
public class LevelTextureSetting : ScriptableObject
{
    public List<TextureCollection> LevelTextureCollections = new List<TextureCollection>();
    public List<TransitionTextureData> Transitions = new List<TransitionTextureData>();

    //public List<TextureCollection> MaskTextureCollections = new List<TextureCollection>();
    public int AtlasDimension = 1024;
    public int HalfTileDimension = 16;

    [HideInInspector]
    public List<string> UsedTexturePixelHashes = new List<string>(8);

    [HideInInspector]
    public Material Material;

    [HideInInspector]
    public Texture2D[] Atlas = new Texture2D[(int)TextureCollection.EAtlasType.Length];
    public bool[] ActiveMaps = new bool[(int)TextureCollection.EAtlasType.Length];

    [HideInInspector]
    public float UVTileSize;
    [HideInInspector]
    public float UVInset;

    //[HideInInspector]
    //public Vector2 MaskUVFactor;
    //[HideInInspector]
    //public Vector2 MaskUVInset;
    //[HideInInspector]
    //public float MaskWrapValue = 1.0f;

    [HideInInspector]
    public int GeneratedWithHash = -1;

    public bool Contains(Texture2D tex)
    {
        HashSet<TextureCollection.EAtlasType> types = new HashSet<TextureCollection.EAtlasType>();
        foreach (var texCol in LevelTextureCollections)
        {
            // todo check normal texture and return wether diffuse or normal atlas have to be regenerated
            if (texCol.Has(tex, types))
                return true;
        }

        //foreach (var texCol in MaskTextureCollections)
        //{
        //    if (texCol.Textures.Contains(tex))
        //        return true;
        //}

        return false;
    }

    //    public int GetHash()
    //    {
    //        int uvHash = UVTexSize.GetHashCode();
    //        uvHash = uvHash * 20 + UVInset.GetHashCode();
    //        if (LevelTextureCollections != null)
    //        {
    //            for (int i = 0; i < LevelTextureCollections.Count; i++)
    //                uvHash = uvHash * 20 + LevelTextureCollections[i].UVOffset.GetHashCode();
    //            //for (int i = 0; i < MaskTextureCollections.Count; i++)
    //            //uvHash = uvHash * 23 + MaskTextureCollections[i].UVOffset.GetHashCode();
    //        }
    //        return uvHash;
    //    }
    //    void Verify()
    //    {
    //#if UNITY_EDITOR
    //        if (GeneratedWithHash != GetHash())
    //            Debug.LogWarning("This ground texture setting might need to regenerate the atlas!");
    //#endif
    //    }

    public TextureCollection GetCollection(int texType)
    {
        if (texType < 0 || texType >= LevelTextureCollections.Count)
            return null;

        return LevelTextureCollections[texType];
    }

    internal void GetFloorUVCoords(int texType, int variation, ref Vector2 uvTileStart, ref Vector2 uvTileEnd)
    {
        var collection = GetCollection(texType);
        if (collection == null)
            return;
        var offset = collection.GetFloorUVOffset(variation);

        uvTileStart = offset + (UVInset * Vector2.one);
        uvTileEnd = uvTileStart + (UVTileSize * Vector2.one);
    }
    internal void GetWallUVCoords(ushort texType, int variation, ref Vector2 uvTileStart, ref Vector2 uvTileEnd)
    {
        var collection = GetCollection(texType);
        if (collection == null)
            return;
        var offset = collection.GetWallUVOffset(variation);

        uvTileStart = offset + (UVInset * Vector2.one);
        uvTileEnd = uvTileStart + (UVTileSize * Vector2.one);
    }

    public static Vector2[] HardEdgeOffsetVector = new Vector2[]
    {
        Vector2.zero,
        new Vector2( 1.5f,  1.5f),  // 1
        new Vector2(-0.5f,  1.5f),  // 2
        new Vector2( 0.5f,  1.5f),  // 3
        new Vector2(-0.5f, -0.5f),  // 4
        Vector2.zero,               // 5
        new Vector2(-0.5f,  0.5f),  // 6
        new Vector2( 0.5f,  0.5f),  // 7
        new Vector2( 1.5f, -0.5f),  // 8
        new Vector2( 1.5f,  0.5f),  // 9
        Vector2.zero,               // 10
        new Vector2( 0.5f,  0.5f),  // 11
        new Vector2( 0.5f, -0.5f),  // 12
        new Vector2( 0.5f,  0.5f),  // 13
        new Vector2( 0.5f,  0.5f),  // 14
    };

    public static Vector2[] DiagEdgeOffsetVector = new Vector2[]
    {
        Vector2.zero,
        Vector2.one,        // 1
        new Vector2(0, 1),  // 2
        new Vector2(1, 0),  // 3
        Vector2.zero,       // 4
        Vector2.zero,       // 5
        Vector2.zero,       // 6
        new Vector2(1, 0),  // 7
        new Vector2(1, 0),  // 8
        Vector2.zero,       // 9
        Vector2.zero,       // 10
        Vector2.zero,       // 11
        Vector2.zero,       // 12
        new Vector2(0, 1),  // 13
        Vector2.one,        // 14
    };

    public static Vector2[] TransitionOffsetVector = new Vector2[]
    {
        Vector2.zero,
        new Vector2( 1.5f,  1.5f),  // 1
        new Vector2(-0.5f,  1.5f),  // 2
        new Vector2( 0.5f,  1.5f),  // 3
        new Vector2(-0.5f, -0.5f),  // 4
        Vector2.zero,               // 5
        new Vector2(-0.5f,  0.5f),  // 6
        new Vector2( 0.5f,  0.5f),  // 7
        new Vector2( 1.5f, -0.5f),  // 8
        new Vector2( 1.5f,  0.5f),  // 9
        Vector2.zero,               // 10
        new Vector2( 0.5f,  0.5f),  // 11
        new Vector2( 0.5f, -0.5f),  // 12
        new Vector2( 0.5f,  0.5f),  // 13
        new Vector2( 0.5f,  0.5f),  // 14
    };

    internal void GetTransitionUVCoords(TransitionTextureData transitionData, int config, int uvConfig, ref Vector2 uvTileStart, ref Vector2 uvTileEnd)
    {
        //uvConfig = ~uvConfig & 14;
        // ignore diagonal tiles for wall-making decisions
        //if (config == 1) uvConfig &= ~4;
        //else if (config == 2) uvConfig &= ~8;
        //else if (config == 4) uvConfig &= ~1;
        //else if (config == 8) uvConfig &= ~2;

        uvTileStart = transitionData.UVOffset + (UVInset * Vector2.one);
        uvTileEnd = uvTileStart + (UVTileSize * Vector2.one);

        Vector2 offsetVector = UVTileSize * TransitionOffsetVector[uvConfig];

        uvTileStart += offsetVector;
        uvTileEnd += offsetVector;
    }

    internal void GetEdgeUVCoords(ushort texType, int variation, int config, int uvConfig, bool hard, ref Vector2 uvTileStart, ref Vector2 uvTileEnd)
    {
        var collection = GetCollection(texType);
        if (collection == null)
            return;

        if (!collection.Edge.IsSet())
        {
            GetFloorUVCoords(texType, variation, ref uvTileStart, ref uvTileEnd);
            return;
        }
        var offset = collection.GetEdgeUVOffset();
        var diagOffset = offset;

        var hasDiagEdge = collection.HasDiagEdge;
        if (hasDiagEdge)
            diagOffset = collection.GetDiagEdgeUVOffset();

        uvTileStart = offset + (UVInset * Vector2.one);
        uvTileEnd = uvTileStart + (UVTileSize * Vector2.one);

        if (collection.Edge.Type == TextureCollection.ETexType.HardEdgeSq && hard)
        {
            // ignore diagonal tiles for wall-making decisions
            if (config != uvConfig)
            {
                if (config == 1) uvConfig &= ~4;
                else if (config == 2) uvConfig &= ~8;
                else if (config == 4) uvConfig &= ~1;
                else if (config == 8) uvConfig &= ~2;
            }

            Vector2 offsetVector = UVTileSize * HardEdgeOffsetVector[uvConfig];

            uvTileStart += offsetVector;
            uvTileEnd += offsetVector;
        }
        else if(hasDiagEdge)
        {
            // for diagonals only the diagonal tile is important
            if (config != uvConfig)
            {
                if (config == 1 && (uvConfig & 4) == 0) uvConfig = 1;
                else if (config == 2 && (uvConfig & 8) == 0) uvConfig = 2;
                else if (config == 4 && (uvConfig & 1) == 0) uvConfig = 4;
                else if (config == 8 && (uvConfig & 2) == 0) uvConfig = 8;
            }

            uvTileStart = diagOffset + (UVInset * Vector2.one);
            uvTileEnd = uvTileStart + (UVTileSize * Vector2.one);

            Vector2 offsetVector = UVTileSize * DiagEdgeOffsetVector[uvConfig];

            uvTileStart += offsetVector;
            uvTileEnd += offsetVector;
        }
    }

    internal bool IsForce(ushort texType)
    {
        var collection = GetCollection(texType);
        return collection?.ForceHardEdge ?? false;
    }

    internal bool TexTypeEdgeOnly(ushort texType)
    {
        var collection = GetCollection(texType);
        return collection?.UseEdgeOnlyForTexTypeChanges ?? false;
    }

    internal TextureCollection.ETexType EdgeType(ushort texType)
    {
        var collection = GetCollection(texType);
        if (collection == null)
            return TextureCollection.ETexType.Invalid;
        if (!collection.Edge.IsSet())
            return TextureCollection.ETexType.Invalid;
        if (collection.DiagEdge.IsSet())
            return TextureCollection.ETexType.DiagEdge;
        return collection.Edge.Type;
    }

    bool DefaultErrorCheck(bool allError, Texture2D texture, int tileDimension, TextureCollection.Data data)
    {
        bool error = allError;

        if (!error && (texture.width < tileDimension || texture.height < tileDimension)) error = true;
        if (!error && (!Mathf.IsPowerOfTwo(texture.width) || !Mathf.IsPowerOfTwo(texture.height))) error = true;
        if (!error && (texture.width % tileDimension != 0 || texture.width % tileDimension != 0)) error = true;

        if (error)
            data.Type = TextureCollection.ETexType.Invalid;
        return error;
    }

    public void UpdateCollectionDataTypes()
    {
        bool allError = false;
        var tileDimension = 2 * HalfTileDimension;
        if (tileDimension <= 0 || !Mathf.IsPowerOfTwo(HalfTileDimension))
            allError = true;

        for (int i=0; i< LevelTextureCollections.Count; ++i)
        {
            var colection = LevelTextureCollections[i];
            Texture2D texture;
            for (int j=0; j < colection.FloorVariations.Count; ++j)
            {
                texture = colection.FloorVariations[j].GetTexture(TextureCollection.EAtlasType.Default);
                if (DefaultErrorCheck(allError, texture, tileDimension, colection.FloorVariations[j]))
                    continue;
                Vector2Int dim = new Vector2Int(texture.width / tileDimension, texture.height / tileDimension);

                if (dim == TextureCollection.TileDimension(TextureCollection.ETexType.SimpleFloorTile))
                    colection.FloorVariations[j].Type = TextureCollection.ETexType.SimpleFloorTile;
                else
                    colection.FloorVariations[j].Type = TextureCollection.ETexType.Invalid;
            }
            for (int j = 0; j < colection.WallVariations.Count; ++j)
            {
                texture = colection.WallVariations[j].GetTexture(TextureCollection.EAtlasType.Default);
                if (DefaultErrorCheck(allError, texture, tileDimension, colection.WallVariations[j]))
                    continue;
                Vector2Int dim = new Vector2Int(texture.width / tileDimension, texture.height / tileDimension);
                if (dim == TextureCollection.TileDimension(TextureCollection.ETexType.SimpleWallTile))
                    colection.WallVariations[j].Type = TextureCollection.ETexType.SimpleWallTile;
                else if (dim == TextureCollection.TileDimension(TextureCollection.ETexType.WallTriplet))
                    colection.WallVariations[j].Type = TextureCollection.ETexType.WallTriplet;
                else if (dim == TextureCollection.TileDimension(TextureCollection.ETexType.WallTripletEdged))
                    colection.WallVariations[j].Type = TextureCollection.ETexType.WallTripletEdged;
                else
                    colection.FloorVariations[j].Type = TextureCollection.ETexType.Invalid;
            }
            texture = colection.Edge.GetTexture(TextureCollection.EAtlasType.Default);
            if (texture != null)
            {
                if (DefaultErrorCheck(allError, texture, tileDimension, colection.Edge))
                    continue;
                Vector2Int dim = new Vector2Int(texture.width / tileDimension, texture.height / tileDimension);
                if (dim == TextureCollection.TileDimension(TextureCollection.ETexType.SimpleEdgeTile))
                    colection.Edge.Type = TextureCollection.ETexType.SimpleEdgeTile;
                else if (dim == TextureCollection.TileDimension(TextureCollection.ETexType.HardEdgeSq))
                    colection.Edge.Type = TextureCollection.ETexType.HardEdgeSq;
                else
                    colection.Edge.Type = TextureCollection.ETexType.Invalid;
            }
            texture = colection.DiagEdge.GetTexture(TextureCollection.EAtlasType.Default);
            if (texture != null)
            {
                if (DefaultErrorCheck(allError, texture, tileDimension, colection.DiagEdge))
                    continue;
                Vector2Int dim = new Vector2Int(texture.width / tileDimension, texture.height / tileDimension);
                if (dim == TextureCollection.TileDimension(TextureCollection.ETexType.DiagEdge))
                    colection.DiagEdge.Type = TextureCollection.ETexType.DiagEdge;
                else
                    colection.Edge.Type = TextureCollection.ETexType.Invalid;
            }
        }

        for (int i = 0; i < Transitions.Count; ++i)
        {
            var texture = Transitions[i].GetTexture(TextureCollection.EAtlasType.Default);
            if (DefaultErrorCheck(allError, texture, tileDimension, Transitions[i]))
                continue;
            Vector2Int dim = new Vector2Int(texture.width / tileDimension, texture.height / tileDimension);
            if (dim == TextureCollection.TileDimension(TextureCollection.ETexType.Transition))
                Transitions[i].Type = TextureCollection.ETexType.Transition;
            else
                Transitions[i].Type = TextureCollection.ETexType.Invalid;
        }
    }

    public void UpdateTransitionNames()
    {
        foreach (var transition in Transitions)
        {
            transition.TexAName = LevelTextureCollections[transition.TexAIdx].Name;
            transition.TexBName = LevelTextureCollections[transition.TexBIdx].Name;
        }
    }

    public void UpdateTransitionIndecies()
    {
        foreach (var transition in Transitions)
        {
            transition.TexAIdx = LevelTextureCollections.FindIndex(a => string.Equals(a.Name, transition.TexAName, StringComparison.Ordinal));
            transition.TexBIdx = LevelTextureCollections.FindIndex(b => string.Equals(b.Name, transition.TexBName, StringComparison.Ordinal));
        }
    }

    public void OnEnable()
    {
        foreach (var dat in LevelTextureCollections)
            dat.OnEnable();
        foreach (var dat in Transitions)
            dat.UpgradeToArray();
    }
}
