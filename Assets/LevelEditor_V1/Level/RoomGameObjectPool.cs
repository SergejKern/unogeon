﻿using System.Collections.Generic;
using Core.Types;
using UnityEngine;

public class RoomGameObjectPool : Singleton<RoomGameObjectPool> {


    protected override void Init()
    {
        FreeObjects = new List<GameObject>();
        ObjectsInUse = new List<GameObject>();
    }

    public List<GameObject> FreeObjects;
    public List<GameObject> ObjectsInUse;

    public GameObject Get(string name)
    {
        GameObject obj=null;
        if (FreeObjects.Count > 1)
        {
            obj = FreeObjects[FreeObjects.Count - 1];
            FreeObjects.RemoveAt(FreeObjects.Count - 1);
        }
        else {
            obj = new GameObject(name);
        }
        ObjectsInUse.Add(obj);
        obj.name = name;
        obj.SetActive(true);

        return obj;
    }

    public void Return(GameObject g)
    {
        g.SetActive(false);
        g.name = "Returned Room";

        int idx = ObjectsInUse.FindIndex((o) => { return (o.Equals(g)); });
        if (idx != -1)
            ObjectsInUse.RemoveAt(idx);
        else
            Debug.LogWarning("Returning Room-GameObject that was not created by Pool!");

        FreeObjects.Add(g);
    }

    internal void ReturnAll()
    {
        FreeObjects.AddRange(ObjectsInUse);
        ObjectsInUse.Clear();
    }
}
