using System.Collections.Generic;
using UnityEngine;

using Core;
using Core.Interface;
using Core.Types;
using Core.Unity.Extensions;
using static RoomMeshOperations;
using static RoomOperations;

public class Room : ScriptableObject, IProvider<Bounds>
{
    public enum ExitRestrictionType
    {
        None = 0,
        AlwaysBlocked,
        NeedItemX,
        NeedItemY,
    }

    [System.Serializable]
    public class ExitInfo
    {
        public Room ConnectedTo;
        public Cardinals ExitDirection;
        public ExitRestrictionType Restriction;
        public int fromIdx;
        public int toIdx;
        public int level;
    }

    [System.Serializable]
    public class Level
    {
        public Vector2Int Position = Vector2Int.zero;
        public Vector2Int Size;
        public Vector2Int Extends { get { return Position + Size; } }

        public Vector2Int TileDim { get { return Size + Vector2Int.one; } }
        public ushort[] Tiles;
    }

    public string Name;
    public List<Level> Levels = new List<Level>();
    public LevelTextureSetting Setting;

    public Vector3Int Size;
    public Vector2Int TileDim { get { return Size.Vector2Int() + Vector2Int.one; } }
    public int MaxLevel { get { return Size.y - 1; } }

    public List<ExitInfo> ExitRestrictions = new List<ExitInfo>();

    #region instanced Rooms
    [System.NonSerialized]
    public List<ExitInfo> OpenExits = new List<ExitInfo>();
    [System.NonSerialized]
    public List<ExitInfo> ClosedExits = new List<ExitInfo>();

    public Vector3Int Position { get; private set; }

    public Bounds Bounds { get; private set; } = default(Bounds);

    public RoomComponent RoomComponent { get; private set; } = null;

    public Bounds LevelBounds(int idx)
    {
        if (Levels.Count <= idx || idx < 0)
            return new Bounds();
        var lvl = Levels[idx];
        return new Bounds(Position.Vector3() + lvl.Position.Vector3(idx) + lvl.Size.Vector3(1.0f) / 2, lvl.Size.Vector3(1.0f));
    }
    public void SetRoomPositioning(Vector3 position)
    {
        Position = position.Vector3Int();
        Bounds = new Bounds(Position.Vector3() + (Size.Vector3() / 2.0f), Size.Vector3());
    }
    public void ShowRoom(OperationData dat)
    {
        if (RoomComponent != null)
        {
            RoomComponent.gameObject.SetActive(true);
            return;
        }
        RoomComponent = CreateObjectForRoom(this, Name);

        dat.Room = this;
        GenerateMesh(dat);
    }

    internal void HideRoom()
    {
        if (RoomComponent == null)
            return;

        RoomComponent.Room = null;
        RoomGameObjectPool.I.Return(RoomComponent.gameObject);
        // TODO: remove this but reset it properly, or there is this bug that collision object is missing
        RoomComponent.DestroyEx();

        RoomComponent = null;
    }
    #endregion

    public void Get(out Bounds provided) => provided = Bounds;
}

