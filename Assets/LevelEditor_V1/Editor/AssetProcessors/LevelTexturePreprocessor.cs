using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

public class LevelTexturePreprocessor : AssetPostprocessor
{
    static List<string> _RecentlyImportedTextures = new List<string>();

    public static List<Texture2D> PullRecentlyImportedTextures()
    {
        var texList = new List<Texture2D>();
        foreach (var newPath in _RecentlyImportedTextures)
        {
            var tex = AssetDatabase.LoadAssetAtPath<Texture2D>(newPath);
            texList.Add(tex);
        }
        _RecentlyImportedTextures.Clear();
        return texList;
    }

    public static string GetHash(Texture2D texture)
    {
        byte[] bytes = texture.GetRawTextureData();
        using (var md5 = System.Security.Cryptography.MD5.Create())
        {
            var hash = md5.ComputeHash(bytes);
            var hashStr = System.BitConverter.ToString(hash);
            return hashStr;
        }
    }

    bool IsInLevelTextureFolder()
    {
        var dirName = System.IO.Path.GetDirectoryName(assetPath);
        return string.Equals(dirName, "Assets/Art/LevelTextures", System.StringComparison.Ordinal);
    }

    void OnPreprocessTexture()
    {
        if (!IsInLevelTextureFolder())
            return;

        TextureImporter textureImporter = (TextureImporter)assetImporter;
        textureImporter.isReadable = true;
        textureImporter.textureCompression = TextureImporterCompression.Uncompressed;
    }

    void OnPostprocessTexture(Texture2D texture)
    {
        if (!IsInLevelTextureFolder())
            return;

        if (!_RecentlyImportedTextures.Contains(assetPath))
            _RecentlyImportedTextures.Add(assetPath);
    }
}
