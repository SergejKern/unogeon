using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;

[CustomEditor(typeof(LevelTextureSetting))]
public class LevelTextureSettingsEditor : Editor
{
    new LevelTextureSetting target => base.target as LevelTextureSetting;

    //bool _HashChanged;
    TextureCollection.EAtlasType CurrentView = TextureCollection.EAtlasType.Default;

    string GroundTextureAtlasPath(TextureCollection.EAtlasType type) => $"Assets/Art/LevelTextureAtlas/{target.name}{type}Atlas.png";

    string MaterialPath => "Assets/Art/LevelMaterials/" + target.name + ".mat";

    string Path
    {
        get
        {
            //chop .asset
            var thisPath = AssetDatabase.GetAssetPath(target);
            return thisPath.Substring(0, thisPath.Length - 6);
        }
    }

    string ValidateTextureCollection(string collectionName, List<TextureCollection> textureCollections)
    {
        var setting = target;
        if (textureCollections == null || textureCollections.Count == 0)
            return "No " + collectionName + " texture collections defined";

        var tileDimension = 2 * setting.HalfTileDimension;
        if (tileDimension <= 0 || !Mathf.IsPowerOfTwo(setting.HalfTileDimension))
            return "Invalid tiledimension in "+ setting.name;
        if (setting.AtlasDimension <= tileDimension || !Mathf.IsPowerOfTwo(setting.AtlasDimension))
            return "Invalid AtlasDimension in " + setting.name;

        foreach (var textureCollection in textureCollections)
        {
            if (textureCollection == null)
                return "There's a null texture " + collectionName + " collection.";

            if (textureCollection.Count() == 0)
                return "No textures in " + collectionName + " collection '" + textureCollection.Name + "'";

            foreach (TextureCollection.Data dat in textureCollection)
            {
                // todo normal textures
                var texture = dat.GetTexture();
                if (texture == null)
                    return "There's a null texture in " + collectionName + " collection '" + textureCollection.Name + "'";

                if (texture.width < tileDimension || texture.height < tileDimension)
                    return "The texture "+ texture.name + " in " + collectionName + " collection '" + textureCollection.Name + "' is too small";

                if (!Mathf.IsPowerOfTwo(texture.width) || !Mathf.IsPowerOfTwo(texture.height))
                    return "The texture " + texture.name + " in " + collectionName + " collection '" + textureCollection.Name + "' is not power of two!";

                if (texture.width % tileDimension != 0 || texture.width % tileDimension != 0)
                    return "The texture " + texture.name + " in " + collectionName + " collection '" + textureCollection.Name + "' is not multiple of the tileDimension "+ tileDimension + "!";

                if(dat.Type == TextureCollection.ETexType.Invalid)
                    return "The texture " + texture.name + " in " + collectionName + " collection '" + textureCollection.Name + "' has invalid TexType (wrong dimensions)!";

                string path = AssetDatabase.GetAssetPath(texture);
                TextureImporter importer = AssetImporter.GetAtPath(path) as TextureImporter;
                if (!importer.isReadable || importer.textureCompression != TextureImporterCompression.Uncompressed)
                    return "Texture " + texture.name + " must be readable and uncompressed";
                try
                {
                    texture.GetPixel(0, 0);
                }
                catch (UnityException e)
                {
                    return e.Message;
                }
            }
        }

        return null;
    }

    string Validate()
    {
        var warning = ValidateTextureCollection("Ground", target.LevelTextureCollections);
        //if (warning != null)
        return warning;

        //warning = ValidateTextureCollection("Mask", target.MaskTextureCollections);
        //if (warning != null)
        //    return warning;

        //return null;
    }

    void UpdateAtlasIfNeeded()
    {
        var setting = target;
        var recentImported = LevelTexturePreprocessor.PullRecentlyImportedTextures();

        bool updateNeeded = false;
        foreach (var newTex in recentImported)
        {
            if (!setting.Contains(newTex))
                continue;

            var newTexHash = LevelTexturePreprocessor.GetHash(newTex);
            if (!setting.UsedTexturePixelHashes.Contains(newTexHash))
            {
                updateNeeded = true;
                break;
            }
        }

        if (updateNeeded)
            GenerateAtlas();
    }

    public override void OnInspectorGUI()
    {
        string warnings = Validate();
        bool hasWarnings = !string.IsNullOrEmpty(warnings);
        if (hasWarnings)
            EditorGUILayout.HelpBox(warnings, MessageType.Warning);

        // todo: validation checks
        //if (_HashChanged)
        //    EditorGUILayout.HelpBox("The Settings have changed. Please check all rooms if they look good (implement feature to list all rooms using atlas)", MessageType.Info);

        using (var disabledGroup = new EditorGUI.DisabledScope(hasWarnings))
        {
            if (GUILayout.Button("Generate Atlas", GUILayout.Height(35)))
            {
                GenerateAtlas();
            }
        }

        serializedObject.Update();

        LevelTextureSettingGUI();
        TextureCollectionUI("Texture Types", "LevelTextureCollections", ref target.LevelTextureCollections);
        //TextureCollectionUI("Mask Texture Collections", "MaskTextureCollections", ref target.MaskTextureCollections);
        TransitionsUI("Transitions", ref target.Transitions);


        UpdateAtlasIfNeeded();
    }

    void LevelTextureSettingGUI()
    {
        var settings = target;
        settings.AtlasDimension = EditorGUILayout.IntField("AtlasDimension", settings.AtlasDimension);
        settings.HalfTileDimension = EditorGUILayout.IntField("HalfTileDimension", settings.HalfTileDimension);

        CurrentView = (TextureCollection.EAtlasType) EditorGUILayout.EnumPopup("Current View: ",CurrentView);
        settings.ActiveMaps[(int)CurrentView] = EditorGUILayout.Toggle("Active", settings.ActiveMaps[(int)CurrentView]);
    }

    void SingleTexDataGUI(TextureCollection.Data dat)
    {
        Texture2D currentTex = null;
        float w = 50.0f, h = 50.0f;
        if (dat.IsSet())
        {
            currentTex = dat.GetTexture(CurrentView);
            var dim = dat.TileDimension();
            w = 50.0f * dim.x;
            h = 50.0f * dim.y;
        }
        var rect = EditorGUILayout.GetControlRect(GUILayout.Width(w), GUILayout.Height(h + 10.0f));
        var newTex = EditorGUI.ObjectField(new Rect(rect.x, rect.y + 10.0f, w, h), currentTex, typeof(Texture2D), false) as Texture2D;
        if (currentTex != newTex)
        {
            dat.SetTexture(CurrentView, newTex);
            target.UpdateCollectionDataTypes();
        }
        if (GUI.Button(new Rect(rect.x, rect.y, w, 10.0f), "X"))
        {
            dat.SetTexture(CurrentView, null);
        }
    }

    void TexDataListGUI(List<TextureCollection.Data> datas, string label)
    {
        using (var hs = new GUILayout.HorizontalScope())
        {
            EditorGUILayout.LabelField(label, GUILayout.Width(50.0f));

            int removeIdx = -1;
            for (int j = 0; j < datas.Count; ++j)
            {
                var rect = EditorGUILayout.GetControlRect(GUILayout.Width(50.0f), GUILayout.Height(60.0f));
                var tex = datas[j].GetTexture(CurrentView);
                var newTex = EditorGUI.ObjectField(new Rect(rect.x, rect.y + 10.0f, 50.0f, 50.0f), tex, typeof(Texture2D), false) as Texture2D;
                if (newTex != tex)
                {
                    datas[j].SetTexture(CurrentView, newTex);
                    target.UpdateCollectionDataTypes();
                }
                if (CurrentView == TextureCollection.EAtlasType.Default && GUI.Button(new Rect(rect.x, rect.y, 50.0f, 10.0f), "X"))
                    removeIdx = j;
            }
            if (GUILayout.Button("+", GUILayout.Width(20.0f)))
                datas.Add(new TextureCollection.Data());

            if (removeIdx != -1)
            {
                datas.RemoveAt(removeIdx);
            }
        }
    }

    void UpdateTexCollectionNames()
    {
        var setting = target;
        TextureCollectionNames = new string[setting.LevelTextureCollections.Count];
        for (int i = 0; i< setting.LevelTextureCollections.Count; ++i)
        {
            TextureCollectionNames[i] = setting.LevelTextureCollections[i].Name;
        }
    }

    void UpdateTransitionNames()
    {
        var setting = target;
        UpdateTexCollectionNames();
        setting.UpdateTransitionNames();
    }

    void UpdateTransitionIndecies()
    {
        var setting = target;
        UpdateTexCollectionNames();
        setting.UpdateTransitionIndecies();
    }

    string[] TextureCollectionNames;

    private void TransitionsUI(string label, ref List<TransitionTextureData> transitions)
    {
        if (TextureCollectionNames == null)
            UpdateTransitionNames();

        EditorGUILayout.LabelField(label);

        int erasedTransition = -1;
        for (int i = 0; i < transitions.Count; ++i)
        {
            using (var hs = new GUILayout.HorizontalScope())
            {
                if (GUILayout.Button("X", GUILayout.Width(20.0f)))
                    erasedTransition = i;

                using (var check = new EditorGUI.ChangeCheckScope())
                {
                    transitions[i].TexAIdx = EditorGUILayout.Popup(transitions[i].TexAIdx, TextureCollectionNames);
                    transitions[i].TexBIdx = EditorGUILayout.Popup(transitions[i].TexBIdx, TextureCollectionNames);
                    if (check.changed)
                    {
                        transitions[i].TexAName = TextureCollectionNames[transitions[i].TexAIdx];
                        transitions[i].TexBName = TextureCollectionNames[transitions[i].TexBIdx];
                    }
                }
                
                Texture2D currentTex = null;
                // float w = 20.0f, h = 20.0f;
                if (transitions[i].IsSet())
                    currentTex = transitions[i].GetTexture();

                if (CurrentView == TextureCollection.EAtlasType.Default &&
                    GUILayout.Button("X"))
                {
                    transitions[i].SetTexture(TextureCollection.EAtlasType.Default, null);
                }
                var newTex = EditorGUILayout.ObjectField(currentTex, typeof(Texture2D), false) as Texture2D;
                if (currentTex != newTex)
                {
                    transitions[i].SetTexture(CurrentView, newTex);
                    target.UpdateCollectionDataTypes();
                }

            }
        }
        if (GUILayout.Button("Add Transition"))
        {
            var data = new TransitionTextureData();
            data.TexAName = TextureCollectionNames[0];
            data.TexBName = TextureCollectionNames[0];
            transitions.Add(data);
        }
        if (erasedTransition != -1)
        {
            transitions.RemoveAt(erasedTransition);
        }
    }

    void TextureCollectionUI(string label, string propertyName, ref List<TextureCollection> collection)
    {
        EditorGUILayout.LabelField(label);
        var groundTexProp = serializedObject.FindProperty(propertyName);

        int erasedTexCollection = -1;
        for (int i = 0; i < collection.Count; ++i)
        {
            if (groundTexProp.arraySize <= i)
                continue;
            var propEl = groundTexProp.GetArrayElementAtIndex(i);

            using (var hs = new GUILayout.HorizontalScope())
            {
                if (GUILayout.Button("X", GUILayout.Width(20.0f)))
                {
                    UpdateTransitionIndecies();
                    erasedTexCollection = i;
                }
                GUILayout.Space(10.0f);
                if (propEl != null)
                    propEl.isExpanded = EditorGUILayout.Foldout(propEl.isExpanded, collection[i].Name, true);

                using (var check = new EditorGUI.ChangeCheckScope())
                {
                    collection[i].Name = EditorGUILayout.TextField(collection[i].Name);
                    if (check.changed)
                        UpdateTransitionNames();
                }
            }
            if (propEl == null || !propEl.isExpanded)
                continue;

            bool forceHardEditDisabled = collection[i].HasDiagEdge || 
                (!collection[i].HasDiagEdge && collection[i].Edge.Type == TextureCollection.ETexType.HardEdgeSq);

            using (var disabledScope = new EditorGUI.DisabledScope(forceHardEditDisabled))
            {
                collection[i].ForceHardEdge = EditorGUILayout.Toggle("Force hard edge", collection[i].ForceHardEdge);
            }

            if (collection[i].Edge.IsSet())
                collection[i].UseEdgeOnlyForTexTypeChanges = EditorGUILayout.Toggle("Use Edge Only For TexType changes", collection[i].UseEdgeOnlyForTexTypeChanges);

            using (var hs = new GUILayout.HorizontalScope())
            {
                EditorGUILayout.LabelField("Edge:", GUILayout.Width(50.0f));
                using (var vs = new GUILayout.VerticalScope())
                {
                    using (var cs = new EditorGUI.ChangeCheckScope())
                    {
                        SingleTexDataGUI(collection[i].Edge);
                        if (cs.changed)
                        {
                            if (collection[i].Edge.Type == TextureCollection.ETexType.HardEdgeSq)
                            {
                                if (!collection[i].HasDiagEdge)
                                    collection[i].ForceHardEdge = true;
                            }
                            else if (collection[i].HasDiagEdge)
                            {
                                collection[i].DiagEdge.SetTexture(TextureCollection.EAtlasType.Default, null);
                            }
                        }
                    }

                    if (collection[i].Edge.Type == TextureCollection.ETexType.HardEdgeSq)
                    {
                        using (var cs = new EditorGUI.ChangeCheckScope())
                        {
                            SingleTexDataGUI(collection[i].DiagEdge);
                            if (cs.changed)
                            {
                                if (collection[i].HasDiagEdge)
                                    collection[i].ForceHardEdge = false;
                                else if (collection[i].Edge.Type == TextureCollection.ETexType.HardEdgeSq)
                                    collection[i].ForceHardEdge = true;
                            }
                        }
                    }
                }
                using (var vs = new GUILayout.VerticalScope())
                {
                    TexDataListGUI(collection[i].FloorVariations, "Floor:");
                    TexDataListGUI(collection[i].WallVariations, "Walls:");
                }
            }
            GUILayout.Space(20.0f);
        }

        if (GUILayout.Button("Add Texture Collection"))
        {
            collection.Add(new TextureCollection());
        }
        if (erasedTexCollection != -1)
        {
            collection.RemoveAt(erasedTexCollection);
        }
    }

    #region Atlas Generation
    void GenerateAtlas()
    {
        var setting = target;

        Debug.Assert(Validate() == null);

        EditorUtility.SetDirty(setting);

        //AddMaskToAtlas(atlas, GroundTextureAtlasPath, target.MaskTextureCollections, out target.MaskUVInset, out target.MaskUVFactor, out target.MaskWrapValue);
        //var newAtlas =
        setting.Material = LoadOrCreateMaterial(MaterialPath);

        for (var i = 0; i < (int)TextureCollection.EAtlasType.Length; i++)
        {
            if (!setting.ActiveMaps[i])
                continue;
            var atType = (TextureCollection.EAtlasType)i;
            var atlas = _GenerateAtlas(atType);
            EditorUtility.DisplayProgressBar("Generating Atlas", "Saving Atlas File", 1f);

            ApplyAndWriteTextureToPath(atlas, GroundTextureAtlasPath(atType));
            DestroyImmediate(atlas);
            setting.Atlas[i] = AssetDatabase.LoadMainAssetAtPath(GroundTextureAtlasPath(atType)) as Texture2D;
            UpdateMaterial(setting.Material, setting.Atlas[i], TextureCollection.kTexNames[i]);
        }

        EditorUtility.ClearProgressBar();

        // todo validation stuff
        //int newHash = setting.GetHash();
        //if (setting.GeneratedWithHash != newHash)
        //    _HashChanged = true;
        //setting.GeneratedWithHash = newHash;

        setting.UsedTexturePixelHashes.Clear();
        GetTextureHashes(setting.LevelTextureCollections, setting.UsedTexturePixelHashes);

        //GetTextureHashes(target.MaskTextureCollections, target.UsedTexturePixelHashes);

        EditorGUIUtility.PingObject(setting.Material);
    }

    static void GetTextureHashes(List<TextureCollection> collections, List<string> result)
    {
        foreach (var collection in collections)
        {
            foreach (TextureCollection.Data dat in collection)
            {
                var tex = dat.GetTexture();
                var hash = LevelTexturePreprocessor.GetHash(tex);
                result.Add(hash);
            }
        }
    }

    static Material LoadOrCreateMaterial(string path)
    {
        var mat = (Material)AssetDatabase.LoadAssetAtPath(path, typeof(Material));
        if (mat != null)
            return mat;

        //Shader shader = Shader.Find("Editor/DungeonEditor");
        mat = new Material(Shader.Find("Standard"));
        AssetDatabase.CreateAsset(mat, path);

        mat = (Material)AssetDatabase.LoadAssetAtPath(path, typeof(Material));
        return mat;
    }

    static void UpdateMaterial(Material mat, Texture2D groundTex, string texName)
    {
        var currentGroundTex = mat.GetTexture(texName);

        if (currentGroundTex == groundTex)
            return;

        EditorUtility.SetDirty(mat);
        mat.SetTexture(texName, groundTex);
        AssetDatabase.SaveAssets();
    }

    static Texture2D ApplyAndWriteTextureToPath(Texture2D texture, string path)
    {
        texture.Apply();
        var bytes = texture.EncodeToPNG();

        try
        {
            File.WriteAllBytes(path, bytes);
        }
        catch (IOException e)
        {
            Debug.LogError($"IO exception while writing atlas texture: {e}");
        }

        AssetDatabase.Refresh();
        AssetDatabase.ImportAsset(path);
        return (Texture2D)AssetDatabase.LoadAssetAtPath(path, typeof(Texture2D));
    }

    const int repeatedPixels = 8;
    static void CopyQuadrantToAtlas(Texture2D texture, Texture2D atlas,
        int atlasX, int atlasY, int quadX, int quadY, int copyDirectionX, int copyDirectionY)
    {
        int xOff = atlasX + repeatedPixels;
        int yOff = atlasY + repeatedPixels;
        var halfWidth = texture.width / 2;
        var halfHeight = texture.height / 2;
        int xRead = halfWidth * quadX;
        int yRead = halfHeight * quadY;

        Color[] pixels = texture.GetPixels(xRead, yRead, halfWidth, halfHeight);

        int repeatedXRead = xRead;
        int repeatedYRead = yRead;

        int repeatedXWrite = xOff + xRead + (texture.width * copyDirectionX);
        int repeatedYWrite = yOff + yRead + (texture.height * copyDirectionY);

        if (copyDirectionX == -1)
        {
            repeatedXWrite += (halfWidth - repeatedPixels);
            repeatedXRead += (halfWidth - repeatedPixels);
        }
        if (copyDirectionY == -1)
        {
            repeatedYWrite += (halfHeight - repeatedPixels);
            repeatedYRead += (halfHeight - repeatedPixels);
        }

        Color[] pixelsRepeatedX = texture.GetPixels(repeatedXRead, yRead, repeatedPixels, halfWidth);
        Color[] pixelsRepeatedY = texture.GetPixels(xRead, repeatedYRead, halfHeight, repeatedPixels);
        Color[] pixelsRepeatedXY = texture.GetPixels(repeatedXRead, repeatedYRead, repeatedPixels, repeatedPixels);

        atlas.SetPixels(xOff + xRead, yOff + yRead, halfWidth, halfHeight, pixels);
        atlas.SetPixels(repeatedXWrite, yOff + yRead, repeatedPixels, halfHeight, pixelsRepeatedX);
        atlas.SetPixels(xOff + xRead, repeatedYWrite, halfWidth, repeatedPixels, pixelsRepeatedY);
        atlas.SetPixels(repeatedXWrite, repeatedYWrite, repeatedPixels, repeatedPixels, pixelsRepeatedXY);
    }

    static void CopyTextureToAtlas(Texture2D texture, Texture2D atlas, int x, int y)
    {
        CopyQuadrantToAtlas(texture, atlas, x, y, 0, 0, 1, 1);
        CopyQuadrantToAtlas(texture, atlas, x, y, 1, 0, -1, 1);
        CopyQuadrantToAtlas(texture, atlas, x, y, 0, 1, 1, -1);
        CopyQuadrantToAtlas(texture, atlas, x, y, 1, 1, -1, -1);
    }

    Texture2D _GenerateAtlas(TextureCollection.EAtlasType atlasType)
    {
        var setting = target;

        var textureCollections = setting.LevelTextureCollections;
        int tileWidthAndHeight = 2 * setting.HalfTileDimension;
        int atlasHeight = setting.AtlasDimension, 
            atlasWidth = setting.AtlasDimension;

        //int widthAndHeight = texWidthAndHeight + (repeatedPixels * 2);
        Texture2D outputTexture = new Texture2D(setting.AtlasDimension, setting.AtlasDimension, TextureFormat.ARGB32, false);

        setting.UVInset = (float)repeatedPixels / setting.AtlasDimension;
        setting.UVTileSize = (float)tileWidthAndHeight / atlasWidth;

        List<TextureCollection.Data> allTexData = new List<TextureCollection.Data>();
        foreach (var texCollection in textureCollections)
        {
            allTexData.AddRange(texCollection.FloorVariations);
            allTexData.AddRange(texCollection.WallVariations);
            if (texCollection.Edge.IsSet())
                allTexData.Add(texCollection.Edge);
            if (texCollection.DiagEdge.IsSet())
                allTexData.Add(texCollection.DiagEdge);
        }
        foreach(var trans in setting.Transitions)
            allTexData.Add(trans);

        allTexData.Sort(TextureCollection.SortBiggestDimensionFirst);

        int x = 0,
            y = 0,
            //currentRowYStart = 0,
            currentRowYEnd = -1;
        //blockXStart = 0,
        //blockXEnd = -1,
        //blockX = 0,
        //blockY = 0;

        Vector2Int currentDim = Vector2Int.zero;

        int progressIndex = 0;
        int initialCount = allTexData.Count;
        while (allTexData.Count > 0)
        {
            EditorUtility.DisplayProgressBar("Generating Atlas", $"{progressIndex}/{initialCount}", (float)(progressIndex++) / initialCount);
            var current = allTexData[0];

            if (currentRowYEnd != -1 && currentDim.y != current.TileDimension().y)
            {
                x = 0;
                y += (currentDim.y * (2 * repeatedPixels + tileWidthAndHeight));
                currentRowYEnd = -1;
            }
            if (currentRowYEnd == -1)
            {
                currentDim = current.TileDimension();
                currentRowYEnd = y + (currentDim.y * (2 * repeatedPixels + tileWidthAndHeight));

                if (currentRowYEnd > atlasHeight)
                {
                    Debug.LogError("Atlas cannot be created because you run out of space!");
                    break;
                }
            }
            currentDim.x = current.TileDimension().x;

            // copy texture
            var tex = current.GetTexture(atlasType);
            if (tex != null)
                CopyTextureToAtlas(tex, outputTexture, x, y);
            var uVOffset = new Vector2((float)x / atlasWidth, (float)y / atlasHeight);
            if (atlasType == TextureCollection.EAtlasType.Default)
                current.UVOffset = uVOffset;

            x += currentDim.x * (2 * repeatedPixels + tileWidthAndHeight);

            var nextXEnd = x + (currentDim.x * (2 * repeatedPixels + tileWidthAndHeight));
            if (nextXEnd > atlasWidth)
            {
                x = 0;
                y = currentRowYEnd;
                currentRowYEnd = -1;
            }
            allTexData.RemoveAt(0);
        }

        EditorUtility.ClearProgressBar();
        return outputTexture;
    }


    //static void CopyQuadrantToAtlas(Texture2D texture, Texture2D atlas, int halfTexWidthAndHeight,
    //int quadX, int quadY, int toX, int toY)
    //{
    //    Color[] pixels = texture.GetPixels(quadX, quadY, halfTexWidthAndHeight + repeatedPixels, halfTexWidthAndHeight + repeatedPixels);
    //    atlas.SetPixels(toX, toY, halfTexWidthAndHeight + repeatedPixels, halfTexWidthAndHeight + repeatedPixels, pixels);
    //}
    //static void CopyTextureToAtlas(Texture2D texture, Texture2D atlas, int halfTexWidthAndHeight, int xOff, int yOff)
    //{
    //    CopyQuadrantToAtlas(texture, atlas, halfTexWidthAndHeight, 0, 0, xOff + halfTexWidthAndHeight, yOff + halfTexWidthAndHeight);
    //    CopyQuadrantToAtlas(texture, atlas, halfTexWidthAndHeight, halfTexWidthAndHeight - repeatedPixels, 0, xOff - repeatedPixels, yOff + halfTexWidthAndHeight);
    //    CopyQuadrantToAtlas(texture, atlas, halfTexWidthAndHeight, 0, halfTexWidthAndHeight - repeatedPixels, xOff + halfTexWidthAndHeight, yOff - repeatedPixels);
    //    CopyQuadrantToAtlas(texture, atlas, halfTexWidthAndHeight, halfTexWidthAndHeight - repeatedPixels, halfTexWidthAndHeight - repeatedPixels, xOff - repeatedPixels, yOff - repeatedPixels);
    //}
    //static Texture2D GenerateAtlas(List<TextureCollection> textureCollections, out Vector2 uvInset, out Vector2 uvFactor, out float wrapValue)
    //{
    //    int texWidthAndHeight = -1;
    //    int maxVariations = 0;
    //    int texCount = 0;
    //    foreach (var texCollection in textureCollections)
    //    {
    //        maxVariations = Mathf.Max(maxVariations, texCollection.Count());
    //        foreach (TextureCollection.Data dat in texCollection)
    //        {
    //            if (texWidthAndHeight == -1)
    //                texWidthAndHeight = dat.DiffuseTex.width;
    //            texCount++;
    //        }
    //    }
    //    int halfTexWidthAndHeight = texWidthAndHeight / 2;
    //    int widthAndHeight = texWidthAndHeight + (repeatedPixels * 2);
    //    int horTexCount = Mathf.CeilToInt(Mathf.Sqrt(texCount));
    //    int atlasWidth = 2;
    //    while (atlasWidth < horTexCount * widthAndHeight)
    //    {
    //        atlasWidth *= 2;
    //        while ((horTexCount + 1) * widthAndHeight <= atlasWidth)
    //        {
    //            horTexCount++;
    //        }
    //    }
    //    wrapValue = (float)(horTexCount * widthAndHeight) / atlasWidth;
    //    if (wrapValue < 0.1f)
    //    {
    //        atlasWidth = horTexCount * widthAndHeight;
    //        wrapValue = 1.0f;
    //        Debug.LogWarning("Failed to make TextureAtlas power of 2!");
    //    }
    //    int atlasHeight = atlasWidth;
    //    Texture2D outputTexture = new Texture2D(atlasWidth, atlasHeight, TextureFormat.ARGB32, false);
    //    uvInset = new Vector2((float)repeatedPixels / atlasWidth, (float)repeatedPixels / atlasHeight);
    //    uvFactor = new Vector2((float)widthAndHeight / atlasWidth, (float)widthAndHeight / atlasHeight);
    //    int x = repeatedPixels;
    //    int y = repeatedPixels;
    //    int progressIndex = 0;
    //    foreach (var texCollection in textureCollections)
    //    {
    //        EditorUtility.DisplayProgressBar("Generating Atlas", texCollection.Name, (float)(progressIndex++) / textureCollections.Count);
    //        texCollection.UVOffset = new Vector2((float)(x - repeatedPixels) / atlasWidth, (float)(y - repeatedPixels) / atlasHeight);
    //        foreach (TextureCollection.Data dat in texCollection)
    //        {
    //            var tex = dat.DiffuseTex;
    //            CopyTextureToAtlas(tex, outputTexture, halfTexWidthAndHeight, x, y);
    //            x += widthAndHeight;
    //            if (x + widthAndHeight > atlasWidth)
    //            {
    //                x = repeatedPixels;
    //                y += widthAndHeight;
    //            }
    //        }
    //    }
    //    EditorUtility.ClearProgressBar();
    //    return outputTexture;
    //}

    //static Texture2D AddMaskToAtlas(Texture2D atlas, string outputPath, List<TextureCollection> textureCollections, out Vector2 uvInset, out Vector2 uvFactor, out float wrapValue)
    //{
    //    int texWidthAndHeight = -1;
    //    int maxVariations = 0;
    //    int texCount = 0;
    //    foreach (var texCollection in textureCollections)
    //    {
    //        maxVariations = Mathf.Max(maxVariations, texCollection.Textures.Count);
    //        for (int i = 0; i < texCollection.Textures.Count; i++)
    //        {
    //            if (texWidthAndHeight == -1)
    //                texWidthAndHeight = texCollection.Textures[i].width;
    //            texCount++;
    //        }
    //    }
    //    //int halfTexWidthAndHeight = texWidthAndHeight / 2;
    //    int widthAndHeight = texWidthAndHeight;
    //    int horTexCount = Mathf.FloorToInt((float)atlas.width / texWidthAndHeight);
    //    int atlasWidth = atlas.width;
    //    wrapValue = (float)(horTexCount * widthAndHeight) / atlasWidth;
    //    int atlasHeight = atlasWidth;
    //    uvInset = Vector3.zero;
    //    uvFactor = new Vector2((float)widthAndHeight / (float)atlasWidth, (float)widthAndHeight / (float)atlasHeight);
    //    if (texCount > (horTexCount * horTexCount))
    //    {
    //        Debug.LogError("GroundTextureSettingsEditor.AddMaskToAtlas(): More Mask then Ground Space needed!");
    //        return null;
    //    }
    //    int x = 0;
    //    int y = 0;
    //    int progressIndex = 0;
    //    foreach (var texCollection in textureCollections)
    //    {
    //        EditorUtility.DisplayProgressBar("Adding Mask to Atlas", texCollection.Name, (float)(progressIndex++) / textureCollections.Count);
    //        texCollection.UVOffset = new Vector2((float)x / (float)atlasWidth, (float)y / (float)atlasHeight);
    //        for (int j = 0; j < texCollection.Textures.Count; j++)
    //        {
    //            Color[] maskPixels = texCollection.Textures[j].GetPixels();
    //            Color[] originalPixels = atlas.GetPixels(x, y, widthAndHeight, widthAndHeight);
    //            if (maskPixels.Length != originalPixels.Length)
    //            {
    //                Debug.LogError("GroundTextureSettingsEditor.AddMaskToAtlas(): Unexpected array length!");
    //                return null;
    //            }
    //            for (int i = 0; i < originalPixels.Length; ++i)
    //            {
    //                originalPixels[i].a = maskPixels[i].g;
    //            }
    //            atlas.SetPixels(x, y, widthAndHeight, widthAndHeight, originalPixels);
    //            x += widthAndHeight;
    //            if (x + widthAndHeight > atlasWidth)
    //            {
    //                x = 0;
    //                y += widthAndHeight;
    //            }
    //        }
    //    }
    //    EditorUtility.DisplayProgressBar("Adding Mask to Atlas", "Saving Atlas File", 1f);
    //    var newAtlas = ApplyAndWriteTextureToPath(atlas, outputPath);
    //    DestroyImmediate(atlas);
    //    return newAtlas;
    //}

    #endregion Atlas Generation
}
