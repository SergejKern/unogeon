﻿using UnityEngine;
using UnityEditor;


public static class EditorExtensions {
    public static void EditorPrefs_SetColor(string key, Color value)
    {
        EditorPrefs.SetFloat(key + "/R", value.r);
        EditorPrefs.SetFloat(key + "/G", value.g);
        EditorPrefs.SetFloat(key + "/B", value.b);
        EditorPrefs.SetFloat(key + "/A", value.a);
    }

    public static Color EditorPrefs_GetColor(string key)
    {
        var r = EditorPrefs.GetFloat(key + "/R");
        var g = EditorPrefs.GetFloat(key + "/G");
        var b = EditorPrefs.GetFloat(key + "/B");
        var a = EditorPrefs.GetFloat(key + "/A");
        return new Color(r, g, b, a);
    }
}
