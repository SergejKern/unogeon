using UnityEditor;
using UnityEditor.AnimatedValues;
using UnityEngine;
using UnityEngine.Events;

using static Tiles;
using static CustomStyles;

public partial class RoomEditor : EditorWindow
{
    #region Editor Prefs
    const string kEditorPref_ShowTileReplacementTool = "RoomEditor.ShowTileReplacementTool";
    #endregion

    void Init_TileReplacementTool()
    {
        showTileReplacementTool.valueChanged.RemoveAllListeners();
        showTileReplacementTool.valueChanged.AddListener(new UnityAction(Repaint));

        showTileReplacementTool.target = EditorPrefs.GetBool(kEditorPref_ShowTileReplacementTool, false);
    }

    AnimBool showTileReplacementTool = new AnimBool();
    string[] tileTypeOptions = null;
    Type replaceType;
    Type replaceWithType;
    ushort replaceTileIdx;
    ushort replaceWithTileIdx;

    void TileReplacementTool()
    {
        using (var tileReplacementTool = new GUILayout.VerticalScope(EditorStyles.helpBox, GUILayout.Width(position.size.x - 22)))
        {
            bool show = EditorGUILayout.Foldout(showTileReplacementTool.target, "Tile-Replacement", true, MiniFoldoutStyle);
            if (show != showTileReplacementTool.target)
            {
                showTileReplacementTool.target = show;
                EditorPrefs.SetBool(kEditorPref_ShowTileReplacementTool, showTileReplacementTool.target);
            }

            using (var fade = new EditorGUILayout.FadeGroupScope(showTileReplacementTool.faded))
            {
                if (fade.visible)
                {
                    _TileReplacementInnerGUI();
                }
            }
        }
    }

    void _TileReplacementInnerGUI()
    {
        bool doReplaceType = false, doReplaceTexIdx = false;
        using (var hs = new GUILayout.HorizontalScope())
        {
            if (tileTypeOptions == null || tileTypeOptions.Length < 1)
                tileTypeOptions = System.Enum.GetNames(typeof(Type));
            replaceType = (Type)EditorGUILayout.Popup("Replace Type", (int)replaceType, tileTypeOptions);
            replaceWithType = (Type)EditorGUILayout.Popup("with Type", (int)replaceWithType, tileTypeOptions);
            doReplaceType = (GUILayout.Button("Replace!", GUILayout.Width(100)));
        }

        using (var hs = new GUILayout.HorizontalScope())
        {
            GUILayout.Label("Replace Tex-Idx", GUILayout.Width(75.0f));
            replaceTileIdx = (ushort)Mathf.Clamp(EditorGUILayout.IntField(replaceTileIdx, GUILayout.Width(25.0f)), 0, TexMax + 1);
            GUILayout.Label("With Idx", GUILayout.Width(60.0f));
            replaceWithTileIdx = (ushort)Mathf.Clamp(EditorGUILayout.IntField(replaceWithTileIdx, GUILayout.Width(25.0f)), 0, TexMax);

            doReplaceTexIdx = (GUILayout.Button("Replace!", GUILayout.Width(100)));
        }

        if (doReplaceType || doReplaceTexIdx)
        {
            Undo.RecordObject(_Room, "Tile-Replacement");

            //for (int i=0; i< _Room.Levels.Count; ++i)
            //{
            var lvl = _Room.Levels[CurrentLevel];
            for (int j = 0; j < lvl.Tiles.Length; ++j)
            {
                if (doReplaceType && OriginType(lvl.Tiles[j]) == replaceType)
                    lvl.SetTile(j, (ushort)replaceWithType);
                if (doReplaceTexIdx && TextureType(lvl.Tiles[j]) == replaceTileIdx)
                    lvl.SetTile(j, replaceWithTileIdx, mask: TexMask, shift: TexShift);
            }

            if (doReplaceType)
            {
                _Room.UpdateResulting(CurrentLevel);
            }
            //}
            UpdateVisuals();
        }
    }
}
