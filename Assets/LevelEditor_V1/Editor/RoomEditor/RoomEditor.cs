using UnityEngine;
using UnityEditor;

using Core.Types;

using static Global.Constants;
using static RoomMeshOperations;
using static RoomOperations;
using static Core.Editor.Utility.CustomGUI;

[InitializeOnLoad]
public partial class RoomEditor : EditorWindow 
{
    public enum EditorState
    {
        None,
        Tiles,
        Object,
        Count
    }
    EditorState _State;

    static RoomEditor _CurrentEditor = null;
    OperationData MeshOperationData;
    GameObject _RoomObject;
    Room _Room;

    int CurrentLevel = 0;
    Vector3Int RoomSize;    

    #region Editor Prefs
    const string kEditorPref_EditorState = "RoomEditor.EditorState";

    const string kEditorPref_RoomSizeX = "RoomEditor.RoomSize.X";
    const string kEditorPref_RoomSizeY = "RoomEditor.RoomSize.Y";
    const string kEditorPref_RoomSizeZ = "RoomEditor.RoomSize.Z";
    #endregion

    Vector2 GUI_scrollPos;

    [MenuItem ("Tools/RoomEditor")]
    public static void OpenWindow()
    {
        var roomEditor= GetWindow(typeof(RoomEditor));
        roomEditor.Show();
    }

    void OnEnable()
    {
        SceneView.duringSceneGui += OnSceneGUI;
        Undo.undoRedoPerformed += UndoRedoPerformed;
        _CurrentEditor = this;

        Init();

        if (_RoomObject != null)
            UpdateVisuals();
    }

    private void Init()
    {
        MeshOperationData = new OperationData();

        _State = (EditorState)EditorPrefs.GetInt(kEditorPref_EditorState, (int)EditorState.None);

        int roomSizeX = EditorPrefs.GetInt(kEditorPref_RoomSizeX, 1);
        int roomSizeY = EditorPrefs.GetInt(kEditorPref_RoomSizeY, 1);
        int roomSizeZ = EditorPrefs.GetInt(kEditorPref_RoomSizeZ, 1);
        RoomSize = new Vector3Int(roomSizeX, roomSizeY, roomSizeZ);

        Init_TilesMode();

        Init_SizePositionTools();
        Init_TileReplacementTool();
        Init_SmartPlacingTools();
        Init_GeneratorTools();
    }

    void OnDisable()
    {
        SceneView.duringSceneGui -= OnSceneGUI;
        Undo.undoRedoPerformed -= UndoRedoPerformed;
        //DestroyImmediate(RoomObject);
        _CurrentEditor = null;
    }

    void OnGUI()
    {
        _CurrentEditor = this;
        using (var scrollViewScope = new GUILayout.ScrollViewScope(GUI_scrollPos))
        {
            GUI_scrollPos = scrollViewScope.scrollPosition;

            if (_RoomObject == null)
            {
                _Room = null;
                RoomSizeGUI(ref RoomSize);

                Room r = null;
                if (GUILayout.Button("Create Room"))
                {
                    r = CreateFlatRoom(RoomSize.x * RoomGridSize, RoomSize.y * RoomYGridSize, RoomSize.z * RoomGridSize);
                    FillLevelWithFloorTiles(r, 0);
                    r.UpdateResulting(0);
                    r.Setting = Global.Resources.Instance.StandardLevelSetting;
                }
                if (GUILayout.Button("Load Room"))
                {
                    string path = EditorUtility.OpenFilePanel("Load Room", "Rooms", "asset");
                    path = path.Replace(Application.dataPath, "Assets");
                    r = AssetDatabase.LoadAssetAtPath(path, typeof(Room)) as Room;
                }
                if (r != null)
                {
                    _Room = r;
                    CurrentLevel = 0;
                    _Room.Name = "Room";
                    _Room.SetRoomPositioning(Vector3.zero);
                    _Room.ShowRoom(MeshOperationData);
                    _RoomObject = _Room.RoomComponent.gameObject;
                    //RoomObject.hideFlags = HideFlags.HideAndDontSave;
                    SetCurrentLevel(CurrentLevel, false);
                }
            }
            if (_Room != null)
            {
                RoomGUI();

                if (GUILayout.Button("Save Room", GUILayout.Width(100)))
                    SaveRoom();

                //GUILayout.Space(5.0f);
                //GUILayout.FlexibleSpace();
            }
        }
    }

    void SceneGUI(SceneView sceneView, Event e)
    {
        if (_RoomObject == null)
            return;

        if (e.alt)
            return;
        if (e.type == EventType.KeyDown && e.keyCode == KeyCode.Escape)
        {
            e.Use();
            _State = EditorState.None;
            EditorPrefs.SetInt(kEditorPref_EditorState, (int)_State);

            Repaint();
        }
        if (e.shift && e.type == EventType.ScrollWheel)
        {
            e.Use();
            if (e.delta.y >= 1)
                SetCurrentLevel(CurrentLevel + 1);
            else if (e.delta.y <= -1)
                SetCurrentLevel(CurrentLevel - 1);
            Repaint();
        }

        SceneGUI_DebugDraw(sceneView, e);

        if (_CurrentEditor._State == EditorState.None)
            return;

        Plane groundPlane = new Plane(Vector3.up, new Vector3(0,CurrentLevel, 0));
        Ray ray = Camera.current.ScreenPointToRay(new Vector3(e.mousePosition.x, sceneView.position.height - e.mousePosition.y - 16, 0.0f));
        float distance;
        if (!groundPlane.Raycast(ray, out distance))
            return;
        var wPos= ray.GetPoint(distance);

        if (_State == EditorState.Tiles)
        {
            SceneGUI_TilesMode(sceneView, e, wPos);
        }
    }

    ChangeCheck RoomSizeGUI(ref Vector3Int size)
    {
        ChangeCheck changed = ChangeCheck.NotChanged;

        using (var hs = new GUILayout.HorizontalScope())
        {
            EditorGUILayout.LabelField("Size-X", GUILayout.Width(50.0f));
            int xSize = Mathf.Max(1, EditorGUILayout.IntField(size.x, GUILayout.Width(25.0f)));
            EditorGUILayout.LabelField("Size-Y", GUILayout.Width(50.0f));
            int ySize = Mathf.Max(1, EditorGUILayout.IntField(size.y, GUILayout.Width(25.0f)));
            EditorGUILayout.LabelField("Size-Z", GUILayout.Width(50.0f));
            int zSize = Mathf.Max(1, EditorGUILayout.IntField(size.z, GUILayout.Width(25.0f)));
            if (xSize != size.x || zSize != size.z || ySize != size.y)
            {
                changed = ChangeCheck.Changed;
                size = new Vector3Int(xSize, ySize, zSize);
            }
            EditorGUILayout.LabelField($"Actual size: x {xSize * RoomGridSize} y {ySize * RoomYGridSize} z { zSize * RoomGridSize}");
        }
        if(changed == ChangeCheck.Changed)
        {
            EditorPrefs.SetInt(kEditorPref_RoomSizeX, size.x);
            EditorPrefs.SetInt(kEditorPref_RoomSizeY, size.y);
            EditorPrefs.SetInt(kEditorPref_RoomSizeZ, size.z);
        }

        return changed;
    }

    void SaveRoom()
    {
        string path = AssetDatabase.GetAssetPath(_Room);
        if (string.IsNullOrEmpty(path))
        {
            path = EditorUtility.SaveFilePanel("Save Room", "Rooms", _Room.name + ".asset", "asset");
            if (!string.IsNullOrEmpty(path))
            {
                path = path.Replace(Application.dataPath, "Assets");
                AssetDatabase.CreateAsset(_Room, path);
                AssetDatabase.SaveAssets();
                return;
            }
        }
        EditorUtility.SetDirty(_Room);
        AssetDatabase.SaveAssets();
    }

    void EditorStateButtons()
    {
        using (var hs = new GUILayout.HorizontalScope())
        {
            for (int i = 0; i < (int)EditorState.Count; ++i)
            {
                if (ActivityButton((int)_State == i, ((EditorState)i).ToString(), GUILayout.Width(100)))
                {
                    _State = (EditorState)i;
                    EditorPrefs.SetInt(kEditorPref_EditorState, (int)_State);
                }
            }
        }
    }

    void EditorStateGUI()
    {
        switch (_State)
        {
            case EditorState.Count:
            case EditorState.None: break;
            case EditorState.Tiles:
                TileStateGUI();
                break;
            case EditorState.Object:
                break;
        }
    }

    void RoomGUI()
    {
        using (var vs = new GUILayout.VerticalScope("Room-Properties", EditorStyles.helpBox, GUILayout.Width(position.width - 22)))
        {
            GUILayout.Space(15.0f);

            using (var hs = new GUILayout.HorizontalScope())
            {
                using (var check = new EditorGUI.ChangeCheckScope())
                {
                    GUILayout.Label("Name: ", GUILayout.Width(50));
                    var newName = EditorGUILayout.TextField(_Room.Name);
                    if (check.changed)
                    {
                        Undo.RecordObject(_Room, "Room-Name");
                        _Room.Name = newName;
                        _Room.RoomComponent?.SetName(newName);
                    }
                }

                GUILayout.FlexibleSpace();
                if (GUILayout.Button("Discard Room", GUILayout.Width(100)))
                    DestroyImmediate(_RoomObject);
            }
            GUILayout.Space(5.0f);

            // size of room
            Vector3Int size = new Vector3Int(_Room.Size.x / RoomGridSize, _Room.Size.y / RoomYGridSize, _Room.Size.z / RoomGridSize);
            if (RoomSizeGUI(ref size) == ChangeCheck.Changed)
            {
                Undo.RecordObject(_Room, "Room resized");

                Vector3Int newSize = new Vector3Int(size.x * RoomGridSize, size.y * RoomYGridSize, size.z * RoomGridSize);
                _Room.Size = newSize;
                _Room.SetRoomPositioning(Vector3.zero);
            }

            LevelGUI();
        }

        using (var vs = new GUILayout.VerticalScope("Editing Mode:", EditorStyles.helpBox, GUILayout.Width(position.width - 22)))
        {
            GUILayout.Space(15.0f);

            EditorStateButtons();
            EditorStateGUI();
        }

        SizeTools();
        GUILayout.Space(5.0f);

        GeneratorTools();
        GUILayout.Space(5.0f);

        TileReplacementTool();
        GUILayout.Space(5.0f);

        SmartTilePlacingToolsGUI();
        GUILayout.Space(5.0f);
    }

    void UndoRedoPerformed()
    {
        if (_Room != null)
        {
            AddOpenExitsFromTiles(_Room);
            UpdateVisuals();
            SceneView.RepaintAll();
            Repaint();
        }
    }

    void UpdateVisuals()
    {
        MeshOperationData.Room = _Room;
        GenerateMesh(MeshOperationData);

        SetCurrentLevel(CurrentLevel, false);
        _Room.RoomComponent?.SetName(_Room.Name);
        _Room.SetRoomPositioning(Vector3.zero);
    }

    static void OnSceneGUI(SceneView sceneView)
    {
        if (_CurrentEditor == null)
            return;

        var e = Event.current;
        if (e.button == 2) // middle mouse button
            return;

        // prevent default SceneGUI stuff part 1, e.Use() for part2
        int controlID = GUIUtility.GetControlID(FocusType.Passive);
        if (e.type == EventType.Layout)
        {
            HandleUtility.AddDefaultControl(controlID);
        }

        _CurrentEditor.SceneGUI(sceneView, e);

        if (e.type != EventType.Repaint && e.type != EventType.Layout)
        {
            SceneView.RepaintAll();
        }
    }
}
