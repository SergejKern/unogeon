using Core;
using Core.Unity.Extensions;
using UnityEditor;
using UnityEditor.AnimatedValues;
using UnityEngine;
using UnityEngine.Events;

using static Core.Editor.Utility.CustomGUI;
using static RoomMeshOperations;
using static RoomOperations;
using static Tiles;
using static CustomStyles;

/// <summary>
/// Editor for Rooms
/// </summary>
public partial class RoomEditor : EditorWindow
{
    enum BrushType
    {
        PointBrush,
        CircleBrush,
        RectangleBrush,
        DragLine,
        DragEllipseDiag,
        DragEllipseRect,
        DragRectangle,
        Fill
    }
    BrushType _Brush;

    bool IsBrushType { get { return _Brush == BrushType.PointBrush || _Brush == BrushType.CircleBrush || _Brush == BrushType.RectangleBrush; } }
    bool IsDragType { get { return _Brush == BrushType.DragEllipseDiag || _Brush == BrushType.DragRectangle; } }

    Vector3? startPos = null;
    float BrushSize = 1;

    Editor _LevelSettingsEditor;

    int _PaintTileTypeLeft;
    int _PaintTileTypeRight;
    int _PaintTexLeft;
    int _PaintTexRight;
    int _PaintVariationLeft;
    int _PaintVariationRight;

    string[] brushTypeOptions;

    AnimBool showLevelSettingEditor = new AnimBool();

    #region Editor Prefs
    const string kEditorPref_PaintTileTypeLeft = "RoomEditor.PaintTileTypeLeft";
    const string kEditorPref_PaintTileTypeRight = "RoomEditor.PaintTileTypeRight";
    const string kEditorPref_ShowLevelSettingEditor = "RoomEditor.ShowLevelSettingEditor";

    #endregion

    void Init_TilesMode()
    {
        _PaintTileTypeLeft = EditorPrefs.GetInt(kEditorPref_PaintTileTypeLeft, (int)Type.Floor);
        _PaintTileTypeRight = EditorPrefs.GetInt(kEditorPref_PaintTileTypeRight, (int)Type.AutoNonFloor);

        showLevelSettingEditor.valueChanged.RemoveAllListeners();
        showLevelSettingEditor.valueChanged.AddListener(new UnityAction(Repaint));
        showLevelSettingEditor.target = EditorPrefs.GetBool(kEditorPref_ShowLevelSettingEditor, true);
    }

    void DrawRect(Vector3 currentPos)
    {
        if (!startPos.HasValue) return;
        var startPosV = startPos.Value;
        //var startGridPos = new Vector2Int(Mathf.FloorToInt(startPosV.x), Mathf.FloorToInt(startPosV.z));
        //var starGridPosVec3 = startGridPos.XZVector3(startPosV.y);

        Handles.DrawLine(currentPos, new Vector3(currentPos.x, currentPos.y, startPosV.z));
        Handles.DrawLine(currentPos, new Vector3(startPosV.x, currentPos.y, currentPos.z));
        Handles.DrawLine(startPosV, new Vector3(currentPos.x, currentPos.y, startPosV.z));
        Handles.DrawLine(startPosV, new Vector3(startPosV.x, currentPos.y, currentPos.z));
    }

    const float Sqrt_2 = 1.4142135623730950488016887242097f;
    void DrawCursor(Vector3 currentPos, Event e)
    {
        var brush = _Brush;
        var brushSize = BrushSize;

        var currentGridPos = new Vector2Int(Mathf.RoundToInt(currentPos.x), Mathf.RoundToInt(currentPos.z));
        var currentGridPosVec3 = currentGridPos.Vector3(currentPos.y);

        var variationOnly = (_PaintTileTypeLeft + _PaintTileTypeRight + _PaintTexLeft + _PaintTexRight == -4 && _PaintVariationLeft + _PaintVariationRight != -2);
        if (variationOnly && _Brush == BrushType.PointBrush)
        {
            brush = BrushType.RectangleBrush;
            brushSize = 0.5f;
            currentPos = new Vector3(Mathf.CeilToInt(currentPos.x)-0.5f, 0.0f, Mathf.CeilToInt(currentPos.z)-0.5f);
        }

        var prevCol = Handles.color;
        Handles.color = Color.green;

        switch (brush)
        {
            case BrushType.Fill:
            case BrushType.PointBrush:
                var gridRealWPos = currentGridPosVec3;
                Handles.DrawLine(gridRealWPos + Vector3.forward * 0.5f, gridRealWPos - Vector3.forward * 0.5f);
                Handles.DrawLine(gridRealWPos + Vector3.right * 0.5f, gridRealWPos - Vector3.right * 0.5f);
                if(_Brush == BrushType.Fill)
                {
                    Handles.DrawLine(gridRealWPos + Vector3.forward * 0.5f + Vector3.right * 0.5f, gridRealWPos - Vector3.forward * 0.5f - Vector3.right * 0.5f);
                    Handles.DrawLine(gridRealWPos - Vector3.forward * 0.5f + Vector3.right * 0.5f, gridRealWPos + Vector3.forward * 0.5f - Vector3.right * 0.5f);
                }
                break;
            case BrushType.RectangleBrush:
                Handles.RectangleHandleCap(-1, currentPos, Quaternion.Euler(90,0,0), brushSize, EventType.Repaint);
                break;
            case BrushType.CircleBrush:
                Handles.CircleHandleCap(-1, currentPos, Quaternion.Euler(90, 0, 0), brushSize, EventType.Repaint);
                break;
            case BrushType.DragLine:
                {
                    Handles.CircleHandleCap(-1, currentPos, Quaternion.Euler(90, 0, 0), brushSize, EventType.Repaint);
                    if (!startPos.HasValue) return;
                    var startPosV = startPos.Value;
                    Handles.DrawLine(startPosV, currentPos);
                    Handles.CircleHandleCap(-1, startPosV, Quaternion.Euler(90, 0, 0), brushSize, EventType.Repaint);
                }
                break;
            case BrushType.DragRectangle:
                DrawRect(currentPos);
                break;
            case BrushType.DragEllipseRect:
            case BrushType.DragEllipseDiag:
                {
                    if (!startPos.HasValue) return;
                    var startPosV = startPos.Value;
                    var halfSize = 0.5f * (currentPos - startPosV);
                    var centerPos = startPosV + halfSize;
                    var v2 = _Brush == BrushType.DragEllipseRect;

                    if(v2)
                        DrawRect(currentPos);
                    else
                    {
                        Handles.DrawLine(startPosV, startPosV+ halfSize.normalized);
                        Handles.DrawLine(currentPos, currentPos - halfSize.normalized);
                    }

                    var mult = v2 ? 1.0f : Sqrt_2;
                    float x = 0;
                    float z = 0;
                    for (int i=0; i< 21; ++i)
                    {
                        var nextX = Mathf.Cos(Mathf.Deg2Rad * i * 18) * mult * halfSize.x + centerPos.x;
                        var nextZ = Mathf.Sin(Mathf.Deg2Rad * i * 18) * mult * halfSize.z + centerPos.z;
                        if (i != 0)
                        {
                            Handles.DrawLine(new Vector3(x, 0, z), new Vector3(nextX, 0, nextZ));
                        }
                        x = nextX;
                        z = nextZ;
                    }
                }
                break;
        }

        Handles.color = prevCol;
    }

    void SceneGUI_TilesMode(SceneView sceneView, Event e, Vector3 pos)
    {
        DrawCursor(pos, e);

        if (e.control && e.type == EventType.ScrollWheel)
        {
            e.Use();
            BrushSize = Mathf.Clamp(BrushSize - 0.1f * e.delta.y, 0.5f, 10.0f);
            Repaint();
        }

        var useEvent = (e.type == EventType.MouseDown || e.type == EventType.MouseDrag || e.type == EventType.MouseUp);
        if (!useEvent)
            return;

        bool doSetTilesNow = false;
        if (IsBrushType)
        {
            doSetTilesNow = true;
        }
        else 
        {
            if (e.type == EventType.MouseDown)
            {
                startPos = pos;
            }

            if (_Brush == BrushType.Fill && startPos.HasValue)
                startPos = pos;

            if (e.type == EventType.MouseUp && startPos.HasValue)
            {
                doSetTilesNow = true;
            }
        }

        e.Use();

        if (!doSetTilesNow)
            return;

        var room = _Room;
        var level = room.Levels[CurrentLevel];

        var variationOnly = (_PaintTileTypeLeft + _PaintTileTypeRight + _PaintTexLeft + _PaintTexRight == -4 && _PaintVariationLeft + _PaintVariationRight != -2);
        if (variationOnly)
        {
            pos -= Vector3.one * 0.5f;
            startPos -= Vector3.one * 0.5f;
        }

        Undo.RecordObject(_Room, "Paint");
        if (Event.current.button == 0)
        {
            ApplyBrush(pos, true);
        }
        else if (Event.current.button == 1)
        {
            ApplyBrush(pos, false);
        }

        MeshOperationData.Room = _Room;
        GenerateMesh(MeshOperationData);

        startPos = null;
    }
    
    void CircleAndRectangleBrush(Vector3 pos, bool circleBrush, int tileType, int texType, int variation)
    {
        int brushMin = Mathf.FloorToInt(-BrushSize);
        int brushMax = Mathf.CeilToInt(BrushSize);

        for (int x = brushMin; x <= brushMax; ++x)
            for (int z = brushMin; z <= brushMax; ++z)
            {
                var gridPos = new Vector2Int(Mathf.RoundToInt(pos.x + x), Mathf.RoundToInt(pos.z + z));
                if (gridPos.x < pos.x - BrushSize) continue;
                if (gridPos.y < pos.z - BrushSize) continue;
                if (gridPos.x > pos.x + BrushSize) continue;
                if (gridPos.y > pos.z + BrushSize) continue;

                if (circleBrush)
                {
                    var rX = gridPos.x  - pos.x;
                    var rZ = gridPos.y - pos.z;
                    if (rX * rX + rZ * rZ > BrushSize * BrushSize) continue;
                }

                ModifyTileAtPos(new Vector2Int(gridPos.x, gridPos.y), tileType, texType, variation);
            }
    }

    void ApplyBrush(Vector3 currentPos, bool left)
    {
        // var gridStartPos = new Vector2Int(Mathf.FloorToInt(startPos.x), Mathf.FloorToInt(startPos.z));

        var tileType = left ? _PaintTileTypeLeft : _PaintTileTypeRight;
        var texType = left ? _PaintTexLeft : _PaintTexRight;
        var variation = left ? _PaintVariationLeft : _PaintVariationRight;

        switch (_Brush)
        {
            case BrushType.PointBrush:
                {
                    var currentGridPos = new Vector2Int(Mathf.RoundToInt(currentPos.x), Mathf.RoundToInt(currentPos.z));
                    ModifyTileAtPos(currentGridPos, tileType, texType, variation);
                }
                break;
            case BrushType.CircleBrush:
            case BrushType.RectangleBrush:
                {
                    CircleAndRectangleBrush(currentPos, _Brush == BrushType.CircleBrush, tileType, texType, variation);
                }
                break;
            case BrushType.DragLine:
                {
                    if (!startPos.HasValue) return;
                    var startPosV = startPos.Value;
                    var lineVec = (currentPos - startPosV);
                    var lineDir = lineVec.normalized;
                    var magn = Mathf.Abs(lineVec.magnitude);
                    int length = Mathf.CeilToInt(magn);
                    for (int i=0; i <= length; ++i)
                    {
                        float step = Mathf.Min(i, magn);
                        var addPos = step * lineDir;
                        CircleAndRectangleBrush(startPosV+ addPos, true, tileType, texType, variation);
                    }
                }
                break;
            case BrushType.DragRectangle:
                {
                    if (!startPos.HasValue) return;
                    var startPosV = startPos.Value;
                    var xMin = Mathf.Min(startPosV.x, currentPos.x);
                    var zMin = Mathf.Min(startPosV.z, currentPos.z);
                    var xMax = Mathf.Max(startPosV.x, currentPos.x);
                    var zMax = Mathf.Max(startPosV.z, currentPos.z);
                    int xStart = Mathf.FloorToInt(xMin);
                    int xEnd = Mathf.CeilToInt(xMax);
                    int zStart = Mathf.FloorToInt(zMin);
                    int zEnd = Mathf.CeilToInt(zMax);

                    for (int x = xStart; x <= xEnd; ++x)
                        for (int z = zStart; z <= zEnd; ++z)
                        {
                            var currentGridPos = new Vector2Int(Mathf.RoundToInt(x), Mathf.RoundToInt(z));
                            if (currentGridPos.x < xMin) continue;
                            if (currentGridPos.y < zMin) continue;
                            if (currentGridPos.x > xMax) continue;
                            if (currentGridPos.y > zMax) continue;

                            ModifyTileAtPos(new Vector2Int(currentGridPos.x, currentGridPos.y), tileType, texType, variation);
                        }
                }
                break;
            case BrushType.DragEllipseRect:
            case BrushType.DragEllipseDiag:
                {
                    if (!startPos.HasValue) return;
                    var startPosV = startPos.Value;
                    var halfSize = 0.5f * (currentPos - startPosV);
                    var centerPos = startPosV + halfSize;

                    var v2 = (_Brush == BrushType.DragEllipseRect);

                    var width = v2 ? Mathf.Abs(halfSize.x) : Mathf.Abs(Mathf.Cos(Mathf.Deg2Rad * 0) * Sqrt_2 * halfSize.x);
                    var height = v2 ? Mathf.Abs(halfSize.z) : Mathf.Abs(Mathf.Sin(Mathf.Deg2Rad * 90) * Sqrt_2 * halfSize.z);

                    int xMin = Mathf.FloorToInt(-width);
                    int xMax = Mathf.CeilToInt(width);
                    int zMin = Mathf.FloorToInt(-height);
                    int zMax = Mathf.CeilToInt(height);

                    for (int x = xMin; x <= xMax; ++x)
                        for (int z = zMin; z <= zMax; ++z)
                        {
                            var currentGridPos = new Vector2Int(Mathf.RoundToInt(centerPos.x + x), Mathf.RoundToInt(centerPos.z + z));
                            var eX = (currentGridPos.x - centerPos.x)/ width;
                            var eZ = (currentGridPos.y - centerPos.z)/ height;
                            if (eX * eX + eZ * eZ > 1) continue;
                            ModifyTileAtPos(new Vector2Int(currentGridPos.x, currentGridPos.y), tileType, texType, variation);
                        }
                }
                break;
            case BrushType.Fill:
                {
                    var currentGridPos = new Vector2Int(Mathf.RoundToInt(currentPos.x), Mathf.RoundToInt(currentPos.z));
                    var level = _Room.Levels[CurrentLevel];
                    var tiles = GetRegionTiles(level, currentGridPos.x -level.Position.x, currentGridPos.y - level.Position.y, compareMask: OriginMask+TexMask);
                    foreach (var tile in tiles)
                    {
                        ModifyTileAtPos(tile + level.Position, tileType, texType, variation);
                    }
                }
                break;
        }
    }

    void ModifyTileAtPos(Vector2Int pos, int tileType, int texType, int variation)
    {
        if (tileType != -1)
        {
            _Room.SetTile(CurrentLevel, pos.x, pos.y, (ushort)tileType, action: OutOfLevelBoundsAction.IgnoreTile);
            if (modifySurroundingLevels)
                ModifySurroundingLevel(pos.x, pos.y);
            UpdateExits(pos);
        }

        if (texType != -1)
            _Room.SetTile(CurrentLevel, pos.x, pos.y, (ushort)texType, mask: TexMask, shift: TexShift, action: OutOfLevelBoundsAction.IgnoreTile);

        if (variation != -1)
            _Room.SetTile(CurrentLevel, pos.x, pos.y, (ushort)variation, mask: VarMask, shift: VarShift, action: OutOfLevelBoundsAction.IgnoreTile);
    }

    void UpdateExits(Vector2Int pos)
    {
        var editedRoomEdge = (pos.x == 0 || pos.y == 0 || pos.x == (_Room.TileDim.x - 1) || pos.y == (_Room.TileDim.y - 1));
        if (!editedRoomEdge)
            return;

        AddOpenExitsFromTiles(_Room);
    }

    public void LevelSettingsEditorGUI()
    {
        if (_Room == null)
            return;
        if (_Room.Setting != null)
        {
            showLevelSettingEditor.target = EditorGUILayout.Foldout(showLevelSettingEditor.target, "Level Settings", true, MiniFoldoutStyle);
        }

        using (var fade = new EditorGUILayout.FadeGroupScope(showLevelSettingEditor.faded))
        {
            if (fade.visible)
            {
                if (_LevelSettingsEditor?.target != null
                    && _LevelSettingsEditor?.target == _Room.Setting)
                {
                    _LevelSettingsEditor.OnInspectorGUI();
                    return;
                }
                _LevelSettingsEditor = null;
                if (_Room.Setting != null)
                {
                    _LevelSettingsEditor = Editor.CreateEditor(_Room.Setting);
                    _LevelSettingsEditor.OnInspectorGUI();
                }
            }
        }
    }

    void TileStateGUI()
    {
        VSplitter();

        var newSetting = EditorGUILayout.ObjectField("Level Texture Setting", _Room.Setting, typeof(LevelTextureSetting), false) as LevelTextureSetting;
        if (_Room.Setting != newSetting)
        {
            Undo.RecordObject(_Room, "Setting changed");
            _Room.Setting = newSetting;
        }

        using (var vs = new GUILayout.VerticalScope(EditorStyles.helpBox))
            LevelSettingsEditorGUI();

        var setting = _Room.Setting;

        GUILayout.Label("Geometry: ", GUILayout.Width(100));

        using (var hs = new GUILayout.HorizontalScope())
        {
            for (int gIdx = -1; gIdx <= (int)Type.Floor; ++gIdx)
            {
                string toolName = ((Type)gIdx).ToString();

                if (ActivityButton2(_PaintTileTypeLeft == gIdx, _PaintTileTypeRight == gIdx, toolName, GUILayout.Width(100)))
                {
                    //var prevLeft = _PaintTileTypeLeft;
                    //var prevRight = _PaintTileTypeRight;
                    if (Event.current.button == 1)
                        _PaintTileTypeRight = gIdx;
                    else
                        _PaintTileTypeLeft = gIdx;

                    //if (_PaintTileTypeLeft == _PaintTileTypeRight && _PaintTileTypeLeft != -1)
                    //{
                    //    if (Event.current.button == 1)
                    //        _PaintTileTypeLeft = prevRight;
                    //    else _PaintTileTypeRight = prevLeft;
                    //}

                    EditorPrefs.SetInt(kEditorPref_PaintTileTypeLeft, _PaintTileTypeLeft);
                    EditorPrefs.SetInt(kEditorPref_PaintTileTypeRight, _PaintTileTypeRight);
                }
            }
        }
        if (setting == null)
            return;
        var collections = setting.LevelTextureCollections;

        GUILayout.Label("Texture: ", GUILayout.Width(100));

        if (ActivityButton2(_PaintTexLeft == -1, _PaintTexRight == -1, "None", GUILayout.Width(100)))
        {
            if (Event.current.button == 1)
                _PaintTexRight = -1;
            else _PaintTexLeft = -1;
        }
        int tIdx = 0;
        for (; tIdx < collections.Count; ++tIdx)
        {
            var current = collections[tIdx];
            if (current == null)
                continue;

            if (tIdx % 5 == 0)
                EditorGUILayout.BeginHorizontal();
            string toolName = current.Name;

            Texture2D previewTex = null;

            var floorVar = current.FloorVariations;
            if (floorVar?.Count > 0 && floorVar[0].IsSet())
                previewTex = AssetPreview.GetAssetPreview(floorVar[0].GetTexture());

            if (ActivityButton2(_PaintTexLeft == tIdx, _PaintTexRight == tIdx, toolName, previewTex, GUILayout.Width(100)))
            {
                //var prevLeft = _PaintTexLeft;
                //var prevRight = _PaintTexRight;
                if (Event.current.button == 1)
                    _PaintTexRight = tIdx;
                else _PaintTexLeft = tIdx;
                //if (_PaintTexLeft == _PaintTexRight && _PaintTileTypeLeft != -1)
                //{
                //    if (Event.current.button == 1)
                //        _PaintTexLeft = prevRight;
                //    else _PaintTexRight = prevLeft;
                //}
            }
            if (tIdx % 5 == 4)
                EditorGUILayout.EndHorizontal();
        }
        if (tIdx % 5 != 0)
            EditorGUILayout.EndHorizontal();

        _PaintVariationLeft = EditorGUILayout.IntField("Variation Left: ", _PaintVariationLeft);
        _PaintVariationRight = EditorGUILayout.IntField("Variation Right: ", _PaintVariationRight);

        DebugDrawGUI();

        //if (brushTypeOptions == null || brushTypeOptions.Length < 1)
            brushTypeOptions = System.Enum.GetNames(typeof(BrushType));
        _Brush = (BrushType) EditorGUILayout.Popup("Brush Type", (int)_Brush, brushTypeOptions);
        BrushSize = EditorGUILayout.FloatField("Brush Size", BrushSize);
    }
}
