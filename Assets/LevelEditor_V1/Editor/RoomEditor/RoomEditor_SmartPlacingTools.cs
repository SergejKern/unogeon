
using UnityEditor;
using UnityEditor.AnimatedValues;
using UnityEngine;
using UnityEngine.Events;

using static RoomOperations;
using static Tiles;
using static CustomStyles;

public partial class RoomEditor : EditorWindow
{
    #region Editor Prefs
    const string kEditorPref_ShowSmartTilePlaceTools = "RoomEditor.ShowSmartTilePlaceTools";

    const string kEditorPref_ModifySurroundingLevels = "RoomEditor.ModifySurroundingLevels";
    const string kEditorPref_AvoidOneSpaceTiles = "RoomEditor.AvoidOneSpaceTiles";
    const string kEditorPref_FixVisibilityIssues = "RoomEditor.FixVisibilityIssues";
    #endregion

    AnimBool showSmartTilePlaceTools = new AnimBool();
    bool avoidOneSpaceTiles;
    bool modifySurroundingLevels;
    bool fixVisibilityIssues;

    void Init_SmartPlacingTools()
    {
        showSmartTilePlaceTools.valueChanged.RemoveAllListeners();
        showSmartTilePlaceTools.valueChanged.AddListener(new UnityAction(Repaint));
        showSmartTilePlaceTools.target = EditorPrefs.GetBool(kEditorPref_ShowSmartTilePlaceTools, false);

        avoidOneSpaceTiles = EditorPrefs.GetBool(kEditorPref_AvoidOneSpaceTiles, false);
        modifySurroundingLevels = EditorPrefs.GetBool(kEditorPref_ModifySurroundingLevels, false);
        fixVisibilityIssues = EditorPrefs.GetBool(kEditorPref_FixVisibilityIssues, false);
    }

    void SmartTilePlacingToolsGUI()
    {
        using (var smartTilePlaceTools = new GUILayout.VerticalScope(EditorStyles.helpBox, GUILayout.Width(position.size.x - 22)))
        {
            bool show = EditorGUILayout.Foldout(showSmartTilePlaceTools.target, "Smart Tile placing", true, MiniFoldoutStyle);
            if (show != showSmartTilePlaceTools.target)
            {
                showSmartTilePlaceTools.target = show;
                EditorPrefs.SetBool(kEditorPref_ShowSmartTilePlaceTools, showSmartTilePlaceTools.target);
            }

            using (var fade = new EditorGUILayout.FadeGroupScope(showSmartTilePlaceTools.faded))
            {
                if (fade.visible)
                {
                    _SmartTilePlacingToolsInnerGUI();
                }
            }
        }
    }

    void _SmartTilePlacingToolsInnerGUI()
    {
        using (var vs = new GUILayout.VerticalScope())
        {
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                using (var hs = new GUILayout.HorizontalScope())
                {
                    GUILayout.Label("modify surrounding levels to fix mesh issues", GUILayout.Width(275.0f));
                    modifySurroundingLevels = EditorGUILayout.Toggle(modifySurroundingLevels, GUILayout.Width(25.0f));
                }

                using (var disableGroup = new EditorGUI.DisabledScope(!modifySurroundingLevels))
                {
                    using (var hs = new GUILayout.HorizontalScope())
                    {
                        GUILayout.Label("Also fix vissibility issues", GUILayout.Width(275.0f));
                        fixVisibilityIssues = EditorGUILayout.Toggle(fixVisibilityIssues, GUILayout.Width(25.0f));
                    }
                    using (var hs = new GUILayout.HorizontalScope())
                    {
                        GUILayout.Label("Fill one-space-empty tiles", GUILayout.Width(275.0f));
                        avoidOneSpaceTiles = EditorGUILayout.Toggle(avoidOneSpaceTiles, GUILayout.Width(25.0f));
                    }
                }

                if (check.changed)
                {
                    EditorPrefs.SetBool(kEditorPref_ModifySurroundingLevels, modifySurroundingLevels);
                    EditorPrefs.SetBool(kEditorPref_FixVisibilityIssues, fixVisibilityIssues);
                    EditorPrefs.SetBool(kEditorPref_AvoidOneSpaceTiles, avoidOneSpaceTiles);
                }
            }

            if (GUILayout.Button("Apply on Level!"))
            {
                ApplySmartPlacingToolsOnLevel();
            }
        }

    }

    void ApplySmartPlacingToolsOnLevel()
    {
        var lvl = _Room.Levels[CurrentLevel];

        for (int x = 0; x < _Room.TileDim.x; ++x)
        {
            for (int z = 0; z < _Room.TileDim.y; ++z)
            {
                ModifySurroundingLevel(x, z);
            }
        }

        UpdateVisuals();
    }

    private void FixVisibility(int x, int z)
    {
        if (!fixVisibilityIssues)
            return;

        var tile = _Room.GetTile(CurrentLevel, x, z, action: OutOfLevelBoundsAction.IgnoreTile);
        Type tileType = OriginType(tile);

        if (tileType == Type.Floor)
        {
            if (CurrentLevel + 1 < _Room.Levels.Count)
                _Room.SetTile(CurrentLevel + 1, x, z, (ushort)Type.AutoNonFloor, action: OutOfLevelBoundsAction.IgnoreTile);
            return;
        }

        if (tileType == Type.Empty)
        {
            if (CurrentLevel + 1 < _Room.Levels.Count)
                _Room.SetTile(CurrentLevel + 1, x, z, (ushort)Type.AutoNonFloor, action: OutOfLevelBoundsAction.IgnoreTile);
        }
    }

    private void FillOneSpaceTiles(int x, int z)
    {
        if (!avoidOneSpaceTiles)
            return;

        var tile = _Room.GetTile(CurrentLevel, x, z, action: OutOfLevelBoundsAction.IgnoreTile);
        Type tileType = ResultingType(tile);

        if (tileType == Type.Floor)
        {
            if (CurrentLevel + 1 < _Room.Levels.Count)
                _Room.SetTile(CurrentLevel + 1, x, z, (ushort)Type.AutoNonFloor, action: OutOfLevelBoundsAction.IgnoreTile);

            if (CurrentLevel + 2 < _Room.Levels.Count)
            {
                tile = _Room.GetTile(CurrentLevel + 1, x, z, OutOfLevelBoundsAction.IgnoreTile);
                var aboveType = ResultingType(tile);
                if (aboveType != Type.Empty)
                    _Room.SetTile(CurrentLevel + 2, x, z, (ushort)Type.AutoNonFloor, action: OutOfLevelBoundsAction.IgnoreTile);
            }
            if (CurrentLevel > 1)
            {
                tile = _Room.GetTile(CurrentLevel - 2, x, z, OutOfLevelBoundsAction.IgnoreTile);
                var belowType = ResultingType(tile);
                //if (belowType == TileType.Empty)
                //{
                //    tile = _Room.GetTile(CurrentLevel - 3, x, z, OutOfLevelBoundsAction.IgnoreTile);
                //    belowType = ResultingType(tile);
                //    if (belowType != TileType.Empty && belowType != TileType.Floor)
                //        _Room.SetTile(CurrentLevel - 2, x, z, (ushort)TileType.Wall, action: OutOfLevelBoundsAction.IgnoreTile);
                //}
                //else 
                if (belowType == Type.Floor)
                    _Room.SetTile(CurrentLevel - 2, x, z, (ushort)Type.Wall, action: OutOfLevelBoundsAction.IgnoreTile);

            }
            return;
        }

        if (tileType == Type.Wall || tileType == Type.AutoNonFloor)
        {
            if (tileType == Type.AutoNonFloor)
            {
                tile = _Room.GetTile(CurrentLevel, x, z, OutOfLevelBoundsAction.IgnoreTile);
                var resultType = ResultingType(tile);
                if (resultType != Type.Wall)
                    return;
            }
            if (CurrentLevel + 1 < _Room.Levels.Count)
            {
                tile = _Room.GetTile(CurrentLevel + 1, x, z, OutOfLevelBoundsAction.IgnoreTile);
                if (tile == ushort.MaxValue)
                    return;
                var aboveType = OriginType(tile);

                if (avoidOneSpaceTiles && aboveType == Type.Floor)
                {
                    if (CurrentLevel + 2 < _Room.Levels.Count)
                    {
                        tile = _Room.GetTile(CurrentLevel + 2, x, z, OutOfLevelBoundsAction.IgnoreTile);
                        aboveType = ResultingType(tile);
                        if (aboveType != Type.Empty)
                            _Room.SetTile(CurrentLevel + 1, x, z, (ushort)Type.AutoNonFloor, action: OutOfLevelBoundsAction.IgnoreTile);
                    }
                }
            }
            if (avoidOneSpaceTiles && CurrentLevel > 0)
            {
                tile = _Room.GetTile(CurrentLevel - 1, x, z, OutOfLevelBoundsAction.IgnoreTile);
                var belowType = ResultingType(tile);
                if (belowType == Type.Floor)
                    _Room.SetTile(CurrentLevel - 1, x, z, (ushort)Type.Wall, action: OutOfLevelBoundsAction.IgnoreTile);
            }

            return;
        }
    }

    void FixVisualProblems(int x, int z)
    {
        var tile = _Room.GetTile(CurrentLevel, x, z, action: OutOfLevelBoundsAction.IgnoreTile);
        Type tileType = ResultingType(tile);

        if (tileType == Type.Wall || tileType == Type.AutoNonFloor)
        {
            if (tileType == Type.AutoNonFloor)
            {
                tile = _Room.GetTile(CurrentLevel, x, z, OutOfLevelBoundsAction.IgnoreTile);
                var resultType = ResultingType(tile);
                if (resultType != Type.Wall)
                    return;
            }
            if (CurrentLevel + 1 < _Room.Levels.Count)
            {
                tile = _Room.GetTile(CurrentLevel + 1, x, z, OutOfLevelBoundsAction.IgnoreTile);
                if (tile == ushort.MaxValue)
                    return;
                var aboveType = OriginType(tile);

                if (aboveType == Type.Empty)
                {
                    aboveType = Type.Floor;
                    _Room.SetTile(CurrentLevel + 1, x, z, (ushort)aboveType, action: OutOfLevelBoundsAction.IgnoreTile);
                }
            }
            return;
        }

        if (tileType == Type.Empty)
        {
            if (CurrentLevel - 1 < 0)
                return;
            tile = _Room.GetTile(CurrentLevel - 1, x, z, OutOfLevelBoundsAction.IgnoreTile);
            if (tile == ushort.MaxValue)
                return;

            var belowType = OriginType(tile);
            var belowResultType = ResultingType(tile);

            if (belowType != Type.Wall && belowResultType != Type.Wall)
                return;

            bool eraseWall = false;
            if (CurrentLevel - 2 >= 0)
            {
                tile = _Room.GetTile(CurrentLevel - 2, x, z, OutOfLevelBoundsAction.IgnoreTile);
                if (tile != ushort.MaxValue)
                {
                    belowType = OriginType(tile);
                    belowResultType = ResultingType(tile);
                }
                if (belowType != Type.Wall && belowResultType != Type.Wall)
                    eraseWall = true;
            }

            if (eraseWall)
                _Room.SetTile(CurrentLevel - 1, x, z, (ushort)Type.AutoNonFloor, action: OutOfLevelBoundsAction.IgnoreTile);
            else
                _Room.SetTile(CurrentLevel - 1, x, z, (ushort)Type.Floor, action: OutOfLevelBoundsAction.IgnoreTile);
        }
    }

    private void ModifySurroundingLevel(int x, int z)
    {
        if (!modifySurroundingLevels)
            return;

        FixVisibility(x, z);
        FixVisualProblems(x, z);
        FillOneSpaceTiles(x, z);
    }
}
