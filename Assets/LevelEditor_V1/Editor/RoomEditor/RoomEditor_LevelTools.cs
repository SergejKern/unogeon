using UnityEditor;
using UnityEngine;

using static Core.Editor.Utility.CustomGUI;

public partial class RoomEditor : EditorWindow
{

    void LevelGUI()
    {
        // how many levels does the room have
        using (var hs = new GUILayout.HorizontalScope())
        {
            int levels = EditorGUILayout.IntField("Levels", _Room.Levels.Count);
            if (GUILayout.Button("+", GUILayout.Width(50))) { levels++; }
            if (GUILayout.Button("-", GUILayout.Width(50))) { levels--; }
            ChangeLevelCount(levels);

            GUILayout.Space(15.0f);
            if (GUILayout.Button("^", GUILayout.Width(25))) { MoveLevelUp(); }
            if (GUILayout.Button("v", GUILayout.Width(25))) { MoveLevelDown(); }
            if (GUILayout.Button("x", GUILayout.Width(25))) { DeleteLevel(); }
        }

        using (var hs = new GUILayout.HorizontalScope())
        {
            EditorGUILayout.LabelField($"Current Level {CurrentLevel}", GUILayout.Width(125.0f));
            for (int i = 0; i < _Room.Levels.Count; ++i)
            {
                if (ActivityButton(CurrentLevel == i, i.ToString()))
                    SetCurrentLevel(i);
            }
        }
    }

    void UpdateLevelEditCollider()
    {
        var collider = _RoomObject.GetComponent<BoxCollider>();
        if (collider == null)
            collider = _RoomObject.AddComponent<BoxCollider>();

        var level = _Room.Levels[CurrentLevel];
        collider.center = new Vector3(level.Position.x + level.Size.x / 2, CurrentLevel, level.Position.y + level.Size.y / 2);
        collider.size = new Vector3(_Room.Levels[CurrentLevel].Size.x, 0.01f, _Room.Levels[CurrentLevel].Size.y);
        _RoomObject.layer = (int)Unity.Layer.EditorCollider;
    }

    void SetCurrentLevel(int currentLevel, bool recordUndo = true)
    {
        if (recordUndo)
            Undo.RecordObject(this, "Current Level");

        CurrentLevel = Mathf.Clamp(currentLevel, 0, _Room.Levels.Count - 1);
        _Room.RoomComponent.SetLevel(CurrentLevel);

        UpdateLevelEditCollider();
    }

    void MoveLevel(int a, int b)
    {
        var aLvl = _Room.Levels[a];
        var bLvl = _Room.Levels[b];
        _Room.Levels[a] = bLvl;
        _Room.Levels[b] = aLvl;
    }

    void MoveLevelUp()
    {
        if (CurrentLevel >= _Room.Levels.Count - 1)
            return;

        MoveLevel(CurrentLevel, CurrentLevel + 1);

        if (CurrentLevel > 0)
            _Room.UpdateResulting(CurrentLevel - 1);
        _Room.UpdateResulting(CurrentLevel);
        _Room.UpdateResulting(CurrentLevel + 1);

        UpdateVisuals();

        SetCurrentLevel(CurrentLevel + 1);
    }

    void MoveLevelDown()
    {
        if (CurrentLevel <= 0 || _Room.Levels.Count <= 1)
            return;

        MoveLevel(CurrentLevel, CurrentLevel - 1);

        if (CurrentLevel > 1)
            _Room.UpdateResulting(CurrentLevel - 2);
        _Room.UpdateResulting(CurrentLevel - 1);
        _Room.UpdateResulting(CurrentLevel);

        UpdateVisuals();
        SetCurrentLevel(CurrentLevel - 1);
        //if (CurrentLevel > 0)
        //    _Room.UpdateResulting(CurrentLevel - 1);
    }

    void DeleteLevel()
    {
        if (_Room.Levels.Count <= 1)
            return;

        _Room.Levels.RemoveAt(CurrentLevel);
        SetCurrentLevel(Mathf.Clamp(CurrentLevel - 1, 0, _Room.Levels.Count));

        if (CurrentLevel > 0)
            _Room.UpdateResulting(CurrentLevel - 1);
        _Room.UpdateResulting(CurrentLevel);
        if (_Room.Levels.Count > CurrentLevel + 1)
            _Room.UpdateResulting(CurrentLevel + 1);

        UpdateVisuals();
    }

    void ChangeLevelCount(int newLevelCount)
    {
        int prevCount = _Room.Levels.Count;

        // how many levels does the room have
        newLevelCount = Mathf.Clamp(newLevelCount, 1, _Room.MaxLevel);
        if (newLevelCount == _Room.Levels.Count)
            return;

        var currlvl = Mathf.Clamp(CurrentLevel, 0, newLevelCount - 1);
        if (CurrentLevel != currlvl)
        {
            Undo.RecordObjects(new Object[] { this, _Room }, "Levels resized");
            CurrentLevel = currlvl;
        }
        else Undo.RecordObject(_Room, "Levels resized");

        RoomOperations.ChangeLevelCount(_Room, newLevelCount);

        UpdateVisuals();
    }

}
