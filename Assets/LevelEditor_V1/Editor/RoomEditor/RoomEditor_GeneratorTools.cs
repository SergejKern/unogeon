using UnityEditor;
using UnityEditor.AnimatedValues;
using UnityEngine;
using UnityEngine.Events;

using static RoomMeshOperations;
using static RoomOperations;
using static Tiles;
using static CustomStyles;

public partial class RoomEditor : EditorWindow
{
    AnimBool showGeneratorTools = new AnimBool();

    string seed;
    bool useRandomSeed;

    [Range(0, 100)]
    int randomFillPercent;

    int wallRegionsTooSmallThreshold;
    int areaRegionTooSmallThreshold;

    int smoothWallThreshold;
    int smoothGroundThreshold;

    #region Editor Prefs
    const string kEditorPref_ShowGeneratorTools = "RoomEditor.ShowGeneratorTools";
    const string kEditorPref_UseRandomSeed = "RoomEditor.UseRandomSeed";
    const string kEditorPref_Seed = "RoomEditor.Seed";
    const string kEditorPref_RandomFillPercent = "RoomEditor.RandomFillPercent";
    const string kEditorPref_WallRegionsTooSmallThreshold = "RoomEditor.WallRegionsTooSmallThreshold";
    const string kEditorPref_AreaRegionTooSmallThreshold = "RoomEditor.AreaRegionTooSmallThreshold";
    const string kEditorPref_SmoothWallThreshold = "RoomEditor.SmoothWallThreshold";
    const string kEditorPref_SmoothGroundThreshold = "RoomEditor.SmoothGroundThreshold";
    #endregion

    void Init_GeneratorTools()
    {
        showGeneratorTools.valueChanged.RemoveAllListeners();
        showGeneratorTools.valueChanged.AddListener(new UnityAction(Repaint));

        showGeneratorTools.target = EditorPrefs.GetBool(kEditorPref_ShowGeneratorTools, false);

        useRandomSeed = EditorPrefs.GetBool(kEditorPref_UseRandomSeed, true);
        seed = EditorPrefs.GetString(kEditorPref_Seed, "");

        randomFillPercent = EditorPrefs.GetInt(kEditorPref_RandomFillPercent, 50);
        wallRegionsTooSmallThreshold = EditorPrefs.GetInt(kEditorPref_WallRegionsTooSmallThreshold, 0);
        areaRegionTooSmallThreshold = EditorPrefs.GetInt(kEditorPref_AreaRegionTooSmallThreshold, 0);
        smoothWallThreshold = EditorPrefs.GetInt(kEditorPref_SmoothWallThreshold, 4);
        smoothGroundThreshold = EditorPrefs.GetInt(kEditorPref_SmoothGroundThreshold, 4);
    }

    void GeneratorTools()
    {
        using (var check = new EditorGUI.ChangeCheckScope())
        {
            using (var vs = new GUILayout.VerticalScope(EditorStyles.helpBox, GUILayout.Width(position.size.x - 22)))
            {
                using (var hs = new GUILayout.HorizontalScope())
                {
                    showGeneratorTools.target = EditorGUILayout.Foldout(showGeneratorTools.target, "Generator Tools", true, MiniFoldoutStyle);
                    if (GUILayout.Button("Reset Default Values", GUILayout.Width(150)))
                    {
                        EditorPrefs.DeleteKey(kEditorPref_UseRandomSeed);
                        EditorPrefs.DeleteKey(kEditorPref_Seed);
                        EditorPrefs.DeleteKey(kEditorPref_RandomFillPercent);
                        EditorPrefs.DeleteKey(kEditorPref_WallRegionsTooSmallThreshold);
                        EditorPrefs.DeleteKey(kEditorPref_AreaRegionTooSmallThreshold);
                        EditorPrefs.DeleteKey(kEditorPref_SmoothWallThreshold);
                        EditorPrefs.DeleteKey(kEditorPref_SmoothGroundThreshold);
                        Init_GeneratorTools();
                    }
                }

                using (var fade = new EditorGUILayout.FadeGroupScope(showGeneratorTools.faded))
                {
                    if (fade.visible)
                        _GeneratorToolsInnerGUI();
                }
            }

            if (check.changed)
            {
                EditorPrefs.SetBool(kEditorPref_ShowGeneratorTools, showGeneratorTools.target);

                EditorPrefs.SetBool(kEditorPref_UseRandomSeed, useRandomSeed);
                EditorPrefs.SetString(kEditorPref_Seed, seed);
                EditorPrefs.SetInt(kEditorPref_RandomFillPercent, randomFillPercent);
                EditorPrefs.SetInt(kEditorPref_WallRegionsTooSmallThreshold, wallRegionsTooSmallThreshold);
                EditorPrefs.SetInt(kEditorPref_AreaRegionTooSmallThreshold, areaRegionTooSmallThreshold);
                EditorPrefs.SetInt(kEditorPref_SmoothWallThreshold, smoothWallThreshold);
                EditorPrefs.SetInt(kEditorPref_SmoothGroundThreshold, areaRegionTooSmallThreshold);

                MeshOperationData.Room = _Room;
                GenerateMesh(MeshOperationData);
                _Room.UpdateResulting(CurrentLevel);
            }
        }
    }

    void _GeneratorToolsInnerGUI()
    {
        using (var vs2 = new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            using (var hs = new GUILayout.HorizontalScope())
            {
                if (GUILayout.Button("Random-Fill"))
                {
                    UnityEditor.Undo.RecordObject(_Room, "Random Fill");
                    RandomFillRoomLevel(_Room, CurrentLevel, randomFillPercent, useRandomSeed ? null : seed);
                }
                using (var disableScope = new EditorGUI.DisabledScope(useRandomSeed))
                {
                    GUILayout.Label("Seed: ", GUILayout.Width(50));
                    seed = EditorGUILayout.TextField(seed);
                }
                useRandomSeed = EditorGUILayout.Toggle("Use Random Seed", useRandomSeed);
            }
            randomFillPercent = EditorGUILayout.IntSlider("Fill Percentage: ", randomFillPercent, 0, 100);
        }

        GUILayout.Space(5.0f);
        //GUILayout.BeginVertical(EditorStyles.helpBox);
        using (var hs = new GUILayout.HorizontalScope(EditorStyles.helpBox))
        {
            if (GUILayout.Button("Smooth"))
            {
                Undo.RecordObject(_Room, "Smooth");
                SmoothMap(_Room, CurrentLevel, smoothWallThreshold, smoothGroundThreshold);
            }
            float smoothGT = smoothGroundThreshold;
            float smoothWT = smoothWallThreshold;
            EditorGUILayout.MinMaxSlider("Ground|Wall Threshold: ", ref smoothGT, ref smoothWT, 0, 8);
            smoothGroundThreshold = Mathf.RoundToInt(smoothGT);
            smoothWallThreshold = Mathf.RoundToInt(smoothWT);
        }
        //GUILayout.EndVertical();
        GUILayout.Space(5.0f);
        using (var vs2 = new GUILayout.VerticalScope(EditorStyles.helpBox))
        {
            using (var hs = new GUILayout.HorizontalScope())
            {
                if (GUILayout.Button("Destroy Small WallRegions"))
                {
                    Undo.RecordObject(_Room, "Destroy Small WallRegions");
                    DestroyRegionsThatAreTooSmall(_Room, CurrentLevel, Type.AutoNonFloor, Type.Floor, wallRegionsTooSmallThreshold);
                }
                GUILayout.Label("Threshold: ", GUILayout.Width(75));
                wallRegionsTooSmallThreshold = EditorGUILayout.IntSlider(wallRegionsTooSmallThreshold, 0, 50);
            }
            using (var hs = new GUILayout.HorizontalScope())
            {
                if (GUILayout.Button("Destroy Small FloorRegions"))
                {
                    Undo.RecordObject(_Room, "Destroy Small FloorRegions");
                    DestroyRegionsThatAreTooSmall(_Room, CurrentLevel, Type.Floor, Type.AutoNonFloor, areaRegionTooSmallThreshold);
                }
                GUILayout.Label("Threshold: ", GUILayout.Width(75));
                areaRegionTooSmallThreshold = EditorGUILayout.IntSlider(areaRegionTooSmallThreshold, 0, 50);
            }
        }
        GUILayout.Space(5.0f);

        if (GUILayout.Button("Connect Areas", GUILayout.Width(100)))
        {
            Undo.RecordObject(_Room, "Connect Areas");
            ConnectClosestAreas(_Room, CurrentLevel);
        }
    }

}
