using Core;
using Core.Unity.Extensions;
using UnityEditor;
using UnityEditor.AnimatedValues;
using UnityEngine;
using UnityEngine.Events;

using static Core.Editor.Utility.CustomGUI;
using static Global.Constants;
using static RoomOperations;
using static Tiles;
using static CustomStyles;

public partial class RoomEditor : EditorWindow
{
    #region Editor Prefs
    const string kEditorPref_ShowSizeTools = "RoomEditor.ShowSizeTools";
    const string kEditorPref_SizeToolsAffectRoom = "RoomEditor.SizeToolsAffectRoom";
    #endregion

    AnimBool showSizeTools = new AnimBool();
    bool sizeToolsAffectRoom;

    void Init_SizePositionTools ()
    {
        showSizeTools.valueChanged.RemoveAllListeners();
        showSizeTools.valueChanged.AddListener(new UnityAction(Repaint));

        showSizeTools.target = EditorPrefs.GetBool(kEditorPref_ShowSizeTools, false);
        sizeToolsAffectRoom = EditorPrefs.GetBool(kEditorPref_SizeToolsAffectRoom, false);
    }

    void SizeTools()
    {
        Vector2Int sizeModifier = Vector2Int.zero;
        Vector2Int posModifier = Vector2Int.zero;
        Vector2Int tileModifier = Vector2Int.zero;
        bool specialCase = false;

        using (var vs = new GUILayout.VerticalScope(EditorStyles.helpBox, GUILayout.Width(position.size.x -22)))
        {
            bool show = EditorGUILayout.Foldout(showSizeTools.target, "Size/Position Tools", true, MiniFoldoutStyle);
            if (show != showSizeTools.target)
            {
                showSizeTools.target = show;
                EditorPrefs.SetBool(kEditorPref_ShowSizeTools, showSizeTools.target);
            }

            using (var fade = new EditorGUILayout.FadeGroupScope(showSizeTools.faded))
            {
                if (fade.visible)
                {
                    _SizeToolsInnerGUI(ref sizeModifier, ref posModifier, ref tileModifier, ref specialCase);
                }
            }
        }

        if (Equals(Vector2Int.zero, posModifier) && (Equals(Vector2Int.zero, sizeModifier)) && (Equals(Vector2Int.zero, tileModifier)))
            return;

        Undo.RecordObject(_Room, "Position/Size Tool");
        if (sizeToolsAffectRoom)
        {
            if (!Equals(Vector2Int.zero, sizeModifier))
            {
                sizeModifier *= RoomGridSize;
                posModifier *= RoomGridSize;
                tileModifier *= RoomGridSize;
            }

            var newSize = _Room.Size + sizeModifier.Vector3Int();
            if (newSize.x <= 0 || newSize.z <= 0)
                return;

            _Room.Size = newSize;
            _Room.SetRoomPositioning(Vector3.zero);

            if (specialCase)
            {
                posModifier = sizeModifier;
                tileModifier = Vector2Int.zero;
            }

            foreach (var lvl in _Room.Levels)
            {
                ApplySizeTool(lvl, Vector2Int.zero, posModifier, tileModifier);
            }
        }
        else if (CurrentLevel >= 0 && CurrentLevel < _Room.Levels.Count)
        {
            if (specialCase)
                tileModifier = (posModifier * -1);

            ApplySizeTool(_Room.Levels[CurrentLevel], sizeModifier, posModifier, tileModifier);
        }

        _Room.UpdateResulting(CurrentLevel);
        if (CurrentLevel > 0)
            _Room.UpdateResulting(0);
        if (_Room.Levels.Count > (CurrentLevel + 1))
            _Room.UpdateResulting(CurrentLevel + 1);

        UpdateVisuals();
    }

    private void _SizeToolsInnerGUI(ref Vector2Int sizeModifier, ref Vector2Int posModifier, ref Vector2Int tileModifier, ref bool specialCase)
    {
        using (var hs = new GUILayout.HorizontalScope())
        {
            bool affectRoom = sizeToolsAffectRoom;
            if (ActivityButton(sizeToolsAffectRoom, "Room"))
                affectRoom = true;
            if (ActivityButton(!sizeToolsAffectRoom, "Level"))
                affectRoom = false;
            if (affectRoom != sizeToolsAffectRoom)
            {
                sizeToolsAffectRoom = affectRoom;
                EditorPrefs.SetBool(kEditorPref_SizeToolsAffectRoom, sizeToolsAffectRoom);
            }
        }

        using (var arrowButtons = new GUILayout.HorizontalScope())
        {
            using (var sizeButtons = new GUILayout.VerticalScope())
            {
                using (var hs = new GUILayout.HorizontalScope())
                {
                    GUILayout.Space(57);
                    if (GUILayout.Button("^", GUILayout.Width(20))) { sizeModifier = Vector2Int.up; }
                }
                using (var hs = new GUILayout.HorizontalScope())
                {
                    GUILayout.Space(57);
                    if (GUILayout.Button("v", GUILayout.Width(20))) { sizeModifier = Vector2Int.down; }
                }
                using (var hs = new GUILayout.HorizontalScope())
                {
                    if (GUILayout.Button("<", GUILayout.Width(20))) { sizeModifier = Vector2Int.right; posModifier = Vector2Int.left; specialCase = true; }
                    if (GUILayout.Button(">", GUILayout.Width(20))) { sizeModifier = Vector2Int.left; posModifier = Vector2Int.right; specialCase = true; }
                    GUILayout.Label("Size", GUILayout.Width(30));
                    if (GUILayout.Button("<", GUILayout.Width(20))) { sizeModifier = Vector2Int.left; }
                    if (GUILayout.Button(">", GUILayout.Width(20))) { sizeModifier = Vector2Int.right; }
                }
                using (var hs = new GUILayout.HorizontalScope())
                {
                    GUILayout.Space(57);
                    if (GUILayout.Button("^", GUILayout.Width(20))) { sizeModifier = Vector2Int.down; posModifier = Vector2Int.up; specialCase = true; }
                }
                using (var hs = new GUILayout.HorizontalScope())
                {
                    GUILayout.Space(57);
                    if (GUILayout.Button("v", GUILayout.Width(20))) { sizeModifier = Vector2Int.up; posModifier = Vector2Int.down; specialCase = true; }
                }
            }

            //GUILayout.Space(100);

            using (var positonButtons = new GUILayout.VerticalScope())
            {
                GUILayout.Space(14);

                using (var hs = new GUILayout.HorizontalScope())
                {
                    GUILayout.Space(42);
                    if (GUILayout.Button("^", GUILayout.Width(20))) { posModifier = Vector2Int.up; }
                }
                GUILayout.Space(10);

                using (var hs = new GUILayout.HorizontalScope())
                {
                    if (GUILayout.Button("<", GUILayout.Width(20))) { posModifier = Vector2Int.left; }
                    GUILayout.Label("Position", GUILayout.Width(50));
                    if (GUILayout.Button(">", GUILayout.Width(20))) { posModifier = Vector2Int.right; }
                }
                GUILayout.Space(10);

                using (var hs = new GUILayout.HorizontalScope())
                {
                    GUILayout.Space(42);
                    if (GUILayout.Button("v", GUILayout.Width(20))) { posModifier = Vector2Int.down; }
                }
            }

            using (var tileShiftButtons = new GUILayout.VerticalScope())
            {
                GUILayout.Space(14);

                using (var hs = new GUILayout.HorizontalScope())
                {
                    GUILayout.Space(42);
                    if (GUILayout.Button("^", GUILayout.Width(20))) { tileModifier = Vector2Int.up; }
                }
                GUILayout.Space(10);

                using (var hs = new GUILayout.HorizontalScope())
                {
                    if (GUILayout.Button("<", GUILayout.Width(20))) { tileModifier = Vector2Int.left; }
                    GUILayout.Label("  Tiles", GUILayout.Width(50));
                    if (GUILayout.Button(">", GUILayout.Width(20))) { tileModifier = Vector2Int.right; }
                }
                GUILayout.Space(10);

                using (var hs = new GUILayout.HorizontalScope())
                {
                    GUILayout.Space(42);
                    if (GUILayout.Button("v", GUILayout.Width(20))) { tileModifier = Vector2Int.down; }
                }
            }
        }
        GUILayout.Space(10);
    }

    private void ApplySizeTool(Room.Level lvl, Vector2Int sizeModifier, Vector2Int offsetModifier, Vector2Int tileModifier)
    {
        var newSize = lvl.TileDim + sizeModifier;
        var newPos = lvl.Position + offsetModifier;

        // level cannot begin before room begins, shrink size, add offset
        if (newPos.x < 0)
        {
            tileModifier += Vector2Int.right * newPos.x;
            newSize = new Vector2Int(Mathf.Max(0, newSize.x + newPos.x), newSize.y);
            newPos = new Vector2Int(0, newPos.y);
        }
        if (newPos.y < 0)
        {
            tileModifier += Vector2Int.up * newPos.y;
            newSize = new Vector2Int(newSize.x, Mathf.Max(0, newSize.y + newPos.y));
            newPos = new Vector2Int(newPos.x, 0);
        }

        // level begins outside of room or a size-dimension is 0 -> empty level
        if (newPos.x >= _Room.TileDim.x || newPos.y >= _Room.TileDim.y
            || newSize.x <= 0 || newSize.y <= 0)
        {
            lvl.Position = Vector2Int.zero;
            lvl.Size = Vector2Int.one;
            lvl.Tiles = new ushort[lvl.TileDim.x * lvl.TileDim.y];
            return;
        }

        // level is not allowed to extend the roomSize, lvl is cut:
        var newExtends = newPos + newSize;
        if (newExtends.x > _Room.TileDim.x)
        {
            newSize = new Vector2Int(_Room.TileDim.x - newPos.x, newSize.y);
        }
        if (newExtends.y > _Room.TileDim.y)
        {
            newSize = new Vector2Int(newSize.x, _Room.TileDim.y - newPos.y);
        }

        ushort[] newTiles = new ushort[newSize.x * newSize.y];
        for (int x = 0; x < newSize.x; ++x)
        {
            for (int z = 0; z < newSize.y; ++z)
            {
                var oldX = x - tileModifier.x;
                var oldZ = z - tileModifier.y;
                var idx = z * newSize.x + x;

                if (!lvl.IsInside(oldX, oldZ))
                {
                    newTiles[idx] = (ushort)Type.AutoNonFloor;
                    continue;
                }
                newTiles[idx] = lvl.GetTile(oldX, oldZ, OutOfLevelBoundsAction.IgnoreTile);
            }
        }
        lvl.Position = newPos;
        lvl.Size = newSize- Vector2Int.one;
        lvl.Tiles = newTiles;
    }
}
