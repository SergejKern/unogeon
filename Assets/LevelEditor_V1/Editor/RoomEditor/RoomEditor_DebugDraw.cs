using Core;
using Core.Editor.Utility;
using Core.Extensions;
using Core.Types;
using Core.Unity.Extensions;
using UnityEditor;
using UnityEngine;
using static Tiles;

public partial class RoomEditor : EditorWindow
{
    #region constants
    const float kDebugTileSize = 0.10f;
    static Color DebugTile_Empty = new Color(1.0f, 0.0f, 0.0f, 0.5f);
    static Color DebugTile_Auto = new Color(1.0f, 0.0f, 1.0f, 0.5f);
    static Color DebugTile_Wall = new Color(0.0f, 0.0f, 1.0f, 0.5f);
    static Color DebugTile_Floor = new Color(0.0f, 1.0f, 0.0f, 0.5f);
    #endregion

    bool debugDrawGeometryTileType;

    void DebugDrawGUI()
    {
        using (var hs = new GUILayout.HorizontalScope())
        {
            GUILayout.Label("Debug Draw Geometry Type");
            debugDrawGeometryTileType = EditorGUILayout.Toggle(debugDrawGeometryTileType);
        }
    }

    void SceneGUI_DebugDraw(SceneView sceneView, Event e)
    {
        DrawExits();
        if (e.type == EventType.Repaint)
        {
            DrawRoomAndLevelBounds();
            if (debugDrawGeometryTileType)
                DrawGeometryType();
        }
    }

    private void DrawRoomAndLevelBounds()
    {
        CustomHandleDraw.DrawBounds(_Room.Bounds, Color.magenta);
        CustomHandleDraw.DrawBounds(_Room.LevelBounds(CurrentLevel), Color.green);
    }

    void DrawExits()
    {
        if (_Room.OpenExits.IsNullOrEmpty())
            return;

        var prevCol = Handles.color;
        Handles.color = Color.green;
        for (int i = 0; i < _Room.OpenExits.Count; ++i)
        {
            var exit = _Room.OpenExits[i];
            float arrPosX = 0.0f, arrPosZ = 0.0f;
            Vector3 arrDir = _Room.OpenExits[i].ExitDirection.ToVector3();
            var halfArrowSize = 0.5f * (exit.toIdx - exit.fromIdx) + 0.5f;
            var centerPos = exit.fromIdx + halfArrowSize - 0.5f;
            float xInf = 0.0f, zInf = 0.0f; // influence

            switch (_Room.OpenExits[i].ExitDirection)
            {
                case Cardinals.North: arrPosX = centerPos; arrPosZ = _Room.Size.z; xInf = 1.0f; break;
                case Cardinals.East: arrPosX = _Room.Size.x; arrPosZ = centerPos; zInf = 1.0f; break;
                case Cardinals.South: arrPosX = centerPos; arrPosZ = 0; xInf = 1.0f; break;
                case Cardinals.West: arrPosX = 0; arrPosZ = centerPos; zInf = 1.0f; break;
            }
            var arrPos = new Vector3(arrPosX, exit.level, arrPosZ);
            var halfVec = new Vector3(xInf * halfArrowSize, 0.0f, zInf * halfArrowSize);

            Handles.DrawLine(arrPos, arrPos + arrDir);
            Handles.DrawLine(arrPos - halfVec, arrPos + arrDir);
            Handles.DrawLine(arrPos + halfVec, arrPos + arrDir);

            //Handles.DrawLine(posVec3 + Vector3.right * 0.5f, posVec3 + Vector3.right * 0.5f + Vector3.forward);
        }
        Handles.color = prevCol;
    }

    private void DrawGeometryType()
    {
        var handleCol = Handles.color;

        var lvl = _Room.Levels[CurrentLevel];
        for (int x = 0; x < lvl.TileDim.x; ++x)
        {
            for (int z = 0; z < lvl.TileDim.y; ++z)
            {
                var rx = x + lvl.Position.x;
                var rz = z + lvl.Position.y;

                var tile = lvl.GetTile(x, z);
                switch (OriginType(tile))
                {
                    case Type.AutoNonFloor:
                        Handles.color = DebugTile_Auto; break;
                    case Type.Empty:
                        Handles.color = DebugTile_Empty; break;
                    case Type.Floor:
                        Handles.color = DebugTile_Floor; break;
                    case Type.Wall:
                        Handles.color = DebugTile_Wall; break;
                }

                Handles.DotHandleCap(-1, new Vector3(rx, CurrentLevel, rz), Quaternion.identity, kDebugTileSize, EventType.Repaint);
            }
        }
        Handles.color = handleCol;
    }
}
