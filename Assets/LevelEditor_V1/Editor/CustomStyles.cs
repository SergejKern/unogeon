﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/// <summary>
/// Custom UnityEditor GUIStyles
/// </summary>
public static class CustomStyles
{
    static GUIStyle _miniFoldoutStyle;
    /// <summary>
    /// Style for foldouts
    /// </summary>
    public static GUIStyle MiniFoldoutStyle {
        get
        {
            if (_miniFoldoutStyle == null)
            {
                _miniFoldoutStyle = new GUIStyle(EditorStyles.foldout);
                _miniFoldoutStyle.fontSize = 9;
            }
            return _miniFoldoutStyle;
        }
    }

    static GUIStyle _selectableLabel;
    /// <summary>
    /// Style for SelectableLabel
    /// </summary>
    public static GUIStyle SelectableLabel
    {
        get
        {
            if (_selectableLabel != null)
                return _selectableLabel;

            _selectableLabel = new GUIStyle(EditorStyles.label);
            var state = new GUIStyleState();
            state.background = new Texture2D(1,1);

            state.background.SetPixel(0, 0, new Color(0.5f, 0.5f, 0.75f, 1.0f));
            state.background.Apply();

            state.textColor = Color.white;
            _selectableLabel.onNormal = state;
            return _selectableLabel;
        }
    }

    static GUIStyle _debugBreakToggle;
    /// <summary>
    /// Red Breakpoint-icon toggle
    /// </summary>
    public static GUIStyle BreakpointToggle {
        get
        {
            if (_debugBreakToggle != null)
                return _debugBreakToggle;

            _debugBreakToggle = new GUIStyle();
            var breakOn = (Texture2D) EditorGUIUtility.Load("breakpoint.png");
            var breakOff = (Texture2D)EditorGUIUtility.Load("breakpointoff.png");

            var stateOn = new GUIStyleState();
            stateOn.background = breakOn;
            var stateOff = new GUIStyleState();
            stateOff.background = breakOff;

            _debugBreakToggle.onNormal = stateOn;
            _debugBreakToggle.onActive = stateOn;
            _debugBreakToggle.onHover = stateOn;
            _debugBreakToggle.onFocused = stateOn;
            _debugBreakToggle.normal = stateOff;
            _debugBreakToggle.active = stateOff;
            _debugBreakToggle.hover = stateOff;
            _debugBreakToggle.focused = stateOff;

            _debugBreakToggle.stretchHeight = false;
            _debugBreakToggle.stretchWidth = false;
            _debugBreakToggle.fixedWidth = 16;
            _debugBreakToggle.fixedHeight = 16;

            return _debugBreakToggle;
        }
    }

}
