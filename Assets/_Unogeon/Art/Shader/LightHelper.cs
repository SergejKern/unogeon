﻿using System.Collections.Generic;
using System.Linq;
using Core.Types;
using GameUtility.Data.Visual;
using UnityEngine;

namespace ShaderScripts
{
    //[ExecuteInEditMode]
    public class LightHelper : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        // Params
        [SerializeField] Vector3 m_meshCenter = Vector3.zero;
        [SerializeField] int m_maxLights = 6;

        [Header("Receive Shadow Check")]
        [SerializeField] bool m_raycast = true;
        [SerializeField] LayerMask m_raycastMask;
        [SerializeField] float m_raycastFadeSpeed = 10f;

        [SerializeField] bool m_drawRay;

#pragma warning restore 0649 // wrong warnings for SerializeField

        // State
        Vector3 m_posAbs;
        readonly Dictionary<int, LightSet> m_lightSets = new Dictionary<int, LightSet>();

        // Refs
        Material[] m_materialInstance;

        void Start()
        {
            Init();
            GetLights();
        }

        //void OnValidate()
        //{
        //    Init();
        //    Update();
        //}

        IMaterialInstanceProvider GetInstanceProvider()
        {
            if (!TryGetComponent(out IMaterialInstanceProvider mip))
                mip = gameObject.AddComponent<MaterialInstanceProvider>();
            return mip;
        }

        void Init()
        {
            if (!TryGetComponent(out Renderer _))
                return;

            var provider = GetInstanceProvider();
            provider.GetInstances(out _, out m_materialInstance);
        }

        // NOTE: If your game loads lights dynamically, this should be called to init new lights
        public void GetLights()
        {
            var lights = FindObjectsOfType<Light>();

            using (var newIdsScoped = SimplePool<List<int>>.I.GetScoped())
            {
                var newIds = newIdsScoped.Obj;

                // Initialize new lights
                foreach (var l in lights)
                {
                    var id = l.GetInstanceID();
                    newIds.Add(id);
                    if (!m_lightSets.ContainsKey(id)) 
                        m_lightSets.Add(id, new LightSet(l));
                }

                // Remove old lights
                var oldIds = new List<int>(m_lightSets.Keys);
                foreach (var id in oldIds)
                {
                    if (!newIds.Contains(id)) 
                        m_lightSets.Remove(id);
                }
            }
        }

        void Update()
        {
            m_posAbs = transform.position + m_meshCenter;

            // Always update lighting while in editor
            //if (Application.isEditor && !Application.isPlaying) 
                GetLights();

            UpdateMaterial();
        }

        void UpdateMaterial()
        {
            if (m_materialInstance == null) 
                return;

            // Refresh light data
            using (var sortedLightsScoped = SimplePool<List<LightSet>>.I.GetScoped())
            {
                var sortedLights = sortedLightsScoped.Obj;

                sortedLights.AddRange(m_lightSets.Values.Select(CalcLight));

                // Sort lights by brightness
                sortedLights.Sort((x, y) =>
                {
                    var yBrightness = y.Color.grayscale * y.Attenuation;
                    var xBrightness = x.Color.grayscale * x.Attenuation;
                    return yBrightness.CompareTo(xBrightness);
                });

                // Apply lighting
                var i = 1;
                foreach (var lightSet in sortedLights)
                {
                    if (i > m_maxLights) 
                        break;
                    if (lightSet.Attenuation <= Mathf.Epsilon) 
                        break;

                    // Use color Alpha to pass attenuation data
                    var color = lightSet.Color;
                    color.a = Mathf.Clamp(lightSet.Attenuation, 0.01f, 0.99f); // UV might wrap around if attenuation is >1 or 0<

                    foreach (var inst in m_materialInstance)
                    {
                        inst.SetVector($"_L{i}_dir", lightSet.Dir.normalized);
                        inst.SetColor($"_L{i}_color", color);
                    }

                    i++;
                }

                // Turn off the remaining light slots
                while (i <= m_maxLights)
                {
                    foreach (var inst in m_materialInstance)
                    {
                        inst.SetVector($"_L{i}_dir", Vector3.up);
                        inst.SetColor($"_L{i}_color", Color.black);
                    }
                    i++;
                }

                // Store updated light data
                foreach (var lightSet in sortedLights) 
                    m_lightSets[lightSet.ID] = lightSet;
            }
        }

        LightSet CalcLight(LightSet lightSet)
        {
            var setLight = lightSet.Light;
            var inView = 1.1f;
            float dist;

            if (!setLight.isActiveAndEnabled)
            {
                lightSet.Attenuation = 0f;
                return lightSet;
            }

            switch (setLight.type)
            {
                case LightType.Directional:
                    lightSet.Dir = setLight.transform.forward * -1f;
                    inView = TestInView(lightSet.Dir, 100f);
                    lightSet.Color = setLight.color * setLight.intensity;
                    lightSet.Attenuation = 1f;
                    break;

                case LightType.Point:
                    lightSet.Dir = setLight.transform.position - m_posAbs;
                    dist = Mathf.Clamp01(lightSet.Dir.magnitude / setLight.range);
                    inView = TestInView(lightSet.Dir, lightSet.Dir.magnitude);
                    lightSet.Attenuation = CalcAttenuation(dist);
                    lightSet.Color = setLight.color * lightSet.Attenuation * setLight.intensity * 0.1f;
                    break;

                case LightType.Spot:
                    lightSet.Dir = setLight.transform.position - m_posAbs;
                    dist = Mathf.Clamp01(lightSet.Dir.magnitude / setLight.range);
                    var angle = Vector3.Angle(setLight.transform.forward * -1f, lightSet.Dir.normalized);
                    var inFront = Mathf.Lerp(0f, 1f, (setLight.spotAngle - angle * 2f) / lightSet.Dir.magnitude); // More edge fade when far away from light source
                    inView = inFront * TestInView(lightSet.Dir, lightSet.Dir.magnitude);
                    lightSet.Attenuation = CalcAttenuation(dist);
                    lightSet.Color = setLight.color * lightSet.Attenuation * setLight.intensity * 0.05f;
                    break;

                default:
                    Debug.Log("Lighting type '" + setLight.type + "' not supported (" + setLight.name + ").");
                    lightSet.Attenuation = 0f;
                    break;
            }

            // Slowly fade lights on and off
            var fadeSpeed = (Application.isEditor && !Application.isPlaying)
                ? m_raycastFadeSpeed / 60f
                : m_raycastFadeSpeed * Time.deltaTime;

            lightSet.InView = Mathf.Lerp(lightSet.InView, inView, fadeSpeed);
            lightSet.Color *= Mathf.Clamp01(lightSet.InView);

            return lightSet;
        }

        float TestInView(Vector3 dir, float dist)
        {
            if (!m_raycast) 
                return 1.1f;

            if (Physics.Raycast(m_posAbs, dir, out var hit, dist, m_raycastMask))
            {
                if (m_drawRay)
                    Debug.DrawRay(m_posAbs, dir.normalized * hit.distance, Color.red);
                return -0.1f;
            }
            if (m_drawRay)
                Debug.DrawRay(m_posAbs, dir.normalized * dist, Color.green);
            return 1.1f;
        }

        // Ref - Light Attenuation calc: https://forum.unity.com/threads/light-attentuation-equation.16006/#post-3354254
        static float CalcAttenuation(float dist) 
            => Mathf.Clamp01(1.0f / (1.0f + 25f * dist * dist) * Mathf.Clamp01((1f - dist) * 5f));

        void OnDrawGizmosSelected()
            => Gizmos.DrawWireSphere(m_posAbs, 0.1f);
    }

    internal struct LightSet
    {
        public readonly int ID;
        public readonly Light Light;
        public Vector3 Dir;
        public Color Color;
        public float Attenuation;
        public float InView;

        public LightSet(Light newLight)
        {
            Light = newLight;
            ID = newLight.GetInstanceID();
            Dir = Vector3.zero;
            Color = Color.black;
            Color.a = 0.01f;
            Attenuation = 0f;
            InView = 1.1f; // Range -0.1 to 1.1 which is clamped 0-1 for faster consistent fade
        }
    }
}