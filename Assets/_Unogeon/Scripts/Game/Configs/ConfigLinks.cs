using GameUtility.Config;
using GameUtility.Configs;
using GameUtility.InitSystem.Config;
using UnityEngine;

namespace Game.Configs
{
    [CreateAssetMenu(menuName = "Game/ConfigLinks")]
    public class ConfigLinks : ScriptableObject
    {
        public static ConfigLinks Instance;

#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] DebugDrawConfig m_debugDrawConfig;
        [SerializeField] SpawnInitListConfig m_spawnInitConfig;
        [SerializeField] CollisionsConfig m_collisionsConfig;

        [SerializeField] PrefabLinks m_prefabLinks;
        [SerializeField] SceneLinks m_sceneLinks;
        //[SerializeField] WeaponConfig m_weaponConfig;

        [SerializeField] bool m_debugDrawUsedConfigs;
#pragma warning restore 0649 // wrong warnings for SerializeField

        // ReSharper disable UnusedMember.Global
        public static DebugDrawConfig DebugDrawConfig => Instance == null ? null : Instance.m_debugDrawConfig;
        public static SpawnInitListConfig SpawnInitConfig => Instance == null ? null : Instance.m_spawnInitConfig;
        public static CollisionsConfig CollisionsConfig => Instance == null ? null : Instance.m_collisionsConfig;
        // ReSharper restore UnusedMember.Global

        public DebugDrawConfig GetDebugDrawConfig() => m_debugDrawConfig;
        public SpawnInitListConfig GetSpawnInitConfig() => m_spawnInitConfig;
        public CollisionsConfig GetCollisionsConfig() => m_collisionsConfig;
        public PrefabLinks GetPrefabLinks() => m_prefabLinks;
        public SceneLinks GetSceneLinks() => m_sceneLinks;

        public void DrawUsedConfigsGUI()
        {
            if (!m_debugDrawUsedConfigs)
                return;
            
            //using (new GUILayout.HorizontalScope())
            //    GUILayout.Box($"Spawn Config: {m_spawnInitConfig.name}, Weapon Config: {m_weaponConfig.name}");
        }
    }
}
