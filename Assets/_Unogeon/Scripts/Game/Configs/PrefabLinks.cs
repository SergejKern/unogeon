using System;
using GameUtility.GameSetup;
using UnityEngine;

namespace Game.Configs
{
    /// <summary>
    /// PrefabLinks
    /// </summary>
    [CreateAssetMenu(menuName = "Game/PrefabLinks")]
    public class PrefabLinks : ScriptableObject
    {
        public static PrefabLinks Instance { get; internal set; }
        public static PrefabLinks I => Instance;

        [Serializable]
        public struct PrefabLink
        {
            public PrefabID ID;
            public GameObject Prefab;
        }

#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] internal PrefabLink[] m_prefabs = new PrefabLink[0];
#pragma warning restore 0649 // wrong warnings for SerializeField

        public GameObject GetPrefab(PrefabID id) => Array.Find(m_prefabs, s => s.ID == id).Prefab;
        public GameObject GetPrefab(string idName) => Array.Find(m_prefabs, 
            s => string.Equals(s.ID.name, idName)).Prefab;
    }

//#if UNITY_EDITOR
//    [CustomEditor(typeof(PrefabLinks))]
//    public class PrefabLinksEditor : Editor
//    {
//        // ReSharper disable once InconsistentNaming
//        new PrefabLinks target => base.target as PrefabLinks;
//        public override void OnInspectorGUI()
//        {
//            var enumNames = Enum.GetNames(typeof(Prefab));
//            if (target.m_prefabs.Length!= enumNames.Length)
//                Array.Resize(ref target.m_prefabs, enumNames.Length);
//            using (var check = new EditorGUI.ChangeCheckScope())
//            {
//                for (var i = 0; i < enumNames.Length; i++)
//                {
//                    using (new EditorGUILayout.HorizontalScope())
//                    {
//                        GUILayout.Label(enumNames[i], GUILayout.Width(0.33f * Screen.width));
//                        target.m_prefabs[i] = (GameObject) 
//                            EditorGUILayout.ObjectField(target.m_prefabs[i], typeof(GameObject), false);
//                    }
//                }
//                if (check.changed) 
//                    OnChanged();
//            }
//        }
//        void OnChanged()
//        {
//            EditorUtility.SetDirty(target);
//            serializedObject.Update();
//        }
//    }
//#endif
}
