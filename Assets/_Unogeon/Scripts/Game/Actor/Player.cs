using System;
using Core.Interface;
using Core.Unity.Types;
using Core.Unity.Types.Fsm;
using Game.Actors.Controls;
using Game.Actors.Energy;
using Game.Actors.Model_Animation;
using Game.Globals;
using GameUtility.Data.PhysicalConfrontation;
using GameUtility.Energy;
using GameUtility.Events;
using GameUtility.Interface;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;
using UnityEngine.InputSystem;

namespace Game.Actors
{
    public class Player: MonoBehaviour, IPlayer, IDamagedByActorMarker
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [Serializable]
        public struct Events
        {
            public FSMUnityCombinedEvent OnDeathEvent;
            public UnityEvent<GameObject> OnModelSpawned;
            public UnityEvent OnDespawn;
            public UnityEvent OnInputConnected;
        }
        [Serializable]
        public struct Animations
        {
            [AnimTypeDrawer]
            public AnimatorStateRef PlayerDeath;
            [AnimTypeDrawer]
            public AnimatorStateRef DefaultStateRef;
        }

        [SerializeField] ControlCentre m_controlCentre;

        [SerializeField] EnergyCentre m_energyCentre;

        [SerializeField] PrefabRef m_playerModelPrefab;

        [SerializeField] Animations m_animations;
        [SerializeField] Events m_events;

#pragma warning restore 0649

        #region Properties and Provider
        // ReSharper disable ConvertToAutoProperty, ConvertToAutoPropertyWhenPossible
        public IControlCentre Centre => m_controlCentre;
        public PlayerInput Input => m_controlCentre.Input;
        public bool IsBeingControlled => Input != null;
        // ReSharper restore ConvertToAutoProperty, ConvertToAutoPropertyWhenPossible

        // player nr. 0-3
        public int PlayerIdx { get; private set; }

        // ReSharper disable MethodNameNotMeaningful
        public void Get(out EnergyCentre provided) => provided = m_energyCentre;
        // ReSharper restore MethodNameNotMeaningful
        #endregion

        ControlBlocker m_controlBlockerDeath;

        #region Init
        void Awake()
        {
            m_controlBlockerDeath = new ControlBlocker() { Source = this };

            CreateAndLinkModel();

            InitAgent();
        }

        void CreateAndLinkModel()
        {
            if (Centre.Model == null)
                Centre.InitializeModel(Instantiate(m_playerModelPrefab.Prefab, transform));

            if (Centre.Model.TryGetComponent<ModelLink>(out var link))
                link.Connect(gameObject);

            m_events.OnModelSpawned.Invoke(Centre.Model);
        }

        void InitAgent()
        {
            if (!m_controlCentre.TryGetComponent<NavMeshAgent>(out var agent))
                return;

            agent.updatePosition = false;
            agent.updateRotation = false;
        }

        public void SetPlayerIdx(int playerIdx) => PlayerIdx = playerIdx;

        #endregion

        #region IControllable methods
        public void InitializeWithController(PlayerInput input)
        {
            m_controlCentre.InitializeWithController(input);

            GlobalOperations.MovePlayerToRuntimeSceneAndConnectHUD(this);

            // m_playerFXComponent.Connect(this);
            m_events.OnInputConnected.Invoke();
        }

        public void RemoveControl() => m_controlCentre.RemoveControl();
        public void AddControlBlocker(ControlBlocker blocker) => m_controlCentre.AddControlBlocker(blocker);
        public void RemoveControlBlocker(ControlBlocker blocker)=> m_controlCentre.RemoveControlBlocker(blocker);
        //public void RemoveControlBlocker(object source) => m_controlCentre.RemoveControlBlocker(source);
        #endregion
       
        #region IPoolable methods
        public void OnSpawn() => enabled = true;
        public void OnDespawn()
        {
            m_controlCentre.Animator.Play(m_animations.DefaultStateRef);
            m_events.OnDespawn.Invoke();
        }
        #endregion

        public void OnDeath()
        {
            // we don't actually know if last aggressor caused the kill, but we account him for it for the Results
            // (could also be that he fell, because of last attack or so) todo 0: poor solution
            KillEvent.Trigger(gameObject, LastAggressor);

            if (!enabled)
                return;
            enabled = false;

            m_events.OnDeathEvent.Invoke();
            m_controlCentre.Animator.Play(m_animations.PlayerDeath);

            // gameObject.AddComponent<DecomposeBody>();

            AddControlBlocker(m_controlBlockerDeath);
        }

        public GameObject LastAggressor { get; set; }
    }
}