﻿using Core.Unity.Types;
using Game.Actors.Controls;
using GameUtility.Data.EntityAnima;
using GameUtility.Data.Visual;
using GameUtility.Energy;
using UnityEngine;

namespace Game.Actors.Energy
{
    public class SmallStamina : EnergyComponent, IReactToModelInitialized
    {
        [SerializeField] BigStamina m_bigStamina;
        [SerializeField] float m_bigFactor;

        [SerializeField] ControlCentre m_centre;
        [SerializeField] AnimatorFloatRef m_exhaust;
        [SerializeField] MaterialFromToColorMap m_exhaustColor;
        [SerializeField] ParticleSystem m_sweatSystem;

        [SerializeField] float m_repeatBurstInSecondsExhausted;
        [SerializeField] float m_repeatBurstInSecondsAlmostFine;

        float m_burstCount;
        float m_repeatBurstInSeconds;
        float m_burstTimer;

        Renderer[] m_renderer;
        readonly MaterialColorMap[] m_map = new MaterialColorMap[1];

        public override float MaxEnergy => m_bigStamina.EnergyPercentage * m_data.MaxEnergy; 

        protected override void Awake()
        {
            base.Awake();
            m_burstCount = m_sweatSystem.emission.GetBurst(0).count.constant;
        }

        protected override void Update()
        {
            base.Update();

            var exhaustP = 1f - EnergyPercentage;
            m_exhaust.Value = exhaustP;

            Colorize(exhaustP);
            if (exhaustP > float.Epsilon)
                UpdateSweatSystem(exhaustP);
        }

        void UpdateSweatSystem(float exhaustP)
        {
            if (m_sweatSystem == null)
                return;

            m_repeatBurstInSeconds =
                Mathf.Lerp(m_repeatBurstInSecondsAlmostFine, m_repeatBurstInSecondsExhausted, exhaustP);
            m_burstTimer += Time.deltaTime;

            if (m_burstTimer < m_repeatBurstInSeconds) 
                return;

            var emissionModule = m_sweatSystem.emission;
            var burst = emissionModule.GetBurst(0);
            burst.count = Mathf.Lerp(0, m_burstCount, exhaustP);
            emissionModule.SetBurst(0, burst);

            m_sweatSystem.Play();
            m_burstTimer = 0f;
        }

        void Colorize(float exhaustP)
        {
            if (m_renderer == null) 
                return;

            m_map[0] = new MaterialColorMap()
            {
                Color = Color.Lerp(m_exhaustColor.FromColor, m_exhaustColor.ToColor, exhaustP),
                ColorKey = m_exhaustColor.ColorKey,
                Material = m_exhaustColor.Material
            };
            MaterialColorize.Colorize(new MaterialColorizeData()
            {
                MaterialColorMap = m_map,
                Renderer = m_renderer
            });
        }

        public override void Regenerate(float amount)
        {
            var gap = MaxEnergy - Energy;
            amount = Mathf.Min(amount, gap);
            if (amount < float.Epsilon)
                return;
            var bigEnergyReq = m_bigFactor * amount;

            if (m_bigStamina.Energy < bigEnergyReq)
                return;
            
            m_bigStamina.Use(bigEnergyReq);

            base.Regenerate(amount);
        }

        public void OnModelInitialized()
        {
            m_exhaust.Animator = m_centre.Animator;
            m_renderer = m_centre.Model.GetComponentsInChildren<Renderer>();
        }
    }
}