﻿using GameUtility.Data.TransformAttachment;
using GameUtility.Energy;
using UnityEngine;

namespace Game.Actors.Energy
{
    public class FoodEnergy : EnergyComponent, IUpdateAttachmentPoints
    {
        [SerializeField] AttachmentID m_bellyID;
        [SerializeField] Vector3 m_fromScale;
        [SerializeField] Vector3 m_toScale;

        Transform bellyTr;
        public void UpdateAttachmentPoints(AttachmentPointsProvider prov) => bellyTr = prov.GetAttachment(m_bellyID).Parent;


        protected override void Update()
        {
            base.Update();

            if (bellyTr == null)
                return;
            bellyTr.localScale = Vector3.Lerp(m_fromScale, m_toScale, EnergyPercentage);
        }
    }
}
