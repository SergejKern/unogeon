﻿using GameUtility.Data.EntityAnima;
using GameUtility.Data.TransformAttachment;
using GameUtility.Energy;
using UnityEngine;

namespace Game.Actors.Energy
{
    public class BigStamina : EnergyComponent, IUpdateAttachmentPoints
    {
        // todo:
        [SerializeField] bool m_sleeping;
        [SerializeField] FoodEnergy m_foodEnergy;
        [SerializeField] float m_staminaToFoodFactor;
        [SerializeField] AttachmentID m_eyeR_ID;
        [SerializeField] AttachmentID m_eyeL_ID;

        [SerializeField] Vector3 m_eyesOpen;
        [SerializeField] Vector3 m_eyesClosed;

        TransformData m_eyeR;
        TransformData m_eyeL;

        Transform m_eyeLidR;
        Transform m_eyeLidL;

        protected override void Update()
        {
            base.Update();

            UpdateLids();
        }

        void UpdateLids()
        {
            if (m_eyeLidR == null || m_eyeLidL == null)
                return;

            var sleepyP = 1f - EnergyPercentage;
            var val = (5f*EnergyPercentage +0.5f) * Time.time;
            var sin = Mathf.Clamp(sleepyP+EnergyPercentage*Mathf.Sin(val), sleepyP, sleepyP+0.5f);
            m_eyeR.LocalPosition = Vector3.Lerp(m_eyesOpen, m_eyesClosed, sin*sin);
            m_eyeL.LocalPosition = Vector3.Lerp(m_eyesOpen, m_eyesClosed, sin*sin);

            m_eyeLidR.position = m_eyeR.Position;
            m_eyeLidL.position = m_eyeL.Position;
        }

        protected override void UpdateRegeneration()
        {
            if (!m_sleeping)
                return;

            base.UpdateRegeneration();
        }

        public override void Regenerate(float amount)
        {
            var gap = MaxEnergy - Energy;
            amount = Mathf.Min(amount, gap);
            if (amount < float.Epsilon)
                return;

            var foodEnergyReq = m_staminaToFoodFactor * amount;

            if (m_foodEnergy.Energy < foodEnergyReq)
                return;
            
            m_foodEnergy.Use(foodEnergyReq);

            base.Regenerate(amount);
        }

        public void SetSleeping(bool sleeping) => m_sleeping = sleeping;

        public void UpdateAttachmentPoints(AttachmentPointsProvider prov)
        {
            m_eyeR = prov.GetAttachment(m_eyeR_ID);
            m_eyeL = prov.GetAttachment(m_eyeL_ID);

            m_eyeLidR = m_eyeR.Parent.GetChild(0);
            m_eyeLidL = m_eyeL.Parent.GetChild(0);
        }
    }
}
