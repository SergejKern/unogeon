﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace Game.Actors.Model_Animation
{
    public class ModelLink : MonoBehaviour
    {
        //[SerializeField] Animator m_animator;
#pragma warning disable 0649 // wrong warnings for SerializeField
        [Tooltip("Link the Collider object here")]
        [SerializeField] GameObject m_colliderParent;

        [Tooltip("Link RelativeCollider.SetAssociatedObject() here to link the logic GameObject")]
        [SerializeField] UnityEvent<GameObject> m_connectLogicActions;
#pragma warning restore 0649 // wrong warnings for SerializeField

        /// <summary> Get collider f.e. to enable/ disable collider components </summary>
        /// <returns> Collider GameObject </returns>
        public GameObject GetColliderParent() => m_colliderParent;

        public void Connect(GameObject logic) { m_connectLogicActions.Invoke(logic); }
    }
}
