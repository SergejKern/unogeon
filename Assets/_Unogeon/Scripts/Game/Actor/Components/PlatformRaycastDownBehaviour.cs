﻿using UnityEngine;
using UnityEngine.Serialization;

namespace Game.Actors.Components
{
    /// <summary>
    /// Updates a raycast downward to get the information of the platform the actor is standing on.
    /// This can then be used by other components to avoid multiple raycasts.
    /// </summary>
    public class PlatformRaycastDownBehaviour : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] LayerMask m_groundLayers;
        [SerializeField] float m_fromHeight = 0.1f;
        [FormerlySerializedAs("m_castDistance")] 
        [SerializeField] float m_castDistanceForGround = 0.5f;
        [SerializeField] float m_fullCastDistance = 10f;
        [SerializeField] Transform m_shadowCookie;

        [SerializeField] float m_sphereCastFallbackRadius = 0.25f;

        [SerializeField] bool m_drawGUI;
#pragma warning restore 0649 // wrong warnings for SerializeField

        RigidbodyBehaviour m_rigid;


        float m_lastHeight;
        public RaycastHit LastRaycastHit { get; private set; }
        public GameObject LastHitGameObject { get; private set; }

        public bool IsGrounded { get; private set; }
        public LayerMask GroundLayers => m_groundLayers;

        void OnEnable() => Init();

        void Init() => TryGetComponent(out m_rigid);

        void Update() => CheckGrounded();

        public void CheckGrounded()
        {
            if (m_rigid == null)
            {
                Init();
                return;
            }

            var position = m_rigid.Position;
            var yTravel = Mathf.Abs(m_lastHeight - position.y);
            var yFrom = Mathf.Max(position.y + m_fromHeight, m_lastHeight + m_fromHeight);
            var castFrom = position;
            castFrom.y = yFrom;

            var acceptedDistanceForGrounded = m_castDistanceForGround + yTravel;
            var castLength = m_fullCastDistance + yTravel;

            //var log =
            //    $"casting from {castFrom.y:F3} to {(castFrom + length * transform.TransformDirection(Vector3.down)).y:F3} length {length:F3}\n" +
            //    $"position {position} m_lastHeight {m_lastHeight} yTravel {yTravel}";
            m_lastHeight = position.y;

            var hitGround = Physics.Raycast(castFrom, transform.TransformDirection(Vector3.down), out var hit,
                castLength, m_groundLayers);
            m_shadowCookie.gameObject.SetActive(hitGround);
            if (hitGround)
                m_shadowCookie.position = hit.point + 0.01f * Vector3.up;

            IsGrounded = hitGround && hit.distance < acceptedDistanceForGrounded;
            if (!IsGrounded)
            {
                IsGrounded = Physics.SphereCast(castFrom, 
                    m_sphereCastFallbackRadius, transform.TransformDirection(Vector3.down),
                    out hit, acceptedDistanceForGrounded, m_groundLayers);
            }

            if (!IsGrounded)
            {
                //Debug.LogError(log);
                return;
            }
            //Debug.Log(log);

            LastRaycastHit = hit;
            LastHitGameObject = LastRaycastHit.transform.gameObject;
        }

        void OnGUI()
        {
            if (!m_drawGUI)
                return;

            var position = m_rigid.Position;
            var from = position + m_fromHeight * Vector3.up;
            var down = m_castDistanceForGround * transform.TransformDirection(Vector3.down);
            var to = IsGrounded ? LastRaycastHit.point : from + down;
            Debug.DrawLine(from, to, IsGrounded ? Color.green : Color.red);
        }
    }
}
