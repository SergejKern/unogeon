﻿using System.Collections;
using System.Collections.Generic;
using Core.Unity.Attributes;
using Core.Unity.Types;
using Core.Unity.Types.Fsm;
using Game.Actors.Controls;
using GameUtility.Coroutines;
using GameUtility.Data.Feedback;
using GameUtility.Data.PhysicalConfrontation;
using GameUtility.Data.Visual;
using GameUtility.Energy;
using UnityEngine;
using static Core.Unity.Types.Attribute.ContextDrawerAttribute;

namespace Game.Actors.Components
{
    /// <summary>
    /// Used to handle damage on actors
    /// </summary>
    public class DamageHandlerComponent : MonoBehaviour, IDamageHandler
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] ControlCentre m_controlCentre;
        [SerializeField] GameObject m_model;
        [SerializeField] Animator m_animator;

        [SerializeField] EnergyComponent m_healthComponent;

        [SerializeField] AnimatorStateRef m_hitAnimatorState;

        [SerializeField, FSMDrawer(setToSelf: Condition.Never)] 
        FSMEventRef m_onHit;

        [SerializeField] bool m_rumble;
        [SerializeField] [ShowIf("m_rumble")]
        RumbleData m_rumbleData;

        [SerializeField] bool m_freezeAnimation;
        [SerializeField] [ShowIf("m_freezeAnimation")]
        float m_freezeTime;

        [SerializeField] bool m_flash;
        [SerializeField, ShowIf("m_flash")]
        FlashDataNoTarget m_flashData;

        [SerializeField] CollisionMaterialID m_materialId;
        [SerializeField] CollisionMaterialID m_absorbedMaterialID;
        [SerializeField] float m_minDamage;

        [SerializeField] float m_collisionPosCenterPercent;
        [SerializeField] Transform m_collisionPos;
#pragma warning restore 0649 // wrong warnings for SerializeField

        IMemorizeMaterials m_memoryAC;

        #region CachedComponents
        IDefense m_defenseControl;
        IDefense Defense
        {
            get
            {
                if (m_defenseControl != null)
                    return m_defenseControl;

                if (m_controlCentre == null)
                    return null;

                m_controlCentre.TryGetComponent(out m_defenseControl);
                return m_defenseControl;
            }
        }

        Animator Animator
        {
            get
            {
                if (m_animator != null)
                    return m_animator;

                if (m_controlCentre == null)
                    return null;

                m_animator = m_controlCentre.Animator;
                return m_animator;
            }
        }

        GameObject Model
        {
            get
            {
                if (m_model != null)
                    return m_model;

                if (m_controlCentre == null)
                    return null;

                m_model = m_controlCentre.Model;
                return m_model;
            }
        }
        #endregion

        public Vector3 LastDamagePos { get; private set; }

        // ReSharper disable ConvertToAutoProperty, ConvertToAutoPropertyWhenPossible
        //public HealthComponent Health => m_healthComponent;
        public Vector3 CollisionPos => m_collisionPos == null ? transform.position : m_collisionPos.position;
        public Quaternion CollisionRotation =>  m_collisionPos == null ? transform.rotation : m_collisionPos.rotation;

        public float CollisionPosCenterPercent => m_collisionPosCenterPercent;

        public CollisionMaterialID Material => m_materialId;

        readonly List<MaterialMemory> m_memory = new List<MaterialMemory>();
        List<MaterialMemory> Memory => m_memoryAC != null ? m_memoryAC.Memory : m_memory;

        // ReSharper restore ConvertToAutoProperty, ConvertToAutoPropertyWhenPossible

        void Awake() => m_memoryAC = GetComponent<IMemorizeMaterials>();
        void OnDisable() => MaterialExchange.Revert(Memory);

        public void HandleDamage(float damage, ref HandleDamageResult result)
        {
            LastDamagePos = result.DamagePos;
            if (Defense != null
                && Defense.IsBlocking(result, out var colMat))
            {
                result.Material = colMat;
                result.Result = DamageResult.Blocked;
                return;
            }

            if (m_healthComponent != null && (m_healthComponent.IsInvulnerable || m_healthComponent.Energy <= 0f))
            {
                result.Result = DamageResult.Ignored;
                return;
            }

            if (damage < m_minDamage)
            {
                result.DamageDone = 0;
                result.DamageAbsorbed = damage;
                result.Result = DamageResult.Absorbed;
                result.Material = m_absorbedMaterialID;
                return;
            }

            //! first hit-animation then damage, otherwise no death-animation
            // we don't check if we currently attacking, because attacking is expected on different layer
            // and will override hit-animation anyway
            if (Animator != null)
                Animator.Play(m_hitAnimatorState);

            m_onHit.Invoke();

            if (m_flash)
            {
                m_flashData.Memory = Memory;
                StartCoroutine(FlashByMaterialExchangeRoutine.Flash(m_flashData, Model));
            }

            var damageAmount = 1f;

            if (m_healthComponent != null)
            {
                damageAmount= Mathf.Min(damage, m_healthComponent.Energy);
                m_healthComponent.Damage(damage);
            }
            // Debug.Log($"Applying Damage {damage} reduced to {damageAmount} HP: {m_healthComponent.HP}");

            if (m_rumble && m_controlCentre != null)
                m_controlCentre.SetRumble(m_rumbleData);
            if (m_freezeAnimation)
                StartCoroutine(FreezeAnimation(m_freezeTime));

            result.Result =(m_healthComponent==null || m_healthComponent.Energy <= 0f) ? DamageResult.Killed : DamageResult.Damaged;
            result.DamageDone = damageAmount;
        }

        IEnumerator FreezeAnimation(float seconds)
        {
            var animator = Animator;
            if (animator == null)
                yield break;

            var prevSpeed = animator.speed;
            animator.speed = 0f;
            yield return new WaitForSeconds(seconds);
            // ReSharper disable once Unity.InefficientPropertyAccess
            animator.speed = prevSpeed;
        }
    }
}
