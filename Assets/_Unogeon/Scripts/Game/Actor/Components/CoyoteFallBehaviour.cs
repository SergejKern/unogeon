﻿using System;
using Core.Extensions;
using Core.Interface;
using Core.Unity.Attributes;
using Core.Unity.Interface;
using Core.Unity.Types;
using Core.Unity.Utility.PoolAttendant;
using Game.Actors.Controls;
using Game.Globals;
using GameUtility.InitSystem;
using GameUtility.Interface;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace Game.Actors.Components
{
    /// <summary>
    /// Actors can have coyote-like fall-down behaviour where they can move then hover in the air before they actually fall down
    /// </summary>
    [RequireComponent(typeof(PlatformRaycastDownBehaviour))]
    public class CoyoteFallBehaviour : MonoBehaviour, IPoolable, IInitDataComponent<CoyoteFallBehaviour.ConfigData>
    {
        [Serializable]
        public struct ConfigData
        {
            public bool Enabled;

            public float TimeInAirWalking;
            public float TimeInAirHanging;
            public float TimeFalling;

            public float ForceDown;
            public float InAirOffset;

            public bool BlockControls;

            public AnimationCurve VelocityCurve;
            public static ConfigData Default = new ConfigData() { Enabled = true, TimeInAirWalking = 0.25f, TimeInAirHanging = 0.5f, ForceDown = 100 };
        }

        enum State
        {
            Grounded,
            WalkingOnAir,
            Hanging,
            Falling,
            FallenDown
        }

#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] PlatformRaycastDownBehaviour m_raycast;
        [CopyPaste]
        [SerializeField] ConfigData m_data = ConfigData.Default;

        [SerializeField] RigidbodyBehaviour m_rigid;
        [SerializeField] ControlCentre m_centre;
        [SerializeField] MoveControl m_moveControl;

        [SerializeField] UnityEvent m_onFallenDown;

        [SerializeField] PrefabData m_fallingDownEffect;
        [SerializeField] RefIAudioAsset m_fallingDownSound;
        [SerializeField, AnimTypeDrawer] AnimatorBoolRef m_coyoteFallAnimBool;

        [SerializeField] RefITimeMultiplier m_timeControl;
        
        //[SerializeField] PlatformAffiliationBehaviour m_platformAffiliation;
#pragma warning restore 0649 // wrong warnings for SerializeField

        State m_state;

        float m_airTimer;
        //bool m_pauseAirTime;
        float m_timeFactor=1;

        ControlBlocker m_blocker;

        Rigidbody Body => m_rigid.GetBody();
        bool FallenDown => m_airTimer < - m_data.TimeInAirHanging - m_data.TimeFalling;
        bool Enabled => m_data.Enabled && enabled && gameObject.activeInHierarchy;
        Vector3 InAirYOffset => m_data.InAirOffset * Vector3.up;



        void FixedUpdate()
        {
            if (!Enabled)
                return;
            if (m_state == State.FallenDown)
                return;
            CheckGrounded();
            HoverInAir();
        }

        void HoverInAir()
        {
            if (m_state == State.Grounded)
                return;
            //if (!m_pauseAirTime)
                m_airTimer -= m_timeControl.DeltaTime * m_timeFactor;

            if (m_airTimer >= 0)
                return;

            if (m_airTimer < -m_data.TimeInAirHanging)
                FallDown();
            else 
                Hanging();

            //FallDown();

            if (FallenDown)
                OnFallenDown();
        }

        void OnFallenDown()
        {
            m_state = State.FallenDown;
            m_onFallenDown?.Invoke();
        }

        void Hanging()
        {
            if (m_state == State.Hanging)
                return;
            m_state = State.Hanging;
            if (m_centre != null && m_centre.Animator != null)
                m_centre.Animator.SetBool(m_coyoteFallAnimBool, true);

            if (m_moveControl != null)
                m_moveControl.FakeMove = true;

            //BlockControls();
        }

        void BlockControls()
        {
            if (!m_data.BlockControls)
                return;
            if (m_centre == null)
                return;

            //m_moveControl.FakeMove = true;
            m_blocker = new ControlBlocker() { Source = this };
            m_centre.AddControlBlocker(m_blocker);
        }

        void UnblockControls()
        {
            if (!m_data.BlockControls)
                return;
            if (m_centre == null)
                return;
            //m_moveControl.FakeMove = false;
            m_centre.RemoveControlBlocker(m_blocker);
        }

        void FallDown()
        {
            if (m_rigid == null)
                return;
            SetAirTimePaused(false);

            var fallProgress = -(m_airTimer + m_data.TimeInAirHanging) / m_data.TimeFalling;
            var velocityValue = m_data.VelocityCurve.Evaluate(fallProgress);
            var force = velocityValue * m_data.ForceDown * Vector3.down;

            if (m_state == State.Falling)
            {
                Body.AddForce(force, ForceMode.VelocityChange);
                return;
            }   

            //Debug.Log(m_state);
            m_state = State.Falling;
            BlockControls();
            if (m_fallingDownEffect.Prefab != null)
                m_fallingDownEffect.Prefab.GetPooledInstance(Body.position, m_fallingDownEffect.Prefab.transform.rotation);
            m_fallingDownSound.Result?.Play(gameObject);

            SetColliderEnabled(false);

            // ReSharper disable once BitwiseOperatorOnEnumWithoutFlags
            //Body.constraints &= ~RigidbodyConstraints.FreezePositionY;
            Body.AddForce(force, ForceMode.VelocityChange);
        }

        void SetColliderEnabled(bool enable)
        {
            var childrenCollider = GetComponentsInChildren<Collider>(true);
            foreach (var c in childrenCollider)
                if (!c.isTrigger)
                    c.enabled = enable;
        }

        public void CheckGrounded()
        {
            if (!Enabled)
                return;

            var grounded = m_raycast.IsGrounded;
            var hitY = m_raycast.LastRaycastHit.point.y;

            //grounded &= Mathf.Abs(GameGlobals.GroundPlaneY - hitY) <= AcceptedRange;
            if (grounded)
                OnGrounded(hitY);
            else if (m_state == State.Grounded)
                OnWalkingOnAir();
        }

        void OnWalkingOnAir()
        {
            if (m_rigid == null)
                return;
            var tr = transform;
            var pos = m_rigid.Position;
            pos += InAirYOffset;
            tr.position = pos;
            m_rigid.Position = pos;
            m_state = State.WalkingOnAir;
        }

        void OnGrounded(float hitPosY)
        {
            //if (Mathf.Abs(GameGlobals.GroundPlaneY - y) > m_acceptedYRange)
            //    return;
            //if (m_platformAffiliation.Current.Platform!= null)
            //    hitPosY = m_platformAffiliation.Current.Platform.OnPlatformPosition.y;

            var pos = m_rigid.Position;
            var heightDiff = Mathf.Abs(pos.y - hitPosY);
            if (m_state != State.Grounded || heightDiff > 0.1f)
                FixYPos(hitPosY);
            
            ResetAirTime();
            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (m_state)
            {
                case State.Hanging:
                    UnblockControls();
                    break;
                case State.Falling:
                case State.FallenDown:
                    // ReSharper disable once BitwiseOperatorOnEnumWithoutFlags
                    //Body.constraints |= RigidbodyConstraints.FreezePositionY;
                    SetColliderEnabled(true);
                    UnblockControls();
                    break;
            }

            m_state = State.Grounded;
            if (m_centre != null && m_centre.Animator != null && !m_coyoteFallAnimBool.ParameterName.IsNullOrEmpty())
                m_centre.Animator.SetBool(m_coyoteFallAnimBool.ParameterHash, false);
            if (m_moveControl != null)
                m_moveControl.FakeMove = false;
        }

        void FixYPos(float y) //, bool hard)
        {
            var pos = m_rigid.Position;

            Body.velocity = Vector3.Lerp(Body.velocity, Vector3.zero, Time.deltaTime);
            pos.y = Mathf.Lerp(pos.y, y + 0.005f, 10f * Time.deltaTime);
            //Debug.Log($"Saved by platform on height {pos.y}");
            //if (hard)
            transform.position = pos;
            m_rigid.Position = pos;
        }

        // ReSharper disable MemberCanBePrivate.Global
        // called from Playmaker
        public void SetAirTimePaused(bool pause) => m_timeFactor = pause ? 0.25f : 1f;
        // called from Playmaker
        public void ResetAirTime() => m_airTimer = m_data.TimeInAirWalking;
        [UsedImplicitly]
        public void GainAirTimeAndPause(float gainFactor)
        {
            m_airTimer = Mathf.Lerp(m_airTimer, m_data.TimeInAirWalking, gainFactor);
            SetAirTimePaused(true);
        }
        // ReSharper restore MemberCanBePrivate.Global

        public void GetData(out ConfigData data) => data = m_data;
        public void InitializeWithData(ConfigData data) => m_data = data;

        public void OnSpawn()
        {
            m_state = State.Grounded;
            if (m_centre != null && m_centre.Animator != null && !m_coyoteFallAnimBool.ParameterName.IsNullOrEmpty())
                m_centre.Animator.SetBool(m_coyoteFallAnimBool, false);
        }

        public void OnDespawn()
        {
            SetColliderEnabled(true);
            if (m_moveControl != null)
                m_moveControl.FakeMove = false;
            UnblockControls();
        }
    }
}
