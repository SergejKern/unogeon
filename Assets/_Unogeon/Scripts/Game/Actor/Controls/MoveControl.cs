#define PHYSICS_BASED_MOVEMENT

using System;
using System.Collections.Generic;
using Core.Unity.Attributes;
using Core.Unity.Extensions;
using Core.Unity.Types;
using Game.Actors.Components;
using Game.Actors.Energy;
using GameUtility.Controls.Data;
using GameUtility.Controls.Interface;
using GameUtility.Data.EntityAnima;
using GameUtility.Data.Input;
using GameUtility.Energy;
using GameUtility.InitSystem;
using GameUtility.Interface;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Scripting;

namespace Game.Actors.Controls
{
    [RequireComponent(typeof(ControlCentre))]
    public class MoveControl : MonoBehaviour, IInitDataComponent<MoveControl.ConfigData>, IReactToModelInitialized, IInputActionReceiver
    {
        const float k_castRayDownFromHeightForSlowdown = 0.01f;
        const float k_castRayDownFromHeightForClimb = 1.25f;

        [Serializable]
        public struct ConfigData
        {
            public float MoveSpeedMetersPerSecond;
            public float RotationSpeedSLerpFactor;
            public bool DirectionControlEnabled;

            public float BlockMoveOverPlatformForSeconds;

            public EnergyID RunEnergyID;
            public float RunEnergyCostPerSecond;
            public static ConfigData Default = new ConfigData()
            {
                MoveSpeedMetersPerSecond = 8,
                RotationSpeedSLerpFactor = 20f,
                DirectionControlEnabled = true,
                BlockMoveOverPlatformForSeconds = 0.5f,
                RunEnergyID = null,
                RunEnergyCostPerSecond = 1f,
            };
        }

#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] EnergyCentre m_energyCentre;

        [Header("Input")]
        [SerializeField] InputVec2ToFSMEvent m_moveInputToFSM;
        [SerializeField] InputActionReference m_dirActionRef;
        [SerializeField] InputActionReference m_runInput;

        [Header("Move")]
        [SerializeField] AnimatorFloatRef m_directionX;
        [SerializeField] AnimatorFloatRef m_directionZ;

        [SerializeField] RefITimeMultiplier m_timeControl;
        [SerializeField] float m_lerpAnimValuesSpeed = 5f;

        [SerializeField] LayerMask m_platformLayerMask;
        [SerializeField] CoyoteFallBehaviour m_coyote;

        [Header("Initialization")]
        [CopyPaste]
        [SerializeField] ConfigData m_data = ConfigData.Default;
#pragma warning restore 0649 // wrong warnings for SerializeField

        ControlCentre m_centre;

        InputAction m_moveAction;
        InputAction m_dirAction;
        InputAction m_runAction;

        Vector2 m_moveInput;
        Vector2 m_dirInput;

        bool m_moveOnUpdate;

        IEnergyComponent m_runEnergy;
        bool m_isRunning;
        float m_speedFactor = 1f;

        bool m_dirControlEnabled;

        float m_blockedMoveNearEdgeSeconds;

        public bool FakeMove;

        // ReSharper disable ConvertToAutoPropertyWhenPossible
        public float MetersPerSecond => m_data.MoveSpeedMetersPerSecond;
        // ReSharper restore ConvertToAutoPropertyWhenPossible

        Vector3 Position
        {
            get
            {
#if PHYSICS_BASED_MOVEMENT
                return m_centre.RigidbodyComponent.Position;
#else
                m_centre.RigidbodyComponent.transform.position;
#endif
            }
            set
            {
#if PHYSICS_BASED_MOVEMENT
                //Debug.Log(value.ToString("F3"));
                m_centre.RigidbodyComponent.Position = value;
#else
                m_centre.RigidbodyComponent.transform.position = value;
#endif
            }
        }
        Quaternion Rotation
        {
            get
            {
#if PHYSICS_BASED_MOVEMENT
                return m_centre.RigidbodyComponent.Rotation;
#else
                return m_centre.RigidbodyComponent.transform.rotation;
#endif
            }
            set
            {
#if PHYSICS_BASED_MOVEMENT
                m_centre.RigidbodyComponent.Rotation = value;
#else
                m_centre.RigidbodyComponent.transform.rotation = value;
#endif
            }
        }
        public bool Enabled => enabled;
        public bool CanInvoke => true;

        void OnDisable()
        {
            if (m_centre == null)
                return;
            m_directionX.Value = 0.0f;
            m_directionZ.Value = 0.0f;
        }

        public void SetRotationLerpValue(float r) => m_data.RotationSpeedSLerpFactor = r;

        public void SetMoveInput(Vector2 input)
        {
            m_moveInput = input;
            m_moveInputToFSM.SetControlInput(input.sqrMagnitude);
        }

        public void SetDirectionInput(Vector2 dir) => m_dirInput = dir;

        public void GetData(out ConfigData data) => data = m_data;
        public void InitializeWithData(ConfigData data)
        {
            m_data = data;
            m_dirControlEnabled = m_data.DirectionControlEnabled;

            m_runEnergy = m_energyCentre.GetEnergyComponent(m_data.RunEnergyID);
        }

        void Awake() => Init();

        void Init()
        {
            TryGetComponent(out m_centre);

            InitializeWithData(m_data);

            if (m_moveInputToFSM.Initialized)
                return;
            m_moveInputToFSM.Init(m_centre, new ActionInfo()
            {
                CanInvoke = () => CanInvoke,
                Enabled = () => Enabled,
                Invokable = null
            },InputEnergyData.Default, ActionInfo.Default, InputEnergyData.Default);
        }

        public void OnModelInitialized()
        {
            Init();

            m_directionX.Animator = m_centre.Animator;
            m_directionZ.Animator = m_centre.Animator;
        }

#if PHYSICS_BASED_MOVEMENT
        // Physics based movement must be in FixedUpdate or camera/movement will jitter
        void FixedUpdate()
        {
            if (m_centre == null || m_centre.RigidbodyComponent == null)
                return;

            UpdateInputFromInputAction();
            if (!m_moveOnUpdate)
            {
                m_directionX.Value = Mathf.Lerp(m_directionX.Value, 0f, m_lerpAnimValuesSpeed * Time.deltaTime);
                m_directionZ.Value = Mathf.Lerp(m_directionZ.Value, 0f, m_lerpAnimValuesSpeed * Time.deltaTime);
                return;
            }

            MoveUpdate(m_timeControl.FixedDeltaTime);
        }

        void Update() => m_moveInputToFSM.Update();
#else
        void Update()
        {
            if (m_controlCentre == null)
                return;
            if (!m_moveOnUpdate)
                return;

            UpdateInputFromInputAction();
            MoveUpdate(m_timeControl.DeltaTime);
        }
#endif

        void UpdateInputFromInputAction()
        {
            if (m_moveAction != null)
                SetMoveInput(m_moveAction.ReadValue<Vector2>());
            if (m_dirAction != null && m_dirAction.enabled && m_dirControlEnabled)
                SetDirectionInput(m_dirAction.ReadValue<Vector2>());
        }

        void MoveUpdate(float delta)
        {
            // Debug.Log(m_moveInput);
            var moveDir = new Vector3(m_moveInput.x, 0, m_moveInput.y);
            var lookDir = m_dirInput;

            Move(moveDir, lookDir, m_speedFactor, delta);
        }

        [Preserve] // FSM
        public void DoMove() => DoMove(m_speedFactor);
        public void DoMove(float speedFactor)
        {
            if (m_centre == null)
                return;

            m_moveOnUpdate = true;

            var runEnergyAvailable = m_runEnergy == null || (m_runEnergy.Energy > m_data.RunEnergyCostPerSecond)
                                                             || (m_isRunning && m_runEnergy.Energy > 0f);

            m_isRunning = speedFactor > 0.01f && m_moveInputToFSM.StateActive 
                                              && m_runAction.ReadValue<float>() > 0.25f && runEnergyAvailable;

            m_speedFactor = m_isRunning ? 1.5f : speedFactor;
        }

        [Preserve, UsedImplicitly] // FSM
        public void DoRotate()
        {
            if (m_centre == null)
                return;
            UpdateInputFromInputAction();
            var moveDir = new Vector3(m_moveInput.x, 0, m_moveInput.y);
            var lookDir = m_dirInput;

            var targetRot = GetTargetRotation(moveDir, lookDir);
            Rotation = targetRot;
        }

        void Move(Vector3 moveDir, Vector2 lookDir, float speedFactor, float delta)
        {
            if (Time.timeScale < float.Epsilon)
                return;
            if (m_centre.Blocked || !enabled)
                return;

            if (m_isRunning) 
                m_runEnergy?.Use(delta * m_data.RunEnergyCostPerSecond);

            var meters = delta * m_data.MoveSpeedMetersPerSecond * speedFactor;

            Rotate(moveDir, lookDir);

            var magnitude = moveDir.sqrMagnitude;
            if (m_centre.Animator != null)
            {
                m_directionX.Sync();
                m_directionZ.Sync();
                var speed = magnitude * speedFactor;

                var animVector = speed * Vector2.up;
                if (m_moveInput.sqrMagnitude > float.Epsilon && lookDir.sqrMagnitude > float.Epsilon)
                {
                    var angle = (360 + Vector2.SignedAngle(m_moveInput, lookDir)) % 360;
                    animVector = speed * Vector2.up.Rotate(angle);
                    //Debug.Log($"moveDir {moveInput} lookDir {lookDir} angle {angle}");
                }
                m_directionX.Value = Mathf.Lerp(m_directionX.Value, animVector.x, m_lerpAnimValuesSpeed * Time.deltaTime);
                m_directionZ.Value = Mathf.Lerp(m_directionZ.Value, animVector.y, m_lerpAnimValuesSpeed * Time.deltaTime);

                if (Mathf.Abs(m_directionX.Value - animVector.x) < 0.25f)
                    m_directionX.Value = animVector.x;
                if (Mathf.Abs(m_directionZ.Value - animVector.y) < 0.25f)
                    m_directionZ.Value = animVector.y;
            }

            var newPos = Position + (meters * moveDir);

            if (NearEdge(moveDir, meters))
                NearEdgeBehaviour(ref newPos, moveDir, meters);
            else
            {
                m_blockedMoveNearEdgeSeconds = 0f;
                m_coyote.enabled = true;
            }

            if (!FakeMove)
                Position = newPos;
        }
        bool NearEdge(Vector3 moveDir, float meters)
        {
            var testMeters = 5f * meters;
            var testPos = Position + testMeters * moveDir;

            var ray = new Ray(testPos + k_castRayDownFromHeightForSlowdown * Vector3.up, Vector3.down);
            var hitGround = Physics.Raycast(ray, out _, 2f, m_platformLayerMask);
            var color = hitGround ? Color.green : Color.red;
            //Debug.DrawRay(ray.origin, ray.direction * 2f, color, 1.0f);
            return !hitGround;
        }

        void NearEdgeBehaviour(ref Vector3 newPos, Vector3 moveDir, float meters)
        {
            var climbing = m_isRunning && ClimbNearEdge(ref newPos, moveDir, meters);
            if (!climbing)
                SlowMoveNearEdge(ref newPos);
        }

        void SlowMoveNearEdge(ref Vector3 pos)
        {
            pos = Vector3.Lerp(Position, pos, m_blockedMoveNearEdgeSeconds / m_data.BlockMoveOverPlatformForSeconds);
            m_blockedMoveNearEdgeSeconds += Time.deltaTime;
        }

        bool ClimbNearEdge(ref Vector3 pos, Vector3 moveDir, float meters)
        {
            var testMeters = 5f * meters;
            var testPos = Position + testMeters * moveDir;

            var climbRay = new Ray(testPos + k_castRayDownFromHeightForClimb * Vector3.up, Vector3.down);
            var highGround = Physics.Raycast(climbRay, out _, 1.25f, m_platformLayerMask);
            var color = highGround ? Color.blue : Color.magenta;
            //Debug.DrawRay(climbRay.origin, climbRay.direction * 2f, color, 1.0f);

            if (!highGround) 
                return false;

            m_blockedMoveNearEdgeSeconds = 0f;
            m_coyote.enabled = false;

            pos = Position + Vector3.up * meters;
            return true;
        }

        void Rotate(Vector3 moveDir, Vector2 lookDir)
        {
            var targetRot = GetTargetRotation(moveDir, lookDir);
            Rotation = Quaternion.Slerp(Rotation, targetRot, m_timeControl.DeltaTime * m_data.RotationSpeedSLerpFactor);
        }

        Quaternion GetTargetRotation(Vector3 moveDir, Vector2 lookDir)
        {
            var magnitude = moveDir.sqrMagnitude;
            var targetRot = Rotation;

            if (lookDir.sqrMagnitude > float.Epsilon)
                targetRot = Quaternion.LookRotation(new Vector3(lookDir.x, 0, lookDir.y));
            else if (magnitude > float.Epsilon)
                targetRot = Quaternion.LookRotation(moveDir);
            return targetRot;
        }


        [Preserve, UsedImplicitly] // FSM
        public void MoveStopped() => m_moveOnUpdate = false;

        public IEnumerable<ActionMapping> Mapping => new[]
        {
            new ActionMapping() { ActionRef = m_moveInputToFSM.ActionRef,
                LinkAction = (a) => 
                {
                    m_moveAction = a;
                    m_moveAction.Enable();
                },
                UnlinkAction = (a) => { m_moveAction = null; },
                Call = m_moveInputToFSM.OnControl
            },
            new ActionMapping() 
            { 
                ActionRef = m_dirActionRef,
                LinkAction = (a) => { m_dirAction = a; },
                UnlinkAction = (a) => { m_dirAction = null; }
            },
            new ActionMapping()
            {
                ActionRef = m_runInput,
                LinkAction = (a) => { m_runAction = a; },
                UnlinkAction = (a) => { m_runAction = null; }
            }
        };

        public void SetAimingEnabled(bool enable) => m_dirControlEnabled = enable && m_data.DirectionControlEnabled;
    }
}