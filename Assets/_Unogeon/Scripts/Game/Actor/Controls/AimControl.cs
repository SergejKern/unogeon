﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using System.Linq;
using Core.Unity.Interface;
using Core.Unity.Attributes;
using Core.Unity.Extensions;
using Core.Unity.Operations;
using Game.Enums;
using Game.Utility;
using GameUtility.Data.EntityAnima;
using GameUtility.Data.FX;
using GameUtility.Data.Input;
using GameUtility.Data.Visual;
using GameUtility.InitSystem;

//using UnityEditor;

namespace Game.Actors.Controls
{
    [RequireComponent(typeof(ControlCentre), typeof(MoveControl))]
    public class AimControl : MonoBehaviour, IInputActionReceiver, IInitDataComponent<AimControl.ConfigData>
    {
        [Serializable]
        public struct ConfigData
        {
            public bool AimVisualsEnabled;
            public bool AimVisualsActivityOnlyOnTarget;
            public bool AimVisualsManipulateChildPosition;

            public bool LockOnEnabled;
            public float LockDistance; // = 20f;
            public int Priority;
            public static ConfigData Default = new ConfigData() { AimVisualsEnabled = true, LockOnEnabled = true, LockDistance = 20f };
        }

#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] InputActionReference m_aimingInputActionRef;
        [SerializeField] InputActionReference m_throwInputActionReference;

        [SerializeField] InputActionReference m_lockInputActionReference;
        [SerializeField] InputActionReference m_nextTargetActionReference;
        [SerializeField] InputActionReference m_previousTargetActionReference;

        [SerializeField] Transform m_aimingDirection;
        [SerializeField] Transform m_aimingSelector;
        //[SerializeField] LayerMask m_lockLayers;
        //[SerializeField] MoveControl m_moveControl;

        // cannot use inRangeFilter, because we have custom range for Aim/Locking
        //[SerializeField] InRangeTargetFilter m_targetFilter;
        [SerializeField] RefIAgent m_agent;
        [SerializeField, SelectAssetByName(nameof(PlayerFX.Aiming))]
        FX_ID m_fxID;

        [CopyPaste]
        [SerializeField] ConfigData m_configData = ConfigData.Default;
#pragma warning restore 0649 // wrong warnings for SerializeField

        //PlayerFXComponent m_fxComponent;

        GameObject AimVisualActivityObject =>
            m_configData.AimVisualsActivityOnlyOnTarget ?
                m_aimingSelector.gameObject
                : m_aimingDirection.gameObject;

        bool m_actorPreparesThrowing;
        Vector2 m_input;
        InputAction m_aimAction;
        InputAction m_throwAction;

        ITarget m_lockTarget;

        ITarget LockTarget
        {
            get => m_lockTarget;
            set
            {
                m_lockTarget = value;
                m_agent.Result.SetTargetWithPriority(m_lockTarget, m_configData.Priority);
            }
        }

        ControlCentre m_controlCentre;

        Vector3 Position => m_controlCentre.Position;
        Vector2 V2Position => m_controlCentre.V2Position;

        public void GetData(out ConfigData data) => data = m_configData;
        public void InitializeWithData(ConfigData data)
        {
            m_configData = data;
            if (m_aimingDirection != null)
                m_aimingDirection.gameObject.SetActive(m_configData.AimVisualsActivityOnlyOnTarget);
        }

        void Start() => m_controlCentre = GetComponent<ControlCentre>();

        void Update()
        {
            if (!m_configData.AimVisualsEnabled && !m_configData.LockOnEnabled)
                return;

            UpdateTargetValid();
            // if (LockTarget!= null)
            //    AimLockTarget();
            // else
            if (LockTarget == null)
                InputActionToInput();

            UpdateSelector();

            m_input = Vector2.zero;
        }

        void UpdateTargetValid()
        {
            if (LockTarget == null)
                return;
            LockTarget.Get(out Transform lockTr);
            var sqrMagnitudePlayer = Vector3.SqrMagnitude(lockTr.position - Position);
            var sqrLockDist = (m_configData.LockDistance * m_configData.LockDistance);
            var inRange = (sqrMagnitudePlayer < sqrLockDist);

            if (m_lockTarget.IsTargetingPossible && inRange)
                return;

            var prevTarget = LockTarget;
            OnNextTarget(default);
            if (LockTarget == prevTarget)
                LockTarget = null;
        }

        void UpdateSelector()
        {
            var shouldBeActive = m_actorPreparesThrowing
                                 && m_configData.AimVisualsEnabled;

            if (m_aimingDirection == null)
                return;

            if (AimVisualActivityObject.IsActiveOrActivating() != shouldBeActive)
                AimVisualActivityObject.SetActiveWithTransition(shouldBeActive);

            if (m_input.sqrMagnitude > float.Epsilon)
                m_aimingDirection.rotation = Quaternion.LookRotation(new Vector3(m_input.x, 0, m_input.y));
            else m_aimingDirection.localRotation = Quaternion.identity;


            if (m_aimingSelector == null || !m_configData.AimVisualsManipulateChildPosition)
                return;
            m_aimingSelector.transform.localPosition = m_configData.LockDistance * m_input.magnitude * Vector3.forward;
        }

        // this is done in Agent now
        //void AimLockTarget()
        //{
        //    //if (m_lockTarget == null)
        //    //    return;
        //    var vec = (LockTarget.Transform.position - transform.position);
        //    var input = new Vector2(vec.x, vec.z);
        //    var l = Mathf.Min(vec.magnitude, m_configData.LockDistance) / m_configData.LockDistance;
        //    m_input = input.normalized * l;
        //    if (m_moveControl!= null)
        //        m_moveControl.SetDirectionInput(m_input);
        //}

        void InputActionToInput()
        {
            if (!m_configData.AimVisualsEnabled)
                return;
            if (m_aimAction == null)
                return;

            m_input = m_aimAction.ReadValue<Vector2>();
            m_actorPreparesThrowing = m_throwAction == null
                ? m_input.sqrMagnitude > float.Epsilon
                : m_throwAction.ReadValue<float>() > float.Epsilon;
        }

        void OnLock(InputAction.CallbackContext obj)
        {
            if (!m_configData.LockOnEnabled)
                return;

            if (LockTarget != null)
            {
                LockTarget = null;
                return;
            }

            GetNearestTargetFromAiming();
            if (LockTarget == null)
                return;
            m_aimAction.Disable();
        }

        void GetNearestTargetFromAiming()
        {
            var targets = TargetsCollector.Targets.Where((t) => t.IsTargetingPossible);

            if (m_aimingSelector == null)
                return;
            var nearestTarget = TargetOperations.GetNearest2(m_aimingSelector, targets);
            if (nearestTarget == null)
                return;

            nearestTarget.Get(out Transform nearestTargetTr);
            var sqrMagnitudePlayer = Vector3.SqrMagnitude(nearestTargetTr.position - Position);
            if (sqrMagnitudePlayer < (m_configData.LockDistance * m_configData.LockDistance))
                LockTarget = nearestTarget;
        }

        void OnNextTarget(InputAction.CallbackContext _) => PickNextTarget(1f);
        void OnPreviousTarget(InputAction.CallbackContext _) => PickNextTarget(-1f);

        void PickNextTarget(float dir)
        {
            if (LockTarget == null)
                return;

            var playerPos = V2Position;
            LockTarget.Get(out Transform lockTr);
            var lockTargetPos = lockTr.position;
            var targetV2Pos = new Vector2(lockTargetPos.x, lockTargetPos.z);
            var v1 = targetV2Pos - playerPos;
            var nearestNextAngle = dir * 360f;

            var nextTarget = LockTarget;
            var targets = TargetsCollector.Targets;
            foreach (var t in targets)
            {
                if (!t.IsTargetingPossible)
                    continue;
                if (t == LockTarget)
                    continue;
                t.Get(out Transform enemyTr);

                var sqrMagnitudePlayer = Vector3.SqrMagnitude(enemyTr.position - Position);
                if (sqrMagnitudePlayer > (m_configData.LockDistance * m_configData.LockDistance))
                    continue;

                var enemyPosition = enemyTr.position;
                var enemyV2Pos = new Vector2(enemyPosition.x, enemyPosition.z);
                var v2 = enemyV2Pos - playerPos;

                var newAng = Vector2.SignedAngle(v2, v1);
                if (dir * newAng < 0)
                    newAng += dir * 360f;

                if (dir * newAng > dir * nearestNextAngle)
                    continue;
                nearestNextAngle = newAng;
                nextTarget = t;
            }

            LockTarget = nextTarget;
        }
        //public void OnFXConnected(IFXComponent playerFX)
        //{
        //    if (playerFX == null)
        //        return;
        //    //m_fxComponent = playerFX;
        //    var go = playerFX.GetInstance(m_fxID);
        //    if (m_configData.AimVisualsActivityOnlyOnTarget)
        //        go.SetActive(true);
        //    m_aimingDirection = go.transform;
        //    //todo 3: hard coded child index
        //    if (m_aimingDirection.childCount > 0) 
        //        m_aimingSelector = m_aimingDirection.GetChild(1);
        //}

        public void OnDeath()
        {
            if (m_aimingDirection != null)
                m_aimingDirection.gameObject.SetActive(false);
            if (m_aimingSelector != null)
                m_aimingSelector.gameObject.SetActive(false);
        }

        public void OnRevive()
        {
            if (m_aimingDirection != null)
                m_aimingDirection.gameObject.SetActive(true);
        }

        //void DebugNextTargetAngle()
        //{
        //    if (m_lockTarget == null)
        //        return;
        //    var playerPos = V2Position;
        //    var targetV2Pos = new Vector2(m_lockTarget.position.x, m_lockTarget.position.z);
        //    var v1 = targetV2Pos - playerPos;
        //    var nearestNextAngle = 360f;
        //    Handles.Label(transform.position, "" + nearestNextAngle, EditorStyles.helpBox);
        //    var targets = FindObjectsOfType<MonoBehaviour>().OfType<ITarget>();
        //    foreach (var t in targets)
        //    {
        //        var enemyTr = t.Transform;
        //        if (enemyTr == m_lockTarget)
        //            continue;
        //        var sqrMagnitudePlayer = Vector3.SqrMagnitude(enemyTr.position - Position);
        //        if (sqrMagnitudePlayer > (m_lockDistance * m_lockDistance))
        //            continue;
        //        var enemyV2Pos = new Vector2(enemyTr.position.x, enemyTr.position.z);
        //        var v2 = enemyV2Pos - playerPos;
        //        var newAng = Vector2.SignedAngle(v2, v1);
        //        if (newAng < 0)
        //            newAng += 360f;
        //        Handles.Label(enemyTr.position, "" + newAng, EditorStyles.helpBox);
        //    }
        //}
        //void OnDrawGizmos()
        //{
        //    DebugNextTargetAngle();
        //}

        public IEnumerable<ActionMapping> Mapping => new[] {
            new ActionMapping()
            {
                ActionRef = m_aimingInputActionRef,
                LinkAction = (a)=> m_aimAction = a,
                UnlinkAction = (a) => m_aimAction = null
            },
            new ActionMapping()
            {
                ActionRef = m_throwInputActionReference,
                LinkAction = (a)=> m_throwAction = a,
                UnlinkAction = (a) => m_throwAction = null
            },
            new ActionMapping()
            {
                ActionRef = m_lockInputActionReference,
                Call = OnLock
            }
            ,
            new ActionMapping()
            {
                ActionRef = m_nextTargetActionReference,
                Call = OnNextTarget
            },
            new ActionMapping()
            {
                ActionRef = m_previousTargetActionReference,
                Call = OnPreviousTarget
            }
        };
    }
}
