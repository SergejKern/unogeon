using System;
using System.Collections.Generic;
using System.Linq;
using Core.Interface;
using Core.Unity.Operations;
using Core.Unity.Types;
using GameUtility.Components.Collision;
using GameUtility.Controls.Data;
using GameUtility.Controls.Interface;
using GameUtility.Data.Input;
using GameUtility.Interface;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Serialization;

namespace Game.Actors.Controls
{
    [RequireComponent(typeof(ControlCentre))]
    public class InteractionControl : MonoBehaviour, IInteractiveSelector,
        IInputActionReceiver,
        ITriggerObjEnterExitHandling
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [FormerlySerializedAs("m_inputStateToFSM")]
        [SerializeField]
        InputStateToFSMEvent m_interactHold;

        [SerializeField] InputToFSMTrigger m_interactPress;
#pragma warning restore 0649 // wrong warnings for SerializeField

        internal struct InteractiveData : IProvider<Transform>
        {
            public readonly GameObject GO;
            public readonly Transform Transform;
            public readonly IInteractive Interactive;
            public readonly IHighlightable Highlightable;

            public bool Holding;
            public InteractiveData(GameObject go)
            {
                GO = go;
                Transform = go.transform;
                Interactive = go.GetComponent<IInteractive>();
                Highlightable = go.GetComponent<IHighlightable>();
                Holding = false;
            }

            public void Get(out Transform provided) => provided = Transform;
        }

        InteractiveData m_currentInteractive;
        internal InteractiveData CurrentInteractive
        {
            get => m_currentInteractive;
            set
            {
                var prevHighlightable = m_currentInteractive.Highlightable;

                Release();
                m_currentInteractive = value;

                SwitchHighlight(prevHighlightable);
            }
        }

        public bool Enabled => enabled;
        public virtual bool CanInvoke => Enabled && InteractPossible;
        protected virtual bool InteractPossible
        {
            get
            {
                if (!enabled)
                    return false;
                if (CurrentInteractive.Interactive == null)
                    return false;
                if (CurrentInteractive.Interactive.InteractEnabled == false)
                    return false;
                return !ControlCentre.Blocked;
            }
        }
        public virtual bool CanPress => CanInvoke && CurrentInteractive.Interactive is IInteractivePress;
        public virtual bool CanHold => CanInvoke && CurrentInteractive.Interactive is IInteractiveHolding;
        public ControlCentre ControlCentre { get; private set; }
        protected bool Initialized => m_interactHold.Initialized
                                      && m_interactPress.Initialized;

        readonly List<InteractiveData> m_interactive = new List<InteractiveData>();

        void Awake() => Init();

        protected virtual void Init()
        {
            TryGetComponent(out ControlCentre centre);
            ControlCentre = centre;

            m_interactHold.Init(ControlCentre, new ActionInfo()
            {
                Enabled = () => Enabled,
                CanInvoke = () => CanHold,
                Invokable = null
            }, InputEnergyData.Default, ActionInfo.Default, InputEnergyData.Default);

            m_interactPress.Init(ControlCentre, new ActionInfo()
            {
                Enabled = () => Enabled,
                CanInvoke = () => CanPress,
                Invokable = null
            }, InputEnergyData.Default);
        }

        // public virtual void OnInteract(InputAction.CallbackContext _) => OnInteract();
        // long hold:
        [UsedImplicitly]
        public virtual void OnInteract()
        {
            if (!InteractPossible)
                return;

            if (!(CurrentInteractive.Interactive is IInteractiveHolding holding)) 
                return;

            holding.Interact(ControlCentre.ControlledObject);
            m_currentInteractive.Holding = true;
        }

        public virtual void OnPress()
        {
            if (!InteractPossible)
                return;
            if (!(CurrentInteractive.Interactive is IInteractivePress interact))
                return;
            interact.Interact(ControlCentre.ControlledObject);
            // release already
            CurrentInteractive = default;
        }

        public void Release()
        {
            if (m_currentInteractive.Interactive is IInteractiveHolding holdingDevice)
                holdingDevice.OnRelease(ControlCentre.ControlledObject);
            m_currentInteractive.Holding = false;

            m_interactHold.SetControlInput(0f);
        }

        protected void Update()
        {
            m_interactHold.Update();

            SelectClosestInteractive();
            UpdateActiveHolding();
        }

        void UpdateActiveHolding()
        {
            if (!m_interactHold.StateActive)
            {
                Release();
                return;
            }
            if (CurrentInteractive.Interactive is IInteractiveHolding)
                OnInteract();
        }

        void SelectClosestInteractive()
        {
            if (m_interactHold.StateActive)
                return;
            m_interactive.RemoveAll((a) => a.GO == null);
            var closest = TargetOperations.GetNearest2(transform, m_interactive.Where((a) => a.Interactive.InteractEnabled));

            if (CurrentInteractive.GO == closest.GO)
                return;

            CurrentInteractive = closest;
        }

        void SwitchHighlight(IHighlightable prevHighlightable)
        {
            prevHighlightable?.DisableHighlight();
            if (CurrentInteractive.GO == null)
                return;
            if (CurrentInteractive.Highlightable == null)
            {
                Debug.LogWarning(
                    $"interactive Object {gameObject} is not highlighted, missing visual communication to player!");
                return;
            }

            m_currentInteractive.Highlightable?.EnableHighlight();
        }

        public void OnTriggerObjectEnter(GameObject root)
        {
            if (!root.TryGetComponent<IInteractive>(out _))
                return;
            //Debug.Log($"OnTriggerObjectEnter {root}");
            AddInteractive(root);
        }

        public void OnTriggerObjectExit(GameObject root)
        {
            if (!root.TryGetComponent<IInteractive>(out _))
                return;

            //Debug.Log($"OnTriggerObjectExit {root}");
            RemoveInteractive(root);
        }

        public void AddInteractive(GameObject root)
        {
            var containedIdx = m_interactive.FindIndex(data => data.GO == root);

            if (containedIdx == -1)
                m_interactive.Add(new InteractiveData(root));
        }
        // ReSharper disable once SuggestBaseTypeForParameter
        public void RemoveInteractive(GameObject root)
        {
            var containedIdx = m_interactive.FindIndex(data => data.GO == root);
            if (containedIdx != -1)
                m_interactive.RemoveAt(containedIdx);
            if (CurrentInteractive.GO == root)
                CurrentInteractive = default;
        }

        public virtual IEnumerable<ActionMapping> Mapping => new[] {
            new ActionMapping()
            {
                ActionRef = m_interactHold.ActionRef,
                Call = m_interactHold.OnControl
            },
            new ActionMapping()
            {
                ActionRef = m_interactPress.ActionRef,
                Call = (cc) =>
                {
                    m_interactPress.OnControl(cc);
                    OnPress();
                }
            }
        };
    }

    public interface IInteractiveSelector
    {
        void AddInteractive(GameObject root);
        void RemoveInteractive(GameObject root);
    }

    [Serializable]
    public class RefIInteractiveSelector : InterfaceContainer<IInteractiveSelector> { }
}