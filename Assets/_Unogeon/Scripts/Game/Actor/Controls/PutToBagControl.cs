using System.Collections.Generic;
using Game.Interactive;
using Game.InteractiveObjects;
using GameUtility.Controls.Data;
using GameUtility.Controls.Interface;
using GameUtility.Data.Input;
using GameUtility.Data.TransformAttachment;
using UnityEngine;
using UnityEngine.Serialization;

namespace Game.Actors.Controls
{
    [RequireComponent(typeof(ControlCentre))]
    public class PutToBagControl : MonoBehaviour, IInputActionReceiver, IInvokable 
        //IInitDataComponent<UseItemControl.ConfigData>,
    {
        [FormerlySerializedAs("m_equipmentProvider")] [SerializeField] 
        RefIEquipment m_equipment;
        [SerializeField] RefIInventory m_bag;

        [SerializeField] AttachmentID m_id;

        [SerializeField] InputToFSMTrigger m_trigger;

        IEquipment Equipment => m_equipment.Result;
        IInventory Inventory => m_bag.Result;

        IGrabbable CurrentGrabbed => Equipment.GetEquipment(m_id);

        public bool Enabled => enabled;
        public bool CanInvoke => CurrentGrabbed != null && !Inventory.Full;

        public ControlCentre ControlCentre { get; private set; }


        void Awake() => Init();

        void Init()
        {
            TryGetComponent(out ControlCentre centre);
            ControlCentre = centre;

            if (m_trigger.Initialized)
                return;
            m_trigger.Init(ControlCentre, new ActionInfo()
            {
                CanInvoke = () => CanInvoke,
                Enabled = () => Enabled,
                Invokable = Invoke
            }, InputEnergyData.Default);
        }

        public void Invoke() => PutToInventory();

        void PutToInventory()
        {
            var item = Equipment.GetEquipment(m_id);
            Equipment.Drop(item);
            m_bag.Result.PutToInventory(item);
        }

        public void OnEquipChanged(IEquipment equipment) 
            => enabled = CanInvoke;

        public IEnumerable<ActionMapping> Mapping => new[]
        {
            new ActionMapping()
            {
                ActionRef = m_trigger.ActionRef,
                Call = m_trigger.OnControl,
            }, 
        };
    }
}