using System.Collections.Generic;
using Core.Unity.Types;
using Game.InteractiveObjects;
using GameUtility.Controls.Data;
using GameUtility.Controls.Interface;
using GameUtility.Data.EntityAnima;
using GameUtility.Data.Input;
using GameUtility.Data.TransformAttachment;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;

namespace Game.Actors.Controls
{
    [RequireComponent(typeof(ControlCentre))]
    public class UseItemControl : MonoBehaviour, IEquipActionComponent,
        IInvokable, IReactToModelInitialized //IInitDataComponent<UseItemControl.ConfigData>,
    {
        [FormerlySerializedAs("m_equipmentProvider")] [SerializeField] RefIEquipment m_equipment;
        [SerializeField] AttachmentID m_slotIdx;
        [SerializeField] InputActionReference m_dPadActionRef;

        [SerializeField] InputToFSMTrigger m_trigger;

        // todo: animation depends on item
        [SerializeField] AnimatorStateRef m_animatorState;

        IGrabbable Equip => m_equipment.Result.GetEquipment(m_slotIdx);
        bool m_dPadPressed;

        public bool Enabled => enabled && !m_dPadPressed;
        public bool CanInvoke => Equip?.ActionType == ActionForActionType; //EquipActionType.Item;
        public EquipActionType ActionForActionType => EquipActionType.Item;

        public ControlCentre ControlCentre { get; private set; }


        void Awake() => Init();

        public void OnModelInitialized()
        {
            Init();
            m_animatorState.Animator = ControlCentre.Animator;
        }

        void Init()
        {
            TryGetComponent(out ControlCentre centre);
            ControlCentre = centre;

            if (m_trigger.Initialized)
                return;
            m_trigger.Init(ControlCentre, new ActionInfo()
            {
                CanInvoke = () => CanInvoke,
                Enabled = () => Enabled,
                Invokable = Invoke
            }, InputEnergyData.Default);
        }

        public void Invoke() => UseItem();
        public void UseItem()
        {
            m_animatorState.Play();
            if (Equip is IUsable usable)
                usable.Use(ControlCentre.ControlledObject);
        }

        void DPad(InputAction.CallbackContext cc)
        {
            var val = cc.ReadValue<Vector2>();   
            m_dPadPressed = Mathf.Abs(val.sqrMagnitude) > float.Epsilon;
        }

        public void OnEquipChanged(IEquipment equipment) 
            => enabled = CanInvoke;

        public IEnumerable<ActionMapping> Mapping => new ActionMapping[]
        {
            new ActionMapping()
            {
                ActionRef = m_trigger.ActionRef,
                Call = m_trigger.OnControl,
            }, 
            new ActionMapping()
            {
                ActionRef = m_dPadActionRef,
                Call = DPad
            }
        };
    }
}