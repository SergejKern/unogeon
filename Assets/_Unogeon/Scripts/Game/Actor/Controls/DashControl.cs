using System;
using System.Collections.Generic;
using Core.Unity.Interface;
using Core.Unity.Attributes;
using Core.Unity.Extensions;
using Core.Unity.Types;

using Game.Enums;
using GameUtility.Controls.Data;
using GameUtility.Controls.Interface;
using GameUtility.Data.FX;
using GameUtility.Data.Input;
using GameUtility.Energy;
using GameUtility.InitSystem;
using GameUtility.Interface;
using GameUtility.Data.Visual;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Scripting;
using static Core.Unity.Types.Attribute.ContextDrawerAttribute;

namespace Game.Actors.Controls
{
    [RequireComponent(typeof(ControlCentre), typeof(MoveControl))]
    public class DashControl : MonoBehaviour, IInputActionReceiver, IInitDataComponent<DashControl.ConfigData>
    //IDamageProtection,
    {
        const float k_rayCastHeightForBlocking = 0.7f;
        const float k_rayCastHeightForPlatforms = -0.5f;

        [Serializable]
        public struct ConfigData
        {
            public bool Enabled;

            public AnimationCurve DashCurve;
            public float DashDistance;
            public float SafeDashBeforeGapFactor;

            public float DashDuration;
            public float CooldownSeconds;

            [SerializeField]
            public EnergyID EnergyID;

            public float EnergyCost;

            [SerializeField] public EnergyID InvincibleEnergyID;
            public bool InvincibleDuringDash;

            public bool RaycastDuringDashUpdate;
            public static ConfigData Default = new ConfigData() 
            { 
                Enabled = true, DashDistance = 5,
                SafeDashBeforeGapFactor = 0.5f, DashDuration = 0.15f, CooldownSeconds = 0.1f,
                EnergyCost = 1
            };
        }

        enum DashState
        {
            Ready,
            Dashing,
            Cooldown
        }
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] InputToFSMTrigger m_fsmTrigger;
        [SerializeField] InputActionReference m_moveActionRef;

        [Header("Dash")]
        [SerializeField, AnimTypeDrawer(setToSelf: Condition.Never)] 
        AnimatorStateRef m_dashAnimState;
        [SerializeField] AnimatorFloatRef m_directionX;
        [SerializeField] AnimatorFloatRef m_directionZ;

        [SerializeField] RefIAudioAsset[] m_dashSounds;

        [SerializeField] EnergyCentre m_energyCentre;
        [SerializeField] LayerMask m_blockingLayerMask;

        [SerializeField] GameObject m_dashCollider;
        [SerializeField] RefITimeMultiplier m_timeMultiplier;

        //[SerializeField] EvadeControl m_fallback;
        [SerializeField, SelectAssetByName(nameof(PlayerFX.Dash))]
        FX_ID m_dashFXID;

        [Header("Initialization")]
        [CopyPaste]
        [SerializeField] ConfigData m_data = ConfigData.Default;
#pragma warning restore 0649 // wrong warnings for SerializeField

        InputAction m_move;

        IEnergyComponent m_energy;
        IEnergyComponent m_invincibleEnergy;

        DashState m_state;
        Vector3 m_startDashPos;
        Vector3 m_targetDashPos;
        float m_executeDashDistance;
        float m_executeDashDuration;

        float m_lastDistance;

        // ReSharper disable ConvertToAutoProperty, ConvertToAutoPropertyWhenPossible
        [UsedImplicitly] 
        public bool DashDone => m_state == DashState.Cooldown || m_state == DashState.Ready;
        [UsedImplicitly]
        bool IsDashing => m_state == DashState.Dashing;
        public bool IsProtected => IsDashing;

        // ReSharper restore ConvertToAutoProperty, ConvertToAutoPropertyWhenPossible

        public bool Enabled => enabled && m_data.Enabled;
        public bool CanInvoke =>
            m_state == DashState.Ready
            && m_move.ReadValue<Vector2>().sqrMagnitude > float.Epsilon;

        protected bool HasEnergy => m_energy == null || m_energy.CanUse(m_data.EnergyCost);
        public ControlCentre ControlCentre { get; private set; }

        protected void NoEnergyFeedback()
        {
            // todo from PitRush
            //if (m_fallback!= null && m_fallback.CanInvokeAsFallback)
            //    m_fallback.Invoke();
            // todo 3: add rumble to config
            //ControlCentre.SetRumble(0.1f, 0.5f);
        }

        Vector3 m_dashVec;
        float m_timer;
        IFXComponent m_fxComponent;
        GameObject m_dashEffect;

        void Awake() => Init();

        void Init()
        {
            TryGetComponent(out ControlCentre centre);
            ControlCentre = centre;
            m_energy = m_energyCentre.GetEnergyComponent(m_data.EnergyID);

            if (m_fsmTrigger.Initialized)
                return;
            m_fsmTrigger.Init(ControlCentre, new ActionInfo()
            {
                CanInvoke = () => CanInvoke,
                Enabled = () => Enabled
            },
            new InputEnergyData()
            {
                EnergyComponent = m_energy,
                EnergyCost = m_data.EnergyCost,
                NoEnergyAction = NoEnergyFeedback
            });
        }

        public void GetData(out ConfigData data) => data = m_data;
        public void InitializeWithData(ConfigData data)
        {
            m_data = data;
            enabled = m_data.Enabled;
            if (m_energyCentre == null)
                return;

            m_energy = m_energyCentre.GetEnergyComponent(m_data.EnergyID);
            m_invincibleEnergy = m_energyCentre.GetEnergyComponent(m_data.InvincibleEnergyID);
        }

        [Preserve, UsedImplicitly] // Is Called from FSM
        public void OnDash() => DoDash();
        
        void DoDash() 
        {
            if (ControlCentre.Blocked)
                return;
            if (m_state != DashState.Ready)
                return;

            var moveInput = m_move.ReadValue<Vector2>().normalized;
            var fwd = ControlCentre.ControlledTransform.forward.Vector2();
            var angle = (360 + Vector2.SignedAngle(moveInput, fwd)) % 360;
            var dir= Vector2.up.Rotate(angle);
            ControlCentre.Animator.SetFloat(m_directionX, dir.x);
            ControlCentre.Animator.SetFloat(m_directionZ, dir.y);

            if (ControlCentre.Animator != null)
                ControlCentre.Animator.Play(m_dashAnimState);
            // ControlCentre.Animator.SetTrigger(animHashJumpBack);

            if (m_dashSounds != null)
                foreach (var soundAsset in m_dashSounds)
                    soundAsset?.Result?.Play(ControlCentre.ControlledTransform);
            
            //var rigid = controlCentre.ControlledRigidBody;
            if (moveInput.sqrMagnitude < float.Epsilon) // <- currently won't happen anymore
            {
                Debug.LogError("Still Happening");
                var backVec = -ControlCentre.ControlledObject.transform.forward;
                m_dashVec = new Vector3(backVec.x, 0, backVec.z);
            }
            else
                m_dashVec = new Vector3(moveInput.x, 0, moveInput.y).normalized;

            InitDashData();
            // todo 3: rumble in config
            // ControlCentre.SetRumble(new ControlCentre.RumbleData() { Duration = m_executeDashDuration, Speed = m_data.DashCurve, Factor = 0.05f });

            m_timer = 0;
            m_energy?.Use(m_data.EnergyCost);
            if (m_dashCollider != null)
                m_dashCollider.SetActive(true);

            if (m_fxComponent != null)
                m_dashEffect = m_fxComponent.GetInstance(m_dashFXID);
            UpdateDashEffectTransform();

            m_state = DashState.Dashing;

            // todo: from PitRush
            //AvoidingDamageActionEvent.Trigger(ControlCentre.ControlledObject,
            //    AvoidingDamageActionEvent.ActionCategory.Dash);
        }

        void UpdateDashEffectTransform()
        {
            if (m_dashEffect == null) 
                return;

            m_dashEffect.transform.position = transform.position;
            var rot = m_dashEffect.transform.rotation;
            rot.SetLookRotation(m_dashVec, Vector3.up);
            m_dashEffect.transform.rotation = rot;
            //m_dashEffect.gameObject.SetActive(true);
        }

        void InitDashData()
        {
            //m_dashVec = new Vector3(m_moveInput.x, 0, m_moveInput.y).normalized;
            m_startDashPos = ControlCentre.Position;
            m_targetDashPos = m_startDashPos + m_data.DashDistance * m_dashVec;
            m_executeDashDistance = m_data.DashDistance;
            m_executeDashDuration = m_data.DashDuration;

            GetLastUnblockedDashValues();
            if (IsValidPlatformPos(m_targetDashPos))
            {
                InitDashDuration();
                return;
            }

            var safeDistance = m_data.SafeDashBeforeGapFactor * m_executeDashDistance;
            // make raycast back to last valid platform
            if (RaycastHitForDash(out var hit, m_targetDashPos + k_rayCastHeightForPlatforms * Vector3.up, 
                -m_dashVec, safeDistance, Color.yellow))
                m_executeDashDistance -= hit.distance;
            InitDashDuration();
        }

        void InitDashDuration()
        {
            m_executeDashDuration = m_executeDashDistance / m_data.DashDistance * m_data.DashDuration;
            if (m_data.InvincibleDuringDash)
                m_invincibleEnergy?.SetInvulnerable(m_executeDashDuration);
        }

        void GetLastUnblockedDashValues()
        {
            if (!RaycastHitForDash(out var hit, m_startDashPos + k_rayCastHeightForBlocking * Vector3.up, 
                m_dashVec, m_data.DashDistance, Color.red)) 
                return;
            m_executeDashDistance = hit.distance;
        }

        bool RaycastHitForDash(out RaycastHit hit, Vector3 start, Vector3 vec, float distance, Color debugColor)
        {
            var ray = new Ray(start, vec);
            var result = Physics.Raycast(ray, out hit, distance, m_blockingLayerMask);
            if (!result)
                return false;
            //Debug.Log(hit.transform);
            Debug.DrawRay(ray.origin, ray.direction * hit.distance, debugColor, 1.0f);

            m_targetDashPos = hit.point;
            m_targetDashPos.y = m_startDashPos.y;
            return true;
        }

        bool IsValidPlatformPos(Vector3 pos)
        {
            var ray = new Ray(pos + k_rayCastHeightForBlocking * Vector3.up, Vector3.down);
            var valid = Physics.Raycast(ray, out var hit, 2f, m_blockingLayerMask);
            if (valid)
                return true;

            Debug.DrawRay(ray.origin, ray.direction * hit.distance, Color.red, 1.0f);
            return false;
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            if (Time.timeScale < float.Epsilon)
                return;

            switch (m_state)
            {
                case DashState.Ready: return;
                case DashState.Dashing: UpdateDash(m_timeMultiplier.FixedDeltaTime); return;
                case DashState.Cooldown: Cooldown(m_timeMultiplier.FixedDeltaTime); return;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        void Cooldown(float delta)
        {
            m_timer += delta;
            if (m_timer >= m_data.CooldownSeconds)
                m_state = DashState.Ready;
        }

        void UpdateDash(float delta)
        {
            m_timer += delta;

            var rigid = ControlCentre.RigidbodyComponent;
            if (rigid == null)
                return;

            var iPol = m_data.DashCurve.Evaluate(m_timer / m_executeDashDuration);
            var traveledTotalDistance = iPol * m_executeDashDistance;
            var newPos = Vector3.Lerp(m_startDashPos, m_targetDashPos, iPol);

            var abort = false;
            UpdateDashEffectTransform();

            if (m_data.RaycastDuringDashUpdate)
            {
                var ray = new Ray(rigid.Position + k_rayCastHeightForBlocking * Vector3.up, m_dashVec);
                var distance = traveledTotalDistance - m_lastDistance;
                if (Physics.Raycast(ray, out var hit, distance, m_blockingLayerMask))
                {
                    newPos = hit.point;
                    abort = true;
                }
                Debug.DrawRay(ray.origin, ray.direction * distance, abort ? Color.red : Color.cyan, 1.0f);
            }

            // fixes dash on rising platforms
            newPos.y = rigid.Position.y;
            rigid.Position = newPos;
            m_lastDistance = traveledTotalDistance;

            if (!(m_timer >= m_executeDashDuration) && !abort) 
                return;

            //if (m_data.InvincibleDuringDash)
            //    m_invincibleEnergy?.SetInvulnerable(0);

            m_timer = 0;

            m_state = DashState.Cooldown;
            if (m_dashCollider != null)
                m_dashCollider.SetActive(false);
            //if (m_dashEffect!=null)
                //m_dashEffect.SetActive(false);

            // todo 3: add rumble to config
            // ControlCentre.SetRumble(0.1f, 0.05f);
        }

        // todo: from PitRush
        //public bool IsProtectedForSeconds(float seconds) => 
        //    IsProtected && (m_timer + seconds) < m_executeDashDuration;
        //[Preserve]
        //public void OnFXConnected(IFXComponent playerFX)
        //{
        //    if (playerFX == null)
        //        return;
        //    m_fxComponent = playerFX;
        //}

        public IEnumerable<ActionMapping> Mapping => new ActionMapping[]
        {
            new ActionMapping()
            {
                ActionRef = m_fsmTrigger.ActionRef,
                Call = m_fsmTrigger.OnControl
            }, 
            new ActionMapping()
            {
                ActionRef = m_moveActionRef,
                LinkAction = (a) => m_move = a,
                UnlinkAction = (a) => m_move = null
            }
        };
    }

}
