using System;
using System.Linq;
using Core.Interface;
using Core.Unity.Attributes;
using Core.Unity.Extensions;
using Core.Unity.Types;
using Core.Unity.Utility.PoolAttendant;
using Game.InteractiveObjects;
using GameUtility.Data.TransformAttachment;
using GameUtility.InitSystem;
using ScriptableUtility;
using UnityEngine;
using UnityEngine.Events;

namespace Game.Actors.Controls
{
    [RequireComponent(typeof(ControlCentre))]
    public class Equipment : MonoBehaviour, IInitDataComponent<Equipment.ConfigData>, 
        IEquipment,
        IUpdateAttachmentPoints
    {
        [Serializable]
        public struct ConfigData
        {
            public ItemInit[] InitialItems;

            public static ConfigData Default = new ConfigData() {};
        }
        [Serializable]
        public struct ItemInit
        {
            public AttachmentID AttachmentID;
            public PrefabData InitialItem;
        }

        [Serializable]
        public struct Appendage
        {
            public AttachmentID AttachmentID;
            public bool IsPrimary;
            public VarReference<GameObject> ItemVar;
        }

        [Serializable]
        public struct TwoHandedPair
        {
            public AttachmentID Primary;
            public AttachmentID Secondary;
        }

        struct GrabSlot
        {
            public Appendage Appendage;
            public TransformData AttachTransform;
            public IGrabbable Item;

            public bool Free => Item == null;
        }

        static void SetItem(ref GrabSlot slot, IGrabbable item)
        {
            slot.Item = item;
            slot.Appendage.ItemVar.Value = item?.gameObject;
        }

#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] RefIInteractiveSelector m_interactiveSelector;

        [SerializeField] EquipChangeEvent m_onEquipChanged;
        [SerializeField] Appendage[] m_appendages;
        [SerializeField] TwoHandedPair[] m_twoHandedPairs;

        [Header("Initialization")]
        [CopyPaste]
        [SerializeField] ConfigData m_data = ConfigData.Default;
#pragma warning restore 0649 // wrong warnings for SerializeField

        GrabSlot[] m_grabSlots;
        GrabSlot m_invalidSlot;

        bool HasFreeAppendages => m_grabSlots.Any(slot=> slot.Free);
        bool Empty => m_grabSlots.All(slot => slot.Free);

        public ControlCentre ControlCentre { get; private set; }

        void Awake() => Init();

        void Init()
        {
            TryGetComponent(out ControlCentre centre);
            ControlCentre = centre;
        }

        public void UpdateAttachmentPoints(AttachmentPointsProvider prov) => InitSlots(prov);

        internal void InitSlots(AttachmentPointsProvider app)
        {
            m_grabSlots = new GrabSlot[m_appendages.Length];

            for (var i = 0; i < m_appendages.Length; i++)
            {
                m_grabSlots[i] = new GrabSlot()
                {
                    Appendage = m_appendages[i],
                    AttachTransform = app.GetAttachment(m_appendages[i].AttachmentID),
                };
            }
        }

        int SlotIdx(AttachmentID id) => Array.FindIndex(m_grabSlots, slot => slot.Appendage.AttachmentID == id);

        public bool CanGrab(IItemProperties properties)
        {
            if (!HasFreeAppendages)
                return false;

            ref var grabSlot = ref GetSlotForGrabbable(properties, out var ok, out var secondIdx);
            return ok;
        }

        public void GrabAt(AttachmentID id, IGrabbable grabbable)
        {
            var slotIdx = SlotIdx(id);
            var secondSlot = -1;
            if (grabbable.GrabRestrictions == GrabRestrictions.TwoHanded)
            {
                var secondaryID = m_twoHandedPairs.FirstOrDefault(p => p.Primary == id).Secondary;
                secondSlot = SlotIdx(secondaryID);
                if (!m_grabSlots[secondSlot].Free)
                {
                    Debug.LogError("Second Slot not Free for two handed!");
                    return;
                }
            }

            Grab(ref m_grabSlots[slotIdx], grabbable, secondSlot);
        }


        public TransformData GetAppendage(AttachmentID id)
        {
            var slotIdx = SlotIdx(id);
            return m_grabSlots[slotIdx].AttachTransform;
        }

        // ReSharper disable once FlagArgument
        public void Grab(IGrabProvider provider, IGrabbable grabbable, IItemProperties itemProperties)
        {
            // cannot grab anything without free hands
            if (!HasFreeAppendages)
                return;

            ref var grabSlot = ref GetSlotForGrabbable(itemProperties, out var ok, out var secondIdx);
            if (!ok)
                return;

            // only if all ok, we can get item from provider, since this potentially spawns prefab
            provider?.Get(out grabbable);

            Grab(ref grabSlot, grabbable, secondIdx);
        }

        void Grab(ref GrabSlot grabSlot, IGrabbable grabbable, int secondIdx)
        {
            // trigger exit is not called when we change hierarchy of object
            m_interactiveSelector.Result.RemoveInteractive(grabbable.gameObject);
            // put to appendage and set appendage as parent
            var handTr = grabSlot.AttachTransform;
            grabbable.AdjustPositioning(handTr);
            var grabbableComponent = (grabbable as MonoBehaviour);
            if (grabbableComponent != null)
                grabbableComponent.transform.SetParent(handTr.Parent, true);

            SetItem(ref grabSlot, grabbable);
            grabbable.OnGrabbed(ControlCentre);

            // optionally set item also on another required appendage
            if (grabbable.GrabRestrictions == GrabRestrictions.TwoHanded)
                SetItem(ref m_grabSlots[secondIdx], grabbable);

            m_onEquipChanged?.Invoke(this);
            ControlCentre.SetRumble(0.2f, 0.1f);
        }

        /// <summary>
        /// Prevent equipping weapon combinations that don't have an animation set eg. one-handed sword+axe
        /// </summary>
        //bool CanEquip(WeaponMovesType animationType)
        //{
        //    ref var slot = ref GetEquippedItem();
        //    return slot.Item == null || 
        //           ConfigLinks.AnimationCombinationConfig.FindCombination(slot.Item.ItemAnimationType, 
        //               animationType, 
        //               out _);
        //}
        //void OnSwitchItems(InputAction.CallbackContext obj) => OnSwitchItems();
        //[UsedImplicitly]
        //public void OnSwitchItems()
        //{
        //    if (!m_data.SwitchEnabled)
        //        return;
        //    //Debug.Log("OnSwitchItems");
        //    var bItem = SecondarySlot.Item;
        //    var aItem = PrimarySlot.Item;
        //    if (bItem!= null && bItem.GrabRestrictions!= GrabRestrictions.None)
        //        return;
        //    if (aItem!= null && aItem.GrabRestrictions!= GrabRestrictions.None)
        //        return;
        //    bItem?.AdjustPositioning(PrimarySlot.HandTransform);
        //    aItem?.AdjustPositioning(SecondarySlot.HandTransform);
        //    PrimarySlot.Item = bItem;
        //    SecondarySlot.Item = aItem;
        //    bItem?.OnGrabbed(ControlCentre);
        //    aItem?.OnGrabbed(ControlCentre);
        //}
        //void OnDrop(InputAction.CallbackContext cc) => OnDrop();
        public bool IsPrimary(AttachmentID id) 
            => m_grabSlots.FirstOrDefault(s => s.Appendage.AttachmentID == id).Appendage.IsPrimary;

        public void Drop(IGrabbable item)
        {
            var slotIdx = Array.FindIndex(m_grabSlots, slot => slot.Item == item);
            if (slotIdx != -1)
                Drop(item, slotIdx);
        }

        public void Drop(AttachmentID id)
        {
            var slotIdx = SlotIdx(id);
            var item = m_grabSlots[slotIdx].Item;
            Drop(item, slotIdx);
        }

        void Drop(IGrabbable item, int slotIdx)
        {
            if (item == null)
                return;
            if (item.GrabRestrictions == GrabRestrictions.TwoHanded)
            {
                // for two-handed:
                var secondSlotIdx = Array.FindLastIndex(m_grabSlots, slot => slot.Item == item);
                OnItemRelease(item, ref m_grabSlots[slotIdx], ref m_grabSlots[secondSlotIdx]);
            }
            else OnItemRelease(m_grabSlots[slotIdx].Item, ref m_grabSlots[slotIdx], ref m_invalidSlot);
            
            m_interactiveSelector.Result.RemoveInteractive(item.gameObject);

            item.Drop();
        }

        ref GrabSlot GetSlotForGrabbable(IItemProperties g, out bool ok, out int pairedSlotIdx)
        {
            var firstSlotIdx = -1;
            pairedSlotIdx = -1;

            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (g.GrabRestrictions)
            {
                case GrabRestrictions.TwoHanded:
                    foreach (var pair in m_twoHandedPairs)
                    {
                        var first = SlotIdx(pair.Primary);
                        if (first == -1 || !m_grabSlots[first].Free)
                            continue;
                        var second = SlotIdx(pair.Secondary);
                        if (second == -1 || !m_grabSlots[second].Free)
                            continue;
                        firstSlotIdx = first;
                        pairedSlotIdx = second;
                    } break;
                case GrabRestrictions.PrimaryOnly:
                    firstSlotIdx = Array.FindIndex(m_grabSlots, s => s.Appendage.IsPrimary && s.Free); break;
                case GrabRestrictions.SecondaryOnly:
                    firstSlotIdx = Array.FindIndex(m_grabSlots, s => !s.Appendage.IsPrimary && s.Free); break;
                default: 
                    firstSlotIdx = Array.FindIndex(m_grabSlots, s => s.Free); break;
            }

            ok = firstSlotIdx != -1;
            if (ok)
                return ref m_grabSlots[firstSlotIdx];
            return ref m_invalidSlot;
        }

        // ReSharper disable once SuggestBaseTypeForParameter
        void OnItemRelease(IGrabbable item, ref GrabSlot slot, ref GrabSlot secondSlot)
        {
            item.transform.SetParent(null, true);

            SetItem(ref slot, null);

            if (item.GrabRestrictions == GrabRestrictions.TwoHanded)
            {
                if (secondSlot.Item != item)
                    Debug.LogError("TwoHanded was wielded one handed!");
                SetItem(ref secondSlot, null);
            }

            m_onEquipChanged?.Invoke(this);
        }

        void InitSlotWithItem(ref GrabSlot slot, GameObject prefab)
        {
            if (!slot.Free)
                return;
            
            if (!prefab.GetPooledInstance().TryGetComponent<IGrabbable>(out var grabbable))
                return;
            //ref var slot = ref m_grabSlots[(int) hand];
            grabbable.AdjustPositioning(slot.AttachTransform);
            var grabbableComponent = grabbable as MonoBehaviour;
            if (grabbableComponent != null)
                grabbableComponent.transform.SetParent(slot.AttachTransform.Parent, true);

            SetItem(ref slot, grabbable);
            grabbable.OnGrabbed(ControlCentre);
        }

        public void GetData(out ConfigData data) => data = m_data;
        public void InitializeWithData(ConfigData data)
        {
            m_data = data;

            foreach (var item in data.InitialItems)
            {
                var prefab = item.InitialItem.Prefab;
                if (prefab == null)
                    continue;
                var slotIdx = SlotIdx(item.AttachmentID);
                if (slotIdx == -1)
                    continue;
                InitSlotWithItem(ref m_grabSlots[slotIdx], prefab);
            }

            m_onEquipChanged?.Invoke(this);
        }

        public IGrabbable GetEquipment(AttachmentID id) => m_grabSlots.FirstOrDefault(s
            =>s.Appendage.AttachmentID == id).Item;

        public void DespawnEquipment()
        {
            for (var i = 0; i < m_grabSlots.Length; i++)
            {
                m_grabSlots[i].Item?.gameObject.TryDespawnOrDestroy();
                SetItem(ref m_grabSlots[i], null);
            }
        }
    }

    public interface IEquipment
    {
        bool CanGrab(IItemProperties props);
        void Grab(IGrabProvider provider, IGrabbable grabbable, IItemProperties itemProperties);
        void GrabAt(AttachmentID id, IGrabbable grabbable);
        TransformData GetAppendage(AttachmentID id);

        IGrabbable GetEquipment(AttachmentID idx);
        void Drop(IGrabbable grabbable);
        void Drop(AttachmentID id);
        bool IsPrimary(AttachmentID id);
    }

    [Serializable]
    public class RefIEquipment : InterfaceContainer<IEquipment> { }

    [Serializable]
    public class EquipChangeEvent : UnityEvent<IEquipment> { }
}