using System;
using System.Collections.Generic;
using Game.Interactive;
using Game.InteractiveObjects;
using GameUtility.Controls.Data;
using GameUtility.Controls.Interface;
using GameUtility.Data.Input;
using GameUtility.Data.TransformAttachment;
using ScriptableUtility;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;

namespace Game.Actors.Controls
{
    [RequireComponent(typeof(ControlCentre))]
    public class SwitchBagControl : MonoBehaviour, IInputActionReceiver 
        //IInitDataComponent<UseItemControl.ConfigData>,
    {
        [FormerlySerializedAs("m_equipmentProvider")] [SerializeField] 
        RefIEquipment m_equipment;
        [SerializeField] RefIInventory m_bag;

        [SerializeField] AttachmentID m_id;
        [SerializeField] AttachmentID m_pairID;

        [SerializeField] InputStateToFSMEvent m_state;
        [SerializeField] InputActionReference m_nextActionRef;
        [SerializeField] InputActionReference m_prevActionRef;

        [SerializeField] VarReference<bool> m_isSwitching;
        [SerializeField] VarReference<GameObject> m_prevItemVar;
        [SerializeField] VarReference<GameObject> m_itemVar;
        [SerializeField] VarReference<GameObject> m_nextItemVar;

        IEquipment Equipment => m_equipment.Result;
        IInventory Inventory => m_bag.Result;

        IGrabbable Equip => Equipment.GetEquipment(m_id);
        IGrabbable PairEquip => Equipment.GetEquipment(m_pairID);

        public bool Enabled => enabled;
        public bool CanInvoke => true;

        public ControlCentre ControlCentre { get; private set; }

        bool m_primary;
        IGrabbable m_intermediate;


        void Awake() => Init();

        void Update() => m_state.Update();

        void Init()
        {
            TryGetComponent(out ControlCentre centre);
            ControlCentre = centre;

            m_primary = Equipment.IsPrimary(m_id);

            if (m_state.Initialized)
                return;
            m_state.Init(ControlCentre, new ActionInfo()
            {
                CanInvoke = () => CanInvoke,
                Enabled = () => Enabled,
                Invokable = StartInventory
            }, InputEnergyData.Default, 
            new ActionInfo()
            {
                CanInvoke = () => m_isSwitching.Value,
                Invokable = EndInventory
            }, InputEnergyData.Default);
        }

        public void StartInventory()
        {
            //Debug.Log($"StartInventory {m_id}");

            m_isSwitching.Value = true;
            Time.timeScale = 0f;
            m_intermediate = Equip;

            Equipment.Drop(m_intermediate);
            m_itemVar.Value = m_intermediate?.gameObject;

            Inventory.GetPreview(out var previous, out var next, m_primary);
            m_prevItemVar.Value = previous?.gameObject;
            m_nextItemVar.Value = next?.gameObject;
        }

        public void EndInventory()
        {
            //Debug.Log($"EndInventory {m_id} {m_isSwitching.Value}");

            if (!m_isSwitching.Value)
                return;

            m_isSwitching.Value = false;
            Time.timeScale = 1f;

            if (m_intermediate == null)
                return;

            if (m_intermediate.GrabRestrictions == GrabRestrictions.TwoHanded)
            {
                var pair = PairEquip;
                if (pair != null)
                {
                    Equipment.Drop(pair);
                    Inventory.PutToInventory(pair);
                }
            }

            Equipment.GrabAt(m_id, m_intermediate);
            m_intermediate = null;
        }

        void Prev()
        {
            if (!m_isSwitching.Value)
                return;

            //Debug.Log($"Prev {m_id}");
            Inventory.Prev(ref m_intermediate, m_primary);

            m_intermediate?.AdjustPositioning(Equipment.GetAppendage(m_id));

            Inventory.GetPreview(out var previous, out var next, m_primary);
            m_itemVar.Value = m_intermediate?.gameObject;
            m_prevItemVar.Value = previous?.gameObject;
            m_nextItemVar.Value = next?.gameObject;
        }

        void Next()
        {
            if (!m_isSwitching.Value)
                return;

            //Debug.Log($"Next {m_id}");
            Inventory.Next(ref m_intermediate, m_primary);

            m_intermediate?.AdjustPositioning(Equipment.GetAppendage(m_id));

            Inventory.GetPreview(out var previous, out var next, m_primary);
            m_itemVar.Value = m_intermediate?.gameObject;
            m_prevItemVar.Value = previous?.gameObject;
            m_nextItemVar.Value = next?.gameObject;
        }

        void PutToInventory()
        {
            var item = Equipment.GetEquipment(m_id);
            Equipment.Drop(item);
            m_bag.Result.PutToInventory(item);
        }

        public void OnEquipChanged(IEquipment equipment) 
            => enabled = CanInvoke;

        public IEnumerable<ActionMapping> Mapping => new[]
        {
            new ActionMapping()
            {
                ActionRef = m_state.ActionRef,
                Call = m_state.OnControl,
            }, 
            new ActionMapping()
            {
                ActionRef = m_nextActionRef,
                Call = (cc) => Next(),
            },
            new ActionMapping()
            {
                ActionRef = m_prevActionRef,
                Call = (cc) => Prev(),
            }
        };
    }
}