using System;
using Core.Unity.Attributes;
using Game.InteractiveObjects;
using GameUtility.InitSystem;
using GameUtility.Interface;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Serialization;

namespace Game.Actors.Controls
{
    [RequireComponent(typeof(ControlCentre))]
    public class GrabControl : InteractionControl, 
        IInitDataComponent<GrabControl.ConfigData>
    {
        [Serializable]
        public struct ConfigData
        {
            public bool GrabEnabled;
            public bool DropEnabled;
            public bool SwitchEnabled;

            public static ConfigData Default = new ConfigData()
            {
                GrabEnabled = true,
                DropEnabled = true,
                SwitchEnabled = true
            };
        }

#pragma warning disable 0649 // wrong warnings for SerializeField
        [FormerlySerializedAs("m_equipmentProvider")]
        [Header("Grabbing")]
        [SerializeField] RefIEquipment m_equipment;

        [Header("Initialization")]
        [CopyPaste]
        [SerializeField] ConfigData m_data = ConfigData.Default;
#pragma warning restore 0649 // wrong warnings for SerializeField

        public override bool CanPress => CanInvoke && (CurrentInteractive.Interactive is IInteractivePress ||
                                                       CurrentInteractive.Interactive is IGrabbable);

        //bool CanDrop
        //{
        //    get
        //    {
        //        if (!enabled)
        //            return false;
        //        return !EmptyHands;
        //    }
        //}

        protected override bool InteractPossible
        {
            get
            {
                if (!TryGetGrabInterfaces(CurrentInteractive.Interactive,
                    out _, out _, out var itemProperties))
                    // if we cannot grab maybe we can interact
                    return base.InteractPossible;

                return m_equipment.Result.CanGrab(itemProperties) && base.InteractPossible;
            }
        }

        static bool TryGetGrabInterfaces(IInteractive interactive, 
            out IGrabProvider provider, out IGrabbable grabbable, out IItemProperties properties)
        {
            provider = interactive as IGrabProvider;
            grabbable = interactive as IGrabbable;
            properties = interactive as IItemProperties;
            
            return (provider != null || grabbable != null);
        }

        //public override void OnInteract(InputAction.CallbackContext _) => OnInteract();
        [UsedImplicitly]
        public override void OnPress()
        {
            if (!InteractPossible)
                return;

            if (!TryGetGrabInterfaces(CurrentInteractive.Interactive,
                out var provider, out var grabbable, out var itemProperties))
            {
                base.OnPress();
                return;
            }

            Grab(provider, grabbable, itemProperties);
        }

        // ReSharper disable once FlagArgument
        public void Grab(IGrabProvider provider, IGrabbable grabbable, IItemProperties itemProperties)
        {
            if (!gameObject.activeInHierarchy)
                return;
            if (!enabled || !m_data.GrabEnabled)
                return;

            m_equipment.Result.Grab(provider, grabbable, itemProperties);
            CurrentInteractive = default;
        }

        /// <summary>
        /// Prevent equipping weapon combinations that don't have an animation set eg. one-handed sword+axe
        /// </summary>
        //bool CanEquip(WeaponMovesType animationType)
        //{
        //    ref var slot = ref GetEquippedItem();
        //    return slot.Item == null || 
        //           ConfigLinks.AnimationCombinationConfig.FindCombination(slot.Item.ItemAnimationType, 
        //               animationType, 
        //               out _);
        //}
        //void OnSwitchItems(InputAction.CallbackContext obj) => OnSwitchItems();
        //[UsedImplicitly]
        //public void OnSwitchItems()
        //{
        //    if (!m_data.SwitchEnabled)
        //        return;
        //    //Debug.Log("OnSwitchItems");
        //    var bItem = SecondarySlot.Item;
        //    var aItem = PrimarySlot.Item;
        //    if (bItem!= null && bItem.GrabRestrictions!= GrabRestrictions.None)
        //        return;
        //    if (aItem!= null && aItem.GrabRestrictions!= GrabRestrictions.None)
        //        return;
        //    bItem?.AdjustPositioning(PrimarySlot.HandTransform);
        //    aItem?.AdjustPositioning(SecondarySlot.HandTransform);
        //    PrimarySlot.Item = bItem;
        //    SecondarySlot.Item = aItem;
        //    bItem?.OnGrabbed(ControlCentre);
        //    aItem?.OnGrabbed(ControlCentre);
        //}
        ////void OnDrop(InputAction.CallbackContext cc) => OnDrop();
        //public void Drop(IGrabbable item)
        //{
        //    var slotIdx = Array.FindIndex(m_grabSlots, slot => slot.Item == item);
        //    if (slotIdx!=-1)
        //        Drop(slotIdx);
        //}
        //public void Drop(int slotIdx)
        //{
        //    OnItemRelease(m_grabSlots[slotIdx].Item, ref m_grabSlots[slotIdx]);
        //    m_grabSlots[slotIdx].Item?.Drop();
        //}

        public void GetData(out ConfigData data) => data = m_data;
        public void InitializeWithData(ConfigData data) => m_data = data;
        public void SetGrabEnabled(bool enable) => m_data.GrabEnabled = enable;
    }

//#if UNITY_EDITOR
//    [CustomEditor(typeof(GrabControl))]
//    public class GrabControlEditor : Editor
//    {
//        // ReSharper disable once InconsistentNaming
//        new GrabControl target => base.target as GrabControl;
//        AttachmentPointsComponent m_apc;
//        AttachmentPointsProvider m_app;
//        public override void OnInspectorGUI()
//        {
//            base.OnInspectorGUI();
//            DefaultEquipGUI();
//        }
//        void DefaultEquipGUI()
//        {
//            m_apc = (AttachmentPointsComponent) EditorGUILayout.ObjectField("Model-Attachments: ", m_apc, typeof(AttachmentPointsComponent),true);
//            if (m_apc == null || !GUILayout.Button("Default Equip"))
//                return;
//            var prov = target.ControlCentre.Attachments;
//            prov.AddAttachmentPoints(m_apc);
//            target.InitSlots(prov);
//            target.GetData(out var data);
//            EditorEquip(data.InitialItemRight.Prefab, target.PrimarySlot.HandTransform);
//            EditorEquip(data.InitialItemLeft.Prefab, target.SecondarySlot.HandTransform);
//        }
//        static void EditorEquip(GameObject prefab, TransformData handTr)
//        {
//            if (prefab == null)
//                return;
//            var obj = Instantiate(prefab);
//            obj.name = $"_{obj.name}_Unsaved";
//            obj.hideFlags = HideFlags.DontSave;
//            if (!obj.TryGetComponent<IGrabbable>(out var grabbable))
//                return;
//            //ref var slot = ref m_grabSlots[(int) hand];
//            grabbable.AdjustPositioning(handTr);
//            var grabbableComponent = grabbable as MonoBehaviour;
//            if (grabbableComponent!= null)
//                grabbableComponent.transform.SetParent(handTr.Parent, true);
//        }
//    }
//#endif
}