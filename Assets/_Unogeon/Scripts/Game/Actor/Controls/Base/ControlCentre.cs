﻿using System;
using System.Collections.Generic;
using Core.Unity.Attributes;
using Game.Actors.Components;
using GameUtility.Data.EntityAnima;
using GameUtility.Data.Feedback;
using GameUtility.Data.Input;
using GameUtility.Data.TransformAttachment;
using GameUtility.InitSystem;
using GameUtility.Interface;
using GameUtility.Operations;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Haptics;

namespace Game.Actors.Controls
{
    public class ControlCentre : MonoBehaviour, 
        IControlCentre, IControllable, IRumble,
        IInitDataComponent<ControlCentre.ConfigData>
    {
        [Serializable]
        public struct ConfigData
        {
            [SerializeField]
            public float MemorizeCommandForSeconds;

            [SerializeField]
            public bool DebugMemorizedCommands;
            public static ConfigData Default = new ConfigData() { MemorizeCommandForSeconds = 1, DebugMemorizedCommands = false };
        }

        public IControlCentre Centre => this;
        public PlayerInput Input { get; private set; }

        // ReSharper disable ConvertToAutoProperty, ConvertToAutoPropertyWhenPossible
        public GameObject Model
        {
            get => m_model;
            private set
            {
                m_model = value;
                m_animator = Model.GetComponent<Animator>();
                var modelAttachments = Model.GetComponent<AttachmentPointsComponent>();
                m_attachmentPointsProvider.AddAttachmentPoints(modelAttachments);

                m_onModelInitialized.Invoke();

                foreach (var comp in m_reactToModelInitComponents)
                    comp.Result?.OnModelInitialized();
            }
        }

        public Animator Animator
        {
            get
            {
                if (m_animator == null && m_model != null)
                    m_animator = m_model.GetComponent<Animator>();

                return m_animator;
            }
        }

        public AttachmentPointsProvider Attachments => m_attachmentPointsProvider;
        public GameObject ControlledObject => m_controlledObject;
        public Transform ControlledTransform => m_controlledObject.transform;
        //public Rigidbody ControlledRigidBody => m_controlledRigidBody.GetBody();

        public RigidbodyBehaviour RigidbodyComponent => m_controlledRigidBody;

        public bool Blocked => m_controlBlockers.Count > 0;
        public Vector3 Position => ControlledObject.transform.position;
        public Vector2 V2Position => new Vector2(Position.x, Position.z);

        // ReSharper restore ConvertToAutoProperty, ConvertToAutoPropertyWhenPossible
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField]
        GameObject m_controlledObject;

        [SerializeField]
        RigidbodyBehaviour m_controlledRigidBody;

        [SerializeField] AttachmentPointsProvider m_attachmentPointsProvider;

        [SerializeField]
        InputActionAsset m_actionAsset;

        [SerializeField]
        [CopyPaste]
        ConfigData m_data = ConfigData.Default;

        [SerializeField] string m_keyboardControlScheme;
        [SerializeField] AnimationCurve m_defaultRumbleSpeedCurve;

        [SerializeField] GameObject m_model;
        [SerializeField] UnityEvent m_onModelInitialized;

        [SerializeField] RefITimeMultiplier m_timeControl;

        [SerializeField, InspectorButton("FillReactToModelList")]
        // ReSharper disable once NotAccessedField.Local
        bool m_ignoreInspectorButtonBool;
        [SerializeField] List<RefIReactToModelInitialized> m_reactToModelInitComponents;
#pragma warning restore 0649 // wrong warnings for SerializeField
        Animator m_animator;
        IDualMotorRumble m_rumbleDevice;
        RumbleData m_currentRumble;

        readonly List<ControlBlocker> m_controlBlockers = new List<ControlBlocker>();
        readonly List<MemoryInput> m_memorizedInputs = new List<MemoryInput>();
        bool MemoryEnabled { get; set; }

        public void GetData(out ConfigData data) => data = m_data;
        public void InitializeWithData(ConfigData data)
        {
            m_data = data;
            MemoryEnabled = m_data.MemorizeCommandForSeconds > 0f;
        }

        public void InitializeWithController(PlayerInput input)
        {
            MemoryEnabled = m_data.MemorizeCommandForSeconds > 0f;

            if (!input.actions.name.Contains(m_actionAsset.name))
            {
                Debug.LogError($"Input {input} does not use {m_actionAsset}!");
                return;
            }
            Input = input;

            foreach (var dev in input.devices)
            {
                if (dev is IDualMotorRumble rumble)
                    m_rumbleDevice = rumble;
            }

            foreach (var a in GetComponents<IInputActionReceiver>())
            {
                foreach (var map in a.Mapping)
                {
                    if(map.ActionRef == null)
                        continue;

                    var action = Input.actions[map.ActionRef.name];

                    if (map.Call != null) 
                        action.Add(map.Call);
                    map.LinkAction?.Invoke(action);
                }
            }

            if (string.Equals(input.currentControlScheme, m_keyboardControlScheme))
                MouseAimProcessor.KeyboardMousePlayer = transform;

            //if (GameGlobals.SceneGUI!= null)
            //    GameGlobals.SceneGUI.AddSceneGUI(DebugMemorizedCommandsGUI);

            gameObject.SetActive(true);
        }

        public void InitializeModel(GameObject model) => Model = model;

        public void RemoveControl()
        {
            if (Input == null)
                return;

            foreach (var a in GetComponents<IInputActionReceiver>())
            {
                foreach (var map in a.Mapping)
                {
                    if (map.ActionRef == null)
                        continue;
                    var action = Input.actions[map.ActionRef.name];

                    if (map.Call != null)
                        action.Remove(map.Call);

                    map.UnlinkAction?.Invoke(action);
                }
            }

            Input = null;
            m_rumbleDevice?.SetMotorSpeeds(0f, 0f);

            m_rumbleDevice = null;

            //if (GameGlobals.SceneGUI!= null)
            //    GameGlobals.SceneGUI.RemoveSceneGUI(DebugMemorizedCommandsGUI);

            gameObject.SetActive(false);
        }

        public void AddControlBlocker(ControlBlocker blocker)
        {
            if (!m_controlBlockers.Contains(blocker))
                m_controlBlockers.Add(blocker);
        }
        public void RemoveControlBlocker(ControlBlocker blocker)
        {
            if (!m_controlBlockers.Contains(blocker))
                return;
            m_controlBlockers.Remove(blocker);
        }

        public void RemoveControlBlocker(object source) => 
            m_controlBlockers.RemoveAll(blocker => blocker.Source == source);

        public void CancelMemorizedInputs() => m_memorizedInputs.Clear();
        public void MemorizeInput(IMemorizedInput input) => m_memorizedInputs.Add( new MemoryInput(){ m_input = input });

        public void SetRumble(float duration) => m_currentRumble = new RumbleData()
            {Duration = duration, Speed = m_defaultRumbleSpeedCurve, Factor = 1f};
        public void SetRumble(float duration, float factor) => m_currentRumble = new RumbleData()
            { Duration = duration, Speed = m_defaultRumbleSpeedCurve, Factor = factor };
        public void SetRumble(RumbleData data) => m_currentRumble = data;

        [UsedImplicitly]
        public void RotationFix() => Model.transform.localRotation = Quaternion.identity;

        void UpdateRumble()
        {
            if (m_rumbleDevice == null)
                return;

            var cr = m_currentRumble;
            if (cr.Duration <= 0f)
                return;
            if (cr.Time >= cr.Duration)
            {
                cr.Duration = 0f;
                m_currentRumble = cr;
                m_rumbleDevice.SetMotorSpeeds(0f, 0f);
                return;
            }

            var sp = cr.Speed.Evaluate(cr.Time / cr.Duration) * cr.Factor;
            m_rumbleDevice.SetMotorSpeeds(sp * 0.5f, sp);
            cr.Time += Time.deltaTime;

            m_currentRumble = cr;
        }

        void Update()
        {
            UpdateMemoryInputs();
            UpdateRumble();
        }

        void UpdateMemoryInputs()
        {
            if (!MemoryEnabled)
                m_memorizedInputs.Clear();

            if (m_memorizedInputs.Count <= 0)
                return;

            for (var i = m_memorizedInputs.Count - 1; i >= 0; i--)
            {
                var memory = m_memorizedInputs[i];
                memory.m_pressedSecondsAgo += m_timeControl.DeltaTime;
                m_memorizedInputs[i] = memory;

                if (memory.m_pressedSecondsAgo <= m_data.MemorizeCommandForSeconds)
                    continue;

                m_memorizedInputs.RemoveAt(i);
            }

            if (m_memorizedInputs.Count <= 0)
                return;
            var memInput = m_memorizedInputs[0];
            if (!memInput.m_input.CanInvoke)
                return;
            m_memorizedInputs.RemoveAt(0);
            memInput.m_input.Invoke();
        }

        void OnDestroy() => RemoveControl();
        void OnValidate()
        {
            Debug.ClearDeveloperConsole();

            if (m_controlledObject == null)
                Debug.LogWarning($"ControlCentre is missing assignment [controlledObject] on {gameObject}");
            if (m_controlledRigidBody == null)
                Debug.LogWarning($"ControlCentre is missing assignment [controlledRigidBody] on {gameObject}");

            foreach (var a in GetComponents<IInputActionReceiver>())
            {
                foreach (var map in a.Mapping)
                {
                    if (map.ActionRef != null && map.ActionRef.asset != m_actionAsset)
                        Debug.LogError($"Action {map.ActionRef} is not in given controlSetting: {m_actionAsset}");
                }
            }
        }

        void DebugMemorizedCommandsGUI()
        {
            if (!m_data.DebugMemorizedCommands)
                return;

            using (new GUILayout.HorizontalScope())
            {
                GUILayout.Box("Memory Commands: ", GUILayout.Width(150));
                foreach (var memory in m_memorizedInputs)
                {
                    GUILayout.Box(memory.m_input.Name, GUILayout.Width(100));
                }
            }
        }

        // ReSharper disable once UnusedMember.Global
        public object GetBlocker() => !Blocked ? null : m_controlBlockers[0].Source;

        [UsedImplicitly]
        void FillReactToModelList()
        {
            if (m_reactToModelInitComponents == null)
                m_reactToModelInitComponents = new List<RefIReactToModelInitialized>();

            m_reactToModelInitComponents.Clear();
            foreach (var a in GetComponentsInChildren<IReactToModelInitialized>())
                m_reactToModelInitComponents.Add(new RefIReactToModelInitialized(){ Result = a });
        }
    }

    internal struct MemoryInput
    {
        internal float m_pressedSecondsAgo;
        internal IMemorizedInput m_input;
    } 

}
