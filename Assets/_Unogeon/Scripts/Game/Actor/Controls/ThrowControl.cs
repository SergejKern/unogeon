using System;
using System.Collections.Generic;
using System.Linq;
using Core.Unity.Attributes;
using Core.Unity.Types;
using Core.Unity.Utility.PoolAttendant;
using Game.Interactive;
using Game.InteractiveObjects;
using GameUtility.Controls.Data;
using GameUtility.Controls.Interface;
using GameUtility.Data.Input;
using GameUtility.Data.TransformAttachment;
using GameUtility.Energy;
using GameUtility.InitSystem;
using JetBrains.Annotations;
using UnityEngine;

namespace Game.Actors.Controls
{
    [RequireComponent(typeof(ControlCentre), typeof(GrabControl))]
    public class ThrowControl : MonoBehaviour, IInitDataComponent<ThrowControl.ConfigData>, IInputActionReceiver
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [Serializable]
        public struct ConfigData
        {
            public bool Enabled;

            public bool ReturnToThrower;
            public float EnergyCost;
            public float CooldownSeconds;

            public EnergyID EnergyID;

            public static ConfigData Default = new ConfigData()
            {
                Enabled = true,
                ReturnToThrower = true,
                EnergyCost = 0,
            };
        }

        [Serializable]
        public struct AppendageInputData
        {
            public InputToFSMTrigger InputToFSMTrigger;
            public AttachmentID AttachmentID;
        }

        //[Header("Input")]
        //[SerializeField] InputActionReference m_throwActionRef;

        [Header("Throwing")] 
        [SerializeField] AppendageInputData[] m_appendageInputData;
        [SerializeField] PrefabData m_throwPrefab;

        [SerializeField] EnergyCentre m_energyCentre;

        [SerializeField, AnimTypeDrawer] AnimatorStateRef m_throwAnim;

        [Header("Initialization")]
        [CopyPaste]
        [SerializeField] ConfigData m_data = ConfigData.Default;
#pragma warning restore 0649 // wrong warnings for SerializeField

        ControlCentre m_controlCentre;
        IEquipment m_equip;
        IEnergyComponent m_energy;

        AttachmentID m_throwID;

        float m_cooldownTimer;

        public bool Enabled => enabled && m_data.Enabled;
        public bool CanInvoke(AttachmentID id)
        {
            var canInvoke = m_cooldownTimer <= 0f
                   && m_equip.GetEquipment(id) != null;
            return canInvoke;
        }

        protected void Start()
        {
            TryGetComponent(out m_equip);
            m_controlCentre = GetComponent<ControlCentre>();
            m_energy = m_energyCentre.GetEnergyComponent(m_data.EnergyID);
            foreach (var a in m_appendageInputData)
            {
                if (a.InputToFSMTrigger.Initialized)
                    continue;

                a.InputToFSMTrigger.Init(m_controlCentre, new ActionInfo()
                {
                    CanInvoke = () => CanInvoke(a.AttachmentID),
                    Enabled = () => Enabled,
                    Invokable = null,
                }, 
                new InputEnergyData()
                {
                    EnergyComponent = m_energy,
                    EnergyCost = m_data.EnergyCost,
                });
            }
        }

        public void Update() => m_cooldownTimer -= Time.deltaTime;

        // so the collider of the container does not move the player
        const float k_throwContainerSpawnDistance = 0.25f;
        const float k_throwContainerSpawnHeight = 0.5f;

        //public void OnThrow(InputAction.CallbackContext obj) => OnThrow();
        [UsedImplicitly]
        public void OnThrow()
        {
            if (!enabled)
                return;
            if (m_energy != null && m_energy.Energy <= 0f)
                return;

            var item = m_equip.GetEquipment(m_throwID);
            if (item == null)
                return;

            m_equip.Drop(item);

            m_controlCentre.Animator.Play(m_throwAnim);
            item.Throw();
            m_energy?.Use(m_data.EnergyCost);

            var tr = transform;
            var throwPos = tr.position + k_throwContainerSpawnDistance * tr.forward +
                           k_throwContainerSpawnHeight * Vector3.up;
            item.gameObject.TryGetComponent(out ThrownBehaviour throwBehaviour);
            if (throwBehaviour == null)
            {
                var throwContainer = m_throwPrefab.Prefab.GetPooledInstance(throwPos, tr.rotation);

                if (!throwContainer.TryGetComponent(out throwBehaviour))
                    return;
            }
            throwBehaviour.Throw(item.transform, m_controlCentre.ControlledObject, transform.forward, new Vector3(0f, 1f, 0f), m_data.ReturnToThrower);
            m_controlCentre.SetRumble(0.2f, 0.1f);
        }

        public void GetData(out ConfigData data) => data = m_data;
        public void InitializeWithData(ConfigData data)
        {
            m_data = data;
            enabled = m_data.Enabled;
            m_energy = m_energyCentre.GetEnergyComponent(m_data.EnergyID);
        }

        public void EquipChanged(IGrabbable primary, IGrabbable secondary) 
            => m_cooldownTimer = m_data.CooldownSeconds;

        public IEnumerable<ActionMapping> Mapping
        {
            get
            {
                return m_appendageInputData.Select((t, i) => i).Select(idx => new ActionMapping()
                {
                    ActionRef = m_appendageInputData[idx].InputToFSMTrigger.ActionRef,
                    Call = (cc)=>
                    {
                        m_throwID = m_appendageInputData[idx].AttachmentID;
                        m_appendageInputData[idx].InputToFSMTrigger.OnControl(cc);
                    },
                });
            }
        }
    }
}