﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Core.Extensions;
using GameUtility.Data.Input;

//using UnityEditor;

namespace Game.Actors.Controls
{
    [RequireComponent(typeof(ControlCentre), typeof(MoveControl))]
    public class ControlVisuals : MonoBehaviour, IInputActionReceiver
    {
        [Serializable]
        public struct InputToVisual
        {
            public InputActionReference InputActionRef;
            [NonSerialized] public InputAction Action;
            public GameObject Visual;
        }

#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] InputToVisual[] m_inputToVisuals;
#pragma warning restore 0649 // wrong warnings for SerializeField

        ControlCentre m_controlCentre;
        void Start() => m_controlCentre = GetComponent<ControlCentre>();

        void Update()
        {
            if (m_inputToVisuals.IsNullOrEmpty())
                return;

            for (var i = 0; i < m_inputToVisuals.Length; i++)
            {
                if (m_inputToVisuals[i].Action == null)
                    continue;
                m_inputToVisuals[i].Visual.SetActive(m_inputToVisuals[i].Action.ReadValue<float>() > float.Epsilon);
            }
        }

        public IEnumerable<ActionMapping> Mapping
        {
            get
            {
                for (var i = 0; i < m_inputToVisuals.Length; i++)
                {
                    var idx = i;
                    yield return new ActionMapping()
                    {
                        ActionRef = m_inputToVisuals[idx].InputActionRef,
                        LinkAction = (a) => m_inputToVisuals[idx].Action = a,
                        UnlinkAction = (a) => m_inputToVisuals[idx].Action = null
                    };
                }
            }
        }
    }
}
