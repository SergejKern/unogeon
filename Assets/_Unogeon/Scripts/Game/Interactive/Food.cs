﻿using Core.Interface;
using Core.Unity.Extensions;
using Game.Actors.Controls;
using GameUtility.Data.TransformAttachment;
using GameUtility.Energy;
using UnityEngine;

namespace Game.InteractiveObjects
{
    public class Food: MonoBehaviour, IGrabbable, IEatable
    {
        [SerializeField] EnergyID m_energyID;
        [SerializeField] float m_energy;
        [SerializeField] Sprite m_icon;

        [SerializeField] Collider m_collider;
        [SerializeField] Rigidbody m_rigidbody;

        // ReSharper disable once ConvertToAutoProperty
        public float Energy => m_energy;
        public bool InteractEnabled => m_owner==null;
        public GrabRestrictions GrabRestrictions => GrabRestrictions.None;
        public EquipActionType ActionType => EquipActionType.Item;
        public Sprite Icon => m_icon;

        public GameObject Owner
        {
            get => m_owner;
            private set
            {
                m_owner = value;
                m_collider.isTrigger = m_owner != null;
                m_rigidbody.isKinematic = m_owner != null;
            }
        }

        GameObject m_owner;
        IEquipment m_actorEquip;

        public void OnGrabbed(ControlCentre grabbingActor)
        {
            grabbingActor.TryGetComponent(out m_actorEquip);
            Owner = grabbingActor.ControlledObject;
        }

        public void AdjustPositioning(TransformData grabbingHand)
        {
            var tr = transform;
            tr.position = grabbingHand.Position;
            tr.rotation = grabbingHand.Rotation;
        }

        public void Drop() => Owner = null;

        public void Throw()
        {
            Drop();
            //throw new System.NotImplementedException();
        }

        public void Use(GameObject actor)
        {
            if (!actor.TryGetComponent(out IProvider<EnergyCentre> centreProv))
                return;

            centreProv.Get(out var energyCentre);
            var energyComponent = energyCentre.GetEnergyComponent(m_energyID);
            energyComponent.Regenerate(Energy);

            m_actorEquip.Drop(this);
            gameObject.TryDespawnOrDestroy();
        }

        public void SetOwner(GameObject owner)
        {
            Owner = owner;
            m_rigidbody.isKinematic = owner != null;
        }
    }
}