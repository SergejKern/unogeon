﻿using Core.Interface;
using Game.Actors.Energy;
using GameUtility.Energy;
using GameUtility.Interface;
using UnityEngine;

namespace Game.Interactive
{
    public class ProtoBed : MonoBehaviour, IInteractiveHolding
    {
        GameObject m_actor;

        public bool InteractEnabled => true;
        public void Interact(GameObject actor)
        {
            if (m_actor != null)
                return;

            //Debug.Log($"Interact {actor}");
            m_actor = actor;
            m_actor.TryGetComponent(out IProvider<EnergyCentre> energyProvider);
            energyProvider.Get(out var energyCentre);
            var bigStamina = energyCentre.GetEnergyComponent<BigStamina>();

            bigStamina.SetSleeping(true);
        }

        public void OnRelease(GameObject actor)
        {
            if (m_actor != actor)
                return;
            //Debug.Log($"OnRelease {actor}");

            m_actor.TryGetComponent(out IProvider<EnergyCentre> energyProvider);
            energyProvider.Get(out var energyCentre);
            var bigStamina = energyCentre.GetEnergyComponent<BigStamina>();
            bigStamina.SetSleeping(false);

            m_actor = null;
        }
    }
}