﻿using System;
using Core.Unity.Utility.PoolAttendant;
using Game.InteractiveObjects;
using Game.UI;
using UnityEngine;

namespace Game.Interactive
{
    public class ProtoPlant : MonoBehaviour, IGrabProvider
    {
        [Serializable]
        struct FruitData
        {
            public Transform Root;
            public Transform Attachment;
            public float Regrowth;
            public IGrabbable Fruit;
        }

        [SerializeField] GameObject m_fruitPrefab;
        [SerializeField] FruitData[] m_fruitData;
        [SerializeField] float m_growthFactor;

        public bool InteractEnabled => Array.FindIndex(m_fruitData, fd => fd.Fruit != null) != -1;
        public GrabRestrictions GrabRestrictions { get; private set; }
        public EquipActionType ActionType { get; private set; }
        public Sprite Icon => null;

        void Awake()
        {
            m_fruitPrefab.TryGetComponent(out IGrabbable g);
            GrabRestrictions = g.GrabRestrictions;
            ActionType = g.ActionType;

            for (var index = 0; index < m_fruitData.Length; index++) 
                SpawnFruit(index);
        }

        void SpawnFruit(int index)
        {
            var fruitGo = m_fruitPrefab.GetPooledInstance(m_fruitData[index].Attachment.position);
            fruitGo.TryGetComponent(out Food grabFruit);
            grabFruit.SetOwner(gameObject);
            m_fruitData[index].Fruit = grabFruit;
        }

        void Update()
        {
            for (var i = 0; i < m_fruitData.Length; i++)
            {
                if (m_fruitData[i].Fruit != null)
                    continue;

                var growth = m_fruitData[i].Regrowth;
                growth += Time.deltaTime * m_growthFactor;
                m_fruitData[i].Regrowth = growth;
                m_fruitData[i].Root.localScale = new Vector3(growth, growth, growth);
                if (m_fruitData[i].Regrowth > 1f) 
                    SpawnFruit(i);
            }

        }

        public void Get(out IGrabbable provided)
        {
            var fruitIndex = Array.FindIndex(m_fruitData, fd => fd.Fruit != null);
            provided = m_fruitData[fruitIndex].Fruit;
            ((Food) provided).SetOwner(null);

            m_fruitData[fruitIndex].Regrowth = 0f;
            m_fruitData[fruitIndex].Root.localScale = Vector3.zero;
            m_fruitData[fruitIndex].Fruit = null;
        }
    }
}
