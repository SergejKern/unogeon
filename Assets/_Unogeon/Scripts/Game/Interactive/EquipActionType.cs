

namespace Game.InteractiveObjects
{

    public enum EquipActionType
    {
        Item, 
        Weapon, 
        Tool
    }
}