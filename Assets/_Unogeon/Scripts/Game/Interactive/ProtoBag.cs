﻿using System;
using System.Collections.Generic;
using Core.Extensions;
using Core.Unity.Types;
using Game.InteractiveObjects;
using UnityEngine;

namespace Game.Interactive
{
    public class ProtoBag : MonoBehaviour, IInventory
    {
        [SerializeField] Transform m_contentParent;

        [SerializeField] AnimatorFloatRef m_contentFilling;
        [SerializeField] int m_fullBag = 10;
        
        readonly List<IGrabbable> m_items = new List<IGrabbable>();

        public bool Full => m_contentParent.childCount >= m_fullBag;

        public void PutToInventory(IGrabbable item)
        {
            if (Full)
                return;
            var itemTr = item.transform;
            itemTr.SetParent(m_contentParent);

            m_items.Add(item);

            m_contentFilling.Value = (float) m_contentParent.childCount / m_fullBag;
        }

        public void Next(ref IGrabbable item, bool isPrimary)
        {
            var puttingItem = item;
            puttingItem?.transform.SetParent(m_contentParent);

            var pullingItem = GetItem(isPrimary);
            if (pullingItem != null)
                m_items.Remove(pullingItem);

            if (puttingItem != null)
                m_items.Add(puttingItem);

            item = pullingItem;
            item?.transform.SetParent(null);

            m_contentFilling.Value = (float) m_contentParent.childCount / m_fullBag;
        }

        public void Prev(ref IGrabbable item, bool isPrimary)
        {
            var puttingItem = item;
            puttingItem?.transform.SetParent(m_contentParent);

            var pullingItem = GetItem(isPrimary, m_items.Count-1, -1);
            if (pullingItem != null)
                m_items.Remove(pullingItem);

            if (puttingItem != null)
                m_items.Insert(0, puttingItem);

            item = pullingItem;
            item?.transform.SetParent(null);

            m_contentFilling.Value = (float) m_contentParent.childCount / m_fullBag;
        }

        IGrabbable GetItem(bool isPrimary, int startIdx = 0, int dir = 1)
        {
            if (m_items.IsNullOrEmpty())
                return null;

            IGrabbable pullingItem = null;

            for (var i = startIdx; i >= 0 && i < m_items.Count; i+=dir)
            {
                if (Filter(isPrimary, m_items[i]))
                    continue;
                pullingItem = m_items[i];
                break;
            }
            return pullingItem;
        }

        public void GetPreview(out IGrabbable prev, out IGrabbable next, bool isPrimary)
        {
            prev = GetItem(isPrimary, m_items.Count-1, -1);
            next = GetItem(isPrimary);
        }

        static bool Filter(bool isPrimary, IItemProperties grabbable)
        {
            if (grabbable == null)
                return false;

            var restrictions = grabbable.GrabRestrictions;
            var primary = (restrictions.IsAny(GrabRestrictions.PrimaryOnly, GrabRestrictions.TwoHanded));
            var secondary = restrictions == GrabRestrictions.SecondaryOnly;

            if (primary && !isPrimary)
                return true;
            if (secondary && isPrimary)
                return true;

            return false;
        }

    }

    public interface IInventory
    {
        void PutToInventory(IGrabbable item);

        void Prev(ref IGrabbable item, bool isPrimary);
        void Next(ref IGrabbable item, bool isPrimary);
        void GetPreview(out IGrabbable prev, out IGrabbable next, bool isPrimary);

        bool Full { get; }
    }

    [Serializable]
    public class RefIInventory : InterfaceContainer<IInventory> {}
}