using System;
using System.Collections;
using Core.Unity.Attributes;
using Core.Unity.Utility.PoolAttendant;
using Game.Configs;
using Game.Enums;
using GameUtility;
using GameUtility.Config;
using GameUtility.Feature.Loading;
using GameUtility.GameSetup;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace Game.Globals
{
    public class GameSetup : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [Serializable]
        struct LoadSceneSetting
        {
            public SceneID SceneID;
            public bool Load;
            public bool Activate;
        }

        [SerializeField]
        internal ConfigLinks m_configLinks;

        [SerializeField] bool m_hudInitialActive;
        [SerializeField] LoadSceneSetting[] m_scenes;
        [SerializeField, SelectAssetByName(nameof(LoadIDs.GameSetup))]
        LoadOperationID m_setupLoad;

        public UnityEvent SceneSetup;
        public UnityEvent OnGameSetupFromThisScene;
#pragma warning restore 0649

        // readonly List<IControllable> m_spawnedControllables = new List<IControllable>();

        internal SceneSettings m_currentSceneSettings;
        public PrefabLinks Links => m_configLinks.GetPrefabLinks();

        static bool HasGameBeenSetup => GameGlobals.I.GameSetup != null;

        // Start is called before the first frame update
        void OnEnable()
        {
            // weird bug if we SetupGame immediately, where when we immediately join,
            // GameCamera-targets will be reset next frame and won't follow player
            if (HasGameBeenSetup)
            {
                SceneSetup.Invoke();
                Destroy(this, 0.1f);
            }
            else StartCoroutine(SetupGame());
        }

        void OnDisable()
        {
            if (GameGlobals.I.GameSetup != this)
                return;

            if (ConfigLinks.SpawnInitConfig)
                ConfigLinks.SpawnInitConfig.StopListen();
            GameGlobals.I.Reset();
        }

        IEnumerator SetupGame()
        {
            GameGlobals.ConfigLinks = m_configLinks;
            GameGlobals.Prefabs = m_configLinks.GetPrefabLinks();

            if (LoadingCanvas.Instance == null)
                Instantiate(GameGlobals.Prefabs.GetPrefab(nameof(PrefabIDs.LoadingCanvas)))
                    .TryGetComponent(out LoadingCanvas.Instance);

            LoadOperations.StartLoading(m_setupLoad);
            yield return null;

            GameGlobals.I.RuntimeScene = SceneManager.CreateScene("Runtime");
            SceneManager.SetActiveScene(GameGlobals.I.RuntimeScene);
            var thisGo = gameObject;
            thisGo.transform.SetParent(null);
            //SceneManager.MoveGameObjectToScene(thisGo, GameGlobals.RuntimeScene);
            GameGlobals.I.GameSetup = this;

            GameGlobals.SceneLinks = m_configLinks.GetSceneLinks();
            DebugDrawConfig.Instance = m_configLinks.GetDebugDrawConfig();

            if (Application.isEditor)
            {
                GameGlobals.I.GO_SceneGUI = Instantiate(GameGlobals.Prefabs.GetPrefab(nameof(PrefabIDs.SceneGUI)));
                GameGlobals.I.SceneGUI.AddSceneGUI(GameGlobals.ConfigLinks.DrawUsedConfigsGUI);
            }

            if (ConfigLinks.SpawnInitConfig)
                ConfigLinks.SpawnInitConfig.StartListen();

            var audioPrefab = PrefabLinks.I.GetPrefab(nameof(PrefabIDs.AudioSetup));
            if (audioPrefab != null)
                GameGlobals.I.GOAudio = Instantiate(PrefabLinks.I.GetPrefab(nameof(PrefabIDs.AudioSetup)), Vector3.zero, Quaternion.identity);

            yield return null;
            var cameraPos = transform;

            // todo 3: Make sure always correct SceneSettings available
            m_currentSceneSettings = FindObjectOfType<SceneSettings>();
            if (m_currentSceneSettings == null)
                Debug.LogWarning("No SceneSettings have been found!");
            else
                cameraPos = m_currentSceneSettings.CameraSetupPos;

            var camSetupPos = cameraPos.position;
            GameGlobals.I.GOGameCamera = Instantiate(PrefabLinks.I.GetPrefab(nameof(PrefabIDs.CameraSetup)), camSetupPos, cameraPos.rotation);
            GameGlobals.I.GameCamera.SetupCameraAtAnchor(camSetupPos);

            GameGlobals.I.GOSceneLights = Instantiate(PrefabLinks.I.GetPrefab(nameof(PrefabIDs.LightSetup)), Vector3.zero, Quaternion.identity);

            var uiCanvasPrefab = PrefabLinks.I.GetPrefab(nameof(PrefabIDs.UICanvas));
            if (uiCanvasPrefab != null)
            {
                GameGlobals.I.GO_UICanvas = Instantiate(PrefabLinks.I.GetPrefab(nameof(PrefabIDs.UICanvas)),
                    Vector3.zero, Quaternion.identity);
                GameGlobals.I.GO_UICanvas.SetActive(m_hudInitialActive);
            }

            GameGlobals.I.GOPlayerInputManager = Instantiate(PrefabLinks.I.GetPrefab(nameof(PrefabIDs.PlayerInputManager)), Vector3.zero, Quaternion.identity);

            if (GameGlobals.I.SceneLights!= null && m_currentSceneSettings != null)
                GameGlobals.I.SceneLights.Apply(m_currentSceneSettings.m_lightSettings);

            yield return null;
            Pool.Instance.CreateDefaultItems();

            yield return StartCoroutine(LoadScenes());

            SceneSetup.Invoke();
            OnGameSetupFromThisScene.Invoke();
        }

        IEnumerator LoadScenes()
        {
            foreach (var sc in m_scenes)
            {
                if (sc.Load)
                    yield return StartCoroutine(LoadAsync(SceneLinks.I.GetScene(sc.SceneID)));
            }
            foreach (var sc in m_scenes)
            {
                if (sc.Load)
                    Setup(sc.SceneID, sc.Activate);
            }

            yield return null;
            LoadOperations.LoadingDone(m_setupLoad);
        }

        static IEnumerator LoadAsync(string sceneName)
        {
            if (SceneManager.GetSceneByName(sceneName).isLoaded)
                yield break;
            var asyncScene = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
            while (!asyncScene.isDone) yield return null;
            yield return null;
        }

        static void Setup(SceneID sceneID, bool activated)
        {
            var sceneRef = SceneLinks.I.GetScene(sceneID);

            var scene = SceneManager.GetSceneByName(sceneRef);
            SceneRoot.GetSceneRoot(scene, out var sceneRoot);
            GameGlobals.I.SetScene(sceneID, sceneRoot);

            sceneRoot.InitActive(activated);
        }
    }
}