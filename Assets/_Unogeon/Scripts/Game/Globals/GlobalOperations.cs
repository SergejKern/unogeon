using System.Collections;
using Core.Unity.Extensions;
using Core.Unity.Utility.PoolAttendant;

using Game.Configs;
using Game.UI;
using GameUtility.Feature.Loading;
using GameUtility.GameSetup;
using GameUtility.Interface;
using GameUtility.Operations;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.Globals
{
    public static class GlobalOperations
    {
        public static void MovePlayersToRuntimeSceneAndConnectHUD()
        {
            var plMan = GameGlobals.I.PlayerManager;
            foreach (var player in plMan.Players) 
                MovePlayerToRuntimeSceneAndConnectHUD(player);
        }

        public static void MovePlayerToRuntimeSceneAndConnectHUD(IPlayer player)
        {
            // players marked for Don't Destroy on load (eg. from game over -> retry || Results -> continue)
            if (player.gameObject.scene == GameGlobals.I.RuntimeScene) 
                return;

            player.transform.SetParent(null, true);
            player.Input.transform.SetParent(null, true);

            SceneManager.MoveGameObjectToScene(player.gameObject, GameGlobals.I.RuntimeScene);
            SceneManager.MoveGameObjectToScene(player.Input.gameObject, GameGlobals.I.RuntimeScene);

            //if (GameGlobals.GO_UICanvas.TryGetComponent<PlayerEnergyHUD>(out var energyHUD))
            //    energyHUD.ConnectPlayer(player);
        }

        public static IEnumerator Reloading(GameObject coroutineGo, 
            SceneID reloadSceneID, LoadOperationID loadID)
        {
            LoadOperations.StartLoading(loadID);

            var scenes = GameGlobals.SceneLinks;
            var elevatorSceneName = scenes.GetScene(reloadSceneID);

            Pool.Instance.Clear();

            var plMan = GameGlobals.I.PlayerManager;
            foreach (var player in plMan.Players)
            {
                if (player == null)
                {
                    Debug.LogError("Player is null!");
                    continue;
                }

                var playerGo = player.gameObject;
                playerGo.transform.SetParent(null);
                // player.OnRevive();

                playerGo.SetActive(false);

                Object.DontDestroyOnLoad(player.gameObject);
                Object.DontDestroyOnLoad(player.Input.gameObject);
            }

            coroutineGo.transform.SetParent(null);
            Object.DontDestroyOnLoad(coroutineGo);
            yield return null;

            var asyncLoad = SceneManager.LoadSceneAsync(elevatorSceneName);
            while (!asyncLoad.isDone)
                yield return null;

            Debug.Log($"Scene Loaded!");
            LoadOperations.LoadingDone(loadID);

            coroutineGo.DestroyEx();
        }

        public static IEnumerator Exit(SceneID titleId, LoadOperationID loadID, MultiplePlayerInputInputModuleFix fix)
        {
            // waiting one frame fixes input bug!
            yield return null;
            Time.timeScale = 1f;

            LoadOperations.StartLoading(loadID);

            if (fix != null)
                fix.RemoveInput();

            GameGlobals.I.RemoveAllInput();
            GameGlobals.I.PlayerManager.Reset();

            SceneManager.LoadScene(SceneLinks.Instance.GetScene(titleId));
            InputOperations.LogAllInputs();
            LoadOperations.LoadingDone(loadID);
        }

    }
}