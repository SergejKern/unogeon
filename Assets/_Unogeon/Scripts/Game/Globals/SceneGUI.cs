﻿using System;
using UnityEngine;

namespace Game.Globals
{
    public class SceneGUI : MonoBehaviour
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] GUISkin m_skin;
#pragma warning restore 0649 // wrong warnings for SerializeField

        Action m_guiActions;

        void OnGUI()
        {
            if (!Application.isEditor)
                return;
            using (new GUILayout.VerticalScope(GUILayout.Width(Screen.width)))
            {
                var prevSkin = GUI.skin;
                GUI.skin = m_skin;
                m_guiActions.Invoke();
                GUI.skin = prevSkin;
            }
        }

        public void AddSceneGUI(Action drawUsedConfigsGUI) => m_guiActions += drawUsedConfigsGUI;

        // ReSharper disable once DelegateSubtraction
        internal void RemoveSceneGUI(Action drawUsedConfigsGUI) => m_guiActions -= drawUsedConfigsGUI;
    }
}
