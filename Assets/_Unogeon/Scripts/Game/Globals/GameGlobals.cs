using System.Collections.Generic;
using Core.Extensions;
using Core.Types;
using Core.Unity.Extensions;
using Game.Actors;
using Game.Configs;
using Game.Utility;
using GameUtility;
using GameUtility.GameSetup;
using GameUtility.Interface;
using GameUtility.Interface.Globals;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

namespace Game.Globals
{
    public class GameGlobals : Singleton<GameGlobals>, IGameGlobals
    {
#region Need Initialization

        internal GameSetup GameSetup;

        internal static PrefabLinks Prefabs
        {
            get => PrefabLinks.Instance;
            set => PrefabLinks.Instance = value;
        }

        public static SceneLinks SceneLinks 
        {
            get => SceneLinks.Instance;
            set => SceneLinks.Instance = value;
        }

        internal static ConfigLinks ConfigLinks
        {
            get => ConfigLinks.Instance;
            set => ConfigLinks.Instance = value;
        }

        #region GameObject Initialization-Properties
        public GameObject GOGameCamera
        {
            get => m_goGameCamera;
            set
            {
                if (m_goGameCamera != null)
                    m_goGameCamera.DestroyEx();

                m_goGameCamera = value;
                GameCamera = null;
                if (m_goGameCamera == null)
                    return;

                GameCamera = m_goGameCamera.GetComponentInChildren<GameCamera>();
            }
        }
        public GameObject GOSceneLights
        {
            get => m_goSceneLights;
            set
            {
                if (m_goSceneLights != null)
                    m_goSceneLights.DestroyEx();

                m_goSceneLights = value;
                SceneLights = null;
                if (m_goSceneLights == null)
                    return;

                SceneLights = m_goSceneLights.GetComponent<SceneLights>();
            }
        }
        public GameObject GOAudio
        {
            get => m_goAudio;
            set
            {
                if (m_goAudio != null)
                    m_goAudio.DestroyEx();

                m_goAudio = value;
            }
        }

        public GameObject GOPlayerInputManager
        {
            get => m_goPlayerInputManager;
            set
            {
                if (m_goPlayerInputManager != null)
                    m_goPlayerInputManager.DestroyEx();

                m_goPlayerInputManager = value;
                PlayerInputManager = null;
                if (m_goPlayerInputManager == null)
                    return;

                PlayerInputManager = m_goPlayerInputManager.GetComponent<PlayerInputManager>();
            }
        }


        public GameObject GO_UICanvas
        {
            get => m_goUICanvas;
            set
            {
                if (m_goUICanvas != null)
                    m_goUICanvas.DestroyEx();

                m_goUICanvas = value;
            }
        }

        public GameObject GO_SceneGUI
        {
            get => m_goSceneGUI;
            set
            {
                if (m_goSceneGUI != null)
                    m_goSceneGUI.DestroyEx();

                m_goSceneGUI = value;
                SceneGUI = null;
                if (m_goSceneGUI == null)
                    return;

                SceneGUI = m_goSceneGUI.GetComponent<SceneGUI>();
            }
        }
        #endregion
        #endregion

        public bool IsTutorial;

        public GameCamera GameCamera { get; private set; }
        IGameCamera IGameGlobals.GameCamera => GameCamera;

        public SceneLights SceneLights { get; private set; }
        public PlayerInputManager PlayerInputManager { get; private set; }
        public IPlayerBookkeeper PlayerManager => PlayerBookkeeper.I;

        public SceneGUI SceneGUI { get; private set; }

        public SceneSettings CurrentSceneSettings => GameSetup.m_currentSceneSettings;
        ISceneSettings IGameGlobals.CurrentSceneSettings => CurrentSceneSettings;

        public Scene RuntimeScene { get; set; }


        public readonly Dictionary<SceneID, SceneRoot> Scenes = new Dictionary<SceneID, SceneRoot>();

        GameObject m_goAudio;
        GameObject m_goGameCamera;
        GameObject m_goSceneLights;
        GameObject m_goPlayerInputManager;
        GameObject m_goAudioManager;
        GameObject m_goUICanvas;
        GameObject m_goSceneGUI;

        public void Reset()
        {
            if (PlayerInputManager != null)
                PlayerInputManager.playerJoinedEvent.RemoveAllListeners();

            GameSetup = null;
            Prefabs = null;
            ConfigLinks = null;

            GOGameCamera = null;
            GOSceneLights = null;
            GOAudio = null;

            GOPlayerInputManager = null;
            GO_UICanvas = null;
            GO_SceneGUI = null;
        }

        public void RemoveAllInput()
        {
            foreach (var p in PlayerManager.Players)
            {
                if (p == null)
                    continue;

                var centre = p.Centre;
                var input = centre.Input;
                p.RemoveControl();
                input.gameObject.DestroyEx();
            }
            GOPlayerInputManager.DestroyEx();
            GOPlayerInputManager = null;
        }

        public void SetScene(SceneID sceneID, SceneRoot sceneRoot)
        {
            if (Scenes.ContainsKey(sceneID))
                Scenes[sceneID] = sceneRoot;
            else 
                Scenes.Add(sceneID, sceneRoot);
        }

        protected override void Init()
        {
            base.Init();
            GlobalAccess.GameGlobals = this;
        }
    }


    public class PlayerBookkeeper : Singleton<PlayerBookkeeper>, IPlayerBookkeeper
    {
        public int PlayerCount { get; private set; }

        // can have null entries
        public List<Player> Players { get; } = new List<Player>();
        IEnumerable<IPlayer> IPlayerBookkeeper.Players => Players;

        public IPlayer GetPlayer(int i) => i.IsInRange(Players) ? Players[i] : null;

        public void SetPlayer(int i, IPlayer pl) => Players[i] = (Player) pl;

        public void AddPlayer(IPlayer pl) => Players.Add((Player) pl);

        public void UpdatePlayerCount()
        {
            var playerCount = 0;
            foreach (var player in Players)
            {
                if (player == null)
                    continue;

                player.SetPlayerIdx(playerCount);
                playerCount++;
            }

            PlayerCount = playerCount;
        }

        public void Reset() => Players.Clear();
    }
}
