using GameUtility;
using GameUtility.Interface.Globals;
using UnityEngine;

namespace Game.Globals
{
    public class SceneSettings : SceneRoot, ISceneSettings
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] Transform m_cameraSetupPos;
        [SerializeField] internal SceneLightSettings m_lightSettings;
#pragma warning restore 0649 // wrong warnings for SerializeField

        // ReSharper disable ConvertToAutoProperty
        public Transform CameraSetupPos => m_cameraSetupPos;

        public SceneLightSettings LightSettings => m_lightSettings;
        // ReSharper restore ConvertToAutoProperty
    }
}