//! THIS FILE IS AUTOGENERATED, DO NOT MODIFY
// ReSharper disable UnusedMember.Global
namespace Game.Enums
{
	public enum DebugDrawIDs
	{
		AgentDestination,
		AgentNextPos,
		AgentPath,
		AgentSteeringTarget,
		ControlsState,
		LogicState,
	}
}
