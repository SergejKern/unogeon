//#define PHYSICS_BASED_MOVEMENT

using System.Collections;
using System.Collections.Generic;
using Core.Extensions;
using GameUtility.Interface.Globals;
using UnityEngine;

namespace Game.Utility
{
    public class GameCamera : MonoBehaviour, IGameCamera
    {
#pragma warning disable 0649 // wrong warnings for SerializeField
        [SerializeField] Transform m_anchorPos;

        [SerializeField] GameCameraConfig m_config;
        [SerializeField] Vector2 m_distanceFac = Vector2.one;
#pragma warning restore 0649 // wrong warnings for SerializeField

        CameraSettings m_settings;
        CameraLerpSpeeds m_lerpSpeeds;

        struct Target
        {
            public Target(Transform tr, float p, float a)
            {
                Transform = tr;
                Priority = p;
                Active = a;
            }

            public readonly Transform Transform;
            public float Priority; // 0-1
            public float Active;
        }

        readonly List<Target> m_targets = new List<Target>();

        public bool PhysicsBasedMovement = true;
        // ReSharper disable ConvertToAutoPropertyWhenPossible
        public CameraSettings Settings
        {
            get => m_settings;
            set => m_settings = value;
        }

        public GameCameraConfig DefaultConfig => m_config;

        Vector3 AnchorPos
        {
            get => m_anchorPos.position;
            set => m_anchorPos.position = value;
        }
        // ReSharper restore ConvertToAutoPropertyWhenPossible

        CameraValues m_values;
        Transform m_transformCached;

        void OnEnable()
        {
            m_transformCached = transform;
            if (m_config == null) 
                return;

            m_settings = m_config.Settings;
            m_lerpSpeeds = m_config.LerpValues;
        }

        public void SetupCameraAtAnchor(Vector3 anchorPos)
        {
            AnchorPos = anchorPos;
            GetCameraValues(1f, m_targets, CameraLerpSpeeds.SetupSpeed, ref m_values);
        }

        public bool HasTarget(Transform tr) => m_targets.FindIndex(t => t.Transform == tr) != -1;

        public void AddTarget(Transform tr) => AddTarget(tr, 1f, 1f);
        public void AddTarget(Transform tr, float prio, float speed)
        {
            if (HasTarget(tr))
                return;
            m_targets.Add(new Target(tr, prio,speed));
        }

        public void RemoveTarget(Transform tr) => m_targets.RemoveAll(t=>t.Transform==tr);

        public void FadeOutTarget(Transform tr, float speed)
        {
            var idx = m_targets.FindIndex(t => t.Transform == tr);
            if (idx == -1)
                return;
            var target = m_targets[idx];
            target.Active = -speed;
            m_targets[idx] = target;
        }

//#if PHYSICS_BASED_MOVEMENT
        void FixedUpdate()
        {
            if (!PhysicsBasedMovement)
                return;

            CalculatePosition(Time.fixedDeltaTime);
            UpdateCameraPos();
        }
//#else
        void LateUpdate()
        {
            if (PhysicsBasedMovement)
                return;

            CalculatePosition(Time.deltaTime);
            UpdateCameraPos();
        }
//#endif

        void UpdateCameraPos()
        {
            m_transformCached.position = m_values.TargetPos;
            m_transformCached.rotation = m_values.TargetRotation;
        }

        void CalculatePosition(float delta)
        {
            UpdatePriorities();
            GetCameraValues(delta, m_targets, m_lerpSpeeds, ref m_values);
            AnchorPos = m_values.AveragePos;
        }

        void UpdatePriorities()
        {
            if (m_targets.IsNullOrEmpty())
                return;

            for (var i = m_targets.Count-1; i >=0; i--)
            {
                var t = m_targets[i];
                t.Priority = Mathf.Clamp01(t.Priority + m_targets[i].Active* Time.deltaTime);

                m_targets[i] = t;

                if (t.Priority <= 0f)
                    m_targets.RemoveAt(i);
            }
        }

        struct CameraValues
        {
            public Vector3 TargetPos;
            public Quaternion TargetRotation;

            public Vector3 AveragePos;
            public Vector3 LocalForward;
            public Vector3 OffsetVec;

            public float LerpDistance;
            public float TargetDistance;
        }

        void GetCameraValues(float delta, ICollection<Target> targets, 
            CameraLerpSpeeds speeds,  ref CameraValues camValues)
        {
            var avgPos = GetTargetsAvgPos(targets);
            camValues.AveragePos = Vector3.Lerp(camValues.AveragePos, avgPos, speeds.FocusPos * delta);
            //camValues.AveragePos.y = GameGlobals.GroundPlaneY;

            var highestPlayerDistance = GetHighestTargetDistance(camValues.AveragePos, targets);

            var distanceLerpValue = GetDistanceInterpolationValue(highestPlayerDistance);
            camValues.LerpDistance = Mathf.Lerp(camValues.LerpDistance, distanceLerpValue, speeds.Distance * delta);

            camValues.TargetDistance = Mathf.Lerp(m_settings.m_cameraDistance.x, m_settings.m_cameraDistance.y, camValues.LerpDistance);
            var targetAngle = Mathf.Lerp(m_settings.m_cameraAngle.x, m_settings.m_cameraAngle.y, camValues.LerpDistance);
            var offset = m_settings.m_cameraOffset.Evaluate(camValues.LerpDistance);

            var targetRot = Quaternion.Euler(targetAngle, 0f, 0f);
            camValues.TargetRotation = Quaternion.Slerp(m_transformCached.rotation, targetRot, speeds.Rotation * delta);

            camValues.LocalForward = camValues.TargetRotation * Vector3.forward;
            camValues.OffsetVec = offset * Vector3.back;

            camValues.TargetPos = camValues.AveragePos + camValues.OffsetVec - camValues.TargetDistance * camValues.LocalForward;
        }

        float GetDistanceInterpolationValue(float highestPlayerDistance)
        {
            var val = Mathf.Clamp(highestPlayerDistance, m_settings.m_targetDistanceThresholds.x, m_settings.m_targetDistanceThresholds.y);
            var range = m_settings.m_targetDistanceThresholds.y - m_settings.m_targetDistanceThresholds.x;
            return (val - m_settings.m_targetDistanceThresholds.x) / (range);
        }

        Vector3 GetTargetsAvgPos(ICollection<Target> targets)
        {
            if (m_targets.IsNullOrEmpty())
                return AnchorPos;

            var pow = 0f;
            var pos = Vector3.zero;
            for (var i = targets.Count-1; i >= 0; i--)
            {
                var target = m_targets[i];
                var tr = target.Transform;
                if (tr == null || !tr.gameObject.activeSelf)
                {
                    m_targets.RemoveAt(i);
                    continue;
                }

                pow += target.Priority;
                pos += target.Priority * tr.position;
            }

            if (pow > 0)
                pos /= pow;

            //var avgPos = new Vector3(pos.x, GameGlobals.GroundPlaneY, pos.z);
            var avgPos = new Vector3(pos.x, pos.y, pos.z);
            return avgPos;
        }

        float GetHighestTargetDistance(Vector3 avgPos, ICollection<Target> targets)
        {
            if (targets.IsNullOrEmpty())
                return 0f;
            if (Camera.main == null)
                return 0f;

            var ratio = Camera.main.aspect;

            var dist = 0f;
            foreach (var t in targets)
            {

                var vec = t.Transform.position - avgPos;
                var position = avgPos + t.Priority * vec;
                //todo 3: very hacky code Clamp ratio * some factor, consider cinemachine, once it works better?
                // I tried cinemachine, but target distance did not work when targeting group with several targets (radius was used)
                var xDist = Mathf.Abs(position.x - avgPos.x) / Mathf.Clamp(ratio,0.5f,1.5f) * m_distanceFac.x;
                var zDist = Mathf.Abs(position.z - avgPos.z) * Mathf.Clamp(ratio,0,1.5f) * m_distanceFac.y;
                dist = Mathf.Max(dist, xDist);
                dist = Mathf.Max(dist, zDist);
            }

            return dist;
        }

        public void LerpSettings(AnimatedCameraSettings from, AnimatedCameraSettings to, float lerpVal)
        {
            var startLerp = from.m_lerpTime;
            var lerpRange = to.m_lerpTime - from.m_lerpTime;

            var newLerpVal = (lerpVal - startLerp) / lerpRange;

            LerpSettings(from.m_settings, to.m_settings, newLerpVal);
        }

        void LerpSettings(CameraSettings from, CameraSettings to, float lerpVal)
        {
            m_settings.m_cameraAngle.x = Mathf.Lerp(from.m_cameraAngle.x, to.m_cameraAngle.x, lerpVal);
            m_settings.m_cameraAngle.y = Mathf.Lerp(from.m_cameraAngle.y, to.m_cameraAngle.y, lerpVal);
            m_settings.m_cameraDistance.x = Mathf.Lerp(from.m_cameraDistance.x, to.m_cameraDistance.x, lerpVal);
            m_settings.m_cameraDistance.y = Mathf.Lerp(from.m_cameraDistance.y, to.m_cameraDistance.y, lerpVal);
            m_settings.m_targetDistanceThresholds.x = Mathf.Lerp(from.m_targetDistanceThresholds.x, to.m_targetDistanceThresholds.x, lerpVal);
            m_settings.m_targetDistanceThresholds.y = Mathf.Lerp(from.m_targetDistanceThresholds.y, to.m_targetDistanceThresholds.y, lerpVal);
        }

        public IEnumerator SwitchCameraSettings(CameraSettings toSettings, float seconds)
        {
            var fromSettings = m_settings;
            var secondsPassed = 0f;
            while (secondsPassed < seconds)
            {
                var lerp = secondsPassed / seconds;
                LerpSettings(fromSettings, toSettings, lerp);
                yield return null;
                secondsPassed += Time.deltaTime;
            }
            LerpSettings(fromSettings, toSettings, 1f);
        }


    }
}
