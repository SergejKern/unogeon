﻿using Game.InteractiveObjects;
using ScriptableUtility;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI
{
    public class ItemUI : MonoBehaviour, IInitWithContext, IScriptableEventListener<GameObject>
    {
        [SerializeField] ScriptableVariable<GameObject> m_item;
        [SerializeField] Image m_iconImage;

        public void Init(IContext ctx)
        {
            var varRef = new VarReference<GameObject>(ctx, m_item);
            m_item.RegisterListener(ctx, this);

            OnEvent(varRef.Value);
        }

        public void OnEvent(GameObject item)
        {
            m_iconImage.sprite = null;
            m_iconImage.gameObject.SetActive(item != null);
            if (item == null)
                return;
            item.TryGetComponent(out IItemProperties itemProperties);
            m_iconImage.sprite = itemProperties.Icon;
        }
    }
}
