﻿using Core.Editor.Inspector;
using Game.Globals;
using UnityEditor;
using UnityEngine;

namespace _Editor.Game.Globals
{
    [CustomEditor(typeof(SceneSettings))]
    public class SceneSettingsInspector : BaseInspector<SceneSettingsEditor>{}
    public class SceneSettingsEditor : BaseEditor<SceneSettings>
    {
        public override void OnGUI(float width)
        {
            base.OnGUI(width);

            if (GameGlobals.I.SceneLights!= null)
                GameGlobals.I.SceneLights.Apply(Target.LightSettings);

            CameraPositioning();
            VerificationGUI();
        }

        void CameraPositioning()
        {
            if (GameGlobals.I.GOGameCamera == null)
                return;

            if (GUILayout.Button("Camera to Setup-Pos"))
                GameGlobals.I.GOGameCamera.transform.position = Target.CameraSetupPos.position;

            if (GUILayout.Button("Setup-Pos to Camera-Pos"))
                Target.CameraSetupPos.position = GameGlobals.I.GOGameCamera.transform.position;
        }
    }
}
