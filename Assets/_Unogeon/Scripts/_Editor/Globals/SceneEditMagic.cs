﻿using Core.Unity.Extensions;
using Core.Unity.Utility;
using Game.Enums;
using Game.Globals;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace _Editor.Game.Globals
{
    [InitializeOnLoad]
    public static class SceneEditMagic
    {
        static GameSetup m_setup;
        static SceneSettings m_settings;

        static bool m_firstInit;

        const HideFlags k_prefabHideFlags = HideFlags.DontSave;
        static GameObject m_editorObject;

        static SceneEditMagic()
        {
            EditorApplication.update += Update;
            EditorSceneManager.sceneOpened += OnSceneOpened;
            EditorSceneManager.sceneOpening += OnSceneOpening;

            EditorApplication.playModeStateChanged += OnChangePlayMode;
        }

        // ReSharper disable once FlagArgument
        static void OnChangePlayMode(PlayModeStateChange playModeStateChange)
        {
            if (playModeStateChange == PlayModeStateChange.ExitingEditMode)
                CleanupOldObjects();
            if (playModeStateChange == PlayModeStateChange.EnteredEditMode)
                InitScene();
        }

        static void OnSceneOpening(string path, OpenSceneMode mode) => CleanupOldObjects();
        static void OnSceneOpened(Scene scene, OpenSceneMode mode)
        {
            CleanupOldObjects();
            InitScene();
        }

        static void CleanupOldObjects()
        {
            if (GameGlobals.I.GOSceneLights != null)
                GameGlobals.I.GOSceneLights.DestroyEx();
            if (GameGlobals.I.GOGameCamera != null)
                GameGlobals.I.GOGameCamera.DestroyEx();

            SafeCleanup();
        }

        static void SafeCleanup()
        {
            var cleanupMarkers = Resources.FindObjectsOfTypeAll<CleanupMarker>();
            foreach (var marker in cleanupMarkers)
                marker.gameObject.DestroyEx();
        } 

        static void InitScene()
        {
            m_editorObject = new GameObject("Unsaved", typeof(CleanupMarker))
            {
                hideFlags = HideFlags.DontSave | HideFlags.NotEditable
            };

            m_settings = Object.FindObjectOfType<SceneSettings>();
            m_setup = Object.FindObjectOfType<GameSetup>();

            InitSceneCamera();
            InitSceneLights();
        }

        static void InitSceneLights()
        {
            if (m_setup == null)
                return;
            GameGlobals.I.GOSceneLights = PrefabUtility.InstantiatePrefab(m_setup.Links.GetPrefab(nameof(PrefabIDs.LightSetup)), m_editorObject.transform) as GameObject;

            if (GameGlobals.I.GOSceneLights == null)
                return;

            SetHideFlagsRecursively(GameGlobals.I.GOSceneLights, k_prefabHideFlags);

            if (m_settings == null)
                return;
            if (GameGlobals.I.SceneLights != null)
                GameGlobals.I.SceneLights.Apply(m_settings.LightSettings);
        }

        static void InitSceneCamera()
        {
            if (m_setup == null)
                return;
            GameGlobals.I.GOGameCamera = PrefabUtility.InstantiatePrefab(m_setup.Links.GetPrefab(nameof(PrefabIDs.CameraSetup)), 
                m_editorObject.transform) as GameObject;

            if (GameGlobals.I.GOGameCamera == null)
                return;

            SetHideFlagsRecursively(GameGlobals.I.GOGameCamera, k_prefabHideFlags);

            if (m_settings == null)
                return;
            if (m_settings.CameraSetupPos != null)
                GameGlobals.I.GOGameCamera.transform.position = m_settings.CameraSetupPos.position;
        }

        static void SetHideFlagsRecursively(GameObject go, HideFlags flags)
        {
            go.hideFlags = flags;
            var tr = go.transform;

            for (var i = 0; i < tr.childCount; i++) 
                tr.GetChild(i).gameObject.hideFlags = flags;
        }

        static void Update() => FirstInit();

        static void FirstInit()
        {
            if (m_firstInit)
                return;
            m_firstInit = true;

            SafeCleanup();
        }
    }
}
