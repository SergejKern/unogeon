﻿using UnityEditor;
using UnityEngine;
using UnityEngine.InputSystem;

#if UNITY_EDITOR
[InitializeOnLoad]
#endif
public class MouseAimProcessor : InputProcessor<Vector2>
{
    public float RayDistance = 100f;
    public float AimDistance = 20f;

    public static Transform KeyboardMousePlayer;
#if UNITY_EDITOR
    static MouseAimProcessor()
    {
        Initialize();
    }
#endif

    [RuntimeInitializeOnLoadMethod]
    static void Initialize()
    {
        InputSystem.RegisterProcessor<MouseAimProcessor>();
    }

    public override Vector2 Process(Vector2 value, InputControl control)
    {
        if (KeyboardMousePlayer == null)
            return value;

        var ray = Camera.main.ScreenPointToRay(new Vector3(value.x, value.y, RayDistance));
        //Debug.DrawRay(ray.origin, ray.direction, Color.cyan);
        var position = KeyboardMousePlayer.position;
        var plane = new Plane(Vector3.up, position);
        plane.Raycast(ray, out var distance);

        var point = ray.GetPoint(distance);
        //Debug.DrawLine(point - Vector3.right, point + Vector3.right, Color.magenta);
        //Debug.DrawLine(point - Vector3.up, point + Vector3.up, Color.magenta);

        var input = point - position;
        if (input.sqrMagnitude < 2f) return Vector2.zero;
        
        input = input.normalized * Mathf.Clamp(input.magnitude, 0f, AimDistance) / AimDistance;

        return new Vector2(input.x, input.z);
    }
}
