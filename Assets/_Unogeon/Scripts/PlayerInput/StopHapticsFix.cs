﻿using UnityEngine;
using UnityEngine.InputSystem;

public class StopHapticsFix : MonoBehaviour
{
    void OnDestroy() => InputSystem.ResetHaptics();
}
